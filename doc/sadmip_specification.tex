% !TeX spellcheck = en_US

\documentclass{scrreprt}

\usepackage[utf8]{inputenc}
\usepackage{booktabs}
\usepackage{listings}
\usepackage{amsmath}

\newcommand{\hex}[1]{\texttt{0x#1}}
\newcommand{\tabbullet}{~~\llap{\textbullet}~~}
\newcommand{\tabspaced}{~~ ~~}

\begin{document}

\author{Philipp Kiener}
\date{2018-11-12}

\title{SADMIP - StackADrop Manipulation Interface Protocol}
\subtitle{Protocol specification}

\maketitle
\tableofcontents

\chapter{SADMIP Protocol description}
\label{chap:sadmip_description}

This document describes \textsc{SADMIP} - the StackADrop Manipulation Interface Protocol. This description can be used to implement the protocol in other languages -- or merely differently -- than the reference implementation given by the author. The description will not concern itself about the why, but rather about the how and what: types of messages, concrete serialization and expected effects of commands.

\section{Terminology}

To resolve any possible ambiguities, the domain-specific terms used in this technical description are listed here with their respective definitions for this specific document. The usage of these terms may be different in e.g. the documentation of the reference implementation.

\begin{itemize}
	\item \textbf{Host}: The actual device that is controllable via commands
	\item \textbf{Client}: The user-controlled device (e.g. a PC) connecting to a host and issuing commands
	\item \textbf{Cell}: A single unit within a frame; the state of a cell can be either \textit{on} or \textit{off}
	\item \textbf{Frame}: The collection of cells in a fixed-size matrix
	\item \textbf{Experiment}: A collection of frames, paired with a name and a number denoting the timestep
	\item \textbf{Timestep}: The amount of time -- usually given in milliseconds -- that passes between two frames of an experiment in playback
	\item \textbf{Meta-Information}: A collection of key-value-pairs containing user-defined information about the host or experiment
\end{itemize}

\section{General description}

The SADMIP protocol is a protocol designed to allow remote manipulation, as well as information retrieval, of a programmable DMFB. While the name suggests it to be used with a StackADrop, it is not locked to that device; any device can be talked to using the protocol, as long as it supports it.

The basis for the SADMIP is made up of commands issued to the host by the client. The command is executed and a fitting answer is returned to the client; this answer can either be positive, optionally containing some information that was requested, or negative, containing an error code. A positive answer is called an \textit{ACK}, a negative answer is called an \textit{ERR}. There is also a \textit{heartbeat}, which may optionally be sent periodically by the host. The goal of the heartbeat is to signal to the client that the device is still up and running. Every message can be put into one of these four categories; the category a message belongs to is called the \textit{type} of a message.

There are a plethora of commands supported by the protocol, which can be identified using their \textit{opcode}. The opcodes of commands also applies to \textit{ACK}s and \textit{ERR}s; since both \textit{ACK} and \textit{ERR} are answers to a certain command, the answer takes the opcode of the command it responds to as its own opcode. Since the heartbeat does not directly belong to a command, its opcode is handled differently. Instead, the opcode of a heartbeat denotes the state the device is currently in; the specialized opcodes of heartbeats can be seen it Table \ref{tbl:sadmip_heartbeat_opcodes}.

\begin{table}
	\centering
	\begin{tabular}{rl}
		\toprule
		opcode   & Meaning\\
		\midrule
		\hex{01} & The device is idle\\
		\hex{02} & The device is currently reading a message\\
		\hex{03} & The device is currently working on executing a command\\
		\bottomrule
	\end{tabular}
	\caption{Specialized opcodes for heartbeats. \label{tbl:sadmip_heartbeat_opcodes}}
\end{table}

A part of every message is the payload, which contains any parameters neccessary for a command or answer. The exact format of the payload is dependent on the combination of type and opcode, and is explained per-command later on.

\chapter{Serialization}

When transmitting messages, there are many recurring types of values. These are either simple data types -- boolean, numeric or character values -- or complex (compound) values like lists or matrices. The concrete serialization of each of these types is explained first, upon which the complex serialization of payloads builds upon.

To help keep the description succint, the following notation is introduced: $$a \triangleright b$$

The operation $a \triangleright b$ denotes that the serialization of $b$ is concatenated to the serialization of $a$. As an example, if the following appears: $$12 \triangleright 3 \triangleright 4 \triangleright name$$ it would mean that a serialization of some compound type would be defined as first serializing the value $12$, then the value $3$, then the value $4$ and then the value of $name$. This notation will be used henceforth.

Examples of binary serializations are given as \textit{hexdumps}, which show the hexadecimal representation of each byte. See the following hexdump for example:

\begin{minipage}{\linewidth}
\begin{verbatim}

0x0000: 00 AA 00 FF
0x0004: 11 BB 11 EE

\end{verbatim}
\end{minipage}

The hexdump consists of eight bytes (\hex{00}, \hex{AA}, \hex{00}; \hex{FF}, \hex{11}, \hex{BB}, \hex{11} and \hex{EE}). The first value of each row (\hex{0000} and \hex{0004}) denotes the index, also as hexadecimal, within the data. While the index is always given with the \texttt{0x}-prefix, the byte values always omit that prefix.

\section{Serializing simple data types}

All complex types are made up of specific sequences of simple data types. The serialization of these simple data types is generally identical to how they are stored in memory\footnote{For typical systems, that is. There can, of course, be exceptions to this rule.}, although the byte order may need to be reversed.

The types used here, and henceforth, are taken from C++. They are either native to the language or imported by including the standard header \texttt{cstdint}. All types spanning more than a single byte are transmitted as little endian\footnote{Most significant byte first.}. It is not possible to differentiate between e.g. a single \texttt{uint16\_t} or two \texttt{int8\_t} based on the data alone; it is neccessary to know the type to extract. All serializations can be seen in Table \ref{tbl:sadmip_primitive_types}.

\begin{table}
	\centering
	\begin{tabular}{r|l}
		\toprule
		Type               & Serialization \\
		\midrule
		\texttt{bool}      & \hex{00} for \texttt{false}, every other value for \texttt{true}. \\
		\texttt{char}      & Binary representation of the character, unsigned, one byte. \\
		\texttt{uint8\_t}   & Binary representation, one byte. \\
		\texttt{int8\_t}  & Binary representation in two's complement, one byte. \\
		\texttt{uint16\_t}  & Binary representation, two bytes. \\
		\texttt{int16\_t} & Binary representation in two's complement, two bytes. \\
		\texttt{uint32\_t}  & Binary representation, four bytes. \\
		\texttt{int32\_t} & Binary representation in two's complement, four bytes. \\
		\texttt{uint64\_t}  & Binary representation, eight bytes. \\
		\texttt{int64\_t} & Binary representation in two's complement, eight bytes. \\
		\texttt{float}     & Binary representation according to IEEE-754, $32$-bit. \\
		\texttt{double}    & Binary representation according to IEEE-754, $64$-bit. \\	
		\bottomrule
	\end{tabular}
	\caption{Serialization of primitive types. \label{tbl:sadmip_primitive_types}}
\end{table}

\section{Serializing complex types}

Complex, or compound, data types are always of dynamic length; no statement can be made before beginning to extract them from a stream of data. To still enable deterministic deserialization, their length has to be encoded in some way. The way this is done differs between lists and matrices.

\subsubsection{List}

A list is essentially a collection of items. These items need not neccessarily be of the same size, although the type of them -- meaing how to extract them -- must be known, as there is no separation between items in the list. Different types within a list are not allowed. A list is a valid item in a list, as is a matrix.

A list is serialized by first transmitting the length of the list -- the number of items -- as a \texttt{uint16\_t}, followed by transmitting each of the items in the list. An empty list is a valid list of size $0$, which causes the serialization to transmit the length and stop there, as there are no items. As the size is transmitted as \texttt{uint16\_t}, the maximum number of elements in a valid list are $2^{16} = 65536$.

In general, a list $X := \{x_1, x_2, ..., x_n\}$ is serialized as $$n \triangleright x_1 \triangleright x_2 \triangleright ... \triangleright x_n$$ iff $0 \leq n < 65536$ and the type of $n$ is \texttt{uint16\_t}.

\textbf{Example:}

Given the list $$X := \{2, 3, 5, 7, 9\}$$ the binary serialization of $X$, transmitting each element as \texttt{uint8\_t}, would be as follows:

\begin{verbatim}
0x0000: 00 05 02 03 05 07 09
\end{verbatim}

\subsubsection{String}

Strings are lists of characters -- as such, they are transmitted as a list of character values (\texttt{char}s). It is not required to terminate a string with a \texttt{NUL} character (\hex{00}), but if done, it must be accounted for in the string's length.

\textbf{Example:}

The binary serialization of the string "\texttt{hello world}" would be as follows:

\begin{verbatim}
0x0000: 00 0B 68 65 6C 6C 6F 20
0x0008: 78 6F 72 6C 64
\end{verbatim}

\subsubsection{Matrix}

A matrix is, in some way, a list of lists. It consists of $rows \cdot cols$ cells and is in many ways similar to the mathematical definition of a matrix. In this case, a matrix' cells are boolean values. A matrix is serialized as $$rows \triangleright cols \triangleright data$$ where $rows$ and $cols$ are \texttt{uint8\_t}s denoting the number of rows and columns the matrix has, respectively. $data$ is a tightly packed representation of the cells' values; each cell is represented as a single bit.

The $data$ is serialized on a per-row basis; the first row is serialized, then the second row is serialized, followed by the next row up to the last row. Each row is padded with bits of undefined value to always use whole bytes; the next row will always start on a new byte. As such, a row with $n$ columns is serialized using $\lceil\frac{n}{8}\rceil$ bytes, with every bit after and including the $n$th bit being padding\footnote{The first bit is the leftmost bit.}. The rows are serialized from top to bottom, the columns from left to right. A cell whose value is \texttt{true} is serialized as an on-bit ($1$), a cell whose value is \texttt{false} is serialized as an off-bit ($0$).

If $rows$ and $cols$ have been read, the $data$ is contained in the next $$rows \cdot \left\lceil\frac{cols}{8}\right\rceil$$ bytes. Since both $row$ and $col$ are stored as \texttt{uint8\_t}, the dimensions of a valid matrix are between $0 \times 0$ and $255 \times 255$, where $0 \times 0$ is generally seen as an invalid, yet legal, matrix.

Indexing in matrices is $0$-based. To access the cell with row $r$ and column $c$ in the $data$-block of a matrix, one would first have to find the starting byte of the row $r$, then find the starting byte of the column $c$ and then use a bitmask to filter out the bit containing the cell at the requested index. This is done by pointing to the first byte in the $data$-block, incrementing that pointer by $r \cdot \lceil\frac{cols}{8}\rceil$ to go to the start of the row, then incrementing that pointer again by $\left\lfloor\frac{c}{8}\right\rfloor$. With the pointer pointing to the correct byte, one now needs to isolate the $c~mod~8$th bit from the left. An example of this logic can be seen in the following snippet:

\begin{lstlisting}[language=C++, basicstyle=\footnotesize]
bool is_active(
  uint8_t rows,
  uint8_t cols,
  uint8_t row,
  uint8_t col,
  uint8_t* data
) {
  uint8_t* byte = data;
  data += row * (ceil(cols / 8.f));

  data += col / 8; // integer division: implicit floor()

  uint8_t bit = 7 - (col % 8);
  return *data & (1 << bit);
}
\end{lstlisting}

The serialization of matrices is best seen in an example.

\textbf{Example:}

Consider a matrix with $5$ rows and $5$ columns. Every cell in an odd column is set to \texttt{true}, every cell in an even column is set to \texttt{false}. Serializing this matrix would lead to the following hexdump, when filling the padding with all $0$ bits:

\begin{verbatim}
0x0000: 05 05 A8 A8 A8 A8 A8
\end{verbatim}

As all rows are similar, the data-portion consists of $5$ times \hex{A8} bytes. This corresponds to the value $10101000_2$. Given the binary representation, one can see that the first, third and fifth bit are set, whereas all other bits are not set, which denotes that the first, third and fifth column contain a \texttt{true} value, whereas all other columns contain a \texttt{false} -- as there are only five columns, the last three bits are be ignored.

Consider now a matrix with $2$ rows and $10$ columns. The matrix can be described as follows, with $1$ meaning \texttt{true} and $0$ meaning \texttt{false}:
$$
\begin{bmatrix}
0 & 1 & 0 & 1 & 0 & 1 & 0 & 0 & 0 & 1\\
1 & 1 & 0 & 0 & 1 & 1 & 1 & 1 & 0 & 1\\
\end{bmatrix}
$$

As each row consists of $10$ columns, and thus $10$ bits, the rows are serialized using two bytes, since $$\left\lceil\frac{10}{8}\right\rceil = \lceil1.25\rceil = 2$$

The first row is serialized as the bytes $01010100_2 = 54_{16}$ and $01000000_2 = 80_{16}$, the second row as the bytes $11001111_2 = CF_{16}$ and $01000000_2 = 80_{16}$. The last $6$ bits of both rows' second byte are the padding bits. The hexdump serializing that matrix would then be:

\begin{verbatim}
0x0000: 02 0A 54 80 CF 80
\end{verbatim}

\section{Message structure}

Common to every message are the three parameters

\begin{itemize}
	\item \textit{type} as \texttt{uint8\_t}, indicating what kind of message is being transmitted
	\item \textit{opcode} as \texttt{uint8\_t}, indicating the command this message belongs to
	\item \textit{payload size} as \texttt{uint32\_t}, indicating the length of the payload in bytes
\end{itemize}

A message is then serialized as $$type \triangleright opcode \triangleright payload~size \triangleright payload$$ with $payload$ being exactly $payload~size$ bytes long. How the payload is (de-)serialized depends on the kind of message. An overview of all message types is given in Table \ref{tbl:sadmip_types}, an overview of all opcodes is given in Table \ref{tbl:sadmip_opcodes_getset} and Table \ref{tbl:sadmip_opcodes_ctrlmeta}. An \textit{ERR} always has a payload containing only a single byte: the error code. All error codes are shown in Table \ref{tbl:sadmip_error_codes}.

\begin{table}
	\centering
	\begin{tabular}{cl}
		\toprule
		Value    & Type\\
		\midrule
		\hex{01} & Command\\
		\hex{02} & \textit{ACK}\\
		\hex{03} & \textit{ERR}\\
		\hex{04} & Heartbeat\\
		\bottomrule
	\end{tabular}
	\caption{All valid message types. \label{tbl:sadmip_types}}
\end{table}

\begin{table}
	\centering
	\begin{tabular}{cl|cl}
		\toprule
		Opcode   & Command        & Opcode   & Command\\
		\midrule
		\hex{11} & Get-Cell       & \hex{21} & Set-Cell\\
		\hex{12} & \textit{None}  & \hex{22} & Set-Path\\
		\hex{13} & Get-Matrix     & \hex{23} & Set-Matrix\\
		\hex{14} & Get-Experiment & \hex{24} & Set-Experiment\\
		\hex{18} & Get-Name       & \hex{28} & Set-Name\\
		\hex{19} & Get-Timestep   & \hex{29} & Set-Timestep\\
		\hex{1A} & Get-Meta       & \hex{2A} & Set-Meta\\
		\hex{1B} & Get-Keys       & \hex{2B} & Set-Keys\\
		\hex{1C} & Get-Value      & \hex{2C} & Set-Value\\
		\bottomrule
	\end{tabular}
	\caption{All opcodes for Get- or Set-Commands. \label{tbl:sadmip_opcodes_getset}}
\end{table}

How each command serializes its payload, as well as how its \textit{ACK} serializes the payload, and what the command is expected to do is explained in the next section. In general, commands with the prefix \textit{Get-} are commands to query some information from the host; they always have a specialized \textit{ACK}. Commands with the prefix \textit{Set-} are commands to modify the experiment or meta-data of the host. Commands with the prefix \textit{Ctrl-} change the playback and currently shown frame of the host. Commands with the prefix \textit{Meta-} are special commands that cannot be categorized otherwise.

\begin{table}
	\centering
	\begin{tabular}{cl|cl}
		\toprule
		Opcode   & Command    & Opcode   & Command\\
		\midrule
		\hex{41} & Ctrl-Start & \hex{81} & Meta-SetPanelType\\
		\hex{42} & Ctrl-Next  & \hex{82} & Meta-Reset\\
		\hex{43} & Ctrl-Prev  & \hex{83} & Meta-FrameCount\\
		\hex{44} & Ctrl-Stop  & \hex{84} & Meta-MemoryInfo\\
		\hex{45} & Ctrl-Goto  &          & \\
		\hex{46} & Ctrl-Reset &          & \\
		\bottomrule
	\end{tabular}
	\caption{All opcodes for Ctrl- or Meta-Commands. \label{tbl:sadmip_opcodes_ctrlmeta}}
\end{table}

\begin{table}
	\centering
	\begin{tabular}{rlp{15em}}
		\toprule
		Code & Name & Description\\
		\midrule
		\hex{01} & Index out of bounds & A command could not be executed due to the requested index being out of bounds.\\
		\hex{02} & Unknown key & A command could not be executed due to the given key being nonexistent.\\
		\hex{03} & Unknown opcode & The message could be parsed, yet the opcode is not known. This error code can be replaced with \hex{07}.\\
		\hex{04} & Not applicable & The command requests an invalid operation to be done.\\
		\hex{05} & Dimension-mismatch & The supplied matrix does not match the dimensions of the device.\\
		\hex{06} & Invalid argument & A given argument contains an illegal value.\\
		\hex{07} & Parse error & The payload of a message could not be parsed due to being incomplete or invalid.\\
		\bottomrule
	\end{tabular}
	\caption{All possible error codes. \label{tbl:sadmip_error_codes}}
\end{table}

\chapter{Commands}

All commands and their specific \textit{ACK}s are now described in detail. The serialization in this case indicates the serialization of the payload -- if no serialization is given for a command or an \textit{ACK}, the respective payload is empty. Any possible errors happening during execution are also listed.

% %%%%%%%%%%%%%%%%%%%%% %
% BEGIN COMMAND LISTING %
% %%%%%%%%%%%%%%%%%%%%% %

\section{Get-Commands}

\subsection{Get-Cell}
\begin{tabular}{rl}
	\textbf{Name}:          & Get-Cell\\
	\textbf{Description}:   & Retrieve the state of a single cell in the current frame.\\
	\textbf{opcode}:        & \hex{11}\\
	\textbf{Serialization}: & $row \triangleright col$\\
	& \tabbullet \textit{row} (\texttt{uint8\_t}):\\
	& \tabspaced Index of the row to access\\
	& \tabbullet \textit{col} (\texttt{uint8\_t}):\\
	& \tabspaced Index of the column to access\\
	\textbf{ACK}:           & $active$\\
	& \tabbullet \textit{active} (\texttt{bool}):\\
	& \tabspaced Whether the queried cell is active\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Index out of bounds}\\
	& \tabspaced \textit{row} or \textit{col} are out of bounds\\
\end{tabular}

\subsection{Get-Matrix}
\begin{tabular}{rl}
	\textbf{Name}:          & Get-Matrix\\
	\textbf{Description}:   & Retrieve the current frame.\\
	\textbf{opcode}:        & \hex{13}\\
	\textbf{Serialization}: & $current \triangleright index$\\
	& \tabbullet \textit{current} (\texttt{bool})\\
	& \tabspaced Whether to retrieve the current frame or a specific frame\\
	& \tabbullet \textit{index} (\texttt{uint16\_t})\\
	& \tabspaced Index of the specific frame retrieve\\
	\textbf{ACK}:           & $frame$\\
	& \tabbullet \textit{frame} (\texttt{matrix})\\
	& \tabspaced The requested frame\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Index out of bounds}\\
	& \tabspaced \textit{index} is out of bounds\\
\end{tabular}

\subsection{Get-Experiment}
\begin{tabular}{rl}
	\textbf{Name}:          & Get-Experiment\\
	\textbf{Description}:   & Retrieve the stored experiment.\\
	\textbf{opcode}:        & \hex{14}\\
	\textbf{ACK}:           & $name \triangleright timestep \triangleright frames$\\
	& \tabbullet \textit{name} (\texttt{string})\\
	& \tabspaced Name of the experiment\\
	& \tabbullet \textit{timestep} (\texttt{uint16\_t})\\
	& \tabspaced Delay (in milliseconds) between frames\\
	& \tabbullet \textit{frames} (\texttt{list of matrix})\\
	& \tabspaced All frames contained in the experiment\\
\end{tabular}

\subsection{Get-Name}
\begin{tabular}{rl}
	\textbf{Name}:          & Get-Name\\
	\textbf{Description}:   & Retrieve the name of the experiment.\\
	\textbf{opcode}:        & \hex{18}\\
	\textbf{ACK}:           & $name$\\
	& \tabbullet \textit{name} (\texttt{string})\\
	& \tabspaced Name of the experiment\\
\end{tabular}

\subsection{Get-Timestep}
\begin{tabular}{rl}
	\textbf{Name}:          & Get-Timestep\\
	\textbf{Description}:   & Retrieve the timestep of the experiment.\\
	\textbf{opcode}:        & \hex{19}\\
	\textbf{ACK}:           & $timestep$\\
	& \tabbullet \textit{timestep} (\texttt{uint16\_t})\\
	& \tabspaced Delay (in milliseconds) between frames\\
\end{tabular}

\subsection{Get-Meta}
\begin{tabular}{rl}
	\textbf{Name}:          & Get-Meta\\
	\textbf{Description}:   & Retrieve all meta-information.\\
	\textbf{opcode}:        & \hex{1A}\\
	\textbf{ACK}:           & $keys \triangleright values$\\
	& \tabbullet \textit{keys} (\texttt{list of string})\\
	& \tabspaced List of all keys for meta-information\\
	& \tabbullet \textit{values} (\texttt{list of string})\\
	& \tabspaced List of all values for meta-information\\
\end{tabular}

\textit{keys} and \textit{values} are \textit{index-associative}; this means that the $n$th value belongs to the $n$th key. As such, both lists need to have the same length.

\subsection{Get-Keys}
\begin{tabular}{rl}
	\textbf{Name}:          & Get-Keys\\
	\textbf{Description}:   & Retrieve the list of all keys for meta-information.\\
	\textbf{opcode}:        & \hex{1B}\\
	\textbf{ACK}:           & $keys$\\
	& \tabbullet \textit{keys} (\texttt{list of string})\\
	& \tabspaced List of all keys for meta-information\\
\end{tabular}

\subsection{Get-Value}
\begin{tabular}{rl}
	\textbf{Name}:          & Get-Value\\
	\textbf{Description}:   & Retrieve the value for a specific key for meta-information.\\
	\textbf{opcode}:        & \hex{1C}\\
	\textbf{Serialization}: & $key$\\
	& \tabbullet \textit{key} (\texttt{string})\\
	& \tabspaced Key whose value to retrieve\\
	\textbf{ACK}:           & $value$\\
	& \tabbullet \textit{value} (\texttt{string})\\
	& \tabspaced Value for the queried key\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Unknown key}\\
	& \tabspaced \textit{key} is not known\\
\end{tabular}

\section{Set-Commands}

\subsection{Set-Cell}
\begin{tabular}{rl}
	\textbf{Name}:          & Set-Cell\\
	\textbf{Description}:   & Set the state of a single cell in the current frame.\\
	\textbf{opcode}:        & \hex{21}\\
	\textbf{Serialization}: & $row \triangleright col \triangleright active$\\
	& \tabbullet \textit{row} (\texttt{uint8\_t})\\
	& \tabspaced Index of the row to access\\
	& \tabbullet \textit{col} (\texttt{uint8\_t})\\
	& \tabspaced Index of the column to access\\
	& \tabbullet \textit{active} (\texttt{bool})\\
	& \tabspaced Whether the queried cell should be active\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Index out of bounds}\\
	& \tabspaced \textit{row} or \textit{col} are out of bounds\\
\end{tabular}

\subsection{Set-Path}
\begin{tabular}{rl}
	\textbf{Name}:          & Set-Path\\
	\textbf{Description}:   & Add a path to the experiment.\\
	\textbf{opcode}:        & \hex{22}\\
	\textbf{Serialization}: & $row \triangleright col \triangleright append \triangleright moves$\\
	& \tabbullet \textit{row} (\texttt{uint8\_t})\\
	& \tabspaced Index of the starting cell's row\\
	& \tabbullet \textit{col} (\texttt{uint8\_t})\\
	& \tabspaced Index of the starting cell's column\\
	& \tabbullet \textit{row} (\texttt{bool})\\
	& \tabspaced Whether to append frames if needed or abort the path\\
	& \tabbullet \textit{moves} (\texttt{list of move})\\
	& \tabspaced List of moves to generate the path\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Index out of bounds}\\
	& \tabspaced Any \textit{row} or \textit{col} are out of bounds\\
\end{tabular}

A \texttt{move} is one of the following:

\begin{itemize}
	\item \textit{up}: Move to the previous row (Serialized as \hex{F0})
	\item \textit{down}: Move to the next row (Serialized as \hex{10})
	\item \textit{left}: Move to the previous column (Serialized as \hex{0F})
	\item \textit{right}: Move to the next column (Serialized as \hex{01})
	\item \textit{still}: Don't move (Serialized as any other value, default \hex{00})
\end{itemize}

\subsection{Set-Matrix}
\begin{tabular}{rl}
	\textbf{Name}:          & Set-Matrix\\
	\textbf{Description}:   & Replace the current matrix or append one to the experiment.\\
	\textbf{opcode}:        & \hex{23}\\
	\textbf{Serialization}: & $append \triangleright frame$\\
	& \tabbullet \textit{append} (\texttt{bool})\\
	& \tabspaced Whether to append or replace the current frame\\
	& \tabbullet \textit{frame} (\texttt{matrix})\\
	& \tabspaced Frame to append or replace\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Dimension mismatch}\\
	& \tabspaced \textit{frame} does not match the dimensions of the device\\
\end{tabular}

\subsection{Set-Experiment}
\begin{tabular}{rl}
	\textbf{Name}:          & Set-Experiment\\
	\textbf{Description}:   & Replace the current experiment with the given experiment.\\
	\textbf{opcode}:        & \hex{24}\\
	\textbf{Serialization}: & $autoplay \triangleright loop \triangleright name \triangleright timestep \triangleright frames$\\
	& \tabbullet \textit{autoplay} (\texttt{bool})\\
	& \tabspaced Whether to immediately start playback\\
	& \tabbullet \textit{loop} (\texttt{bool})\\
	& \tabspaced Whether to loop the playback\\
	& \tabbullet \textit{name} (\texttt{string})\\
	& \tabspaced Name of the experiment\\
	& \tabbullet \textit{timestep} (\texttt{uin16\_t})\\
	& \tabspaced Delay (in milliseconds) between frames\\
	& \tabbullet \textit{frames} (\texttt{list of matrix})\\
	& \tabspaced Frames of the experiment\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Dimension mismatch}\\
	& \tabspaced A frame does not match the dimensions of the device\\
\end{tabular}

If \textit{timestep} is $0$, the previously configured timestep is kept.

\subsection{Set-Name}
\begin{tabular}{rl}
	\textbf{Name}:          & Set-Name\\
	\textbf{Description}:   & Set the name of the experiment.\\
	\textbf{opcode}:        & \hex{28}\\
	\textbf{Serialization}: & $name$\\
	& \tabbullet \textit{name} (\texttt{string})\\
	& \tabspaced Name of the experiment\\
\end{tabular}

\subsection{Set-Timestep}
\begin{tabular}{rl}
	\textbf{Name}:          & Set-Timestep\\
	\textbf{Description}:   & Retrieve the timestep of the experiment.\\
	\textbf{opcode}:        & \hex{29}\\
	\textbf{Serialization}: & $timestep$\\
	& \tabbullet \textit{timestep} (\texttt{uint16\_t})\\
	& \tabspaced Delay (in milliseconds) between frames\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Invalid argument}\\
	& \tabspaced If the timestep is $0$\\
\end{tabular}

\subsection{Set-Meta}
\begin{tabular}{rl}
	\textbf{Name}:          & Set-Meta\\
	\textbf{Description}:   & Replace all meta-information.\\
	\textbf{opcode}:        & \hex{2A}\\
	\textbf{Serialization}: & $keys \triangleright values$\\
	& \tabbullet \textit{keys} (\texttt{list of string})\\
	& \tabspaced List of all keys for meta-information\\
	& \tabbullet \textit{values} (\texttt{list of string})\\
	& \tabspaced List of all values for meta-information\\
\end{tabular}

\textit{keys} and \textit{values} are \textit{index-associative}; this means that the $n$th value belongs to the $n$th key. As such, both lists need to have the same length.

\subsection{Set-Keys}
\begin{tabular}{rl}
	\textbf{Name}:          & Set-Keys\\
	\textbf{Description}:   & Modify the keys of meta-information.\\
	\textbf{opcode}:        & \hex{2B}\\
	\textbf{Serialization}: & $action \triangleright keys$\\
	& \tabbullet \textit{action} (\texttt{uint8\_t})\\
	& \tabspaced The action to take\\
	& \tabbullet \textit{keys} (\texttt{list} of \texttt{string})\\
	& \tabspaced The keys to add to, remove from or replace the current set with\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Unknown key}\\
	& \tabspaced \textit{key} to remove is not known\\
	& \tabbullet \textit{Invalid argument}\\
	& \tabspaced \textit{key} to add is not already present\\
\end{tabular}

Possible \textit{action}s are:
\begin{itemize}
	\item \hex{01}: Add the keys to the current set of keys
	\item \hex{02}: Remove the keys from the current set of keys
	\item \hex{03}: Replace the current set of keys with the given set of keys
\end{itemize}

\subsection{Set-Value}
\begin{tabular}{rl}
	\textbf{Name}:          & Set-Value\\
	\textbf{Description}:   & Set the value for a key of meta-information.\\
	\textbf{opcode}:        & \hex{2C}\\
	\textbf{Serialization}: & $key \triangleright value$\\
	& \tabbullet \textit{key} (\texttt{string})\\
	& \tabspaced Key whose value to replace\\
	& \tabbullet \textit{value} (\texttt{string})\\
	& \tabspaced Value for the key\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Unknown key}\\
	& \tabspaced \textit{key} is not known\\
\end{tabular}

\section{Ctrl-Commands}

\subsection{Ctrl-Start}
\begin{tabular}{rl}
	\textbf{Name}:          & Ctrl-Start\\
	\textbf{Description}:   & Start playback of the experiment.\\
	\textbf{opcode}:        & \hex{41}\\
	\textbf{Serialization}: & $loop \triangleright timestep$\\
	& \tabbullet \textit{loop} (\texttt{bool})\\
	& \tabspaced Whether to loop the playback\\
	& \tabbullet \textit{timestep} (\texttt{uint16\_t})\\
	& \tabspaced Delay (in milliseconds) between frames\\
\end{tabular}

If \textit{timestep} is $0$, the previously configured timestep is kept.

\subsection{Ctrl-Next}
\begin{tabular}{rl}
	\textbf{Name}:          & Ctrl-Next\\
	\textbf{Description}:   & Skip to the next frame in the experiment.\\
	\textbf{opcode}:        & \hex{42}\\
	\textbf{Serialization}: & $skip$\\
	& \tabbullet \textit{skip} (\texttt{uint8\_t})\\
	& \tabspaced Number of frames to skip\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Index out of bounds}\\
	& \tabspaced The frame to skip to is out of bounds\\
\end{tabular}

\subsection{Ctrl-Prev}
\begin{tabular}{rl}
	\textbf{Name}:          & Ctrl-Prev\\
	\textbf{Description}:   & Skip to the previous frame in the experiment.\\
	\textbf{opcode}:        & \hex{43}\\
	\textbf{Serialization}: & $skip$\\
	& \tabbullet \textit{skip} (\texttt{uint8\_t})\\
	& \tabspaced Number of frames to skip\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Index out of bounds}\\
	& \tabspaced The frame to skip to is out of bounds\\
\end{tabular}

\subsection{Ctrl-Stop}
\begin{tabular}{rl}
	\textbf{Name}:          & Ctrl-Stop\\
	\textbf{Description}:   & Stop playback of the experiment.\\
	\textbf{opcode}:        & \hex{44}\\
\end{tabular}

\subsection{Ctrl-Goto}
\begin{tabular}{rl}
	\textbf{Name}:          & Ctrl-Goto\\
	\textbf{Description}:   & Skip to a specific frame in the experiment.\\
	\textbf{opcode}:        & \hex{45}\\
	\textbf{Serialization}: & $index$\\
	& \tabbullet \textit{index} (\texttt{uint16\_t})\\
	& \tabspaced Index of the frame to skip to\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Index out of bounds}\\
	& \tabspaced The frame to skip to is out of bounds\\
\end{tabular}

\subsection{Ctrl-Reset}
\begin{tabular}{rl}
	\textbf{Name}:          & Ctrl-Reset\\
	\textbf{Description}:   & Skip to the first frame, halting playback.\\
	\textbf{opcode}:        & \hex{46}\\
	\textbf{Serialization}: & $continue$\\
	& \tabbullet \textit{continue} (\texttt{bool})\\
	& \tabspaced Whether to continue playback instead of halting it\\
\end{tabular}

\section{Meta-Commands}

\subsection{Meta-ConfigureGrid}
\begin{tabular}{rl}
	\textbf{Name}:          & Meta-SetPanelType\\
	\textbf{Description}:   & Configure the mounted panel.\\
	\textbf{opcode}:        & \hex{81}\\
	\textbf{Serialization}: & $type$\\
	& \tabbullet \textit{type} (\texttt{string}, optional)\\
	& \tabspaced The type of panel that shall be configured\\
	\textbf{ACK}:           & $types$\\
	& \tabbullet \textit{types} (\texttt{list of string}, optional)\\
	& \tabspaced List of available panel types to mount\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Not applicable}\\
	& \tabspaced The given type is not a valid type.\\
\end{tabular}

If a $type$ is provided as payload in the command, the panel shall be configured and the device shall return a default \textit{ACK}. If no $type$ is provided, the device shall instead return a list of available panels as $types$ in a custom \textit{ACK}.

\subsection{Meta-Reset}
\begin{tabular}{rl}
	\textbf{Name}:          & Meta-Reset\\
	\textbf{Description}:   & Clear the stored experiment and all meta-information.\\
	\textbf{opcode}:        & \hex{82}\\
\end{tabular}

\subsection{Meta-FrameCount}
\begin{tabular}{rl}
	\textbf{Name}:          & Meta-FrameCount\\
	\textbf{Description}:   & Return the number of frames in the stored experiment.\\
	\textbf{opcode}:        & \hex{83}\\
	\textbf{ACK}:           & $count$\\
	& \tabbullet \textit{count} (\texttt{uint16\_t})\\
	& \tabspaced Number of frames in the experiment\\
\end{tabular}

\subsection{Meta-MemoryInfo}
\begin{tabular}{rl}
	\textbf{Name}:          & Meta-MemoryInfo\\
	\textbf{Description}:   & Return memory diagnostics.\\
	\textbf{opcode}:        & \hex{84}\\
	\textbf{ACK}:           & $reservedstack \triangleright maxstack \triangleright reservedheap \triangleright heap \triangleright fails$\\
	& \tabbullet \textit{reservedstack} (\texttt{uint32\_t})\\
	& \tabspaced Number of bytes reserved for the stack\\
	& \tabbullet \textit{maxstack} (\texttt{uint32\_t})\\
	& \tabspaced Maximum number of bytes used for the stack so far\\
	& \tabbullet \textit{reservedheap} (\texttt{uint32\_t})\\
	& \tabspaced Number of bytes reserved for the heap\\
	& \tabbullet \textit{heap} (\texttt{uint32\_t})\\
	& \tabspaced Number of bytes dynamically allocated on the heap currently\\
	& \tabbullet \textit{fails} (\texttt{uint32\_t})\\
	& \tabspaced Number of failed allocations\\
	\textbf{Errors}:        & \\
	& \tabbullet \textit{Not applicable}\\
	& \tabspaced The device does not support memory diagnostics\\
\end{tabular}

\subsection{Meta-Dimensions}
\begin{tabular}{rl}
	\textbf{Name}:          & Meta-Dimensions\\
	\textbf{Description}:   & Return the (maximum) dimensions for frames the device supports.\\
	\textbf{opcode}:        & \hex{85}\\
	\textbf{ACK}:           & $rows \triangleright cols$\\
	& \tabbullet \textit{rows} (\texttt{uint8\_t})\\
	& \tabspaced Maximum number of rows for frames\\
	& \tabbullet \textit{cols} (\texttt{uint8\_t})\\
	& \tabspaced Maximum number of columns for frames\\
\end{tabular}

\end{document}