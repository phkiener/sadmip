// Copyright (c) 2018 Philipp Kiener
#pragma once

#include "options.h"

namespace machine_mode
{
    void main(commandline_options opts);
}
