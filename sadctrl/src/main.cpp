// Copyright (c) 2018 Philipp Kiener

#include <iostream>

#include "machine_mode.h"
#include "normal_mode.h"
#include "options.h"

#include "CLI11.hpp"

#include <cstring>
#include <exception>

namespace
{
    const char* program_name(const char* str)
    {
        char dirsep;
#ifdef _WIN32
        dirsep = '\\';
#else
        dirsep = '/';
#endif

        size_t start = 0;
        size_t end = strlen(str) - 1;

        for (size_t i = end; i > 0; --i)
        {
            if (str[i] == dirsep)
            {
                start = i + 1;
                break;
            }
        }

        if (start == end)
        {
            return str;
        }

        return str + start;
    }
}

int main(int argc, char **argv)
{
    CLI::App app{"sadctrl - Commandline Client to manipulate a StackADrop\n", program_name(argv[0])};
    app.ignore_case(true);
    app.footer(
        "\nIf the command is extended with \"-- COMMAND\", that command is used for one-shot mode.\n"
        "In one-shot mode, only the given command is executed and the program exits after receiving an answer.\n"
        "One-shot mode is not available in machine mode.\n"
        "\n"
        "Machine mode is an opt-in change to output and behaviour. In machine mode, the output complexity is\n"
        "greatly reduced, making this mode ideal for external calls. Also, all heartbeats are displayed as\n"
        "soon as they are received in machine mode."
    );
    
    commandline_options opt;
    bool machine_mode = false;

    app.add_flag("--machine", machine_mode, "Enable machine mode");
    app.add_option("-t,--timeout", opt.timeout_ms, "Timeout for reading operations in ms");
    app.add_option("device", opt.device, "The serial port")->required(true);

    int argc_end = argc;
    for (int i = 0; i < argc; ++i)
    {
        if (std::string(argv[i]) == "--")
        {
            opt.oneshot_argv = argv + i;
            opt.oneshot_argc = argc - i;
            argc_end = i;
            break;
        }
    }

    try
    {
        app.parse(argc_end, argv);
    }
    catch (const CLI::Error& e)
    {
        return app.exit(e);
    }

    try
    {
        if (machine_mode)
        {
            machine_mode::main(opt);
        }
        else
        {
            return normal_mode::main(opt);
        }
    }
    catch (std::exception& e)
    {
        std::string error(e.what());

        if (*(error.end() - 1) != '\n')
        {
            error.push_back('\n');
        }

        std::cerr << error;

        return -2;
    }
}
