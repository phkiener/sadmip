// Copyright (c) 2018 Philipp Kiener

#include <machine_mode.h>

#include "format_answer.h"
#include <command_parser/command_parser.h>
#include <serial.h>

#include <communication/data_buffer.h>
#include <message/message.h>

#include <iostream>
#include <mutex>
#include <thread>
#include <string>

namespace
{
    void receive_loop(sadmip::data_buffer* connection, std::ostream* whence, bool* stop, std::mutex* mutex)
    {
        using namespace machine_mode;

        while (!(*stop))
        {
            auto msg = sadmip::message::deserialize(*connection);

            mutex->lock();
            if (msg)
            {
                *whence << *msg << "\n";
            }
            mutex->unlock();
        }
    }
}

void machine_mode::main(commandline_options opts)
{
    std::ostream& output = std::cout;
    std::mutex output_mutex;

    serial connection(opts.device);

    sadmip::data_buffer buffer(connection);
    bool stop = false;

    std::thread fetcher(receive_loop, &buffer, &output, &stop, &output_mutex);

    command_parser parser;

    while (true)
    {
        std::string input = "";
        std::getline(std::cin, input);

        if (input.compare("exit") == 0)
        {
            break;
        }

        auto cmd = parser.parse(input);

        if (!cmd)
        {
            output_mutex.lock();
            output << "ERR: Malformed command.\n";
            output_mutex.unlock();
        }
        else
        {
            cmd->serialize(buffer);
            buffer.flush();
        }

    }

    stop = true;
    fetcher.join();
}