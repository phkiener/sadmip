// Copyright (c) 2018 Philipp Kiener

#include "format_answer.h"

#include <message/message.h>
#include <message/ack.h>
#include <message/error.h>
#include <message/heartbeat.h>
#include <commands/all.h>

namespace
{
    std::ostream& operator<<(std::ostream& whence, const sadmip::bit_matrix& m)
    {
        if (!m.valid()) return whence << ";;";

        for (uint8_t r = 0; r < m.rows(); ++r)
        {
            for (uint8_t c = 0; c < m.cols(); ++c)
            {
                whence << (m.at(r, c) ? '1' : '0');
            }

            whence << ';';
        }

        return whence;
    }

    std::ostream& print(std::ostream& whence, const sadmip::commands::get_cell::result& ack)
    {
        return whence << "ACK: " << (ack.active() ? "Active" : "Inactive") << ".";
    }


    std::ostream& print(std::ostream& whence, const sadmip::commands::get_experiment::result& ack)
    {
        whence << "ACK: " 
            << "Name: \'" << ack.name() << "\', " 
            << "Timestep: " << ack.timestep() << ", "
            << ack.frames().size() << ":";

        for (const auto& frame : ack.frames())
        {
            whence << ' ' << frame;
        }

        return whence;
    }


    std::ostream& print(std::ostream& whence, const sadmip::commands::get_keys::result& ack)
    {
        whence << "ACK:";

        for (const auto& key : ack.keys())
        {
            whence << " \'" << key << "\'";
        }

        return whence << '.';
    }

    std::ostream& print(std::ostream& whence, const sadmip::commands::get_matrix::result& ack)
    {
        return whence << "ACK: " << ack.matrix() << '.';
    }

    std::ostream&print(std::ostream& whence, const sadmip::commands::get_meta::result& ack)
    {
        whence << "ACK:";

        for (const auto& pair : ack.meta())
        {
            whence << " \'" << pair.first << '=' << pair.second << "\'";
        }

        return whence << '.';
    }

    std::ostream& print(std::ostream& whence, const sadmip::commands::get_name::result& ack)
    {
        return whence << "ACK: \'" << ack.name() << "\'.";
    }

    std::ostream& print(std::ostream& whence, const sadmip::commands::get_timestep::result& ack)
    {
        return whence << "ACK: " << ack.timestep() << ".";
    }

    std::ostream& print(std::ostream& whence, const sadmip::commands::get_value::result& ack)
    {
        return whence << "ACK: \'" << ack.value() << "\'.";
    }

    std::ostream& print(std::ostream& whence, const sadmip::commands::meta_configuregrid::result& ack)
    {
        if (ack.report())
        {
            whence << "ACK: ";

            for (const auto& t : ack.types())
            {
                whence << " \'" << t << "\'";
            }

            return whence;
        }
        else
        {
            return whence << "ACK.";
        }
        
    }
    std::ostream& print(std::ostream& whence, const sadmip::commands::meta_framecount::result& ack)
    {
        return whence << "ACK: " << ack.count() << ".";
    }

    std::ostream& print(std::ostream& whence, const sadmip::commands::meta_memoryinfo::result& ack)
    {
        return whence << "ACK: "
            << "Stack: " << ack.stack_reserved() << ", " << ack.stack_max() << "; "
            << "Heap: " << ack.heap_reserved() << " , " << ack.heap_current() << "; "
            << ack.heap_fails() << ".";
    }

    std::ostream& print(std::ostream& whence, const sadmip::commands::meta_dimensions::result& ack)
    {
        return whence << "ACK: " << (int)ack.rows() << "x" << (int)ack.cols() << ".";
    }
}

std::ostream& machine_mode::operator<<(std::ostream& whence, const sadmip::message& msg)
{
    try
    {
        switch (msg.type())
        {
            case sadmip::message_type::ack:       return whence << dynamic_cast<const sadmip::ack&>(msg);
            case sadmip::message_type::error:     return whence << dynamic_cast<const sadmip::error&>(msg);
            case sadmip::message_type::heartbeat: return whence << dynamic_cast<const sadmip::heartbeat&>(msg);
            default:
                return whence << "ERR: Message cannot be printed.";
        }
    }
    catch (std::exception&)
    {
        return whence << "ERR: Invalid message.";
    }
}

std::ostream& machine_mode::operator<<(std::ostream& whence, const sadmip::ack& ack)
{
    try
    {
        switch (ack.opcode())
        {
            case 0x11:
                return print(whence, dynamic_cast<const sadmip::commands::get_cell::result&>(ack));
            case 0x13:
                return print(whence, dynamic_cast<const sadmip::commands::get_matrix::result&>(ack));
            case 0x14:
                return print(whence, dynamic_cast<const sadmip::commands::get_experiment::result&>(ack));
            case 0x18:
                return print(whence, dynamic_cast<const sadmip::commands::get_name::result&>(ack));
            case 0x19:
                return print(whence, dynamic_cast<const sadmip::commands::get_timestep::result&>(ack));
            case 0x1A:
                return print(whence, dynamic_cast<const sadmip::commands::get_meta::result&>(ack));
            case 0x1B:
                return print(whence, dynamic_cast<const sadmip::commands::get_keys::result&>(ack));
            case 0x1C:
                return print(whence, dynamic_cast<const sadmip::commands::get_value::result&>(ack));
            case 0x81:
                return print(whence, dynamic_cast<const sadmip::commands::meta_configuregrid::result&>(ack));
            case 0x83:
                return print(whence, dynamic_cast<const sadmip::commands::meta_framecount::result&>(ack));
            case 0x84:
                return print(whence, dynamic_cast<const sadmip::commands::meta_memoryinfo::result&>(ack));
            case 0x85:
                return print(whence, dynamic_cast<const sadmip::commands::meta_dimensions::result&>(ack));
            default:
                return whence << "ACK.";
        }
    }
    catch (std::exception&)
    {
        return whence << "ACK.";
    }
}

std::ostream& machine_mode::operator<<(std::ostream& whence, const sadmip::error& err)
{
    switch (err.error_code())
    {
        case 1:  whence << "ERR: Index out of bounds"; break;
        case 2:  whence << "ERR: Unknown key"; break;
        case 3:  whence << "ERR: Unknown opcode"; break;
        case 4:  whence << "ERR: Not applicable"; break;
        case 5:  whence << "ERR: Dimension mismatch"; break;
        case 6:  whence << "ERR: Invalid argument"; break;
        case 7:  whence << "ERR: Parse error"; break;
        default: whence << "ERR: Error code: " << static_cast<uint32_t>(err.error_code());
    }

    if (err.what().empty())
    {
        return whence << '.';
    }
    else
    {
        return whence << ": " << err.what() << ".";
    }
}

std::ostream& machine_mode::operator<<(std::ostream& whence, const sadmip::heartbeat& hbt)
{
    switch (hbt.opcode())
    {
        case 1: return whence << "HBT: Device idle.";
        case 2: return whence << "HBT: Device reading data.";
        case 3: return whence << "HBT: Device working.";
        default: 
            return whence << "HBT: Unknown state.";
    }
}
