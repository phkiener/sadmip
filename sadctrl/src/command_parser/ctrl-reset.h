// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Ctrl-Reset.
    */
    class ctrl_reset : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        bool continue_playing = false; //!< whether to continue playing after reset

    };
}
