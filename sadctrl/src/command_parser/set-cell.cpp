// Copyright (c) 2018 Philipp Kiener

#include "set-cell.h"
#include <commands/set-cell.h>

#include <util/string_utils.h>

namespace
{
    std::string boolean_validator(const std::string& boolean)
    {
        std::vector<std::string> legal
        {
            "on", "true", "enable", "yes",
            "off", "false", "disable", "no"
        };

        for (const auto& s : legal)
        {
            if (strequal_icase(s, boolean))
            {
                return std::string();
            }
        }

        std::stringstream ss;
        ss << "\'" << boolean << "\' is not a legal boolean.";
        return ss.str();
    }

    bool boolean_parser(const std::string& boolean)
    {
        if (strequal_icase(boolean, "on")
            || strequal_icase(boolean, "true")
            || strequal_icase(boolean, "enable")
            || strequal_icase(boolean, "yes"))
        {
            return true;
        }

        return false;
    }
}

void parsers::set_cell::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Set-Cell",
        "Set the status of a cell\n"
    );

    sub->add_option("row", row, "The row to set")
        ->required();

    sub->add_option("col", col, "The column to set")
        ->required();

    sub->add_option("active", boolean, "Whether to set the cell active or inactive")
        ->check(boolean_validator)
        ->required();

    sub->callback([this]()
    {
        success(new sadmip::commands::set_cell(row, col, boolean_parser(boolean)));
    });
}
