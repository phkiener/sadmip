// Copyright (c) 2018 Philipp Kiener

#include "get-cell.h"
#include <commands/get-cell.h>

void parsers::get_cell::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Get-Cell",
        "Retrieve the status of a single cell\n"
    );

    sub->add_option("row", row, "The cell's row")
        ->required();

    sub->add_option("col", col, "The cell's column")
        ->required();

    sub->callback([this]()
    {
        success(new sadmip::commands::get_cell(row, col));
    });
}
