// Copyright (c) 2018 Philipp Kiener

#include "set-path.h"
#include <commands/set-path.h>

namespace
{
    std::string move_validator(const std::string& move)
    {
        std::string legals("uUdDlLrRsS");

        for (const auto c : move)
        {
            if (std::find(legals.cbegin(), legals.cend(), c) == legals.cend())
            {
                std::stringstream ss;
                ss << "\'" << c << "\' is not a legal move.";
                return ss.str();
            }
        }
        return std::string();
    }

    sadmip::commands::set_path::direction move_parser(const char c)
    {
        if (c == 'u' || c == 'U')
        {
            return sadmip::commands::set_path::direction::up;
        }

        if (c == 'd' || c == 'D')
        {
            return sadmip::commands::set_path::direction::down;
        }

        if (c == 'l' || c == 'L')
        {
            return sadmip::commands::set_path::direction::left;
        }

        if (c == 'r' || c == 'R')
        {
            return sadmip::commands::set_path::direction::right;
        }

        return sadmip::commands::set_path::direction::still;
    }
}
void parsers::set_path::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Set-Path",
        "Add a droplet path to the experiment\n"
    );

    sub->add_flag("-a,--append", append, "Append frames as needed");

    sub->add_option("row", startrow, "The starting row")
        ->required();

    sub->add_option("col", startcol, "The starting column")
        ->required();

    sub->add_option("moves", moves, "The moves to execute")
        ->check(move_validator)
        ->required();

    sub->callback([this]()
    {
        auto* command = new sadmip::commands::set_path(startrow, startcol, append);

        for (const auto& c : moves)
        {
            command->add_move(move_parser(c));
        }

        success(command);
    });
}
