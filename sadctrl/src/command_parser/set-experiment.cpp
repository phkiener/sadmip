// Copyright (c) 2018 Philipp Kiener

#include "set-experiment.h"
#include <commands/set-experiment.h>

#include <util/string_utils.h>
#include <util/import/decoders.h>

void parsers::set_experiment::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Set-Experiment",
        "Set the complete experiment\n"
    );

    sub->footer("\nThis command supports GIF images and BioGram files.");

    auto play_flag = sub->add_flag(
        "-p,--autoplay",
        autoplay,
        "Start the playback immediately after receiving all frames");

    auto loop_flag = sub->add_flag("-l,--loop", loop, "Enable looped playback");
    loop_flag->needs(play_flag);

    sub->add_option("-t,--timestep", timestep, "The timestep to use");
    sub->add_option("-n, --name", name, "The name of the experiment");

    sub->add_flag("--inverted", inverted, "Whether to invert the generated frames (only GIF)");
    sub->add_option("--threshold", binary_threshold, "The threshold for binarization (only GIF)");

    sub->add_option("--mode", import_mode, "Type of file; if not specified, the file extension is used");
    sub->add_option("file", filename, "The file to extract the frames from")
        ->check(CLI::ExistingFile)
        ->required();

    sub->callback([this]()
    {
        auto command = new sadmip::commands::set_experiment(
            name.empty() ? filename : name,
            timestep);

        command->set_autostart(autoplay, loop);

        std::vector<sadmip::bit_matrix> frames;

        if (import_mode.empty())
        {
            import_mode = extract_extension(filename);
        }

        if (strequal_icase("gif", import_mode) && decode_gif(filename, frames, binary_threshold, inverted))
        {
            command->set_frames(frames);
            success(command);
        }
        else if (strequal_icase("bio", import_mode) && decode_biogram(filename, frames))
        {
            command->set_frames(frames);
            success(command);
        }
        else
        {
            throw CLI::Error("Decoding error", "Could not decode given file");
        }
    });
}
