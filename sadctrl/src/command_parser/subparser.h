// Copyright (c) 2018 Philipp Kiener

#pragma once

#include <cstdint>
#include <memory>

#include <message/command.h>
#include <CLI11.hpp>

// forward declare because of circular dependency
class command_parser;

namespace parsers
{
    /**
     * An abstract base subparser.
     *
     * This parser defines the basic workings of a subcommand parser to be
     * added to the main command parser.
     *
     * To implement a subcommand, derive from this class and provide an overload
     * for /CLI::App* build(CLI::App*)/, which creates the subparser.
     *
     * Write a positive result into /result/ and a negative result into /error/.
     */
    class subparser
    {
    public:
        /**
         * Construct and register the subparser.
         *
         * @param[in] parent the parent parser
         */
        subparser(command_parser* parent);

        /**
         * Construct the subparser.
         *
         * To create a subparser, call /parent->add_subcommand/.
         *
         * @param[in] parent the parent parser
         */
        virtual void build(CLI::App& parent) {}

    protected:
        /**
         * Store a successfully parsed command.
         *
         * The pointer will be owned and will be automatically deleted once
         * no longer required.
         *
         * @param[in] msg the message to store
         */
        void success(sadmip::command* msg);

        /**
         * Store an error due to failing the parsing process.
         *
         * @param[in] error the error to store
         */
        void fail(CLI::Error error);

    private:
        command_parser* _parent = nullptr; //!< the parent parser
    };
}
