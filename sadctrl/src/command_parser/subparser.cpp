// Copyright (c) 2018 Philipp Kiener

#include "subparser.h"
#include "command_parser.h"

parsers::subparser::subparser(command_parser* parent)
    : _parent(parent)
{
    build(_parent->_app);
}

void parsers::subparser::success(sadmip::command* msg)
{
    auto ptr = std::unique_ptr<sadmip::command>(msg);
    _parent->_result = std::move(ptr);
}

void parsers::subparser::fail(CLI::Error error)
{
    _parent->_error = error;
}
