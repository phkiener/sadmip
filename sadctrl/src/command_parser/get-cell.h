// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Get-Cell.
    */
    class get_cell : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        uint8_t row = 0; //!< row to access
        uint8_t col = 0; //!< column to access
    };
}
