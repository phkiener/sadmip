// Copyright (c) 2018 Philipp Kiener

#include "set-name.h"
#include <commands/set-name.h>

void parsers::set_name::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Set-Name",
        "Set the name of the experiment\n"
    );

    sub->add_option("name", name, "The name to set")->required();

    sub->callback([this]()
    {
        success(new sadmip::commands::set_name(name));
    });
}
