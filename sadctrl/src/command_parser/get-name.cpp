// Copyright (c) 2018 Philipp Kiener

#include "get-name.h"
#include <commands/get-name.h>

void parsers::get_name::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Get-Name",
        "Retrieve the name of the experiment\n"
    );

    sub->callback([this]()
    {
        success(new sadmip::commands::get_name());
    });
}
