// Copyright (c) 2018 Philipp Kiener

#include "ctrl-start.h"
#include <commands/ctrl-start.h>

void parsers::ctrl_start::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Ctrl-Start", 
        "Start the playback\n"
    );

    sub->add_flag("-l,--loop", loop, "Loop the playback");
    sub->add_option("-t,--timestep",timestep, "The timestep for the playback");

    sub->callback([this]()
    {
        success(new sadmip::commands::ctrl_start(timestep, loop));
    });
}
