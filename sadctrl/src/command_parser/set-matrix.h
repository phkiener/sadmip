// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Set-Matrix.
    */
    class set_matrix : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        bool append = false; //!< whether to append the matrix
        std::string matrix = ""; //!< the matrix as string
    };
}
