// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Set-Meta.
    */
    class set_meta : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        std::vector<std::string> assignments; //!< the key=value pairs to assign
    };
}
