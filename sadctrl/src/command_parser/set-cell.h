// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Set-Cell.
    */
    class set_cell : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        uint8_t row = 0; //!< the row to modify
        uint8_t col = 0; //!< the column to modify
        std::string boolean = ""; //!< string to parse into a boolean
    };
}
