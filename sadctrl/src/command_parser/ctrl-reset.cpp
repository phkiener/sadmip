// Copyright (c) 2018 Philipp Kiener

#include "ctrl-reset.h"
#include <commands/ctrl-reset.h>

void parsers::ctrl_reset::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Ctrl-Reset",
        "Go to the first frame\n"
    );

    sub->add_option("--continue-playing", continue_playing, "Continue the playback");

    sub->callback([this]()
    {
        success(new sadmip::commands::ctrl_reset(continue_playing));
    });
}
