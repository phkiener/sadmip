// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Set-Value.
    */
    class set_value : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        std::string key; //!< the key whose value to set
        std::string value; //!< the value to set
    };
}
