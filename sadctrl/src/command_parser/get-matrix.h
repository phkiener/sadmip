// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Get-Matrix.
    */
    class get_matrix : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        int32_t index = -1; //!< frame to jump to; will be cut to uint16_t if > 0
    };
}
