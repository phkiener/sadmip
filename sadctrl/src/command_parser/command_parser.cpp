// Copyright (c) 2018 Philipp Kiener

#include "command_parser.h"
#include "all_subparsers.h"

#include <util/string_utils.h>

command_parser::command_parser(bool keep_help)
    : _app("sadctrl-command parser", "--"),
    _result(nullptr),
    _error(CLI::Success())
{
    _app.allow_extras(true);
    _app.require_subcommand(0, 1);
    _app.ignore_case(true);

    register_parser<parsers::ctrl_goto>();
    register_parser<parsers::ctrl_next>();
    register_parser<parsers::ctrl_prev>();
    register_parser<parsers::ctrl_reset>();
    register_parser<parsers::ctrl_start>();
    register_parser<parsers::ctrl_stop>();

    register_parser<parsers::get_cell>();
    register_parser<parsers::get_experiment>();
    register_parser<parsers::get_matrix>();
    register_parser<parsers::get_meta>();
    register_parser<parsers::get_name>();
    register_parser<parsers::get_timestep>();
    register_parser<parsers::get_value>();

    register_parser<parsers::meta_configuregrid>();
    register_parser<parsers::meta_dimensions>();
    register_parser<parsers::meta_framecount>();
    register_parser<parsers::meta_memoryinfo>();
    register_parser<parsers::meta_reset>();

    register_parser<parsers::set_cell>();
    register_parser<parsers::set_experiment>();
    register_parser<parsers::set_keys>();
    register_parser<parsers::set_matrix>();
    register_parser<parsers::set_meta>();
    register_parser<parsers::set_name>();
    register_parser<parsers::set_path>();
    register_parser<parsers::set_timestep>();
    register_parser<parsers::set_value>();

    if (!keep_help)
    {
        _app.remove_option(_app.get_help_ptr());

        for (auto cmd : _app.get_subcommands([](const CLI::App* app) { return true; }))
        {
            cmd->remove_option(cmd->get_help_ptr());
        }
    }
}

std::unique_ptr<sadmip::command> command_parser::parse(std::string& line)
{
    auto args = split_into_arguments(line);
    std::reverse(args.begin(), args.end()); // CLI11 wants it to be reversed. Huh.

    _error = CLI::Success();

    try
    {
        _app.parse(args);
    }
    catch (CLI::Error& e)
    {
        _result = nullptr;
        _error = e;
        return nullptr;
    }

    auto temp = std::move(_result);
    _result = nullptr;

    if (temp == nullptr)
    {
        _error = CLI::Error("Parser Error", "Not a valid subcommand", CLI::ExitCodes::BaseClass);
    }

    return temp;
}

std::unique_ptr<sadmip::command> command_parser::parse(int argc, char** argv)
{
    _error = CLI::Success();

    try
    {
        _app.parse(argc, argv);
    }
    catch (CLI::Error& e)
    {
        _result = nullptr;
        _error = e;
        return nullptr;
    }

    auto temp = std::move(_result);
    _result = nullptr;

    if (temp == nullptr)
    {
        _error = CLI::Error("Parser Error", "Not a valid subcommand", CLI::ExitCodes::BaseClass);
    }

    return temp;
}

std::ostream& command_parser::help(std::ostream& whence, std::string topic)
{
    auto subcommands = _app.get_subcommands([](const CLI::App* app) { return true; });

    if (topic.empty())
    {
        whence << "Possible commands:\n";

        for (auto sub : subcommands)
        {
            whence << "  " << sub->get_name() << "\n";
        }

        whence << "\nType help COMMAND to get usage information about COMMAND.\n";
    }
    else
    {
        for (auto sub : subcommands)
        {
            if (strequal_icase(topic, sub->get_name()))
            {
                return whence << sub->help() << "\n";
            }
        }

        whence << "No matching entry for \'" << topic << "\'\n";
    }

    return whence;
}

std::vector<std::string> command_parser::subcommands() const
{
    auto subcommands = _app.get_subcommands([](const CLI::App* app) { return true; });
    std::vector<std::string> names;

    for (auto cmd : subcommands)
    {
        names.push_back(cmd->get_name());
    }

    return names;
}

CLI::Error command_parser::error() const
{
    return _error;
}
