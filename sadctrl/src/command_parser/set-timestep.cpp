// Copyright (c) 2018 Philipp Kiener

#include "set-timestep.h"
#include <commands/set-timestep.h>

void parsers::set_timestep::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Set-Timestep",
        "Set the timestep of the experiment\n"
    );

    sub->add_option("timestep", timestep, "The timestep to set")
        ->required();

    sub->callback([this]()
    {
        success(new sadmip::commands::set_timestep(timestep));
    });
}
