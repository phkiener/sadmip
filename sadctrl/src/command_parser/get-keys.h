// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Get-Keys.
    */
    class get_keys : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;
    };
}
