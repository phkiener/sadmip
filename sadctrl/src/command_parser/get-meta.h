// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Get-Meta.
    */
    class get_meta : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;
    };
}
