// Copyright (c) 2018 Philipp Kiener

#include "ctrl-goto.h"
#include <commands/ctrl-goto.h>

void parsers::ctrl_goto::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Ctrl-Goto",
        "Jump to a certain frame\n"
    );

    sub->add_option("frame", target, "Index of the frame to jump to")
        ->required();

    sub->callback([this]()
    {
        success(new sadmip::commands::ctrl_goto(target));
    });
}
