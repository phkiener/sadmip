// Copyright (c) 2018 Philipp Kiener

#include "meta-framecount.h"
#include <commands/meta-framecount.h>

void parsers::meta_framecount::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Meta-FrameCount",
        "Retrieve the amount of frames in the experiment\n"
    );

    sub->callback([this]()
    {
        success(new sadmip::commands::meta_framecount());
    });
}
