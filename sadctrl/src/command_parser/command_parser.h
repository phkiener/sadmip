// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

#include <message/command.h>

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <CLI11.hpp>

/*
 * A parser for sadmip-commands.
 */
class command_parser
{
    friend parsers::subparser;

public:
    /**
     * Create (and initialize) the commandline parser.
     *
     * @param[in] keep_help whether to keep the help flags in the parser
     */
    explicit command_parser(bool keep_help = false);

    /**
     * Tries to parse a line into a sadmip-command.
     *
     * If parsing is successful, a smart pointer to the command is returned.
     * On an error, a null pointer is returned.
     *
     * @param[in] line the line to parse
     * @return         pointer to a valid command or a nullptr if parsing fails
     */
    std::unique_ptr<sadmip::command> parse(std::string& line);

    /**
     * Tries to parse a line into a sadmip-command.
     *
     * If parsing is successful, a smart pointer to the command is returned.
     * On an error, a null pointer is returned.
     *
     * @param[in] argc number of arguments
     * @param[in] argv pointer to an array of c-string arguments
     * @return         pointer to a valid command or a nullptr if parsing fails
     */
    std::unique_ptr<sadmip::command> parse(int argc, char** argv);

    /**
     * Prints a help text to the given stream.
     *
     * The help text is formatted by the commandline parser.
     * If given a topic, help for the specific command is printed. If
     * /topic/ is an empty string, a list of all subcommands is printed.
     *
     * @param[in] whence where to write to
     * @param[in] topic  what to print help about
     * @return           the modified stream
     */
    std::ostream& help(std::ostream& whence, std::string topic = "");

    /**
     * Returns a list of the names of all registered subcommands.
     *
     * @return list of all subcommands' names
     */
    std::vector<std::string> subcommands() const;

    /**
     * Returns the error that has happened in the last parsing operation.
     *
     * @return error of the last parsing operation
     */
    CLI::Error error() const;

private:
    CLI::App _app; //!< the parser
    CLI::Error _error; //!< the error
    std::unique_ptr<sadmip::command> _result; //!< the result of the last parse

    /**
     * Register a subparser class.
     *
     * A parser of the template type is created and stored.
     *
     * @tparam Parser type of parser to register
     */
    template<typename Parser>
    void register_parser();

    std::vector<std::unique_ptr<parsers::subparser>> _subparsers; //!< all subparsers
};

template<typename Parser>
inline void command_parser::register_parser()
{
    static_assert(
        std::is_base_of<parsers::subparser, Parser>::value,
        "Parser must derive from parsers::subparser"
    );

    std::unique_ptr<Parser> sub = std::make_unique<Parser>(this);
    sub->build(_app);

    _subparsers.emplace_back(std::move(sub));
}
