// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Set-Experiment.
    */
    class set_experiment : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        bool autoplay = false; //!< whether to immediately start playback
        bool loop = false; //!< whether to loop the playback

        std::string name = ""; //!< the name to use
        uint16_t timestep = 0; //!< the timestep to use

        std::string import_mode = "";
        std::string filename = ""; //!< the GIF to read

        uint8_t binary_threshold = 127; //!< highest possible "off" pixel value in GIF
        bool inverted = false; //!< if true, invert the bit matrix (black means on then)
    };
}
