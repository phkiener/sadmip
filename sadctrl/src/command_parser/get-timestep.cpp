// Copyright (c) 2018 Philipp Kiener

#include "get-timestep.h"
#include <commands/get-timestep.h>

void parsers::get_timestep::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Get-Timestep",
        "Retrieve the timestep of the experiment\n"
    );

    sub->callback([this]()
    {
        success(new sadmip::commands::get_timestep());
    });
}
