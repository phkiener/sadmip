// Copyright (c) 2018 Philipp Kiener

#include "set-matrix.h"
#include <commands/set-matrix.h>

#include <util/string_utils.h>

#include <datatypes/bit_matrix.h>

namespace
{
    sadmip::bit_matrix matrix_parser(const std::string& text)
    {
        auto rows = split_at(text, ';', true);

        if (rows.size() < 1 || rows.size() >= UINT8_MAX)
        {
            return sadmip::bit_matrix(0, 0);
        }

        uint8_t num_rows = static_cast<uint8_t>(rows.size());
        uint8_t num_cols = static_cast<uint8_t>(rows.at(0).size());

        for (auto r : rows)
        {
            if (r.size() != num_cols)
            {
                return sadmip::bit_matrix(0, 0);
            }
        }

        sadmip::bit_matrix matrix(num_rows, num_cols);

        for (uint8_t r = 0; r < num_rows; ++r)
        {
            for (uint8_t c = 0; c < num_cols; ++c)
            {
                matrix.set(r, c, rows.at(r).at(c) == '1' ? true : false);
            }
        }

        return matrix;
    }

    std::string matrix_validator(const std::string& text)
    {
        auto res = matrix_parser(text);

        if (res.cols() == 0 && res.rows() == 0)
        {
            return std::string("Not a valid matrix.");
        }

        return std::string();
    }
}

void parsers::set_matrix::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Set-Matrix",
        "Set the current matrix\n"
    );

    sub->footer(
        "\nMatries are given as semicolon-separated chunks of 1's and 0's,\n"
        "where a 1 indicates an active cell and a 0 indicates an inactive cell.\n"
        "\n"
        "As example, 4x4 checkerboard matrix is as follows:\n"
        "    1010;0101;1010;0101"
    );

    sub->add_flag("-a,--append", append, "Append the matrix instead");
    sub->add_option("matrix", matrix, "The matrix to set")
        ->check(matrix_validator)
        ->required();

    sub->callback([this]()
    {
        success(new sadmip::commands::set_matrix(matrix_parser(matrix), append));
    });

}
