// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Meta-ConfigureGrid.
    */
    class meta_configuregrid : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        std::string grid = ""; //!< type of grid to configure
    };
}
