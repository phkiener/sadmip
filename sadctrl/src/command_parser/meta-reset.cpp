// Copyright (c) 2018 Philipp Kiener

#include "meta-reset.h"
#include <commands/meta-reset.h>

void parsers::meta_reset::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Meta-Reset",
        "Reset the device's experiment\n"
    );

    sub->callback([this]()
    {
        success(new sadmip::commands::meta_reset());
    });
}
