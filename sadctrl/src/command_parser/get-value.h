// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Get-Value.
    */
    class get_value : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        std::string key; //!< the key whose value to get
    };
}
