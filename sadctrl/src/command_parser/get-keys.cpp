// Copyright (c) 2018 Philipp Kiener

#include "get-keys.h"
#include <commands/get-keys.h>

void parsers::get_keys::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Get-Keys",
        "Retreive a list of all meta-keys\n"
    );

    sub->callback([this]()
    {
        success(new sadmip::commands::get_keys());
    });
}
