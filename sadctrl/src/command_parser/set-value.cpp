// Copyright (c) 2018 Philipp Kiener

#include "set-value.h"
#include <commands/set-value.h>

void parsers::set_value::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Set-Value",
        "Set meta-information for a given key\n"
    );

    sub->add_option("key", key, "The key whose value to set")
        ->required();

    sub->add_option("value", value, "The value to set")
        ->required();

    sub->callback([this]()
    {
        success(new sadmip::commands::set_value(key, value));
    });
}
