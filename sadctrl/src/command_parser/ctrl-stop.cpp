// Copyright (c) 2018 Philipp Kiener

#include "ctrl-stop.h"
#include <commands/ctrl-stop.h>

void parsers::ctrl_stop::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Ctrl-Stop", 
        "Stop the playback\n"
    );

    sub->callback([this]()
    {
        success(new sadmip::commands::ctrl_stop());
    });
}
