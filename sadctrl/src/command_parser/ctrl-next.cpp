// Copyright (c) 2018 Philipp Kiener

#include "ctrl-next.h"
#include <commands/ctrl-next.h>

void parsers::ctrl_next::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Ctrl-Next",
        "Go to the next frame\n"
    );

    sub->add_option("skip", skip, "Number of frames to skip");

    sub->callback([this]()
    {
        success(new sadmip::commands::ctrl_next(skip));
    });
}
