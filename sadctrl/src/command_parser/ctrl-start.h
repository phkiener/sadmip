// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Ctrl-Start.
    */
    class ctrl_start : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        bool loop = false; //!< whether to loop
        uint16_t timestep = 0; //!< timestep to configure
    };
}
