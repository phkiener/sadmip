// Copyright (c) 2018 Philipp Kiener

#include "get-matrix.h"
#include <commands/get-matrix.h>

void parsers::get_matrix::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Get-Matrix",
        "Retrieve the current frame\n"
    );

    sub->add_option("-f,--frame", index, "Specific frame to retrieve");

    sub->callback([this]()
    {
        if (index >= 0)
        {
            success(new sadmip::commands::get_matrix(static_cast<uint16_t>(index)));
        }
        else
        {
            success(new sadmip::commands::get_matrix());
        }

    });
}
