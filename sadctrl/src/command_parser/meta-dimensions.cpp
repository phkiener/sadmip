// Copyright (c) 2018 Philipp Kiener

#include "meta-dimensions.h"
#include <commands/meta-dimensions.h>

void parsers::meta_dimensions::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Meta-Dimensions",
        "Retrieve the dimensions of the device\n"
    );

    sub->callback([this]()
    {
        success(new sadmip::commands::meta_dimensions());
    });
}
