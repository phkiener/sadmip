// Copyright (c) 2018 Philipp Kiener

#include "get-meta.h"
#include <commands/get-meta.h>

void parsers::get_meta::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Get-Meta", 
        "Retreive all meta information\n"
    );

    sub->callback([this]()
    {
        success(new sadmip::commands::get_meta());
    });
}
