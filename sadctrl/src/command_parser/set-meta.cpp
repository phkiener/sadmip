// Copyright (c) 2018 Philipp Kiener

#include "set-meta.h"
#include <commands/set-meta.h>

#include <util/string_utils.h>

namespace
{
    std::string key_value_assignment_validator(const std::string& assignment)
    {
        if (assignment.find('=') != std::string::npos)
        {
            return std::string();
        }

        std::stringstream ss;
        ss << "\'" << assignment << "\' is not a legal assignment.";
        return ss.str();
    }

    std::tuple<std::string, std::string> key_value_assignment_parser(const std::string& assignment)
    {
        size_t pos = assignment.find('=');

        if (pos == std::string::npos)
        {
            return std::make_tuple("", "");
        }

        std::string key = assignment.substr(0, pos);
        std::string value = assignment.substr(pos + 1);

        return std::make_tuple(key, value);
    }
}

void parsers::set_meta::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Set-Meta",
        "Set the meta-information of the experiment\n"
    );

    sub->add_option("assignments", assignments, "The key=value pairs")
        ->check(key_value_assignment_validator)
        ->required();

    sub->callback([this]()
    {
        auto* cmd = new sadmip::commands::set_meta();

        for (const std::string& s : assignments)
        {
            auto tuple = key_value_assignment_parser(s);
            cmd->add_pair(std::get<0>(tuple), std::get<1>(tuple));
        }

        success(cmd);
    });
}
