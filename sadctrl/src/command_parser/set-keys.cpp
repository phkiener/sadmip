// Copyright (c) 2018 Philipp Kiener

#include "set-keys.h"
#include <commands/set-keys.h>

void parsers::set_keys::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Set-Keys",
        "Replace the set of meta-keys\n"
    );

    auto set_keys_add_flag = sub->add_flag("-a,--add", add, "Append the listed keys instead");
    auto set_keys_erase_flag = sub->add_flag("-e,--erase", erase, "Erase the listed keys instead");

    set_keys_add_flag->excludes(set_keys_erase_flag);
    set_keys_erase_flag->excludes(set_keys_add_flag);

    sub->add_option("keys", keys, "The keys to act upon")
        ->required();

    sub->callback([this]()
    {       
        using action = sadmip::commands::set_keys::mode;
        action mode = add ? action::add : (erase ? action::erase : action::replace);
        
        success(new sadmip::commands::set_keys(mode, keys));
    });
}
