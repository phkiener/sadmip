// Copyright (c) 2018 Philipp Kiener

#include "get-value.h"
#include <commands/get-value.h>

void parsers::get_value::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Get-Value",
        "Retrieve information about a specific meta value\n"
    );

    sub->add_option("key", key, "The key to retrieve information of")
        ->required();

    sub->callback([this]()
    {
        success(new sadmip::commands::get_value(key));
    });
}
