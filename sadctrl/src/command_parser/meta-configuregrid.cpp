// Copyright (c) 2018 Philipp Kiener

#include "meta-configuregrid.h"
#include <commands/meta-configuregrid.h>

void parsers::meta_configuregrid::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Meta-ConfigureGrid",
        "Configure the mounted grid\n"
    );

    sub->add_option("grid", grid, "The type of grid to use");
    sub->footer("\nIf no grid is passed, the command instead queries the list of available grids.");

    sub->callback([this]()
    {
        if (grid.empty())
        {
            success(new sadmip::commands::meta_configuregrid());
        }
        else
        {
            success(new sadmip::commands::meta_configuregrid(grid));
        }
    });
}
