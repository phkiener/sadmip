// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Ctrl-Next.
    */
    class ctrl_next : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        uint8_t skip = 0; //!< the amount of frames to skip

    };
}
