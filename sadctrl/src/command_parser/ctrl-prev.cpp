// Copyright (c) 2018 Philipp Kiener

#include "ctrl-prev.h"
#include <commands/ctrl-prev.h>

void parsers::ctrl_prev::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Ctrl-Prev",
        "Go to the previous frame\n"
    );

    sub->add_option("skip", skip, "Number of frames to skip");

    sub->callback([this]()
    {
        success(new sadmip::commands::ctrl_prev(skip));
    });
}
