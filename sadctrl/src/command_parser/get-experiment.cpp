// Copyright (c) 2018 Philipp Kiener

#include "get-experiment.h"
#include <commands/get-experiment.h>

void parsers::get_experiment::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Get-Experiment",
        "Retrieve the complete experiment\n"
    );

    sub->callback([this]()
    {
        success(new sadmip::commands::get_experiment());
    });
}
