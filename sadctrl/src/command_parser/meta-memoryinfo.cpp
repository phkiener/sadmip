// Copyright (c) 2018 Philipp Kiener

#include "meta-memoryinfo.h"
#include <commands/meta-memoryinfo.h>

void parsers::meta_memoryinfo::build(CLI::App& parent)
{
    auto sub = parent.add_subcommand(
        "Meta-MemoryInfo",
        "Retrieve memory usage diagnostics\n"
    );

    sub->callback([this]()
    {
        success(new sadmip::commands::meta_memoryinfo());
    });
}
