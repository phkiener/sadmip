// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Set-Keys.
    */
    class set_keys : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        bool add = false; //!< whether to add the keys instead
        bool erase = false; //!< whether to erase the keys instead
        std::vector<std::string> keys; //!< the keys to act upon
    };
}
