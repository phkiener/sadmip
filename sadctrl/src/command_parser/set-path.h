// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
    * Subcommand parser for Set-Path.
    */
    class set_path : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        uint8_t startrow = 0; //!< the starting cell's row
        uint8_t startcol = 0; //!< the starting cell's column
        bool append = false; //!< whether to append frames
        std::string moves; //!< the list of moves to parse
    };
}
