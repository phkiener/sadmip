// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "subparser.h"

namespace parsers
{
    /**
     * Subcommand parser for Ctrl-Goto.
     */
    class ctrl_goto : public subparser
    {
        using subparser::subparser;

    public:
        void build(CLI::App& parent) override;

    private:
        uint16_t target = 0; //!< the target timestep

    };
}
