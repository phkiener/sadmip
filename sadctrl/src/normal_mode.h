// Copyright (c) 2018 Philipp Kiener
#pragma once

#include "options.h"

namespace normal_mode
{
    int main(commandline_options opts);
}
