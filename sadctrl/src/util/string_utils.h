// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cctype>
#include <cstring>
#include <string>
#include <vector>

/**
 * Splits a given string into a list of strings.
 *
 * The contents of /str/ are copied as needed.
 *
 * @param[in] str        the string to split
 * @param[in] split      the character on which to split
 * @param[in] keep_empty whether to keep empty parts
 * @return               the list of strings
 */
std::vector<std::string> split_at(const std::string& source, char split, bool keep_empty = true);

/**
 * Splits a string into a vector of arguments.
 * 
 * The splitting occurs similarily to how a shell might split
 * a line of input into tokens. Normally, whitespace marks the difference
 * between tokens where multiple spaces are collapsed into a single one.
 * Quotes are initiated with " and ended with a single "; characters can
 * be escaped using \, which e.g. allows a " in a quoted token with \".
 * 
 * @param[in]  line the string to split
 * @return          the resulting arguments
 */
std::vector<std::string> split_into_arguments(const std::string& line);

/**
 * Checks if /a/ is a prefix of /b/ while ignoring the case.
 *
 * @param[in] a first string
 * @param[in] b second string
 * @return      true if /a/ is a prefix of /b/ case-insensitively, false otherwise
 */
bool prefix_of_icase(const std::string& a, const std::string& b);

/**
 * Checks if two strings are equal while ignoring the case.
 *
 * @param[in] a first string
 * @param[in] b second string
 * @return      true if both strings are equal case-insensitively, false otherwise
 */
bool strequal_icase(const std::string& a, const std::string& b);

/**
 * Extracts the extension of a filename.
 *
 * The extension is any number of characters followed by a dot (.). If no
 * extension is present, an empty string is returned.
 * If /shortest/ is set to true, the shortest possible extension is returned.
 * If it is set to false, the longest possible extension is returned.
 *
 * Take for example the filename "archive.tar.gz";
 *  with /shortest == true/: "gz"
 *  with /longest == true/: "tar.gz"
 *
 * The dots are not included in the returned string.
 *
 * @param[in] file     the filename to extract the extension of
 * @param[in] shortest whether to return the shortest possible extension
 * @return             the found extension or an empty string
 */
std::string extract_extension(const std::string& file, bool shortest = true);
