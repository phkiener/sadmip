// Copyright (c) 2018 Philipp Kiener

#include "string_utils.h"

std::vector<std::string> split_at(const std::string& source, char split, bool keep_empty)
{
    std::vector<std::string> parts;
    std::string current = "";

    for (char c : source)
    {
        if (c == split)
        {
            if (!current.empty() || keep_empty)
            {
                parts.push_back(current);
                current = "";
            }
        }
        else
        {
            current.push_back(c);
        }
    }

    if (!current.empty())
    {
        parts.push_back(current);
    }

    return parts;
}

std::vector<std::string> split_into_arguments(const std::string& line)
{
    static const char QUOTE = '\"';
    static const char ESCAPE = '\\';

    std::vector<std::string> args;
    std::string current_arg = "";

    bool in_quote = false;
    bool escaped = false;

    for (char c : line)
    {
        if (escaped)
        {
            current_arg.push_back(c);
            escaped = false;
        }
        else if (in_quote)
        {
            if (c == QUOTE)
            {
                args.push_back(current_arg);
                current_arg = "";
                in_quote = false;
            }
            else if (c == ESCAPE)
            {
                escaped = true;
            }
            else
            {
                current_arg.push_back(c);
            }
        }
        else
        {
            switch (c)
            {
                case QUOTE: in_quote = true; break;
                case ESCAPE: escaped = true; break;

                    /* whitespace */
                case '\t': // fallthrough
                case '\r': // fallthrough
                case '\n': // fallthrough
                case ' ':
                    if (!current_arg.empty())
                    {
                        args.push_back(current_arg);
                        current_arg = "";
                    }
                    break;

                default: current_arg.push_back(c); break;
            }
        }
    }

    if (!current_arg.empty())
    {
        args.push_back(current_arg);
    }

    return args;
}

bool prefix_of_icase(const std::string& a, const std::string& b)
{
    if (a.size() > b.size()) return false;
    
    std::string shortened(b.cbegin(), b.cbegin() + a.length());

    return strequal_icase(a, shortened);
}

bool strequal_icase(const std::string& a, const std::string& b)
{
    if (a.size() != b.size())
    {
        return false;
    }

    for (size_t i = 0; i < a.size(); ++i)
    {
        const unsigned char a_c = a.at(i);
        const unsigned char b_c = b.at(i);

        if (std::tolower(a_c) != std::tolower(b_c))
        {
            return false;
        }
    }

    return true;
}

std::string extract_extension(const std::string& file, bool shortest)
{
    const auto dot = shortest ? file.find_last_of('.') : file.find_first_of('.');
    return dot == std::string::npos ? "" : file.substr(dot + 1);
}
