
// Generated from BioLexerGrammar.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"




class  BioLexerGrammar : public antlr4::Lexer {
public:
  enum {
    Sinks = 1, Droplets = 2, PinAssignments = 3, Fluids = 4, Blockages = 5, 
    Nets = 6, Routes = 7, Grid = 8, Dispensers = 9, Detectors = 10, Mixers = 11, 
    Heaters = 12, Magnets = 13, Annotations = 14, MedaRoutes = 15, MedaNets = 16, 
    CellActuations = 17, PinActuations = 18, Direction = 19, END = 20, Integer = 21, 
    Identifier = 22, Annotation = 23, Comment = 24, Newlines = 25, NEWLINE = 26, 
    WS = 27, LParen = 28, RParen = 29, LBracket = 30, RBracket = 31, Dash = 32, 
    Plus = 33, Slash = 34, Equals = 35, Comma = 36, Asterisk = 37, Arrow = 38, 
    Colon = 39, Delimiters = 40, Signs = 41, LessThan = 42, ActuationVector = 43, 
    WhiteSpaceInActuationMode = 44, AreaAnnotationText = 45
  };

  enum {
    COMMENT = 2, ANNOTATION = 3
  };

  enum {
    ACTUATION = 1, AREAANNOTATION = 2
  };

  BioLexerGrammar(antlr4::CharStream *input);
  ~BioLexerGrammar();

  virtual std::string getGrammarFileName() const override;
  virtual const std::vector<std::string>& getRuleNames() const override;

  virtual const std::vector<std::string>& getChannelNames() const override;
  virtual const std::vector<std::string>& getModeNames() const override;
  virtual const std::vector<std::string>& getTokenNames() const override; // deprecated, use vocabulary instead
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;

  virtual const std::vector<uint16_t> getSerializedATN() const override;
  virtual const antlr4::atn::ATN& getATN() const override;

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;
  static std::vector<std::string> _channelNames;
  static std::vector<std::string> _modeNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  // Individual action functions triggered by action() above.

  // Individual semantic predicate functions triggered by sempred() above.

  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

