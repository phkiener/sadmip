// Copyright (c) 2018 Philipp Kiener

#include "bio_types.h"

biogram::position::position(Bio::PositionContext* context)
{ 
    x = static_cast<uint8_t>(std::stoi(context->xpos()->Integer()->getText()));
    y = static_cast<uint8_t>(std::stoi(context->ypos()->Integer()->getText()));
}

std::vector<biogram::actuation> biogram::actuation_vector(const std::string& str)
{
    std::vector<actuation> actuations;
    actuations.reserve(str.size());

    for (char a : str)
    {
        switch (a)
        {
            case '1': actuations.push_back(actuation::on); break;
            case '0': actuations.push_back(actuation::off); break;
            case 'x': /* fallthrough */
            case 'X': actuations.push_back(actuation::dont_care); break;
        }
    }

    return actuations;
}

std::vector<biogram::actuation> biogram::actuation_vector(Bio::CellActuationContext* context)
{
    std::string text = context->ActuationVector()->getText();
    
    return actuation_vector(text);
}

std::vector<biogram::actuation> biogram::actuation_vector(Bio::PinActuationContext* context)
{
    std::string text = context->ActuationVector()->getText();

    return actuation_vector(text);
}

biogram::time_range::time_range(Bio::TimeRangeContext* context)
{
    begin = static_cast<uint16_t>(std::stoi(context->Integer(0)->getText()));
    end = static_cast<uint16_t>(std::stoi(context->Integer(1)->getText()));
}

biogram::grid_block::grid_block(Bio::GridblockContext* context)
    : one(context->position(0)), two(context->position(1))
{

}

biogram::route::route(Bio::RouteContext* context)
{
    for (auto pos : context->position())
    {
        emplace_back(pos);
    }
}

biogram::cell_actuations::cell_actuations(Bio::CellActuationContext* context)
    : cell(context->position()), actuations(actuation_vector(context))
{

}

biogram::pin_assignments::pin_assignments(Bio::PinToPositionsAssignmentContext* context)
{
    id = std::stoi(context->pinID()->getText());

    for (Bio::PositionContext* pos : context->position())
    {
        positions.emplace_back(pos);
    }
}

biogram::pin_assignments::pin_assignments(Bio::PositionToPinAssignmentContext* context)
{
    id = std::stoi(context->pinID()->getText());
    positions.emplace_back(context->position());
}

biogram::pin_actuations::pin_actuations(Bio::PinActuationContext* context)
    : id(std::stoi(context->pinID()->getText())), actuations(actuation_vector(context))
{

}

biogram::mixer::mixer(Bio::MixerContext* context)
{
    time = time_range(context->timeRange());

    block.one = position(context->position(0));
    block.two = position(context->position(1));
}
