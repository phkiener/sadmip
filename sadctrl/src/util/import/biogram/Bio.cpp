
// Generated from Bio.g4 by ANTLR 4.7.1



#include "Bio.h"


using namespace antlrcpp;
using namespace antlr4;

Bio::Bio(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

Bio::~Bio() {
  delete _interpreter;
}

std::string Bio::getGrammarFileName() const {
  return "Bio.g4";
}

const std::vector<std::string>& Bio::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& Bio::getVocabulary() const {
  return _vocabulary;
}


//----------------- BioContext ------------------------------------------------------------------

Bio::BioContext::BioContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Bio::GridContext *> Bio::BioContext::grid() {
  return getRuleContexts<Bio::GridContext>();
}

Bio::GridContext* Bio::BioContext::grid(size_t i) {
  return getRuleContext<Bio::GridContext>(i);
}

std::vector<Bio::NetsContext *> Bio::BioContext::nets() {
  return getRuleContexts<Bio::NetsContext>();
}

Bio::NetsContext* Bio::BioContext::nets(size_t i) {
  return getRuleContext<Bio::NetsContext>(i);
}

std::vector<Bio::MixersContext *> Bio::BioContext::mixers() {
  return getRuleContexts<Bio::MixersContext>();
}

Bio::MixersContext* Bio::BioContext::mixers(size_t i) {
  return getRuleContext<Bio::MixersContext>(i);
}

std::vector<Bio::SinksContext *> Bio::BioContext::sinks() {
  return getRuleContexts<Bio::SinksContext>();
}

Bio::SinksContext* Bio::BioContext::sinks(size_t i) {
  return getRuleContext<Bio::SinksContext>(i);
}

std::vector<Bio::DetectorsContext *> Bio::BioContext::detectors() {
  return getRuleContexts<Bio::DetectorsContext>();
}

Bio::DetectorsContext* Bio::BioContext::detectors(size_t i) {
  return getRuleContext<Bio::DetectorsContext>(i);
}

std::vector<Bio::HeatersContext *> Bio::BioContext::heaters() {
  return getRuleContexts<Bio::HeatersContext>();
}

Bio::HeatersContext* Bio::BioContext::heaters(size_t i) {
  return getRuleContext<Bio::HeatersContext>(i);
}

std::vector<Bio::MagnetsContext *> Bio::BioContext::magnets() {
  return getRuleContexts<Bio::MagnetsContext>();
}

Bio::MagnetsContext* Bio::BioContext::magnets(size_t i) {
  return getRuleContext<Bio::MagnetsContext>(i);
}

std::vector<Bio::DispensersContext *> Bio::BioContext::dispensers() {
  return getRuleContexts<Bio::DispensersContext>();
}

Bio::DispensersContext* Bio::BioContext::dispensers(size_t i) {
  return getRuleContext<Bio::DispensersContext>(i);
}

std::vector<Bio::RoutesContext *> Bio::BioContext::routes() {
  return getRuleContexts<Bio::RoutesContext>();
}

Bio::RoutesContext* Bio::BioContext::routes(size_t i) {
  return getRuleContext<Bio::RoutesContext>(i);
}

std::vector<Bio::PinActuationsContext *> Bio::BioContext::pinActuations() {
  return getRuleContexts<Bio::PinActuationsContext>();
}

Bio::PinActuationsContext* Bio::BioContext::pinActuations(size_t i) {
  return getRuleContext<Bio::PinActuationsContext>(i);
}

std::vector<Bio::CellActuationsContext *> Bio::BioContext::cellActuations() {
  return getRuleContexts<Bio::CellActuationsContext>();
}

Bio::CellActuationsContext* Bio::BioContext::cellActuations(size_t i) {
  return getRuleContext<Bio::CellActuationsContext>(i);
}

std::vector<Bio::BlockagesContext *> Bio::BioContext::blockages() {
  return getRuleContexts<Bio::BlockagesContext>();
}

Bio::BlockagesContext* Bio::BioContext::blockages(size_t i) {
  return getRuleContext<Bio::BlockagesContext>(i);
}

std::vector<Bio::PinAssignmentsContext *> Bio::BioContext::pinAssignments() {
  return getRuleContexts<Bio::PinAssignmentsContext>();
}

Bio::PinAssignmentsContext* Bio::BioContext::pinAssignments(size_t i) {
  return getRuleContext<Bio::PinAssignmentsContext>(i);
}

std::vector<Bio::FluidsContext *> Bio::BioContext::fluids() {
  return getRuleContexts<Bio::FluidsContext>();
}

Bio::FluidsContext* Bio::BioContext::fluids(size_t i) {
  return getRuleContext<Bio::FluidsContext>(i);
}

std::vector<Bio::DropletsContext *> Bio::BioContext::droplets() {
  return getRuleContexts<Bio::DropletsContext>();
}

Bio::DropletsContext* Bio::BioContext::droplets(size_t i) {
  return getRuleContext<Bio::DropletsContext>(i);
}

std::vector<Bio::MedaRoutesContext *> Bio::BioContext::medaRoutes() {
  return getRuleContexts<Bio::MedaRoutesContext>();
}

Bio::MedaRoutesContext* Bio::BioContext::medaRoutes(size_t i) {
  return getRuleContext<Bio::MedaRoutesContext>(i);
}

std::vector<Bio::MedaNetsContext *> Bio::BioContext::medaNets() {
  return getRuleContexts<Bio::MedaNetsContext>();
}

Bio::MedaNetsContext* Bio::BioContext::medaNets(size_t i) {
  return getRuleContext<Bio::MedaNetsContext>(i);
}

std::vector<Bio::AnnotationsContext *> Bio::BioContext::annotations() {
  return getRuleContexts<Bio::AnnotationsContext>();
}

Bio::AnnotationsContext* Bio::BioContext::annotations(size_t i) {
  return getRuleContext<Bio::AnnotationsContext>(i);
}

std::vector<tree::TerminalNode *> Bio::BioContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::BioContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}


size_t Bio::BioContext::getRuleIndex() const {
  return Bio::RuleBio;
}


Bio::BioContext* Bio::bio() {
  BioContext *_localctx = _tracker.createInstance<BioContext>(_ctx, getState());
  enterRule(_localctx, 0, Bio::RuleBio);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(135); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(135);
      _errHandler->sync(this);
      switch (_input->LA(1)) {
        case Bio::Grid: {
          setState(116);
          grid();
          break;
        }

        case Bio::Nets: {
          setState(117);
          nets();
          break;
        }

        case Bio::Mixers: {
          setState(118);
          mixers();
          break;
        }

        case Bio::Sinks: {
          setState(119);
          sinks();
          break;
        }

        case Bio::Detectors: {
          setState(120);
          detectors();
          break;
        }

        case Bio::Heaters: {
          setState(121);
          heaters();
          break;
        }

        case Bio::Magnets: {
          setState(122);
          magnets();
          break;
        }

        case Bio::Dispensers: {
          setState(123);
          dispensers();
          break;
        }

        case Bio::Routes: {
          setState(124);
          routes();
          break;
        }

        case Bio::PinActuations: {
          setState(125);
          pinActuations();
          break;
        }

        case Bio::CellActuations: {
          setState(126);
          cellActuations();
          break;
        }

        case Bio::Blockages: {
          setState(127);
          blockages();
          break;
        }

        case Bio::PinAssignments: {
          setState(128);
          pinAssignments();
          break;
        }

        case Bio::Fluids: {
          setState(129);
          fluids();
          break;
        }

        case Bio::Droplets: {
          setState(130);
          droplets();
          break;
        }

        case Bio::MedaRoutes: {
          setState(131);
          medaRoutes();
          break;
        }

        case Bio::MedaNets: {
          setState(132);
          medaNets();
          break;
        }

        case Bio::Annotations: {
          setState(133);
          annotations();
          break;
        }

        case Bio::Newlines: {
          setState(134);
          match(Bio::Newlines);
          break;
        }

      default:
        throw NoViableAltException(this);
      }
      setState(137); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << Bio::Sinks)
      | (1ULL << Bio::Droplets)
      | (1ULL << Bio::PinAssignments)
      | (1ULL << Bio::Fluids)
      | (1ULL << Bio::Blockages)
      | (1ULL << Bio::Nets)
      | (1ULL << Bio::Routes)
      | (1ULL << Bio::Grid)
      | (1ULL << Bio::Dispensers)
      | (1ULL << Bio::Detectors)
      | (1ULL << Bio::Mixers)
      | (1ULL << Bio::Heaters)
      | (1ULL << Bio::Magnets)
      | (1ULL << Bio::Annotations)
      | (1ULL << Bio::MedaRoutes)
      | (1ULL << Bio::MedaNets)
      | (1ULL << Bio::CellActuations)
      | (1ULL << Bio::PinActuations)
      | (1ULL << Bio::Newlines))) != 0));
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SinksContext ------------------------------------------------------------------

Bio::SinksContext::SinksContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::SinksContext::Sinks() {
  return getToken(Bio::Sinks, 0);
}

std::vector<tree::TerminalNode *> Bio::SinksContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::SinksContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::SinksContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::SinkContext *> Bio::SinksContext::sink() {
  return getRuleContexts<Bio::SinkContext>();
}

Bio::SinkContext* Bio::SinksContext::sink(size_t i) {
  return getRuleContext<Bio::SinkContext>(i);
}


size_t Bio::SinksContext::getRuleIndex() const {
  return Bio::RuleSinks;
}


Bio::SinksContext* Bio::sinks() {
  SinksContext *_localctx = _tracker.createInstance<SinksContext>(_ctx, getState());
  enterRule(_localctx, 2, Bio::RuleSinks);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(139);
    match(Bio::Sinks);
    setState(140);
    match(Bio::Newlines);
    setState(144); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(141);
      sink();
      setState(142);
      match(Bio::Newlines);
      setState(146); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::LParen);
    setState(148);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SinkContext ------------------------------------------------------------------

Bio::SinkContext::SinkContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::IoportContext* Bio::SinkContext::ioport() {
  return getRuleContext<Bio::IoportContext>(0);
}


size_t Bio::SinkContext::getRuleIndex() const {
  return Bio::RuleSink;
}


Bio::SinkContext* Bio::sink() {
  SinkContext *_localctx = _tracker.createInstance<SinkContext>(_ctx, getState());
  enterRule(_localctx, 4, Bio::RuleSink);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(150);
    ioport();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DispensersContext ------------------------------------------------------------------

Bio::DispensersContext::DispensersContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::DispensersContext::Dispensers() {
  return getToken(Bio::Dispensers, 0);
}

std::vector<tree::TerminalNode *> Bio::DispensersContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::DispensersContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::DispensersContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::DispenserContext *> Bio::DispensersContext::dispenser() {
  return getRuleContexts<Bio::DispenserContext>();
}

Bio::DispenserContext* Bio::DispensersContext::dispenser(size_t i) {
  return getRuleContext<Bio::DispenserContext>(i);
}


size_t Bio::DispensersContext::getRuleIndex() const {
  return Bio::RuleDispensers;
}


Bio::DispensersContext* Bio::dispensers() {
  DispensersContext *_localctx = _tracker.createInstance<DispensersContext>(_ctx, getState());
  enterRule(_localctx, 6, Bio::RuleDispensers);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(152);
    match(Bio::Dispensers);
    setState(153);
    match(Bio::Newlines);
    setState(157); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(154);
      dispenser();
      setState(155);
      match(Bio::Newlines);
      setState(159); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::Integer

    || _la == Bio::LParen);
    setState(161);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DispenserContext ------------------------------------------------------------------

Bio::DispenserContext::DispenserContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::IoportContext* Bio::DispenserContext::ioport() {
  return getRuleContext<Bio::IoportContext>(0);
}

Bio::FluidIDContext* Bio::DispenserContext::fluidID() {
  return getRuleContext<Bio::FluidIDContext>(0);
}


size_t Bio::DispenserContext::getRuleIndex() const {
  return Bio::RuleDispenser;
}


Bio::DispenserContext* Bio::dispenser() {
  DispenserContext *_localctx = _tracker.createInstance<DispenserContext>(_ctx, getState());
  enterRule(_localctx, 8, Bio::RuleDispenser);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(164);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::Integer) {
      setState(163);
      fluidID();
    }
    setState(166);
    ioport();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DetectorsContext ------------------------------------------------------------------

Bio::DetectorsContext::DetectorsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::DetectorsContext::Detectors() {
  return getToken(Bio::Detectors, 0);
}

std::vector<tree::TerminalNode *> Bio::DetectorsContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::DetectorsContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::DetectorsContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::DetectorContext *> Bio::DetectorsContext::detector() {
  return getRuleContexts<Bio::DetectorContext>();
}

Bio::DetectorContext* Bio::DetectorsContext::detector(size_t i) {
  return getRuleContext<Bio::DetectorContext>(i);
}


size_t Bio::DetectorsContext::getRuleIndex() const {
  return Bio::RuleDetectors;
}


Bio::DetectorsContext* Bio::detectors() {
  DetectorsContext *_localctx = _tracker.createInstance<DetectorsContext>(_ctx, getState());
  enterRule(_localctx, 10, Bio::RuleDetectors);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(168);
    match(Bio::Detectors);
    setState(169);
    match(Bio::Newlines);
    setState(173); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(170);
      detector();
      setState(171);
      match(Bio::Newlines);
      setState(175); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::LParen);
    setState(177);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DetectorContext ------------------------------------------------------------------

Bio::DetectorContext::DetectorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Bio::PositionContext *> Bio::DetectorContext::position() {
  return getRuleContexts<Bio::PositionContext>();
}

Bio::PositionContext* Bio::DetectorContext::position(size_t i) {
  return getRuleContext<Bio::PositionContext>(i);
}

Bio::Detector_specContext* Bio::DetectorContext::detector_spec() {
  return getRuleContext<Bio::Detector_specContext>(0);
}


size_t Bio::DetectorContext::getRuleIndex() const {
  return Bio::RuleDetector;
}


Bio::DetectorContext* Bio::detector() {
  DetectorContext *_localctx = _tracker.createInstance<DetectorContext>(_ctx, getState());
  enterRule(_localctx, 12, Bio::RuleDetector);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(179);
    position();
    setState(181);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LParen) {
      setState(180);
      position();
    }
    setState(184);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LBracket) {
      setState(183);
      detector_spec();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Detector_specContext ------------------------------------------------------------------

Bio::Detector_specContext::Detector_specContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::TimeConstraintContext* Bio::Detector_specContext::timeConstraint() {
  return getRuleContext<Bio::TimeConstraintContext>(0);
}

Bio::FluidIDContext* Bio::Detector_specContext::fluidID() {
  return getRuleContext<Bio::FluidIDContext>(0);
}


size_t Bio::Detector_specContext::getRuleIndex() const {
  return Bio::RuleDetector_spec;
}


Bio::Detector_specContext* Bio::detector_spec() {
  Detector_specContext *_localctx = _tracker.createInstance<Detector_specContext>(_ctx, getState());
  enterRule(_localctx, 14, Bio::RuleDetector_spec);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(186);
    timeConstraint();
    setState(188);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::Integer) {
      setState(187);
      fluidID();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- HeatersContext ------------------------------------------------------------------

Bio::HeatersContext::HeatersContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::HeatersContext::Heaters() {
  return getToken(Bio::Heaters, 0);
}

std::vector<tree::TerminalNode *> Bio::HeatersContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::HeatersContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::HeatersContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::HeaterContext *> Bio::HeatersContext::heater() {
  return getRuleContexts<Bio::HeaterContext>();
}

Bio::HeaterContext* Bio::HeatersContext::heater(size_t i) {
  return getRuleContext<Bio::HeaterContext>(i);
}


size_t Bio::HeatersContext::getRuleIndex() const {
  return Bio::RuleHeaters;
}


Bio::HeatersContext* Bio::heaters() {
  HeatersContext *_localctx = _tracker.createInstance<HeatersContext>(_ctx, getState());
  enterRule(_localctx, 16, Bio::RuleHeaters);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(190);
    match(Bio::Heaters);
    setState(191);
    match(Bio::Newlines);
    setState(195); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(192);
      heater();
      setState(193);
      match(Bio::Newlines);
      setState(197); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::LParen);
    setState(199);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- HeaterContext ------------------------------------------------------------------

Bio::HeaterContext::HeaterContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Bio::PositionContext *> Bio::HeaterContext::position() {
  return getRuleContexts<Bio::PositionContext>();
}

Bio::PositionContext* Bio::HeaterContext::position(size_t i) {
  return getRuleContext<Bio::PositionContext>(i);
}


size_t Bio::HeaterContext::getRuleIndex() const {
  return Bio::RuleHeater;
}


Bio::HeaterContext* Bio::heater() {
  HeaterContext *_localctx = _tracker.createInstance<HeaterContext>(_ctx, getState());
  enterRule(_localctx, 18, Bio::RuleHeater);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(201);
    position();
    setState(203);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LParen) {
      setState(202);
      position();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MagnetsContext ------------------------------------------------------------------

Bio::MagnetsContext::MagnetsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::MagnetsContext::Magnets() {
  return getToken(Bio::Magnets, 0);
}

std::vector<tree::TerminalNode *> Bio::MagnetsContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::MagnetsContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::MagnetsContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::MagnetContext *> Bio::MagnetsContext::magnet() {
  return getRuleContexts<Bio::MagnetContext>();
}

Bio::MagnetContext* Bio::MagnetsContext::magnet(size_t i) {
  return getRuleContext<Bio::MagnetContext>(i);
}


size_t Bio::MagnetsContext::getRuleIndex() const {
  return Bio::RuleMagnets;
}


Bio::MagnetsContext* Bio::magnets() {
  MagnetsContext *_localctx = _tracker.createInstance<MagnetsContext>(_ctx, getState());
  enterRule(_localctx, 20, Bio::RuleMagnets);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(205);
    match(Bio::Magnets);
    setState(206);
    match(Bio::Newlines);
    setState(210); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(207);
      magnet();
      setState(208);
      match(Bio::Newlines);
      setState(212); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::LParen);
    setState(214);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MagnetContext ------------------------------------------------------------------

Bio::MagnetContext::MagnetContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Bio::PositionContext *> Bio::MagnetContext::position() {
  return getRuleContexts<Bio::PositionContext>();
}

Bio::PositionContext* Bio::MagnetContext::position(size_t i) {
  return getRuleContext<Bio::PositionContext>(i);
}


size_t Bio::MagnetContext::getRuleIndex() const {
  return Bio::RuleMagnet;
}


Bio::MagnetContext* Bio::magnet() {
  MagnetContext *_localctx = _tracker.createInstance<MagnetContext>(_ctx, getState());
  enterRule(_localctx, 22, Bio::RuleMagnet);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(216);
    position();
    setState(218);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LParen) {
      setState(217);
      position();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IoportContext ------------------------------------------------------------------

Bio::IoportContext::IoportContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::PositionContext* Bio::IoportContext::position() {
  return getRuleContext<Bio::PositionContext>(0);
}

tree::TerminalNode* Bio::IoportContext::Direction() {
  return getToken(Bio::Direction, 0);
}


size_t Bio::IoportContext::getRuleIndex() const {
  return Bio::RuleIoport;
}


Bio::IoportContext* Bio::ioport() {
  IoportContext *_localctx = _tracker.createInstance<IoportContext>(_ctx, getState());
  enterRule(_localctx, 24, Bio::RuleIoport);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(220);
    position();
    setState(221);
    match(Bio::Direction);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- GridContext ------------------------------------------------------------------

Bio::GridContext::GridContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::GridContext::Grid() {
  return getToken(Bio::Grid, 0);
}

std::vector<tree::TerminalNode *> Bio::GridContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::GridContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::GridContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::GridblockContext *> Bio::GridContext::gridblock() {
  return getRuleContexts<Bio::GridblockContext>();
}

Bio::GridblockContext* Bio::GridContext::gridblock(size_t i) {
  return getRuleContext<Bio::GridblockContext>(i);
}


size_t Bio::GridContext::getRuleIndex() const {
  return Bio::RuleGrid;
}


Bio::GridContext* Bio::grid() {
  GridContext *_localctx = _tracker.createInstance<GridContext>(_ctx, getState());
  enterRule(_localctx, 26, Bio::RuleGrid);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(223);
    match(Bio::Grid);
    setState(224);
    match(Bio::Newlines);
    setState(228); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(225);
      gridblock();
      setState(226);
      match(Bio::Newlines);
      setState(230); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::LParen);
    setState(232);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- GridblockContext ------------------------------------------------------------------

Bio::GridblockContext::GridblockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Bio::PositionContext *> Bio::GridblockContext::position() {
  return getRuleContexts<Bio::PositionContext>();
}

Bio::PositionContext* Bio::GridblockContext::position(size_t i) {
  return getRuleContext<Bio::PositionContext>(i);
}


size_t Bio::GridblockContext::getRuleIndex() const {
  return Bio::RuleGridblock;
}


Bio::GridblockContext* Bio::gridblock() {
  GridblockContext *_localctx = _tracker.createInstance<GridblockContext>(_ctx, getState());
  enterRule(_localctx, 28, Bio::RuleGridblock);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(234);
    position();
    setState(235);
    position();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RoutesContext ------------------------------------------------------------------

Bio::RoutesContext::RoutesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::RoutesContext::Routes() {
  return getToken(Bio::Routes, 0);
}

std::vector<tree::TerminalNode *> Bio::RoutesContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::RoutesContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::RoutesContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::RouteContext *> Bio::RoutesContext::route() {
  return getRuleContexts<Bio::RouteContext>();
}

Bio::RouteContext* Bio::RoutesContext::route(size_t i) {
  return getRuleContext<Bio::RouteContext>(i);
}


size_t Bio::RoutesContext::getRuleIndex() const {
  return Bio::RuleRoutes;
}


Bio::RoutesContext* Bio::routes() {
  RoutesContext *_localctx = _tracker.createInstance<RoutesContext>(_ctx, getState());
  enterRule(_localctx, 30, Bio::RuleRoutes);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(237);
    match(Bio::Routes);
    setState(238);
    match(Bio::Newlines);
    setState(242); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(239);
      route();
      setState(240);
      match(Bio::Newlines);
      setState(244); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::Integer);
    setState(246);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RouteContext ------------------------------------------------------------------

Bio::RouteContext::RouteContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::DropletIDContext* Bio::RouteContext::dropletID() {
  return getRuleContext<Bio::DropletIDContext>(0);
}

Bio::TimeConstraintContext* Bio::RouteContext::timeConstraint() {
  return getRuleContext<Bio::TimeConstraintContext>(0);
}

std::vector<Bio::PositionContext *> Bio::RouteContext::position() {
  return getRuleContexts<Bio::PositionContext>();
}

Bio::PositionContext* Bio::RouteContext::position(size_t i) {
  return getRuleContext<Bio::PositionContext>(i);
}


size_t Bio::RouteContext::getRuleIndex() const {
  return Bio::RuleRoute;
}


Bio::RouteContext* Bio::route() {
  RouteContext *_localctx = _tracker.createInstance<RouteContext>(_ctx, getState());
  enterRule(_localctx, 32, Bio::RuleRoute);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(248);
    dropletID();
    setState(250);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LBracket) {
      setState(249);
      timeConstraint();
    }
    setState(253); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(252);
      position();
      setState(255); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::LParen);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MedaRoutesContext ------------------------------------------------------------------

Bio::MedaRoutesContext::MedaRoutesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::MedaRoutesContext::MedaRoutes() {
  return getToken(Bio::MedaRoutes, 0);
}

std::vector<tree::TerminalNode *> Bio::MedaRoutesContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::MedaRoutesContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::MedaRoutesContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::MedaRouteContext *> Bio::MedaRoutesContext::medaRoute() {
  return getRuleContexts<Bio::MedaRouteContext>();
}

Bio::MedaRouteContext* Bio::MedaRoutesContext::medaRoute(size_t i) {
  return getRuleContext<Bio::MedaRouteContext>(i);
}


size_t Bio::MedaRoutesContext::getRuleIndex() const {
  return Bio::RuleMedaRoutes;
}


Bio::MedaRoutesContext* Bio::medaRoutes() {
  MedaRoutesContext *_localctx = _tracker.createInstance<MedaRoutesContext>(_ctx, getState());
  enterRule(_localctx, 34, Bio::RuleMedaRoutes);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(257);
    match(Bio::MedaRoutes);
    setState(258);
    match(Bio::Newlines);
    setState(262); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(259);
      medaRoute();
      setState(260);
      match(Bio::Newlines);
      setState(264); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::Integer);
    setState(266);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MedaRouteContext ------------------------------------------------------------------

Bio::MedaRouteContext::MedaRouteContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::DropletIDContext* Bio::MedaRouteContext::dropletID() {
  return getRuleContext<Bio::DropletIDContext>(0);
}

Bio::TimeConstraintContext* Bio::MedaRouteContext::timeConstraint() {
  return getRuleContext<Bio::TimeConstraintContext>(0);
}

std::vector<Bio::LocationContext *> Bio::MedaRouteContext::location() {
  return getRuleContexts<Bio::LocationContext>();
}

Bio::LocationContext* Bio::MedaRouteContext::location(size_t i) {
  return getRuleContext<Bio::LocationContext>(i);
}


size_t Bio::MedaRouteContext::getRuleIndex() const {
  return Bio::RuleMedaRoute;
}


Bio::MedaRouteContext* Bio::medaRoute() {
  MedaRouteContext *_localctx = _tracker.createInstance<MedaRouteContext>(_ctx, getState());
  enterRule(_localctx, 36, Bio::RuleMedaRoute);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(268);
    dropletID();
    setState(270);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LBracket) {
      setState(269);
      timeConstraint();
    }
    setState(273); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(272);
      location();
      setState(275); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::LParen);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MixersContext ------------------------------------------------------------------

Bio::MixersContext::MixersContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::MixersContext::Mixers() {
  return getToken(Bio::Mixers, 0);
}

std::vector<tree::TerminalNode *> Bio::MixersContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::MixersContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::MixersContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::MixerContext *> Bio::MixersContext::mixer() {
  return getRuleContexts<Bio::MixerContext>();
}

Bio::MixerContext* Bio::MixersContext::mixer(size_t i) {
  return getRuleContext<Bio::MixerContext>(i);
}


size_t Bio::MixersContext::getRuleIndex() const {
  return Bio::RuleMixers;
}


Bio::MixersContext* Bio::mixers() {
  MixersContext *_localctx = _tracker.createInstance<MixersContext>(_ctx, getState());
  enterRule(_localctx, 38, Bio::RuleMixers);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(277);
    match(Bio::Mixers);
    setState(278);
    match(Bio::Newlines);
    setState(282); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(279);
      mixer();
      setState(280);
      match(Bio::Newlines);
      setState(284); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::Integer);
    setState(286);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MixerContext ------------------------------------------------------------------

Bio::MixerContext::MixerContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::MixerIDContext* Bio::MixerContext::mixerID() {
  return getRuleContext<Bio::MixerIDContext>(0);
}

Bio::TimeRangeContext* Bio::MixerContext::timeRange() {
  return getRuleContext<Bio::TimeRangeContext>(0);
}

std::vector<Bio::PositionContext *> Bio::MixerContext::position() {
  return getRuleContexts<Bio::PositionContext>();
}

Bio::PositionContext* Bio::MixerContext::position(size_t i) {
  return getRuleContext<Bio::PositionContext>(i);
}


size_t Bio::MixerContext::getRuleIndex() const {
  return Bio::RuleMixer;
}


Bio::MixerContext* Bio::mixer() {
  MixerContext *_localctx = _tracker.createInstance<MixerContext>(_ctx, getState());
  enterRule(_localctx, 40, Bio::RuleMixer);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(288);
    mixerID();
    setState(289);
    timeRange();
    setState(290);
    position();
    setState(291);
    position();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AnnotationsContext ------------------------------------------------------------------

Bio::AnnotationsContext::AnnotationsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::AnnotationsContext::Annotations() {
  return getToken(Bio::Annotations, 0);
}

std::vector<tree::TerminalNode *> Bio::AnnotationsContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::AnnotationsContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::AnnotationsContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::AreaAnnotationContext *> Bio::AnnotationsContext::areaAnnotation() {
  return getRuleContexts<Bio::AreaAnnotationContext>();
}

Bio::AreaAnnotationContext* Bio::AnnotationsContext::areaAnnotation(size_t i) {
  return getRuleContext<Bio::AreaAnnotationContext>(i);
}


size_t Bio::AnnotationsContext::getRuleIndex() const {
  return Bio::RuleAnnotations;
}


Bio::AnnotationsContext* Bio::annotations() {
  AnnotationsContext *_localctx = _tracker.createInstance<AnnotationsContext>(_ctx, getState());
  enterRule(_localctx, 42, Bio::RuleAnnotations);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(293);
    match(Bio::Annotations);
    setState(294);
    match(Bio::Newlines);
    setState(298); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(295);
      areaAnnotation();
      setState(296);
      match(Bio::Newlines);
      setState(300); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::LParen);
    setState(302);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AreaAnnotationContext ------------------------------------------------------------------

Bio::AreaAnnotationContext::AreaAnnotationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Bio::PositionContext *> Bio::AreaAnnotationContext::position() {
  return getRuleContexts<Bio::PositionContext>();
}

Bio::PositionContext* Bio::AreaAnnotationContext::position(size_t i) {
  return getRuleContext<Bio::PositionContext>(i);
}

tree::TerminalNode* Bio::AreaAnnotationContext::LessThan() {
  return getToken(Bio::LessThan, 0);
}

tree::TerminalNode* Bio::AreaAnnotationContext::AreaAnnotationText() {
  return getToken(Bio::AreaAnnotationText, 0);
}


size_t Bio::AreaAnnotationContext::getRuleIndex() const {
  return Bio::RuleAreaAnnotation;
}


Bio::AreaAnnotationContext* Bio::areaAnnotation() {
  AreaAnnotationContext *_localctx = _tracker.createInstance<AreaAnnotationContext>(_ctx, getState());
  enterRule(_localctx, 44, Bio::RuleAreaAnnotation);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(304);
    position();
    setState(306);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LParen) {
      setState(305);
      position();
    }
    setState(308);
    match(Bio::LessThan);
    setState(309);
    match(Bio::AreaAnnotationText);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PinActuationsContext ------------------------------------------------------------------

Bio::PinActuationsContext::PinActuationsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::PinActuationsContext::PinActuations() {
  return getToken(Bio::PinActuations, 0);
}

std::vector<tree::TerminalNode *> Bio::PinActuationsContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::PinActuationsContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::PinActuationsContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::PinActuationContext *> Bio::PinActuationsContext::pinActuation() {
  return getRuleContexts<Bio::PinActuationContext>();
}

Bio::PinActuationContext* Bio::PinActuationsContext::pinActuation(size_t i) {
  return getRuleContext<Bio::PinActuationContext>(i);
}


size_t Bio::PinActuationsContext::getRuleIndex() const {
  return Bio::RulePinActuations;
}


Bio::PinActuationsContext* Bio::pinActuations() {
  PinActuationsContext *_localctx = _tracker.createInstance<PinActuationsContext>(_ctx, getState());
  enterRule(_localctx, 46, Bio::RulePinActuations);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(311);
    match(Bio::PinActuations);
    setState(312);
    match(Bio::Newlines);
    setState(319); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(313);
      pinActuation();
      setState(315); 
      _errHandler->sync(this);
      _la = _input->LA(1);
      do {
        setState(314);
        match(Bio::Newlines);
        setState(317); 
        _errHandler->sync(this);
        _la = _input->LA(1);
      } while (_la == Bio::Newlines);
      setState(321); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::Integer);
    setState(323);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PinActuationContext ------------------------------------------------------------------

Bio::PinActuationContext::PinActuationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::PinIDContext* Bio::PinActuationContext::pinID() {
  return getRuleContext<Bio::PinIDContext>(0);
}

tree::TerminalNode* Bio::PinActuationContext::Colon() {
  return getToken(Bio::Colon, 0);
}

tree::TerminalNode* Bio::PinActuationContext::ActuationVector() {
  return getToken(Bio::ActuationVector, 0);
}


size_t Bio::PinActuationContext::getRuleIndex() const {
  return Bio::RulePinActuation;
}


Bio::PinActuationContext* Bio::pinActuation() {
  PinActuationContext *_localctx = _tracker.createInstance<PinActuationContext>(_ctx, getState());
  enterRule(_localctx, 48, Bio::RulePinActuation);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(325);
    pinID();
    setState(326);
    match(Bio::Colon);
    setState(327);
    match(Bio::ActuationVector);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CellActuationsContext ------------------------------------------------------------------

Bio::CellActuationsContext::CellActuationsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::CellActuationsContext::CellActuations() {
  return getToken(Bio::CellActuations, 0);
}

std::vector<tree::TerminalNode *> Bio::CellActuationsContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::CellActuationsContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::CellActuationsContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::CellActuationContext *> Bio::CellActuationsContext::cellActuation() {
  return getRuleContexts<Bio::CellActuationContext>();
}

Bio::CellActuationContext* Bio::CellActuationsContext::cellActuation(size_t i) {
  return getRuleContext<Bio::CellActuationContext>(i);
}


size_t Bio::CellActuationsContext::getRuleIndex() const {
  return Bio::RuleCellActuations;
}


Bio::CellActuationsContext* Bio::cellActuations() {
  CellActuationsContext *_localctx = _tracker.createInstance<CellActuationsContext>(_ctx, getState());
  enterRule(_localctx, 50, Bio::RuleCellActuations);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(329);
    match(Bio::CellActuations);
    setState(330);
    match(Bio::Newlines);
    setState(334); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(331);
      cellActuation();
      setState(332);
      match(Bio::Newlines);
      setState(336); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::LParen);
    setState(338);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CellActuationContext ------------------------------------------------------------------

Bio::CellActuationContext::CellActuationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::PositionContext* Bio::CellActuationContext::position() {
  return getRuleContext<Bio::PositionContext>(0);
}

tree::TerminalNode* Bio::CellActuationContext::Colon() {
  return getToken(Bio::Colon, 0);
}

tree::TerminalNode* Bio::CellActuationContext::ActuationVector() {
  return getToken(Bio::ActuationVector, 0);
}


size_t Bio::CellActuationContext::getRuleIndex() const {
  return Bio::RuleCellActuation;
}


Bio::CellActuationContext* Bio::cellActuation() {
  CellActuationContext *_localctx = _tracker.createInstance<CellActuationContext>(_ctx, getState());
  enterRule(_localctx, 52, Bio::RuleCellActuation);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(340);
    position();
    setState(341);
    match(Bio::Colon);
    setState(342);
    match(Bio::ActuationVector);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NetsContext ------------------------------------------------------------------

Bio::NetsContext::NetsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::NetsContext::Nets() {
  return getToken(Bio::Nets, 0);
}

std::vector<tree::TerminalNode *> Bio::NetsContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::NetsContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::NetsContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::NetContext *> Bio::NetsContext::net() {
  return getRuleContexts<Bio::NetContext>();
}

Bio::NetContext* Bio::NetsContext::net(size_t i) {
  return getRuleContext<Bio::NetContext>(i);
}


size_t Bio::NetsContext::getRuleIndex() const {
  return Bio::RuleNets;
}


Bio::NetsContext* Bio::nets() {
  NetsContext *_localctx = _tracker.createInstance<NetsContext>(_ctx, getState());
  enterRule(_localctx, 54, Bio::RuleNets);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(344);
    match(Bio::Nets);
    setState(345);
    match(Bio::Newlines);
    setState(349); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(346);
      net();
      setState(347);
      match(Bio::Newlines);
      setState(351); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::Integer);
    setState(353);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NetContext ------------------------------------------------------------------

Bio::NetContext::NetContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Bio::SourceContext *> Bio::NetContext::source() {
  return getRuleContexts<Bio::SourceContext>();
}

Bio::SourceContext* Bio::NetContext::source(size_t i) {
  return getRuleContext<Bio::SourceContext>(i);
}

tree::TerminalNode* Bio::NetContext::Arrow() {
  return getToken(Bio::Arrow, 0);
}

Bio::TargetContext* Bio::NetContext::target() {
  return getRuleContext<Bio::TargetContext>(0);
}

std::vector<tree::TerminalNode *> Bio::NetContext::Comma() {
  return getTokens(Bio::Comma);
}

tree::TerminalNode* Bio::NetContext::Comma(size_t i) {
  return getToken(Bio::Comma, i);
}


size_t Bio::NetContext::getRuleIndex() const {
  return Bio::RuleNet;
}


Bio::NetContext* Bio::net() {
  NetContext *_localctx = _tracker.createInstance<NetContext>(_ctx, getState());
  enterRule(_localctx, 56, Bio::RuleNet);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(355);
    source();
    setState(360);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == Bio::Comma) {
      setState(356);
      match(Bio::Comma);
      setState(357);
      source();
      setState(362);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(363);
    match(Bio::Arrow);
    setState(364);
    target();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SourceContext ------------------------------------------------------------------

Bio::SourceContext::SourceContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::DropletIDContext* Bio::SourceContext::dropletID() {
  return getRuleContext<Bio::DropletIDContext>(0);
}

Bio::PositionContext* Bio::SourceContext::position() {
  return getRuleContext<Bio::PositionContext>(0);
}

Bio::TimeConstraintContext* Bio::SourceContext::timeConstraint() {
  return getRuleContext<Bio::TimeConstraintContext>(0);
}


size_t Bio::SourceContext::getRuleIndex() const {
  return Bio::RuleSource;
}


Bio::SourceContext* Bio::source() {
  SourceContext *_localctx = _tracker.createInstance<SourceContext>(_ctx, getState());
  enterRule(_localctx, 58, Bio::RuleSource);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(366);
    dropletID();
    setState(367);
    position();
    setState(369);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LBracket) {
      setState(368);
      timeConstraint();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TargetContext ------------------------------------------------------------------

Bio::TargetContext::TargetContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::PositionContext* Bio::TargetContext::position() {
  return getRuleContext<Bio::PositionContext>(0);
}

Bio::TimeConstraintContext* Bio::TargetContext::timeConstraint() {
  return getRuleContext<Bio::TimeConstraintContext>(0);
}


size_t Bio::TargetContext::getRuleIndex() const {
  return Bio::RuleTarget;
}


Bio::TargetContext* Bio::target() {
  TargetContext *_localctx = _tracker.createInstance<TargetContext>(_ctx, getState());
  enterRule(_localctx, 60, Bio::RuleTarget);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(371);
    position();
    setState(373);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LBracket) {
      setState(372);
      timeConstraint();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MedaNetsContext ------------------------------------------------------------------

Bio::MedaNetsContext::MedaNetsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::MedaNetsContext::MedaNets() {
  return getToken(Bio::MedaNets, 0);
}

std::vector<tree::TerminalNode *> Bio::MedaNetsContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::MedaNetsContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::MedaNetsContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::MedaNetContext *> Bio::MedaNetsContext::medaNet() {
  return getRuleContexts<Bio::MedaNetContext>();
}

Bio::MedaNetContext* Bio::MedaNetsContext::medaNet(size_t i) {
  return getRuleContext<Bio::MedaNetContext>(i);
}


size_t Bio::MedaNetsContext::getRuleIndex() const {
  return Bio::RuleMedaNets;
}


Bio::MedaNetsContext* Bio::medaNets() {
  MedaNetsContext *_localctx = _tracker.createInstance<MedaNetsContext>(_ctx, getState());
  enterRule(_localctx, 62, Bio::RuleMedaNets);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(375);
    match(Bio::MedaNets);
    setState(376);
    match(Bio::Newlines);
    setState(380); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(377);
      medaNet();
      setState(378);
      match(Bio::Newlines);
      setState(382); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::Integer);
    setState(384);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MedaNetContext ------------------------------------------------------------------

Bio::MedaNetContext::MedaNetContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Bio::MedaSourceContext *> Bio::MedaNetContext::medaSource() {
  return getRuleContexts<Bio::MedaSourceContext>();
}

Bio::MedaSourceContext* Bio::MedaNetContext::medaSource(size_t i) {
  return getRuleContext<Bio::MedaSourceContext>(i);
}

tree::TerminalNode* Bio::MedaNetContext::Arrow() {
  return getToken(Bio::Arrow, 0);
}

Bio::MedaTargetContext* Bio::MedaNetContext::medaTarget() {
  return getRuleContext<Bio::MedaTargetContext>(0);
}

std::vector<tree::TerminalNode *> Bio::MedaNetContext::Comma() {
  return getTokens(Bio::Comma);
}

tree::TerminalNode* Bio::MedaNetContext::Comma(size_t i) {
  return getToken(Bio::Comma, i);
}


size_t Bio::MedaNetContext::getRuleIndex() const {
  return Bio::RuleMedaNet;
}


Bio::MedaNetContext* Bio::medaNet() {
  MedaNetContext *_localctx = _tracker.createInstance<MedaNetContext>(_ctx, getState());
  enterRule(_localctx, 64, Bio::RuleMedaNet);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(386);
    medaSource();
    setState(391);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == Bio::Comma) {
      setState(387);
      match(Bio::Comma);
      setState(388);
      medaSource();
      setState(393);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(394);
    match(Bio::Arrow);
    setState(395);
    medaTarget();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MedaSourceContext ------------------------------------------------------------------

Bio::MedaSourceContext::MedaSourceContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::DropletIDContext* Bio::MedaSourceContext::dropletID() {
  return getRuleContext<Bio::DropletIDContext>(0);
}

Bio::LocationContext* Bio::MedaSourceContext::location() {
  return getRuleContext<Bio::LocationContext>(0);
}

Bio::TimeConstraintContext* Bio::MedaSourceContext::timeConstraint() {
  return getRuleContext<Bio::TimeConstraintContext>(0);
}


size_t Bio::MedaSourceContext::getRuleIndex() const {
  return Bio::RuleMedaSource;
}


Bio::MedaSourceContext* Bio::medaSource() {
  MedaSourceContext *_localctx = _tracker.createInstance<MedaSourceContext>(_ctx, getState());
  enterRule(_localctx, 66, Bio::RuleMedaSource);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(397);
    dropletID();
    setState(398);
    location();
    setState(400);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LBracket) {
      setState(399);
      timeConstraint();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MedaTargetContext ------------------------------------------------------------------

Bio::MedaTargetContext::MedaTargetContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::LocationContext* Bio::MedaTargetContext::location() {
  return getRuleContext<Bio::LocationContext>(0);
}

Bio::TimeConstraintContext* Bio::MedaTargetContext::timeConstraint() {
  return getRuleContext<Bio::TimeConstraintContext>(0);
}


size_t Bio::MedaTargetContext::getRuleIndex() const {
  return Bio::RuleMedaTarget;
}


Bio::MedaTargetContext* Bio::medaTarget() {
  MedaTargetContext *_localctx = _tracker.createInstance<MedaTargetContext>(_ctx, getState());
  enterRule(_localctx, 68, Bio::RuleMedaTarget);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(402);
    location();
    setState(404);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LBracket) {
      setState(403);
      timeConstraint();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlockagesContext ------------------------------------------------------------------

Bio::BlockagesContext::BlockagesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::BlockagesContext::Blockages() {
  return getToken(Bio::Blockages, 0);
}

std::vector<tree::TerminalNode *> Bio::BlockagesContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::BlockagesContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::BlockagesContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::BlockageContext *> Bio::BlockagesContext::blockage() {
  return getRuleContexts<Bio::BlockageContext>();
}

Bio::BlockageContext* Bio::BlockagesContext::blockage(size_t i) {
  return getRuleContext<Bio::BlockageContext>(i);
}


size_t Bio::BlockagesContext::getRuleIndex() const {
  return Bio::RuleBlockages;
}


Bio::BlockagesContext* Bio::blockages() {
  BlockagesContext *_localctx = _tracker.createInstance<BlockagesContext>(_ctx, getState());
  enterRule(_localctx, 70, Bio::RuleBlockages);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(406);
    match(Bio::Blockages);
    setState(407);
    match(Bio::Newlines);
    setState(411); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(408);
      blockage();
      setState(409);
      match(Bio::Newlines);
      setState(413); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::LParen);
    setState(415);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlockageContext ------------------------------------------------------------------

Bio::BlockageContext::BlockageContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Bio::PositionContext *> Bio::BlockageContext::position() {
  return getRuleContexts<Bio::PositionContext>();
}

Bio::PositionContext* Bio::BlockageContext::position(size_t i) {
  return getRuleContext<Bio::PositionContext>(i);
}

Bio::TimingContext* Bio::BlockageContext::timing() {
  return getRuleContext<Bio::TimingContext>(0);
}


size_t Bio::BlockageContext::getRuleIndex() const {
  return Bio::RuleBlockage;
}


Bio::BlockageContext* Bio::blockage() {
  BlockageContext *_localctx = _tracker.createInstance<BlockageContext>(_ctx, getState());
  enterRule(_localctx, 72, Bio::RuleBlockage);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(417);
    position();
    setState(418);
    position();
    setState(420);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Bio::LParen) {
      setState(419);
      timing();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TimingContext ------------------------------------------------------------------

Bio::TimingContext::TimingContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::TimingContext::LParen() {
  return getToken(Bio::LParen, 0);
}

Bio::BeginTimingContext* Bio::TimingContext::beginTiming() {
  return getRuleContext<Bio::BeginTimingContext>(0);
}

tree::TerminalNode* Bio::TimingContext::Comma() {
  return getToken(Bio::Comma, 0);
}

Bio::EndTimingContext* Bio::TimingContext::endTiming() {
  return getRuleContext<Bio::EndTimingContext>(0);
}

tree::TerminalNode* Bio::TimingContext::RParen() {
  return getToken(Bio::RParen, 0);
}


size_t Bio::TimingContext::getRuleIndex() const {
  return Bio::RuleTiming;
}


Bio::TimingContext* Bio::timing() {
  TimingContext *_localctx = _tracker.createInstance<TimingContext>(_ctx, getState());
  enterRule(_localctx, 74, Bio::RuleTiming);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(422);
    match(Bio::LParen);
    setState(423);
    beginTiming();
    setState(424);
    match(Bio::Comma);
    setState(425);
    endTiming();
    setState(426);
    match(Bio::RParen);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BeginTimingContext ------------------------------------------------------------------

Bio::BeginTimingContext::BeginTimingContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::BeginTimingContext::Integer() {
  return getToken(Bio::Integer, 0);
}

tree::TerminalNode* Bio::BeginTimingContext::Asterisk() {
  return getToken(Bio::Asterisk, 0);
}


size_t Bio::BeginTimingContext::getRuleIndex() const {
  return Bio::RuleBeginTiming;
}


Bio::BeginTimingContext* Bio::beginTiming() {
  BeginTimingContext *_localctx = _tracker.createInstance<BeginTimingContext>(_ctx, getState());
  enterRule(_localctx, 76, Bio::RuleBeginTiming);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(428);
    _la = _input->LA(1);
    if (!(_la == Bio::Integer

    || _la == Bio::Asterisk)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- EndTimingContext ------------------------------------------------------------------

Bio::EndTimingContext::EndTimingContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::EndTimingContext::Integer() {
  return getToken(Bio::Integer, 0);
}

tree::TerminalNode* Bio::EndTimingContext::Asterisk() {
  return getToken(Bio::Asterisk, 0);
}


size_t Bio::EndTimingContext::getRuleIndex() const {
  return Bio::RuleEndTiming;
}


Bio::EndTimingContext* Bio::endTiming() {
  EndTimingContext *_localctx = _tracker.createInstance<EndTimingContext>(_ctx, getState());
  enterRule(_localctx, 78, Bio::RuleEndTiming);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(430);
    _la = _input->LA(1);
    if (!(_la == Bio::Integer

    || _la == Bio::Asterisk)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FluidsContext ------------------------------------------------------------------

Bio::FluidsContext::FluidsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::FluidsContext::Fluids() {
  return getToken(Bio::Fluids, 0);
}

std::vector<tree::TerminalNode *> Bio::FluidsContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::FluidsContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::FluidsContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::FluiddefContext *> Bio::FluidsContext::fluiddef() {
  return getRuleContexts<Bio::FluiddefContext>();
}

Bio::FluiddefContext* Bio::FluidsContext::fluiddef(size_t i) {
  return getRuleContext<Bio::FluiddefContext>(i);
}


size_t Bio::FluidsContext::getRuleIndex() const {
  return Bio::RuleFluids;
}


Bio::FluidsContext* Bio::fluids() {
  FluidsContext *_localctx = _tracker.createInstance<FluidsContext>(_ctx, getState());
  enterRule(_localctx, 80, Bio::RuleFluids);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(432);
    match(Bio::Fluids);
    setState(433);
    match(Bio::Newlines);
    setState(437); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(434);
      fluiddef();
      setState(435);
      match(Bio::Newlines);
      setState(439); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::Integer);
    setState(441);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FluiddefContext ------------------------------------------------------------------

Bio::FluiddefContext::FluiddefContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::FluidIDContext* Bio::FluiddefContext::fluidID() {
  return getRuleContext<Bio::FluidIDContext>(0);
}

tree::TerminalNode* Bio::FluiddefContext::Identifier() {
  return getToken(Bio::Identifier, 0);
}


size_t Bio::FluiddefContext::getRuleIndex() const {
  return Bio::RuleFluiddef;
}


Bio::FluiddefContext* Bio::fluiddef() {
  FluiddefContext *_localctx = _tracker.createInstance<FluiddefContext>(_ctx, getState());
  enterRule(_localctx, 82, Bio::RuleFluiddef);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(443);
    fluidID();
    setState(444);
    match(Bio::Identifier);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PinAssignmentsContext ------------------------------------------------------------------

Bio::PinAssignmentsContext::PinAssignmentsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::PinAssignmentsContext::PinAssignments() {
  return getToken(Bio::PinAssignments, 0);
}

std::vector<tree::TerminalNode *> Bio::PinAssignmentsContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::PinAssignmentsContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::PinAssignmentsContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::AssignmentContext *> Bio::PinAssignmentsContext::assignment() {
  return getRuleContexts<Bio::AssignmentContext>();
}

Bio::AssignmentContext* Bio::PinAssignmentsContext::assignment(size_t i) {
  return getRuleContext<Bio::AssignmentContext>(i);
}


size_t Bio::PinAssignmentsContext::getRuleIndex() const {
  return Bio::RulePinAssignments;
}


Bio::PinAssignmentsContext* Bio::pinAssignments() {
  PinAssignmentsContext *_localctx = _tracker.createInstance<PinAssignmentsContext>(_ctx, getState());
  enterRule(_localctx, 84, Bio::RulePinAssignments);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(446);
    match(Bio::PinAssignments);
    setState(447);
    match(Bio::Newlines);
    setState(451); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(448);
      assignment();
      setState(449);
      match(Bio::Newlines);
      setState(453); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::Integer

    || _la == Bio::LParen);
    setState(455);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AssignmentContext ------------------------------------------------------------------

Bio::AssignmentContext::AssignmentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::PositionToPinAssignmentContext* Bio::AssignmentContext::positionToPinAssignment() {
  return getRuleContext<Bio::PositionToPinAssignmentContext>(0);
}

Bio::PinToPositionsAssignmentContext* Bio::AssignmentContext::pinToPositionsAssignment() {
  return getRuleContext<Bio::PinToPositionsAssignmentContext>(0);
}


size_t Bio::AssignmentContext::getRuleIndex() const {
  return Bio::RuleAssignment;
}


Bio::AssignmentContext* Bio::assignment() {
  AssignmentContext *_localctx = _tracker.createInstance<AssignmentContext>(_ctx, getState());
  enterRule(_localctx, 86, Bio::RuleAssignment);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(459);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case Bio::LParen: {
        enterOuterAlt(_localctx, 1);
        setState(457);
        positionToPinAssignment();
        break;
      }

      case Bio::Integer: {
        enterOuterAlt(_localctx, 2);
        setState(458);
        pinToPositionsAssignment();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PositionToPinAssignmentContext ------------------------------------------------------------------

Bio::PositionToPinAssignmentContext::PositionToPinAssignmentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::PositionContext* Bio::PositionToPinAssignmentContext::position() {
  return getRuleContext<Bio::PositionContext>(0);
}

Bio::PinIDContext* Bio::PositionToPinAssignmentContext::pinID() {
  return getRuleContext<Bio::PinIDContext>(0);
}


size_t Bio::PositionToPinAssignmentContext::getRuleIndex() const {
  return Bio::RulePositionToPinAssignment;
}


Bio::PositionToPinAssignmentContext* Bio::positionToPinAssignment() {
  PositionToPinAssignmentContext *_localctx = _tracker.createInstance<PositionToPinAssignmentContext>(_ctx, getState());
  enterRule(_localctx, 88, Bio::RulePositionToPinAssignment);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(461);
    position();
    setState(462);
    pinID();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PinToPositionsAssignmentContext ------------------------------------------------------------------

Bio::PinToPositionsAssignmentContext::PinToPositionsAssignmentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::PinIDContext* Bio::PinToPositionsAssignmentContext::pinID() {
  return getRuleContext<Bio::PinIDContext>(0);
}

std::vector<Bio::PositionContext *> Bio::PinToPositionsAssignmentContext::position() {
  return getRuleContexts<Bio::PositionContext>();
}

Bio::PositionContext* Bio::PinToPositionsAssignmentContext::position(size_t i) {
  return getRuleContext<Bio::PositionContext>(i);
}

std::vector<tree::TerminalNode *> Bio::PinToPositionsAssignmentContext::Comma() {
  return getTokens(Bio::Comma);
}

tree::TerminalNode* Bio::PinToPositionsAssignmentContext::Comma(size_t i) {
  return getToken(Bio::Comma, i);
}


size_t Bio::PinToPositionsAssignmentContext::getRuleIndex() const {
  return Bio::RulePinToPositionsAssignment;
}


Bio::PinToPositionsAssignmentContext* Bio::pinToPositionsAssignment() {
  PinToPositionsAssignmentContext *_localctx = _tracker.createInstance<PinToPositionsAssignmentContext>(_ctx, getState());
  enterRule(_localctx, 90, Bio::RulePinToPositionsAssignment);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(464);
    pinID();
    setState(465);
    position();
    setState(470);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == Bio::Comma) {
      setState(466);
      match(Bio::Comma);
      setState(467);
      position();
      setState(472);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DropletsContext ------------------------------------------------------------------

Bio::DropletsContext::DropletsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::DropletsContext::Droplets() {
  return getToken(Bio::Droplets, 0);
}

std::vector<tree::TerminalNode *> Bio::DropletsContext::Newlines() {
  return getTokens(Bio::Newlines);
}

tree::TerminalNode* Bio::DropletsContext::Newlines(size_t i) {
  return getToken(Bio::Newlines, i);
}

tree::TerminalNode* Bio::DropletsContext::END() {
  return getToken(Bio::END, 0);
}

std::vector<Bio::DropToFluidContext *> Bio::DropletsContext::dropToFluid() {
  return getRuleContexts<Bio::DropToFluidContext>();
}

Bio::DropToFluidContext* Bio::DropletsContext::dropToFluid(size_t i) {
  return getRuleContext<Bio::DropToFluidContext>(i);
}


size_t Bio::DropletsContext::getRuleIndex() const {
  return Bio::RuleDroplets;
}


Bio::DropletsContext* Bio::droplets() {
  DropletsContext *_localctx = _tracker.createInstance<DropletsContext>(_ctx, getState());
  enterRule(_localctx, 92, Bio::RuleDroplets);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(473);
    match(Bio::Droplets);
    setState(474);
    match(Bio::Newlines);
    setState(478); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(475);
      dropToFluid();
      setState(476);
      match(Bio::Newlines);
      setState(480); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == Bio::Integer);
    setState(482);
    match(Bio::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DropToFluidContext ------------------------------------------------------------------

Bio::DropToFluidContext::DropToFluidContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Bio::DropletIDContext* Bio::DropToFluidContext::dropletID() {
  return getRuleContext<Bio::DropletIDContext>(0);
}

Bio::FluidIDContext* Bio::DropToFluidContext::fluidID() {
  return getRuleContext<Bio::FluidIDContext>(0);
}


size_t Bio::DropToFluidContext::getRuleIndex() const {
  return Bio::RuleDropToFluid;
}


Bio::DropToFluidContext* Bio::dropToFluid() {
  DropToFluidContext *_localctx = _tracker.createInstance<DropToFluidContext>(_ctx, getState());
  enterRule(_localctx, 94, Bio::RuleDropToFluid);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(484);
    dropletID();
    setState(485);
    fluidID();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DropletIDContext ------------------------------------------------------------------

Bio::DropletIDContext::DropletIDContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::DropletIDContext::Integer() {
  return getToken(Bio::Integer, 0);
}


size_t Bio::DropletIDContext::getRuleIndex() const {
  return Bio::RuleDropletID;
}


Bio::DropletIDContext* Bio::dropletID() {
  DropletIDContext *_localctx = _tracker.createInstance<DropletIDContext>(_ctx, getState());
  enterRule(_localctx, 96, Bio::RuleDropletID);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(487);
    match(Bio::Integer);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FluidIDContext ------------------------------------------------------------------

Bio::FluidIDContext::FluidIDContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::FluidIDContext::Integer() {
  return getToken(Bio::Integer, 0);
}


size_t Bio::FluidIDContext::getRuleIndex() const {
  return Bio::RuleFluidID;
}


Bio::FluidIDContext* Bio::fluidID() {
  FluidIDContext *_localctx = _tracker.createInstance<FluidIDContext>(_ctx, getState());
  enterRule(_localctx, 98, Bio::RuleFluidID);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(489);
    match(Bio::Integer);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PinIDContext ------------------------------------------------------------------

Bio::PinIDContext::PinIDContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::PinIDContext::Integer() {
  return getToken(Bio::Integer, 0);
}


size_t Bio::PinIDContext::getRuleIndex() const {
  return Bio::RulePinID;
}


Bio::PinIDContext* Bio::pinID() {
  PinIDContext *_localctx = _tracker.createInstance<PinIDContext>(_ctx, getState());
  enterRule(_localctx, 100, Bio::RulePinID);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(491);
    match(Bio::Integer);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MixerIDContext ------------------------------------------------------------------

Bio::MixerIDContext::MixerIDContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::MixerIDContext::Integer() {
  return getToken(Bio::Integer, 0);
}


size_t Bio::MixerIDContext::getRuleIndex() const {
  return Bio::RuleMixerID;
}


Bio::MixerIDContext* Bio::mixerID() {
  MixerIDContext *_localctx = _tracker.createInstance<MixerIDContext>(_ctx, getState());
  enterRule(_localctx, 102, Bio::RuleMixerID);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(493);
    match(Bio::Integer);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PositionContext ------------------------------------------------------------------

Bio::PositionContext::PositionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::PositionContext::LParen() {
  return getToken(Bio::LParen, 0);
}

Bio::XposContext* Bio::PositionContext::xpos() {
  return getRuleContext<Bio::XposContext>(0);
}

tree::TerminalNode* Bio::PositionContext::Comma() {
  return getToken(Bio::Comma, 0);
}

Bio::YposContext* Bio::PositionContext::ypos() {
  return getRuleContext<Bio::YposContext>(0);
}

tree::TerminalNode* Bio::PositionContext::RParen() {
  return getToken(Bio::RParen, 0);
}


size_t Bio::PositionContext::getRuleIndex() const {
  return Bio::RulePosition;
}


Bio::PositionContext* Bio::position() {
  PositionContext *_localctx = _tracker.createInstance<PositionContext>(_ctx, getState());
  enterRule(_localctx, 104, Bio::RulePosition);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(495);
    match(Bio::LParen);
    setState(496);
    xpos();
    setState(497);
    match(Bio::Comma);
    setState(498);
    ypos();
    setState(499);
    match(Bio::RParen);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LocationContext ------------------------------------------------------------------

Bio::LocationContext::LocationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::LocationContext::LParen() {
  return getToken(Bio::LParen, 0);
}

std::vector<Bio::PositionContext *> Bio::LocationContext::position() {
  return getRuleContexts<Bio::PositionContext>();
}

Bio::PositionContext* Bio::LocationContext::position(size_t i) {
  return getRuleContext<Bio::PositionContext>(i);
}

tree::TerminalNode* Bio::LocationContext::Comma() {
  return getToken(Bio::Comma, 0);
}

tree::TerminalNode* Bio::LocationContext::RParen() {
  return getToken(Bio::RParen, 0);
}


size_t Bio::LocationContext::getRuleIndex() const {
  return Bio::RuleLocation;
}


Bio::LocationContext* Bio::location() {
  LocationContext *_localctx = _tracker.createInstance<LocationContext>(_ctx, getState());
  enterRule(_localctx, 106, Bio::RuleLocation);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(501);
    match(Bio::LParen);
    setState(502);
    position();
    setState(503);
    match(Bio::Comma);
    setState(504);
    position();
    setState(505);
    match(Bio::RParen);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- XposContext ------------------------------------------------------------------

Bio::XposContext::XposContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::XposContext::Integer() {
  return getToken(Bio::Integer, 0);
}


size_t Bio::XposContext::getRuleIndex() const {
  return Bio::RuleXpos;
}


Bio::XposContext* Bio::xpos() {
  XposContext *_localctx = _tracker.createInstance<XposContext>(_ctx, getState());
  enterRule(_localctx, 108, Bio::RuleXpos);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(507);
    match(Bio::Integer);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- YposContext ------------------------------------------------------------------

Bio::YposContext::YposContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::YposContext::Integer() {
  return getToken(Bio::Integer, 0);
}


size_t Bio::YposContext::getRuleIndex() const {
  return Bio::RuleYpos;
}


Bio::YposContext* Bio::ypos() {
  YposContext *_localctx = _tracker.createInstance<YposContext>(_ctx, getState());
  enterRule(_localctx, 110, Bio::RuleYpos);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(509);
    match(Bio::Integer);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TimeConstraintContext ------------------------------------------------------------------

Bio::TimeConstraintContext::TimeConstraintContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::TimeConstraintContext::LBracket() {
  return getToken(Bio::LBracket, 0);
}

tree::TerminalNode* Bio::TimeConstraintContext::Integer() {
  return getToken(Bio::Integer, 0);
}

tree::TerminalNode* Bio::TimeConstraintContext::RBracket() {
  return getToken(Bio::RBracket, 0);
}


size_t Bio::TimeConstraintContext::getRuleIndex() const {
  return Bio::RuleTimeConstraint;
}


Bio::TimeConstraintContext* Bio::timeConstraint() {
  TimeConstraintContext *_localctx = _tracker.createInstance<TimeConstraintContext>(_ctx, getState());
  enterRule(_localctx, 112, Bio::RuleTimeConstraint);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(511);
    match(Bio::LBracket);
    setState(512);
    match(Bio::Integer);
    setState(513);
    match(Bio::RBracket);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TimeRangeContext ------------------------------------------------------------------

Bio::TimeRangeContext::TimeRangeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Bio::TimeRangeContext::LBracket() {
  return getToken(Bio::LBracket, 0);
}

std::vector<tree::TerminalNode *> Bio::TimeRangeContext::Integer() {
  return getTokens(Bio::Integer);
}

tree::TerminalNode* Bio::TimeRangeContext::Integer(size_t i) {
  return getToken(Bio::Integer, i);
}

tree::TerminalNode* Bio::TimeRangeContext::Dash() {
  return getToken(Bio::Dash, 0);
}

tree::TerminalNode* Bio::TimeRangeContext::RBracket() {
  return getToken(Bio::RBracket, 0);
}


size_t Bio::TimeRangeContext::getRuleIndex() const {
  return Bio::RuleTimeRange;
}


Bio::TimeRangeContext* Bio::timeRange() {
  TimeRangeContext *_localctx = _tracker.createInstance<TimeRangeContext>(_ctx, getState());
  enterRule(_localctx, 114, Bio::RuleTimeRange);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(515);
    match(Bio::LBracket);
    setState(516);
    match(Bio::Integer);
    setState(517);
    match(Bio::Dash);
    setState(518);
    match(Bio::Integer);
    setState(519);
    match(Bio::RBracket);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> Bio::_decisionToDFA;
atn::PredictionContextCache Bio::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN Bio::_atn;
std::vector<uint16_t> Bio::_serializedATN;

std::vector<std::string> Bio::_ruleNames = {
  "bio", "sinks", "sink", "dispensers", "dispenser", "detectors", "detector", 
  "detector_spec", "heaters", "heater", "magnets", "magnet", "ioport", "grid", 
  "gridblock", "routes", "route", "medaRoutes", "medaRoute", "mixers", "mixer", 
  "annotations", "areaAnnotation", "pinActuations", "pinActuation", "cellActuations", 
  "cellActuation", "nets", "net", "source", "target", "medaNets", "medaNet", 
  "medaSource", "medaTarget", "blockages", "blockage", "timing", "beginTiming", 
  "endTiming", "fluids", "fluiddef", "pinAssignments", "assignment", "positionToPinAssignment", 
  "pinToPositionsAssignment", "droplets", "dropToFluid", "dropletID", "fluidID", 
  "pinID", "mixerID", "position", "location", "xpos", "ypos", "timeConstraint", 
  "timeRange"
};

std::vector<std::string> Bio::_literalNames = {
  "", "'sinks'", "'droplets'", "'pin assignments'", "'fluids'", "'blockages'", 
  "'nets'", "'routes'", "'grid'", "'dispensers'", "'detectors'", "'mixers'", 
  "'heaters'", "'magnets'", "'annotations'", "'meda routes'", "'meda nets'", 
  "'cell actuations'", "'pin actuations'", "", "'end'", "", "", "", "", 
  "", "", "", "'('", "')'", "'['", "']'", "", "'+'", "'/'", "'='", "','", 
  "'*'", "'->'", "':'", "", "", "'<'"
};

std::vector<std::string> Bio::_symbolicNames = {
  "", "Sinks", "Droplets", "PinAssignments", "Fluids", "Blockages", "Nets", 
  "Routes", "Grid", "Dispensers", "Detectors", "Mixers", "Heaters", "Magnets", 
  "Annotations", "MedaRoutes", "MedaNets", "CellActuations", "PinActuations", 
  "Direction", "END", "Integer", "Identifier", "Annotation", "Comment", 
  "Newlines", "NEWLINE", "WS", "LParen", "RParen", "LBracket", "RBracket", 
  "Dash", "Plus", "Slash", "Equals", "Comma", "Asterisk", "Arrow", "Colon", 
  "Delimiters", "Signs", "LessThan", "ActuationVector", "WhiteSpaceInActuationMode", 
  "AreaAnnotationText"
};

dfa::Vocabulary Bio::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> Bio::_tokenNames;

Bio::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x2f, 0x20c, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x4, 0x15, 
    0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 0x9, 0x17, 0x4, 0x18, 0x9, 
    0x18, 0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b, 
    0x4, 0x1c, 0x9, 0x1c, 0x4, 0x1d, 0x9, 0x1d, 0x4, 0x1e, 0x9, 0x1e, 0x4, 
    0x1f, 0x9, 0x1f, 0x4, 0x20, 0x9, 0x20, 0x4, 0x21, 0x9, 0x21, 0x4, 0x22, 
    0x9, 0x22, 0x4, 0x23, 0x9, 0x23, 0x4, 0x24, 0x9, 0x24, 0x4, 0x25, 0x9, 
    0x25, 0x4, 0x26, 0x9, 0x26, 0x4, 0x27, 0x9, 0x27, 0x4, 0x28, 0x9, 0x28, 
    0x4, 0x29, 0x9, 0x29, 0x4, 0x2a, 0x9, 0x2a, 0x4, 0x2b, 0x9, 0x2b, 0x4, 
    0x2c, 0x9, 0x2c, 0x4, 0x2d, 0x9, 0x2d, 0x4, 0x2e, 0x9, 0x2e, 0x4, 0x2f, 
    0x9, 0x2f, 0x4, 0x30, 0x9, 0x30, 0x4, 0x31, 0x9, 0x31, 0x4, 0x32, 0x9, 
    0x32, 0x4, 0x33, 0x9, 0x33, 0x4, 0x34, 0x9, 0x34, 0x4, 0x35, 0x9, 0x35, 
    0x4, 0x36, 0x9, 0x36, 0x4, 0x37, 0x9, 0x37, 0x4, 0x38, 0x9, 0x38, 0x4, 
    0x39, 0x9, 0x39, 0x4, 0x3a, 0x9, 0x3a, 0x4, 0x3b, 0x9, 0x3b, 0x3, 0x2, 
    0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 
    0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 
    0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x6, 0x2, 0x8a, 0xa, 0x2, 0xd, 
    0x2, 0xe, 0x2, 0x8b, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x6, 0x3, 0x93, 0xa, 0x3, 0xd, 0x3, 0xe, 0x3, 0x94, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 
    0x6, 0x5, 0xa0, 0xa, 0x5, 0xd, 0x5, 0xe, 0x5, 0xa1, 0x3, 0x5, 0x3, 0x5, 
    0x3, 0x6, 0x5, 0x6, 0xa7, 0xa, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x7, 0x3, 
    0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x6, 0x7, 0xb0, 0xa, 0x7, 0xd, 0x7, 
    0xe, 0x7, 0xb1, 0x3, 0x7, 0x3, 0x7, 0x3, 0x8, 0x3, 0x8, 0x5, 0x8, 0xb8, 
    0xa, 0x8, 0x3, 0x8, 0x5, 0x8, 0xbb, 0xa, 0x8, 0x3, 0x9, 0x3, 0x9, 0x5, 
    0x9, 0xbf, 0xa, 0x9, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 
    0x6, 0xa, 0xc6, 0xa, 0xa, 0xd, 0xa, 0xe, 0xa, 0xc7, 0x3, 0xa, 0x3, 0xa, 
    0x3, 0xb, 0x3, 0xb, 0x5, 0xb, 0xce, 0xa, 0xb, 0x3, 0xc, 0x3, 0xc, 0x3, 
    0xc, 0x3, 0xc, 0x3, 0xc, 0x6, 0xc, 0xd5, 0xa, 0xc, 0xd, 0xc, 0xe, 0xc, 
    0xd6, 0x3, 0xc, 0x3, 0xc, 0x3, 0xd, 0x3, 0xd, 0x5, 0xd, 0xdd, 0xa, 0xd, 
    0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 
    0x3, 0xf, 0x6, 0xf, 0xe7, 0xa, 0xf, 0xd, 0xf, 0xe, 0xf, 0xe8, 0x3, 0xf, 
    0x3, 0xf, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 
    0x11, 0x3, 0x11, 0x3, 0x11, 0x6, 0x11, 0xf5, 0xa, 0x11, 0xd, 0x11, 0xe, 
    0x11, 0xf6, 0x3, 0x11, 0x3, 0x11, 0x3, 0x12, 0x3, 0x12, 0x5, 0x12, 0xfd, 
    0xa, 0x12, 0x3, 0x12, 0x6, 0x12, 0x100, 0xa, 0x12, 0xd, 0x12, 0xe, 0x12, 
    0x101, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x6, 0x13, 
    0x109, 0xa, 0x13, 0xd, 0x13, 0xe, 0x13, 0x10a, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x14, 0x3, 0x14, 0x5, 0x14, 0x111, 0xa, 0x14, 0x3, 0x14, 0x6, 0x14, 
    0x114, 0xa, 0x14, 0xd, 0x14, 0xe, 0x14, 0x115, 0x3, 0x15, 0x3, 0x15, 
    0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x6, 0x15, 0x11d, 0xa, 0x15, 0xd, 0x15, 
    0xe, 0x15, 0x11e, 0x3, 0x15, 0x3, 0x15, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 
    0x3, 0x16, 0x3, 0x16, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 
    0x17, 0x6, 0x17, 0x12d, 0xa, 0x17, 0xd, 0x17, 0xe, 0x17, 0x12e, 0x3, 
    0x17, 0x3, 0x17, 0x3, 0x18, 0x3, 0x18, 0x5, 0x18, 0x135, 0xa, 0x18, 
    0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 
    0x19, 0x6, 0x19, 0x13e, 0xa, 0x19, 0xd, 0x19, 0xe, 0x19, 0x13f, 0x6, 
    0x19, 0x142, 0xa, 0x19, 0xd, 0x19, 0xe, 0x19, 0x143, 0x3, 0x19, 0x3, 
    0x19, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1b, 0x3, 0x1b, 
    0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1b, 0x6, 0x1b, 0x151, 0xa, 0x1b, 0xd, 0x1b, 
    0xe, 0x1b, 0x152, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 
    0x3, 0x1c, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1d, 0x6, 
    0x1d, 0x160, 0xa, 0x1d, 0xd, 0x1d, 0xe, 0x1d, 0x161, 0x3, 0x1d, 0x3, 
    0x1d, 0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1e, 0x7, 0x1e, 0x169, 0xa, 0x1e, 
    0xc, 0x1e, 0xe, 0x1e, 0x16c, 0xb, 0x1e, 0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1e, 
    0x3, 0x1f, 0x3, 0x1f, 0x3, 0x1f, 0x5, 0x1f, 0x174, 0xa, 0x1f, 0x3, 0x20, 
    0x3, 0x20, 0x5, 0x20, 0x178, 0xa, 0x20, 0x3, 0x21, 0x3, 0x21, 0x3, 0x21, 
    0x3, 0x21, 0x3, 0x21, 0x6, 0x21, 0x17f, 0xa, 0x21, 0xd, 0x21, 0xe, 0x21, 
    0x180, 0x3, 0x21, 0x3, 0x21, 0x3, 0x22, 0x3, 0x22, 0x3, 0x22, 0x7, 0x22, 
    0x188, 0xa, 0x22, 0xc, 0x22, 0xe, 0x22, 0x18b, 0xb, 0x22, 0x3, 0x22, 
    0x3, 0x22, 0x3, 0x22, 0x3, 0x23, 0x3, 0x23, 0x3, 0x23, 0x5, 0x23, 0x193, 
    0xa, 0x23, 0x3, 0x24, 0x3, 0x24, 0x5, 0x24, 0x197, 0xa, 0x24, 0x3, 0x25, 
    0x3, 0x25, 0x3, 0x25, 0x3, 0x25, 0x3, 0x25, 0x6, 0x25, 0x19e, 0xa, 0x25, 
    0xd, 0x25, 0xe, 0x25, 0x19f, 0x3, 0x25, 0x3, 0x25, 0x3, 0x26, 0x3, 0x26, 
    0x3, 0x26, 0x5, 0x26, 0x1a7, 0xa, 0x26, 0x3, 0x27, 0x3, 0x27, 0x3, 0x27, 
    0x3, 0x27, 0x3, 0x27, 0x3, 0x27, 0x3, 0x28, 0x3, 0x28, 0x3, 0x29, 0x3, 
    0x29, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x6, 0x2a, 
    0x1b8, 0xa, 0x2a, 0xd, 0x2a, 0xe, 0x2a, 0x1b9, 0x3, 0x2a, 0x3, 0x2a, 
    0x3, 0x2b, 0x3, 0x2b, 0x3, 0x2b, 0x3, 0x2c, 0x3, 0x2c, 0x3, 0x2c, 0x3, 
    0x2c, 0x3, 0x2c, 0x6, 0x2c, 0x1c6, 0xa, 0x2c, 0xd, 0x2c, 0xe, 0x2c, 
    0x1c7, 0x3, 0x2c, 0x3, 0x2c, 0x3, 0x2d, 0x3, 0x2d, 0x5, 0x2d, 0x1ce, 
    0xa, 0x2d, 0x3, 0x2e, 0x3, 0x2e, 0x3, 0x2e, 0x3, 0x2f, 0x3, 0x2f, 0x3, 
    0x2f, 0x3, 0x2f, 0x7, 0x2f, 0x1d7, 0xa, 0x2f, 0xc, 0x2f, 0xe, 0x2f, 
    0x1da, 0xb, 0x2f, 0x3, 0x30, 0x3, 0x30, 0x3, 0x30, 0x3, 0x30, 0x3, 0x30, 
    0x6, 0x30, 0x1e1, 0xa, 0x30, 0xd, 0x30, 0xe, 0x30, 0x1e2, 0x3, 0x30, 
    0x3, 0x30, 0x3, 0x31, 0x3, 0x31, 0x3, 0x31, 0x3, 0x32, 0x3, 0x32, 0x3, 
    0x33, 0x3, 0x33, 0x3, 0x34, 0x3, 0x34, 0x3, 0x35, 0x3, 0x35, 0x3, 0x36, 
    0x3, 0x36, 0x3, 0x36, 0x3, 0x36, 0x3, 0x36, 0x3, 0x36, 0x3, 0x37, 0x3, 
    0x37, 0x3, 0x37, 0x3, 0x37, 0x3, 0x37, 0x3, 0x37, 0x3, 0x38, 0x3, 0x38, 
    0x3, 0x39, 0x3, 0x39, 0x3, 0x3a, 0x3, 0x3a, 0x3, 0x3a, 0x3, 0x3a, 0x3, 
    0x3b, 0x3, 0x3b, 0x3, 0x3b, 0x3, 0x3b, 0x3, 0x3b, 0x3, 0x3b, 0x3, 0x3b, 
    0x2, 0x2, 0x3c, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 0x14, 
    0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 
    0x2e, 0x30, 0x32, 0x34, 0x36, 0x38, 0x3a, 0x3c, 0x3e, 0x40, 0x42, 0x44, 
    0x46, 0x48, 0x4a, 0x4c, 0x4e, 0x50, 0x52, 0x54, 0x56, 0x58, 0x5a, 0x5c, 
    0x5e, 0x60, 0x62, 0x64, 0x66, 0x68, 0x6a, 0x6c, 0x6e, 0x70, 0x72, 0x74, 
    0x2, 0x3, 0x4, 0x2, 0x17, 0x17, 0x27, 0x27, 0x2, 0x20b, 0x2, 0x89, 0x3, 
    0x2, 0x2, 0x2, 0x4, 0x8d, 0x3, 0x2, 0x2, 0x2, 0x6, 0x98, 0x3, 0x2, 0x2, 
    0x2, 0x8, 0x9a, 0x3, 0x2, 0x2, 0x2, 0xa, 0xa6, 0x3, 0x2, 0x2, 0x2, 0xc, 
    0xaa, 0x3, 0x2, 0x2, 0x2, 0xe, 0xb5, 0x3, 0x2, 0x2, 0x2, 0x10, 0xbc, 
    0x3, 0x2, 0x2, 0x2, 0x12, 0xc0, 0x3, 0x2, 0x2, 0x2, 0x14, 0xcb, 0x3, 
    0x2, 0x2, 0x2, 0x16, 0xcf, 0x3, 0x2, 0x2, 0x2, 0x18, 0xda, 0x3, 0x2, 
    0x2, 0x2, 0x1a, 0xde, 0x3, 0x2, 0x2, 0x2, 0x1c, 0xe1, 0x3, 0x2, 0x2, 
    0x2, 0x1e, 0xec, 0x3, 0x2, 0x2, 0x2, 0x20, 0xef, 0x3, 0x2, 0x2, 0x2, 
    0x22, 0xfa, 0x3, 0x2, 0x2, 0x2, 0x24, 0x103, 0x3, 0x2, 0x2, 0x2, 0x26, 
    0x10e, 0x3, 0x2, 0x2, 0x2, 0x28, 0x117, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x122, 
    0x3, 0x2, 0x2, 0x2, 0x2c, 0x127, 0x3, 0x2, 0x2, 0x2, 0x2e, 0x132, 0x3, 
    0x2, 0x2, 0x2, 0x30, 0x139, 0x3, 0x2, 0x2, 0x2, 0x32, 0x147, 0x3, 0x2, 
    0x2, 0x2, 0x34, 0x14b, 0x3, 0x2, 0x2, 0x2, 0x36, 0x156, 0x3, 0x2, 0x2, 
    0x2, 0x38, 0x15a, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x165, 0x3, 0x2, 0x2, 0x2, 
    0x3c, 0x170, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x175, 0x3, 0x2, 0x2, 0x2, 0x40, 
    0x179, 0x3, 0x2, 0x2, 0x2, 0x42, 0x184, 0x3, 0x2, 0x2, 0x2, 0x44, 0x18f, 
    0x3, 0x2, 0x2, 0x2, 0x46, 0x194, 0x3, 0x2, 0x2, 0x2, 0x48, 0x198, 0x3, 
    0x2, 0x2, 0x2, 0x4a, 0x1a3, 0x3, 0x2, 0x2, 0x2, 0x4c, 0x1a8, 0x3, 0x2, 
    0x2, 0x2, 0x4e, 0x1ae, 0x3, 0x2, 0x2, 0x2, 0x50, 0x1b0, 0x3, 0x2, 0x2, 
    0x2, 0x52, 0x1b2, 0x3, 0x2, 0x2, 0x2, 0x54, 0x1bd, 0x3, 0x2, 0x2, 0x2, 
    0x56, 0x1c0, 0x3, 0x2, 0x2, 0x2, 0x58, 0x1cd, 0x3, 0x2, 0x2, 0x2, 0x5a, 
    0x1cf, 0x3, 0x2, 0x2, 0x2, 0x5c, 0x1d2, 0x3, 0x2, 0x2, 0x2, 0x5e, 0x1db, 
    0x3, 0x2, 0x2, 0x2, 0x60, 0x1e6, 0x3, 0x2, 0x2, 0x2, 0x62, 0x1e9, 0x3, 
    0x2, 0x2, 0x2, 0x64, 0x1eb, 0x3, 0x2, 0x2, 0x2, 0x66, 0x1ed, 0x3, 0x2, 
    0x2, 0x2, 0x68, 0x1ef, 0x3, 0x2, 0x2, 0x2, 0x6a, 0x1f1, 0x3, 0x2, 0x2, 
    0x2, 0x6c, 0x1f7, 0x3, 0x2, 0x2, 0x2, 0x6e, 0x1fd, 0x3, 0x2, 0x2, 0x2, 
    0x70, 0x1ff, 0x3, 0x2, 0x2, 0x2, 0x72, 0x201, 0x3, 0x2, 0x2, 0x2, 0x74, 
    0x205, 0x3, 0x2, 0x2, 0x2, 0x76, 0x8a, 0x5, 0x1c, 0xf, 0x2, 0x77, 0x8a, 
    0x5, 0x38, 0x1d, 0x2, 0x78, 0x8a, 0x5, 0x28, 0x15, 0x2, 0x79, 0x8a, 
    0x5, 0x4, 0x3, 0x2, 0x7a, 0x8a, 0x5, 0xc, 0x7, 0x2, 0x7b, 0x8a, 0x5, 
    0x12, 0xa, 0x2, 0x7c, 0x8a, 0x5, 0x16, 0xc, 0x2, 0x7d, 0x8a, 0x5, 0x8, 
    0x5, 0x2, 0x7e, 0x8a, 0x5, 0x20, 0x11, 0x2, 0x7f, 0x8a, 0x5, 0x30, 0x19, 
    0x2, 0x80, 0x8a, 0x5, 0x34, 0x1b, 0x2, 0x81, 0x8a, 0x5, 0x48, 0x25, 
    0x2, 0x82, 0x8a, 0x5, 0x56, 0x2c, 0x2, 0x83, 0x8a, 0x5, 0x52, 0x2a, 
    0x2, 0x84, 0x8a, 0x5, 0x5e, 0x30, 0x2, 0x85, 0x8a, 0x5, 0x24, 0x13, 
    0x2, 0x86, 0x8a, 0x5, 0x40, 0x21, 0x2, 0x87, 0x8a, 0x5, 0x2c, 0x17, 
    0x2, 0x88, 0x8a, 0x7, 0x1b, 0x2, 0x2, 0x89, 0x76, 0x3, 0x2, 0x2, 0x2, 
    0x89, 0x77, 0x3, 0x2, 0x2, 0x2, 0x89, 0x78, 0x3, 0x2, 0x2, 0x2, 0x89, 
    0x79, 0x3, 0x2, 0x2, 0x2, 0x89, 0x7a, 0x3, 0x2, 0x2, 0x2, 0x89, 0x7b, 
    0x3, 0x2, 0x2, 0x2, 0x89, 0x7c, 0x3, 0x2, 0x2, 0x2, 0x89, 0x7d, 0x3, 
    0x2, 0x2, 0x2, 0x89, 0x7e, 0x3, 0x2, 0x2, 0x2, 0x89, 0x7f, 0x3, 0x2, 
    0x2, 0x2, 0x89, 0x80, 0x3, 0x2, 0x2, 0x2, 0x89, 0x81, 0x3, 0x2, 0x2, 
    0x2, 0x89, 0x82, 0x3, 0x2, 0x2, 0x2, 0x89, 0x83, 0x3, 0x2, 0x2, 0x2, 
    0x89, 0x84, 0x3, 0x2, 0x2, 0x2, 0x89, 0x85, 0x3, 0x2, 0x2, 0x2, 0x89, 
    0x86, 0x3, 0x2, 0x2, 0x2, 0x89, 0x87, 0x3, 0x2, 0x2, 0x2, 0x89, 0x88, 
    0x3, 0x2, 0x2, 0x2, 0x8a, 0x8b, 0x3, 0x2, 0x2, 0x2, 0x8b, 0x89, 0x3, 
    0x2, 0x2, 0x2, 0x8b, 0x8c, 0x3, 0x2, 0x2, 0x2, 0x8c, 0x3, 0x3, 0x2, 
    0x2, 0x2, 0x8d, 0x8e, 0x7, 0x3, 0x2, 0x2, 0x8e, 0x92, 0x7, 0x1b, 0x2, 
    0x2, 0x8f, 0x90, 0x5, 0x6, 0x4, 0x2, 0x90, 0x91, 0x7, 0x1b, 0x2, 0x2, 
    0x91, 0x93, 0x3, 0x2, 0x2, 0x2, 0x92, 0x8f, 0x3, 0x2, 0x2, 0x2, 0x93, 
    0x94, 0x3, 0x2, 0x2, 0x2, 0x94, 0x92, 0x3, 0x2, 0x2, 0x2, 0x94, 0x95, 
    0x3, 0x2, 0x2, 0x2, 0x95, 0x96, 0x3, 0x2, 0x2, 0x2, 0x96, 0x97, 0x7, 
    0x16, 0x2, 0x2, 0x97, 0x5, 0x3, 0x2, 0x2, 0x2, 0x98, 0x99, 0x5, 0x1a, 
    0xe, 0x2, 0x99, 0x7, 0x3, 0x2, 0x2, 0x2, 0x9a, 0x9b, 0x7, 0xb, 0x2, 
    0x2, 0x9b, 0x9f, 0x7, 0x1b, 0x2, 0x2, 0x9c, 0x9d, 0x5, 0xa, 0x6, 0x2, 
    0x9d, 0x9e, 0x7, 0x1b, 0x2, 0x2, 0x9e, 0xa0, 0x3, 0x2, 0x2, 0x2, 0x9f, 
    0x9c, 0x3, 0x2, 0x2, 0x2, 0xa0, 0xa1, 0x3, 0x2, 0x2, 0x2, 0xa1, 0x9f, 
    0x3, 0x2, 0x2, 0x2, 0xa1, 0xa2, 0x3, 0x2, 0x2, 0x2, 0xa2, 0xa3, 0x3, 
    0x2, 0x2, 0x2, 0xa3, 0xa4, 0x7, 0x16, 0x2, 0x2, 0xa4, 0x9, 0x3, 0x2, 
    0x2, 0x2, 0xa5, 0xa7, 0x5, 0x64, 0x33, 0x2, 0xa6, 0xa5, 0x3, 0x2, 0x2, 
    0x2, 0xa6, 0xa7, 0x3, 0x2, 0x2, 0x2, 0xa7, 0xa8, 0x3, 0x2, 0x2, 0x2, 
    0xa8, 0xa9, 0x5, 0x1a, 0xe, 0x2, 0xa9, 0xb, 0x3, 0x2, 0x2, 0x2, 0xaa, 
    0xab, 0x7, 0xc, 0x2, 0x2, 0xab, 0xaf, 0x7, 0x1b, 0x2, 0x2, 0xac, 0xad, 
    0x5, 0xe, 0x8, 0x2, 0xad, 0xae, 0x7, 0x1b, 0x2, 0x2, 0xae, 0xb0, 0x3, 
    0x2, 0x2, 0x2, 0xaf, 0xac, 0x3, 0x2, 0x2, 0x2, 0xb0, 0xb1, 0x3, 0x2, 
    0x2, 0x2, 0xb1, 0xaf, 0x3, 0x2, 0x2, 0x2, 0xb1, 0xb2, 0x3, 0x2, 0x2, 
    0x2, 0xb2, 0xb3, 0x3, 0x2, 0x2, 0x2, 0xb3, 0xb4, 0x7, 0x16, 0x2, 0x2, 
    0xb4, 0xd, 0x3, 0x2, 0x2, 0x2, 0xb5, 0xb7, 0x5, 0x6a, 0x36, 0x2, 0xb6, 
    0xb8, 0x5, 0x6a, 0x36, 0x2, 0xb7, 0xb6, 0x3, 0x2, 0x2, 0x2, 0xb7, 0xb8, 
    0x3, 0x2, 0x2, 0x2, 0xb8, 0xba, 0x3, 0x2, 0x2, 0x2, 0xb9, 0xbb, 0x5, 
    0x10, 0x9, 0x2, 0xba, 0xb9, 0x3, 0x2, 0x2, 0x2, 0xba, 0xbb, 0x3, 0x2, 
    0x2, 0x2, 0xbb, 0xf, 0x3, 0x2, 0x2, 0x2, 0xbc, 0xbe, 0x5, 0x72, 0x3a, 
    0x2, 0xbd, 0xbf, 0x5, 0x64, 0x33, 0x2, 0xbe, 0xbd, 0x3, 0x2, 0x2, 0x2, 
    0xbe, 0xbf, 0x3, 0x2, 0x2, 0x2, 0xbf, 0x11, 0x3, 0x2, 0x2, 0x2, 0xc0, 
    0xc1, 0x7, 0xe, 0x2, 0x2, 0xc1, 0xc5, 0x7, 0x1b, 0x2, 0x2, 0xc2, 0xc3, 
    0x5, 0x14, 0xb, 0x2, 0xc3, 0xc4, 0x7, 0x1b, 0x2, 0x2, 0xc4, 0xc6, 0x3, 
    0x2, 0x2, 0x2, 0xc5, 0xc2, 0x3, 0x2, 0x2, 0x2, 0xc6, 0xc7, 0x3, 0x2, 
    0x2, 0x2, 0xc7, 0xc5, 0x3, 0x2, 0x2, 0x2, 0xc7, 0xc8, 0x3, 0x2, 0x2, 
    0x2, 0xc8, 0xc9, 0x3, 0x2, 0x2, 0x2, 0xc9, 0xca, 0x7, 0x16, 0x2, 0x2, 
    0xca, 0x13, 0x3, 0x2, 0x2, 0x2, 0xcb, 0xcd, 0x5, 0x6a, 0x36, 0x2, 0xcc, 
    0xce, 0x5, 0x6a, 0x36, 0x2, 0xcd, 0xcc, 0x3, 0x2, 0x2, 0x2, 0xcd, 0xce, 
    0x3, 0x2, 0x2, 0x2, 0xce, 0x15, 0x3, 0x2, 0x2, 0x2, 0xcf, 0xd0, 0x7, 
    0xf, 0x2, 0x2, 0xd0, 0xd4, 0x7, 0x1b, 0x2, 0x2, 0xd1, 0xd2, 0x5, 0x18, 
    0xd, 0x2, 0xd2, 0xd3, 0x7, 0x1b, 0x2, 0x2, 0xd3, 0xd5, 0x3, 0x2, 0x2, 
    0x2, 0xd4, 0xd1, 0x3, 0x2, 0x2, 0x2, 0xd5, 0xd6, 0x3, 0x2, 0x2, 0x2, 
    0xd6, 0xd4, 0x3, 0x2, 0x2, 0x2, 0xd6, 0xd7, 0x3, 0x2, 0x2, 0x2, 0xd7, 
    0xd8, 0x3, 0x2, 0x2, 0x2, 0xd8, 0xd9, 0x7, 0x16, 0x2, 0x2, 0xd9, 0x17, 
    0x3, 0x2, 0x2, 0x2, 0xda, 0xdc, 0x5, 0x6a, 0x36, 0x2, 0xdb, 0xdd, 0x5, 
    0x6a, 0x36, 0x2, 0xdc, 0xdb, 0x3, 0x2, 0x2, 0x2, 0xdc, 0xdd, 0x3, 0x2, 
    0x2, 0x2, 0xdd, 0x19, 0x3, 0x2, 0x2, 0x2, 0xde, 0xdf, 0x5, 0x6a, 0x36, 
    0x2, 0xdf, 0xe0, 0x7, 0x15, 0x2, 0x2, 0xe0, 0x1b, 0x3, 0x2, 0x2, 0x2, 
    0xe1, 0xe2, 0x7, 0xa, 0x2, 0x2, 0xe2, 0xe6, 0x7, 0x1b, 0x2, 0x2, 0xe3, 
    0xe4, 0x5, 0x1e, 0x10, 0x2, 0xe4, 0xe5, 0x7, 0x1b, 0x2, 0x2, 0xe5, 0xe7, 
    0x3, 0x2, 0x2, 0x2, 0xe6, 0xe3, 0x3, 0x2, 0x2, 0x2, 0xe7, 0xe8, 0x3, 
    0x2, 0x2, 0x2, 0xe8, 0xe6, 0x3, 0x2, 0x2, 0x2, 0xe8, 0xe9, 0x3, 0x2, 
    0x2, 0x2, 0xe9, 0xea, 0x3, 0x2, 0x2, 0x2, 0xea, 0xeb, 0x7, 0x16, 0x2, 
    0x2, 0xeb, 0x1d, 0x3, 0x2, 0x2, 0x2, 0xec, 0xed, 0x5, 0x6a, 0x36, 0x2, 
    0xed, 0xee, 0x5, 0x6a, 0x36, 0x2, 0xee, 0x1f, 0x3, 0x2, 0x2, 0x2, 0xef, 
    0xf0, 0x7, 0x9, 0x2, 0x2, 0xf0, 0xf4, 0x7, 0x1b, 0x2, 0x2, 0xf1, 0xf2, 
    0x5, 0x22, 0x12, 0x2, 0xf2, 0xf3, 0x7, 0x1b, 0x2, 0x2, 0xf3, 0xf5, 0x3, 
    0x2, 0x2, 0x2, 0xf4, 0xf1, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xf6, 0x3, 0x2, 
    0x2, 0x2, 0xf6, 0xf4, 0x3, 0x2, 0x2, 0x2, 0xf6, 0xf7, 0x3, 0x2, 0x2, 
    0x2, 0xf7, 0xf8, 0x3, 0x2, 0x2, 0x2, 0xf8, 0xf9, 0x7, 0x16, 0x2, 0x2, 
    0xf9, 0x21, 0x3, 0x2, 0x2, 0x2, 0xfa, 0xfc, 0x5, 0x62, 0x32, 0x2, 0xfb, 
    0xfd, 0x5, 0x72, 0x3a, 0x2, 0xfc, 0xfb, 0x3, 0x2, 0x2, 0x2, 0xfc, 0xfd, 
    0x3, 0x2, 0x2, 0x2, 0xfd, 0xff, 0x3, 0x2, 0x2, 0x2, 0xfe, 0x100, 0x5, 
    0x6a, 0x36, 0x2, 0xff, 0xfe, 0x3, 0x2, 0x2, 0x2, 0x100, 0x101, 0x3, 
    0x2, 0x2, 0x2, 0x101, 0xff, 0x3, 0x2, 0x2, 0x2, 0x101, 0x102, 0x3, 0x2, 
    0x2, 0x2, 0x102, 0x23, 0x3, 0x2, 0x2, 0x2, 0x103, 0x104, 0x7, 0x11, 
    0x2, 0x2, 0x104, 0x108, 0x7, 0x1b, 0x2, 0x2, 0x105, 0x106, 0x5, 0x26, 
    0x14, 0x2, 0x106, 0x107, 0x7, 0x1b, 0x2, 0x2, 0x107, 0x109, 0x3, 0x2, 
    0x2, 0x2, 0x108, 0x105, 0x3, 0x2, 0x2, 0x2, 0x109, 0x10a, 0x3, 0x2, 
    0x2, 0x2, 0x10a, 0x108, 0x3, 0x2, 0x2, 0x2, 0x10a, 0x10b, 0x3, 0x2, 
    0x2, 0x2, 0x10b, 0x10c, 0x3, 0x2, 0x2, 0x2, 0x10c, 0x10d, 0x7, 0x16, 
    0x2, 0x2, 0x10d, 0x25, 0x3, 0x2, 0x2, 0x2, 0x10e, 0x110, 0x5, 0x62, 
    0x32, 0x2, 0x10f, 0x111, 0x5, 0x72, 0x3a, 0x2, 0x110, 0x10f, 0x3, 0x2, 
    0x2, 0x2, 0x110, 0x111, 0x3, 0x2, 0x2, 0x2, 0x111, 0x113, 0x3, 0x2, 
    0x2, 0x2, 0x112, 0x114, 0x5, 0x6c, 0x37, 0x2, 0x113, 0x112, 0x3, 0x2, 
    0x2, 0x2, 0x114, 0x115, 0x3, 0x2, 0x2, 0x2, 0x115, 0x113, 0x3, 0x2, 
    0x2, 0x2, 0x115, 0x116, 0x3, 0x2, 0x2, 0x2, 0x116, 0x27, 0x3, 0x2, 0x2, 
    0x2, 0x117, 0x118, 0x7, 0xd, 0x2, 0x2, 0x118, 0x11c, 0x7, 0x1b, 0x2, 
    0x2, 0x119, 0x11a, 0x5, 0x2a, 0x16, 0x2, 0x11a, 0x11b, 0x7, 0x1b, 0x2, 
    0x2, 0x11b, 0x11d, 0x3, 0x2, 0x2, 0x2, 0x11c, 0x119, 0x3, 0x2, 0x2, 
    0x2, 0x11d, 0x11e, 0x3, 0x2, 0x2, 0x2, 0x11e, 0x11c, 0x3, 0x2, 0x2, 
    0x2, 0x11e, 0x11f, 0x3, 0x2, 0x2, 0x2, 0x11f, 0x120, 0x3, 0x2, 0x2, 
    0x2, 0x120, 0x121, 0x7, 0x16, 0x2, 0x2, 0x121, 0x29, 0x3, 0x2, 0x2, 
    0x2, 0x122, 0x123, 0x5, 0x68, 0x35, 0x2, 0x123, 0x124, 0x5, 0x74, 0x3b, 
    0x2, 0x124, 0x125, 0x5, 0x6a, 0x36, 0x2, 0x125, 0x126, 0x5, 0x6a, 0x36, 
    0x2, 0x126, 0x2b, 0x3, 0x2, 0x2, 0x2, 0x127, 0x128, 0x7, 0x10, 0x2, 
    0x2, 0x128, 0x12c, 0x7, 0x1b, 0x2, 0x2, 0x129, 0x12a, 0x5, 0x2e, 0x18, 
    0x2, 0x12a, 0x12b, 0x7, 0x1b, 0x2, 0x2, 0x12b, 0x12d, 0x3, 0x2, 0x2, 
    0x2, 0x12c, 0x129, 0x3, 0x2, 0x2, 0x2, 0x12d, 0x12e, 0x3, 0x2, 0x2, 
    0x2, 0x12e, 0x12c, 0x3, 0x2, 0x2, 0x2, 0x12e, 0x12f, 0x3, 0x2, 0x2, 
    0x2, 0x12f, 0x130, 0x3, 0x2, 0x2, 0x2, 0x130, 0x131, 0x7, 0x16, 0x2, 
    0x2, 0x131, 0x2d, 0x3, 0x2, 0x2, 0x2, 0x132, 0x134, 0x5, 0x6a, 0x36, 
    0x2, 0x133, 0x135, 0x5, 0x6a, 0x36, 0x2, 0x134, 0x133, 0x3, 0x2, 0x2, 
    0x2, 0x134, 0x135, 0x3, 0x2, 0x2, 0x2, 0x135, 0x136, 0x3, 0x2, 0x2, 
    0x2, 0x136, 0x137, 0x7, 0x2c, 0x2, 0x2, 0x137, 0x138, 0x7, 0x2f, 0x2, 
    0x2, 0x138, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x139, 0x13a, 0x7, 0x14, 0x2, 
    0x2, 0x13a, 0x141, 0x7, 0x1b, 0x2, 0x2, 0x13b, 0x13d, 0x5, 0x32, 0x1a, 
    0x2, 0x13c, 0x13e, 0x7, 0x1b, 0x2, 0x2, 0x13d, 0x13c, 0x3, 0x2, 0x2, 
    0x2, 0x13e, 0x13f, 0x3, 0x2, 0x2, 0x2, 0x13f, 0x13d, 0x3, 0x2, 0x2, 
    0x2, 0x13f, 0x140, 0x3, 0x2, 0x2, 0x2, 0x140, 0x142, 0x3, 0x2, 0x2, 
    0x2, 0x141, 0x13b, 0x3, 0x2, 0x2, 0x2, 0x142, 0x143, 0x3, 0x2, 0x2, 
    0x2, 0x143, 0x141, 0x3, 0x2, 0x2, 0x2, 0x143, 0x144, 0x3, 0x2, 0x2, 
    0x2, 0x144, 0x145, 0x3, 0x2, 0x2, 0x2, 0x145, 0x146, 0x7, 0x16, 0x2, 
    0x2, 0x146, 0x31, 0x3, 0x2, 0x2, 0x2, 0x147, 0x148, 0x5, 0x66, 0x34, 
    0x2, 0x148, 0x149, 0x7, 0x29, 0x2, 0x2, 0x149, 0x14a, 0x7, 0x2d, 0x2, 
    0x2, 0x14a, 0x33, 0x3, 0x2, 0x2, 0x2, 0x14b, 0x14c, 0x7, 0x13, 0x2, 
    0x2, 0x14c, 0x150, 0x7, 0x1b, 0x2, 0x2, 0x14d, 0x14e, 0x5, 0x36, 0x1c, 
    0x2, 0x14e, 0x14f, 0x7, 0x1b, 0x2, 0x2, 0x14f, 0x151, 0x3, 0x2, 0x2, 
    0x2, 0x150, 0x14d, 0x3, 0x2, 0x2, 0x2, 0x151, 0x152, 0x3, 0x2, 0x2, 
    0x2, 0x152, 0x150, 0x3, 0x2, 0x2, 0x2, 0x152, 0x153, 0x3, 0x2, 0x2, 
    0x2, 0x153, 0x154, 0x3, 0x2, 0x2, 0x2, 0x154, 0x155, 0x7, 0x16, 0x2, 
    0x2, 0x155, 0x35, 0x3, 0x2, 0x2, 0x2, 0x156, 0x157, 0x5, 0x6a, 0x36, 
    0x2, 0x157, 0x158, 0x7, 0x29, 0x2, 0x2, 0x158, 0x159, 0x7, 0x2d, 0x2, 
    0x2, 0x159, 0x37, 0x3, 0x2, 0x2, 0x2, 0x15a, 0x15b, 0x7, 0x8, 0x2, 0x2, 
    0x15b, 0x15f, 0x7, 0x1b, 0x2, 0x2, 0x15c, 0x15d, 0x5, 0x3a, 0x1e, 0x2, 
    0x15d, 0x15e, 0x7, 0x1b, 0x2, 0x2, 0x15e, 0x160, 0x3, 0x2, 0x2, 0x2, 
    0x15f, 0x15c, 0x3, 0x2, 0x2, 0x2, 0x160, 0x161, 0x3, 0x2, 0x2, 0x2, 
    0x161, 0x15f, 0x3, 0x2, 0x2, 0x2, 0x161, 0x162, 0x3, 0x2, 0x2, 0x2, 
    0x162, 0x163, 0x3, 0x2, 0x2, 0x2, 0x163, 0x164, 0x7, 0x16, 0x2, 0x2, 
    0x164, 0x39, 0x3, 0x2, 0x2, 0x2, 0x165, 0x16a, 0x5, 0x3c, 0x1f, 0x2, 
    0x166, 0x167, 0x7, 0x26, 0x2, 0x2, 0x167, 0x169, 0x5, 0x3c, 0x1f, 0x2, 
    0x168, 0x166, 0x3, 0x2, 0x2, 0x2, 0x169, 0x16c, 0x3, 0x2, 0x2, 0x2, 
    0x16a, 0x168, 0x3, 0x2, 0x2, 0x2, 0x16a, 0x16b, 0x3, 0x2, 0x2, 0x2, 
    0x16b, 0x16d, 0x3, 0x2, 0x2, 0x2, 0x16c, 0x16a, 0x3, 0x2, 0x2, 0x2, 
    0x16d, 0x16e, 0x7, 0x28, 0x2, 0x2, 0x16e, 0x16f, 0x5, 0x3e, 0x20, 0x2, 
    0x16f, 0x3b, 0x3, 0x2, 0x2, 0x2, 0x170, 0x171, 0x5, 0x62, 0x32, 0x2, 
    0x171, 0x173, 0x5, 0x6a, 0x36, 0x2, 0x172, 0x174, 0x5, 0x72, 0x3a, 0x2, 
    0x173, 0x172, 0x3, 0x2, 0x2, 0x2, 0x173, 0x174, 0x3, 0x2, 0x2, 0x2, 
    0x174, 0x3d, 0x3, 0x2, 0x2, 0x2, 0x175, 0x177, 0x5, 0x6a, 0x36, 0x2, 
    0x176, 0x178, 0x5, 0x72, 0x3a, 0x2, 0x177, 0x176, 0x3, 0x2, 0x2, 0x2, 
    0x177, 0x178, 0x3, 0x2, 0x2, 0x2, 0x178, 0x3f, 0x3, 0x2, 0x2, 0x2, 0x179, 
    0x17a, 0x7, 0x12, 0x2, 0x2, 0x17a, 0x17e, 0x7, 0x1b, 0x2, 0x2, 0x17b, 
    0x17c, 0x5, 0x42, 0x22, 0x2, 0x17c, 0x17d, 0x7, 0x1b, 0x2, 0x2, 0x17d, 
    0x17f, 0x3, 0x2, 0x2, 0x2, 0x17e, 0x17b, 0x3, 0x2, 0x2, 0x2, 0x17f, 
    0x180, 0x3, 0x2, 0x2, 0x2, 0x180, 0x17e, 0x3, 0x2, 0x2, 0x2, 0x180, 
    0x181, 0x3, 0x2, 0x2, 0x2, 0x181, 0x182, 0x3, 0x2, 0x2, 0x2, 0x182, 
    0x183, 0x7, 0x16, 0x2, 0x2, 0x183, 0x41, 0x3, 0x2, 0x2, 0x2, 0x184, 
    0x189, 0x5, 0x44, 0x23, 0x2, 0x185, 0x186, 0x7, 0x26, 0x2, 0x2, 0x186, 
    0x188, 0x5, 0x44, 0x23, 0x2, 0x187, 0x185, 0x3, 0x2, 0x2, 0x2, 0x188, 
    0x18b, 0x3, 0x2, 0x2, 0x2, 0x189, 0x187, 0x3, 0x2, 0x2, 0x2, 0x189, 
    0x18a, 0x3, 0x2, 0x2, 0x2, 0x18a, 0x18c, 0x3, 0x2, 0x2, 0x2, 0x18b, 
    0x189, 0x3, 0x2, 0x2, 0x2, 0x18c, 0x18d, 0x7, 0x28, 0x2, 0x2, 0x18d, 
    0x18e, 0x5, 0x46, 0x24, 0x2, 0x18e, 0x43, 0x3, 0x2, 0x2, 0x2, 0x18f, 
    0x190, 0x5, 0x62, 0x32, 0x2, 0x190, 0x192, 0x5, 0x6c, 0x37, 0x2, 0x191, 
    0x193, 0x5, 0x72, 0x3a, 0x2, 0x192, 0x191, 0x3, 0x2, 0x2, 0x2, 0x192, 
    0x193, 0x3, 0x2, 0x2, 0x2, 0x193, 0x45, 0x3, 0x2, 0x2, 0x2, 0x194, 0x196, 
    0x5, 0x6c, 0x37, 0x2, 0x195, 0x197, 0x5, 0x72, 0x3a, 0x2, 0x196, 0x195, 
    0x3, 0x2, 0x2, 0x2, 0x196, 0x197, 0x3, 0x2, 0x2, 0x2, 0x197, 0x47, 0x3, 
    0x2, 0x2, 0x2, 0x198, 0x199, 0x7, 0x7, 0x2, 0x2, 0x199, 0x19d, 0x7, 
    0x1b, 0x2, 0x2, 0x19a, 0x19b, 0x5, 0x4a, 0x26, 0x2, 0x19b, 0x19c, 0x7, 
    0x1b, 0x2, 0x2, 0x19c, 0x19e, 0x3, 0x2, 0x2, 0x2, 0x19d, 0x19a, 0x3, 
    0x2, 0x2, 0x2, 0x19e, 0x19f, 0x3, 0x2, 0x2, 0x2, 0x19f, 0x19d, 0x3, 
    0x2, 0x2, 0x2, 0x19f, 0x1a0, 0x3, 0x2, 0x2, 0x2, 0x1a0, 0x1a1, 0x3, 
    0x2, 0x2, 0x2, 0x1a1, 0x1a2, 0x7, 0x16, 0x2, 0x2, 0x1a2, 0x49, 0x3, 
    0x2, 0x2, 0x2, 0x1a3, 0x1a4, 0x5, 0x6a, 0x36, 0x2, 0x1a4, 0x1a6, 0x5, 
    0x6a, 0x36, 0x2, 0x1a5, 0x1a7, 0x5, 0x4c, 0x27, 0x2, 0x1a6, 0x1a5, 0x3, 
    0x2, 0x2, 0x2, 0x1a6, 0x1a7, 0x3, 0x2, 0x2, 0x2, 0x1a7, 0x4b, 0x3, 0x2, 
    0x2, 0x2, 0x1a8, 0x1a9, 0x7, 0x1e, 0x2, 0x2, 0x1a9, 0x1aa, 0x5, 0x4e, 
    0x28, 0x2, 0x1aa, 0x1ab, 0x7, 0x26, 0x2, 0x2, 0x1ab, 0x1ac, 0x5, 0x50, 
    0x29, 0x2, 0x1ac, 0x1ad, 0x7, 0x1f, 0x2, 0x2, 0x1ad, 0x4d, 0x3, 0x2, 
    0x2, 0x2, 0x1ae, 0x1af, 0x9, 0x2, 0x2, 0x2, 0x1af, 0x4f, 0x3, 0x2, 0x2, 
    0x2, 0x1b0, 0x1b1, 0x9, 0x2, 0x2, 0x2, 0x1b1, 0x51, 0x3, 0x2, 0x2, 0x2, 
    0x1b2, 0x1b3, 0x7, 0x6, 0x2, 0x2, 0x1b3, 0x1b7, 0x7, 0x1b, 0x2, 0x2, 
    0x1b4, 0x1b5, 0x5, 0x54, 0x2b, 0x2, 0x1b5, 0x1b6, 0x7, 0x1b, 0x2, 0x2, 
    0x1b6, 0x1b8, 0x3, 0x2, 0x2, 0x2, 0x1b7, 0x1b4, 0x3, 0x2, 0x2, 0x2, 
    0x1b8, 0x1b9, 0x3, 0x2, 0x2, 0x2, 0x1b9, 0x1b7, 0x3, 0x2, 0x2, 0x2, 
    0x1b9, 0x1ba, 0x3, 0x2, 0x2, 0x2, 0x1ba, 0x1bb, 0x3, 0x2, 0x2, 0x2, 
    0x1bb, 0x1bc, 0x7, 0x16, 0x2, 0x2, 0x1bc, 0x53, 0x3, 0x2, 0x2, 0x2, 
    0x1bd, 0x1be, 0x5, 0x64, 0x33, 0x2, 0x1be, 0x1bf, 0x7, 0x18, 0x2, 0x2, 
    0x1bf, 0x55, 0x3, 0x2, 0x2, 0x2, 0x1c0, 0x1c1, 0x7, 0x5, 0x2, 0x2, 0x1c1, 
    0x1c5, 0x7, 0x1b, 0x2, 0x2, 0x1c2, 0x1c3, 0x5, 0x58, 0x2d, 0x2, 0x1c3, 
    0x1c4, 0x7, 0x1b, 0x2, 0x2, 0x1c4, 0x1c6, 0x3, 0x2, 0x2, 0x2, 0x1c5, 
    0x1c2, 0x3, 0x2, 0x2, 0x2, 0x1c6, 0x1c7, 0x3, 0x2, 0x2, 0x2, 0x1c7, 
    0x1c5, 0x3, 0x2, 0x2, 0x2, 0x1c7, 0x1c8, 0x3, 0x2, 0x2, 0x2, 0x1c8, 
    0x1c9, 0x3, 0x2, 0x2, 0x2, 0x1c9, 0x1ca, 0x7, 0x16, 0x2, 0x2, 0x1ca, 
    0x57, 0x3, 0x2, 0x2, 0x2, 0x1cb, 0x1ce, 0x5, 0x5a, 0x2e, 0x2, 0x1cc, 
    0x1ce, 0x5, 0x5c, 0x2f, 0x2, 0x1cd, 0x1cb, 0x3, 0x2, 0x2, 0x2, 0x1cd, 
    0x1cc, 0x3, 0x2, 0x2, 0x2, 0x1ce, 0x59, 0x3, 0x2, 0x2, 0x2, 0x1cf, 0x1d0, 
    0x5, 0x6a, 0x36, 0x2, 0x1d0, 0x1d1, 0x5, 0x66, 0x34, 0x2, 0x1d1, 0x5b, 
    0x3, 0x2, 0x2, 0x2, 0x1d2, 0x1d3, 0x5, 0x66, 0x34, 0x2, 0x1d3, 0x1d8, 
    0x5, 0x6a, 0x36, 0x2, 0x1d4, 0x1d5, 0x7, 0x26, 0x2, 0x2, 0x1d5, 0x1d7, 
    0x5, 0x6a, 0x36, 0x2, 0x1d6, 0x1d4, 0x3, 0x2, 0x2, 0x2, 0x1d7, 0x1da, 
    0x3, 0x2, 0x2, 0x2, 0x1d8, 0x1d6, 0x3, 0x2, 0x2, 0x2, 0x1d8, 0x1d9, 
    0x3, 0x2, 0x2, 0x2, 0x1d9, 0x5d, 0x3, 0x2, 0x2, 0x2, 0x1da, 0x1d8, 0x3, 
    0x2, 0x2, 0x2, 0x1db, 0x1dc, 0x7, 0x4, 0x2, 0x2, 0x1dc, 0x1e0, 0x7, 
    0x1b, 0x2, 0x2, 0x1dd, 0x1de, 0x5, 0x60, 0x31, 0x2, 0x1de, 0x1df, 0x7, 
    0x1b, 0x2, 0x2, 0x1df, 0x1e1, 0x3, 0x2, 0x2, 0x2, 0x1e0, 0x1dd, 0x3, 
    0x2, 0x2, 0x2, 0x1e1, 0x1e2, 0x3, 0x2, 0x2, 0x2, 0x1e2, 0x1e0, 0x3, 
    0x2, 0x2, 0x2, 0x1e2, 0x1e3, 0x3, 0x2, 0x2, 0x2, 0x1e3, 0x1e4, 0x3, 
    0x2, 0x2, 0x2, 0x1e4, 0x1e5, 0x7, 0x16, 0x2, 0x2, 0x1e5, 0x5f, 0x3, 
    0x2, 0x2, 0x2, 0x1e6, 0x1e7, 0x5, 0x62, 0x32, 0x2, 0x1e7, 0x1e8, 0x5, 
    0x64, 0x33, 0x2, 0x1e8, 0x61, 0x3, 0x2, 0x2, 0x2, 0x1e9, 0x1ea, 0x7, 
    0x17, 0x2, 0x2, 0x1ea, 0x63, 0x3, 0x2, 0x2, 0x2, 0x1eb, 0x1ec, 0x7, 
    0x17, 0x2, 0x2, 0x1ec, 0x65, 0x3, 0x2, 0x2, 0x2, 0x1ed, 0x1ee, 0x7, 
    0x17, 0x2, 0x2, 0x1ee, 0x67, 0x3, 0x2, 0x2, 0x2, 0x1ef, 0x1f0, 0x7, 
    0x17, 0x2, 0x2, 0x1f0, 0x69, 0x3, 0x2, 0x2, 0x2, 0x1f1, 0x1f2, 0x7, 
    0x1e, 0x2, 0x2, 0x1f2, 0x1f3, 0x5, 0x6e, 0x38, 0x2, 0x1f3, 0x1f4, 0x7, 
    0x26, 0x2, 0x2, 0x1f4, 0x1f5, 0x5, 0x70, 0x39, 0x2, 0x1f5, 0x1f6, 0x7, 
    0x1f, 0x2, 0x2, 0x1f6, 0x6b, 0x3, 0x2, 0x2, 0x2, 0x1f7, 0x1f8, 0x7, 
    0x1e, 0x2, 0x2, 0x1f8, 0x1f9, 0x5, 0x6a, 0x36, 0x2, 0x1f9, 0x1fa, 0x7, 
    0x26, 0x2, 0x2, 0x1fa, 0x1fb, 0x5, 0x6a, 0x36, 0x2, 0x1fb, 0x1fc, 0x7, 
    0x1f, 0x2, 0x2, 0x1fc, 0x6d, 0x3, 0x2, 0x2, 0x2, 0x1fd, 0x1fe, 0x7, 
    0x17, 0x2, 0x2, 0x1fe, 0x6f, 0x3, 0x2, 0x2, 0x2, 0x1ff, 0x200, 0x7, 
    0x17, 0x2, 0x2, 0x200, 0x71, 0x3, 0x2, 0x2, 0x2, 0x201, 0x202, 0x7, 
    0x20, 0x2, 0x2, 0x202, 0x203, 0x7, 0x17, 0x2, 0x2, 0x203, 0x204, 0x7, 
    0x21, 0x2, 0x2, 0x204, 0x73, 0x3, 0x2, 0x2, 0x2, 0x205, 0x206, 0x7, 
    0x20, 0x2, 0x2, 0x206, 0x207, 0x7, 0x17, 0x2, 0x2, 0x207, 0x208, 0x7, 
    0x22, 0x2, 0x2, 0x208, 0x209, 0x7, 0x17, 0x2, 0x2, 0x209, 0x20a, 0x7, 
    0x21, 0x2, 0x2, 0x20a, 0x75, 0x3, 0x2, 0x2, 0x2, 0x2b, 0x89, 0x8b, 0x94, 
    0xa1, 0xa6, 0xb1, 0xb7, 0xba, 0xbe, 0xc7, 0xcd, 0xd6, 0xdc, 0xe8, 0xf6, 
    0xfc, 0x101, 0x10a, 0x110, 0x115, 0x11e, 0x12e, 0x134, 0x13f, 0x143, 
    0x152, 0x161, 0x16a, 0x173, 0x177, 0x180, 0x189, 0x192, 0x196, 0x19f, 
    0x1a6, 0x1b9, 0x1c7, 0x1cd, 0x1d8, 0x1e2, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

Bio::Initializer Bio::_init;
