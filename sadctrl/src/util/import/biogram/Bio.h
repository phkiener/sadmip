
// Generated from Bio.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"




class  Bio : public antlr4::Parser {
public:
  enum {
    Sinks = 1, Droplets = 2, PinAssignments = 3, Fluids = 4, Blockages = 5, 
    Nets = 6, Routes = 7, Grid = 8, Dispensers = 9, Detectors = 10, Mixers = 11, 
    Heaters = 12, Magnets = 13, Annotations = 14, MedaRoutes = 15, MedaNets = 16, 
    CellActuations = 17, PinActuations = 18, Direction = 19, END = 20, Integer = 21, 
    Identifier = 22, Annotation = 23, Comment = 24, Newlines = 25, NEWLINE = 26, 
    WS = 27, LParen = 28, RParen = 29, LBracket = 30, RBracket = 31, Dash = 32, 
    Plus = 33, Slash = 34, Equals = 35, Comma = 36, Asterisk = 37, Arrow = 38, 
    Colon = 39, Delimiters = 40, Signs = 41, LessThan = 42, ActuationVector = 43, 
    WhiteSpaceInActuationMode = 44, AreaAnnotationText = 45
  };

  enum {
    RuleBio = 0, RuleSinks = 1, RuleSink = 2, RuleDispensers = 3, RuleDispenser = 4, 
    RuleDetectors = 5, RuleDetector = 6, RuleDetector_spec = 7, RuleHeaters = 8, 
    RuleHeater = 9, RuleMagnets = 10, RuleMagnet = 11, RuleIoport = 12, 
    RuleGrid = 13, RuleGridblock = 14, RuleRoutes = 15, RuleRoute = 16, 
    RuleMedaRoutes = 17, RuleMedaRoute = 18, RuleMixers = 19, RuleMixer = 20, 
    RuleAnnotations = 21, RuleAreaAnnotation = 22, RulePinActuations = 23, 
    RulePinActuation = 24, RuleCellActuations = 25, RuleCellActuation = 26, 
    RuleNets = 27, RuleNet = 28, RuleSource = 29, RuleTarget = 30, RuleMedaNets = 31, 
    RuleMedaNet = 32, RuleMedaSource = 33, RuleMedaTarget = 34, RuleBlockages = 35, 
    RuleBlockage = 36, RuleTiming = 37, RuleBeginTiming = 38, RuleEndTiming = 39, 
    RuleFluids = 40, RuleFluiddef = 41, RulePinAssignments = 42, RuleAssignment = 43, 
    RulePositionToPinAssignment = 44, RulePinToPositionsAssignment = 45, 
    RuleDroplets = 46, RuleDropToFluid = 47, RuleDropletID = 48, RuleFluidID = 49, 
    RulePinID = 50, RuleMixerID = 51, RulePosition = 52, RuleLocation = 53, 
    RuleXpos = 54, RuleYpos = 55, RuleTimeConstraint = 56, RuleTimeRange = 57
  };

  Bio(antlr4::TokenStream *input);
  ~Bio();

  virtual std::string getGrammarFileName() const override;
  virtual const antlr4::atn::ATN& getATN() const override { return _atn; };
  virtual const std::vector<std::string>& getTokenNames() const override { return _tokenNames; }; // deprecated: use vocabulary instead.
  virtual const std::vector<std::string>& getRuleNames() const override;
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;


  class BioContext;
  class SinksContext;
  class SinkContext;
  class DispensersContext;
  class DispenserContext;
  class DetectorsContext;
  class DetectorContext;
  class Detector_specContext;
  class HeatersContext;
  class HeaterContext;
  class MagnetsContext;
  class MagnetContext;
  class IoportContext;
  class GridContext;
  class GridblockContext;
  class RoutesContext;
  class RouteContext;
  class MedaRoutesContext;
  class MedaRouteContext;
  class MixersContext;
  class MixerContext;
  class AnnotationsContext;
  class AreaAnnotationContext;
  class PinActuationsContext;
  class PinActuationContext;
  class CellActuationsContext;
  class CellActuationContext;
  class NetsContext;
  class NetContext;
  class SourceContext;
  class TargetContext;
  class MedaNetsContext;
  class MedaNetContext;
  class MedaSourceContext;
  class MedaTargetContext;
  class BlockagesContext;
  class BlockageContext;
  class TimingContext;
  class BeginTimingContext;
  class EndTimingContext;
  class FluidsContext;
  class FluiddefContext;
  class PinAssignmentsContext;
  class AssignmentContext;
  class PositionToPinAssignmentContext;
  class PinToPositionsAssignmentContext;
  class DropletsContext;
  class DropToFluidContext;
  class DropletIDContext;
  class FluidIDContext;
  class PinIDContext;
  class MixerIDContext;
  class PositionContext;
  class LocationContext;
  class XposContext;
  class YposContext;
  class TimeConstraintContext;
  class TimeRangeContext; 

  class  BioContext : public antlr4::ParserRuleContext {
  public:
    BioContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<GridContext *> grid();
    GridContext* grid(size_t i);
    std::vector<NetsContext *> nets();
    NetsContext* nets(size_t i);
    std::vector<MixersContext *> mixers();
    MixersContext* mixers(size_t i);
    std::vector<SinksContext *> sinks();
    SinksContext* sinks(size_t i);
    std::vector<DetectorsContext *> detectors();
    DetectorsContext* detectors(size_t i);
    std::vector<HeatersContext *> heaters();
    HeatersContext* heaters(size_t i);
    std::vector<MagnetsContext *> magnets();
    MagnetsContext* magnets(size_t i);
    std::vector<DispensersContext *> dispensers();
    DispensersContext* dispensers(size_t i);
    std::vector<RoutesContext *> routes();
    RoutesContext* routes(size_t i);
    std::vector<PinActuationsContext *> pinActuations();
    PinActuationsContext* pinActuations(size_t i);
    std::vector<CellActuationsContext *> cellActuations();
    CellActuationsContext* cellActuations(size_t i);
    std::vector<BlockagesContext *> blockages();
    BlockagesContext* blockages(size_t i);
    std::vector<PinAssignmentsContext *> pinAssignments();
    PinAssignmentsContext* pinAssignments(size_t i);
    std::vector<FluidsContext *> fluids();
    FluidsContext* fluids(size_t i);
    std::vector<DropletsContext *> droplets();
    DropletsContext* droplets(size_t i);
    std::vector<MedaRoutesContext *> medaRoutes();
    MedaRoutesContext* medaRoutes(size_t i);
    std::vector<MedaNetsContext *> medaNets();
    MedaNetsContext* medaNets(size_t i);
    std::vector<AnnotationsContext *> annotations();
    AnnotationsContext* annotations(size_t i);
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);

   
  };

  BioContext* bio();

  class  SinksContext : public antlr4::ParserRuleContext {
  public:
    SinksContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Sinks();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<SinkContext *> sink();
    SinkContext* sink(size_t i);

   
  };

  SinksContext* sinks();

  class  SinkContext : public antlr4::ParserRuleContext {
  public:
    SinkContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    IoportContext *ioport();

   
  };

  SinkContext* sink();

  class  DispensersContext : public antlr4::ParserRuleContext {
  public:
    DispensersContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Dispensers();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<DispenserContext *> dispenser();
    DispenserContext* dispenser(size_t i);

   
  };

  DispensersContext* dispensers();

  class  DispenserContext : public antlr4::ParserRuleContext {
  public:
    DispenserContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    IoportContext *ioport();
    FluidIDContext *fluidID();

   
  };

  DispenserContext* dispenser();

  class  DetectorsContext : public antlr4::ParserRuleContext {
  public:
    DetectorsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Detectors();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<DetectorContext *> detector();
    DetectorContext* detector(size_t i);

   
  };

  DetectorsContext* detectors();

  class  DetectorContext : public antlr4::ParserRuleContext {
  public:
    DetectorContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<PositionContext *> position();
    PositionContext* position(size_t i);
    Detector_specContext *detector_spec();

   
  };

  DetectorContext* detector();

  class  Detector_specContext : public antlr4::ParserRuleContext {
  public:
    Detector_specContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    TimeConstraintContext *timeConstraint();
    FluidIDContext *fluidID();

   
  };

  Detector_specContext* detector_spec();

  class  HeatersContext : public antlr4::ParserRuleContext {
  public:
    HeatersContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Heaters();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<HeaterContext *> heater();
    HeaterContext* heater(size_t i);

   
  };

  HeatersContext* heaters();

  class  HeaterContext : public antlr4::ParserRuleContext {
  public:
    HeaterContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<PositionContext *> position();
    PositionContext* position(size_t i);

   
  };

  HeaterContext* heater();

  class  MagnetsContext : public antlr4::ParserRuleContext {
  public:
    MagnetsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Magnets();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<MagnetContext *> magnet();
    MagnetContext* magnet(size_t i);

   
  };

  MagnetsContext* magnets();

  class  MagnetContext : public antlr4::ParserRuleContext {
  public:
    MagnetContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<PositionContext *> position();
    PositionContext* position(size_t i);

   
  };

  MagnetContext* magnet();

  class  IoportContext : public antlr4::ParserRuleContext {
  public:
    IoportContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    PositionContext *position();
    antlr4::tree::TerminalNode *Direction();

   
  };

  IoportContext* ioport();

  class  GridContext : public antlr4::ParserRuleContext {
  public:
    GridContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Grid();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<GridblockContext *> gridblock();
    GridblockContext* gridblock(size_t i);

   
  };

  GridContext* grid();

  class  GridblockContext : public antlr4::ParserRuleContext {
  public:
    GridblockContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<PositionContext *> position();
    PositionContext* position(size_t i);

   
  };

  GridblockContext* gridblock();

  class  RoutesContext : public antlr4::ParserRuleContext {
  public:
    RoutesContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Routes();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<RouteContext *> route();
    RouteContext* route(size_t i);

   
  };

  RoutesContext* routes();

  class  RouteContext : public antlr4::ParserRuleContext {
  public:
    RouteContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    DropletIDContext *dropletID();
    TimeConstraintContext *timeConstraint();
    std::vector<PositionContext *> position();
    PositionContext* position(size_t i);

   
  };

  RouteContext* route();

  class  MedaRoutesContext : public antlr4::ParserRuleContext {
  public:
    MedaRoutesContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *MedaRoutes();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<MedaRouteContext *> medaRoute();
    MedaRouteContext* medaRoute(size_t i);

   
  };

  MedaRoutesContext* medaRoutes();

  class  MedaRouteContext : public antlr4::ParserRuleContext {
  public:
    MedaRouteContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    DropletIDContext *dropletID();
    TimeConstraintContext *timeConstraint();
    std::vector<LocationContext *> location();
    LocationContext* location(size_t i);

   
  };

  MedaRouteContext* medaRoute();

  class  MixersContext : public antlr4::ParserRuleContext {
  public:
    MixersContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Mixers();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<MixerContext *> mixer();
    MixerContext* mixer(size_t i);

   
  };

  MixersContext* mixers();

  class  MixerContext : public antlr4::ParserRuleContext {
  public:
    MixerContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    MixerIDContext *mixerID();
    TimeRangeContext *timeRange();
    std::vector<PositionContext *> position();
    PositionContext* position(size_t i);

   
  };

  MixerContext* mixer();

  class  AnnotationsContext : public antlr4::ParserRuleContext {
  public:
    AnnotationsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Annotations();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<AreaAnnotationContext *> areaAnnotation();
    AreaAnnotationContext* areaAnnotation(size_t i);

   
  };

  AnnotationsContext* annotations();

  class  AreaAnnotationContext : public antlr4::ParserRuleContext {
  public:
    AreaAnnotationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<PositionContext *> position();
    PositionContext* position(size_t i);
    antlr4::tree::TerminalNode *LessThan();
    antlr4::tree::TerminalNode *AreaAnnotationText();

   
  };

  AreaAnnotationContext* areaAnnotation();

  class  PinActuationsContext : public antlr4::ParserRuleContext {
  public:
    PinActuationsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *PinActuations();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<PinActuationContext *> pinActuation();
    PinActuationContext* pinActuation(size_t i);

   
  };

  PinActuationsContext* pinActuations();

  class  PinActuationContext : public antlr4::ParserRuleContext {
  public:
    PinActuationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    PinIDContext *pinID();
    antlr4::tree::TerminalNode *Colon();
    antlr4::tree::TerminalNode *ActuationVector();

   
  };

  PinActuationContext* pinActuation();

  class  CellActuationsContext : public antlr4::ParserRuleContext {
  public:
    CellActuationsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *CellActuations();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<CellActuationContext *> cellActuation();
    CellActuationContext* cellActuation(size_t i);

   
  };

  CellActuationsContext* cellActuations();

  class  CellActuationContext : public antlr4::ParserRuleContext {
  public:
    CellActuationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    PositionContext *position();
    antlr4::tree::TerminalNode *Colon();
    antlr4::tree::TerminalNode *ActuationVector();

   
  };

  CellActuationContext* cellActuation();

  class  NetsContext : public antlr4::ParserRuleContext {
  public:
    NetsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Nets();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<NetContext *> net();
    NetContext* net(size_t i);

   
  };

  NetsContext* nets();

  class  NetContext : public antlr4::ParserRuleContext {
  public:
    NetContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<SourceContext *> source();
    SourceContext* source(size_t i);
    antlr4::tree::TerminalNode *Arrow();
    TargetContext *target();
    std::vector<antlr4::tree::TerminalNode *> Comma();
    antlr4::tree::TerminalNode* Comma(size_t i);

   
  };

  NetContext* net();

  class  SourceContext : public antlr4::ParserRuleContext {
  public:
    SourceContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    DropletIDContext *dropletID();
    PositionContext *position();
    TimeConstraintContext *timeConstraint();

   
  };

  SourceContext* source();

  class  TargetContext : public antlr4::ParserRuleContext {
  public:
    TargetContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    PositionContext *position();
    TimeConstraintContext *timeConstraint();

   
  };

  TargetContext* target();

  class  MedaNetsContext : public antlr4::ParserRuleContext {
  public:
    MedaNetsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *MedaNets();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<MedaNetContext *> medaNet();
    MedaNetContext* medaNet(size_t i);

   
  };

  MedaNetsContext* medaNets();

  class  MedaNetContext : public antlr4::ParserRuleContext {
  public:
    MedaNetContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<MedaSourceContext *> medaSource();
    MedaSourceContext* medaSource(size_t i);
    antlr4::tree::TerminalNode *Arrow();
    MedaTargetContext *medaTarget();
    std::vector<antlr4::tree::TerminalNode *> Comma();
    antlr4::tree::TerminalNode* Comma(size_t i);

   
  };

  MedaNetContext* medaNet();

  class  MedaSourceContext : public antlr4::ParserRuleContext {
  public:
    MedaSourceContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    DropletIDContext *dropletID();
    LocationContext *location();
    TimeConstraintContext *timeConstraint();

   
  };

  MedaSourceContext* medaSource();

  class  MedaTargetContext : public antlr4::ParserRuleContext {
  public:
    MedaTargetContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    LocationContext *location();
    TimeConstraintContext *timeConstraint();

   
  };

  MedaTargetContext* medaTarget();

  class  BlockagesContext : public antlr4::ParserRuleContext {
  public:
    BlockagesContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Blockages();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<BlockageContext *> blockage();
    BlockageContext* blockage(size_t i);

   
  };

  BlockagesContext* blockages();

  class  BlockageContext : public antlr4::ParserRuleContext {
  public:
    BlockageContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<PositionContext *> position();
    PositionContext* position(size_t i);
    TimingContext *timing();

   
  };

  BlockageContext* blockage();

  class  TimingContext : public antlr4::ParserRuleContext {
  public:
    TimingContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LParen();
    BeginTimingContext *beginTiming();
    antlr4::tree::TerminalNode *Comma();
    EndTimingContext *endTiming();
    antlr4::tree::TerminalNode *RParen();

   
  };

  TimingContext* timing();

  class  BeginTimingContext : public antlr4::ParserRuleContext {
  public:
    BeginTimingContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Integer();
    antlr4::tree::TerminalNode *Asterisk();

   
  };

  BeginTimingContext* beginTiming();

  class  EndTimingContext : public antlr4::ParserRuleContext {
  public:
    EndTimingContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Integer();
    antlr4::tree::TerminalNode *Asterisk();

   
  };

  EndTimingContext* endTiming();

  class  FluidsContext : public antlr4::ParserRuleContext {
  public:
    FluidsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Fluids();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<FluiddefContext *> fluiddef();
    FluiddefContext* fluiddef(size_t i);

   
  };

  FluidsContext* fluids();

  class  FluiddefContext : public antlr4::ParserRuleContext {
  public:
    FluiddefContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    FluidIDContext *fluidID();
    antlr4::tree::TerminalNode *Identifier();

   
  };

  FluiddefContext* fluiddef();

  class  PinAssignmentsContext : public antlr4::ParserRuleContext {
  public:
    PinAssignmentsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *PinAssignments();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<AssignmentContext *> assignment();
    AssignmentContext* assignment(size_t i);

   
  };

  PinAssignmentsContext* pinAssignments();

  class  AssignmentContext : public antlr4::ParserRuleContext {
  public:
    AssignmentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    PositionToPinAssignmentContext *positionToPinAssignment();
    PinToPositionsAssignmentContext *pinToPositionsAssignment();

   
  };

  AssignmentContext* assignment();

  class  PositionToPinAssignmentContext : public antlr4::ParserRuleContext {
  public:
    PositionToPinAssignmentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    PositionContext *position();
    PinIDContext *pinID();

   
  };

  PositionToPinAssignmentContext* positionToPinAssignment();

  class  PinToPositionsAssignmentContext : public antlr4::ParserRuleContext {
  public:
    PinToPositionsAssignmentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    PinIDContext *pinID();
    std::vector<PositionContext *> position();
    PositionContext* position(size_t i);
    std::vector<antlr4::tree::TerminalNode *> Comma();
    antlr4::tree::TerminalNode* Comma(size_t i);

   
  };

  PinToPositionsAssignmentContext* pinToPositionsAssignment();

  class  DropletsContext : public antlr4::ParserRuleContext {
  public:
    DropletsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Droplets();
    std::vector<antlr4::tree::TerminalNode *> Newlines();
    antlr4::tree::TerminalNode* Newlines(size_t i);
    antlr4::tree::TerminalNode *END();
    std::vector<DropToFluidContext *> dropToFluid();
    DropToFluidContext* dropToFluid(size_t i);

   
  };

  DropletsContext* droplets();

  class  DropToFluidContext : public antlr4::ParserRuleContext {
  public:
    DropToFluidContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    DropletIDContext *dropletID();
    FluidIDContext *fluidID();

   
  };

  DropToFluidContext* dropToFluid();

  class  DropletIDContext : public antlr4::ParserRuleContext {
  public:
    DropletIDContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Integer();

   
  };

  DropletIDContext* dropletID();

  class  FluidIDContext : public antlr4::ParserRuleContext {
  public:
    FluidIDContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Integer();

   
  };

  FluidIDContext* fluidID();

  class  PinIDContext : public antlr4::ParserRuleContext {
  public:
    PinIDContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Integer();

   
  };

  PinIDContext* pinID();

  class  MixerIDContext : public antlr4::ParserRuleContext {
  public:
    MixerIDContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Integer();

   
  };

  MixerIDContext* mixerID();

  class  PositionContext : public antlr4::ParserRuleContext {
  public:
    PositionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LParen();
    XposContext *xpos();
    antlr4::tree::TerminalNode *Comma();
    YposContext *ypos();
    antlr4::tree::TerminalNode *RParen();

   
  };

  PositionContext* position();

  class  LocationContext : public antlr4::ParserRuleContext {
  public:
    LocationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LParen();
    std::vector<PositionContext *> position();
    PositionContext* position(size_t i);
    antlr4::tree::TerminalNode *Comma();
    antlr4::tree::TerminalNode *RParen();

   
  };

  LocationContext* location();

  class  XposContext : public antlr4::ParserRuleContext {
  public:
    XposContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Integer();

   
  };

  XposContext* xpos();

  class  YposContext : public antlr4::ParserRuleContext {
  public:
    YposContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Integer();

   
  };

  YposContext* ypos();

  class  TimeConstraintContext : public antlr4::ParserRuleContext {
  public:
    TimeConstraintContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LBracket();
    antlr4::tree::TerminalNode *Integer();
    antlr4::tree::TerminalNode *RBracket();

   
  };

  TimeConstraintContext* timeConstraint();

  class  TimeRangeContext : public antlr4::ParserRuleContext {
  public:
    TimeRangeContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LBracket();
    std::vector<antlr4::tree::TerminalNode *> Integer();
    antlr4::tree::TerminalNode* Integer(size_t i);
    antlr4::tree::TerminalNode *Dash();
    antlr4::tree::TerminalNode *RBracket();

   
  };

  TimeRangeContext* timeRange();


private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

