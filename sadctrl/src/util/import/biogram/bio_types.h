// Copyright (c) 2018 Philipp Kiener
#pragma once

#include "Bio.h"
#include "BioLexerGrammar.h"

#include <string>
#include <vector>

namespace biogram
{
    /**
     * Enumeration describing the actuation state of a cell.
     */
    enum class actuation : uint8_t
    {
        on = 1, //!< the cell is active
        off = 0, //!< the cell is inactive
        dont_care = 255 //!< the actuation doesn't matter
    };

    /**
     * Parses an actuation vector from a given string.
     *
     * Characters that are illegal in the context of an actuation vector
     * are skipped.
     *
     * @param[in] str the string to parse
     * @return        the actuation vector
     *
     */
    std::vector<actuation> actuation_vector(const std::string& str);

    /**
     * Parses the actuation vector from a cell actuation context.
     *
     * @param[in] context the parsed context
     * @return            the actuation vector
     */
    std::vector<actuation> actuation_vector(Bio::CellActuationContext* context);

    /**
     * Parses the actuation vector from a pin actuation context.
     *
     * @param[in] context the parsed context
     * @return            the actuation vector
     */
    std::vector<actuation> actuation_vector(Bio::PinActuationContext* context);

    /**
     * A position on the BioViz coordinate field.
     *
     * NOTE: SADMIP-space differs from BioGram-space!
     * BioGram uses a typical coordinate system with (X, Y) coordinates,
     * where X is the "column" and y is the "row" and growth happens upwards
     * and to the left; coordinates are 1-based.
     * SADMIP uses a different model with (row, col) coordinates,
     * where row is the index of the row and col the index of the column and
     * growth happens downwards and to the right; coordinates are 0-based.
     *
     * Keep this in mind when converting between these two spaces.
     * In particular, a BioGram's X is SADMIP's Y and vice-versa!
     */
    struct position
    {
        uint8_t x; //! x coordinate
        uint8_t y; //! y coordinate
        
        /**
         * Create a position with zero coordinates.
         */
        position() : x(0), y(0) {}

        /**
         * Construct a position from a /PositionContext/.
         *
         * @param[in] context the parsed context
         */
        position(Bio::PositionContext* context);
    };

    /**
     * A time range within the experiment.
     */
    struct time_range
    {
        uint16_t begin; //!< beginning timestep
        uint16_t end; //!< ending timestep

        /**
         * Create a time range from and to zero.
         */
        time_range() : begin(0), end(0) {}

        /**
         * Construct a time range from a /TimeRangeContext/.
         *
         * @param[in] context the time-range context
         */
        time_range(Bio::TimeRangeContext* context);
    };

    /** 
     * A block of the grid of an experiment.
     */
    struct grid_block
    {
        position one; //!< one vertex of the block
        position two; //!< diagonally opposite vertex of the block

        /**
         * Create a gridblock with zero positions.
         */
        grid_block() : one(), two() {}

        /**
         * Construct a grid block from a /GridblockContext/.
         *
         * @param[in] context the grid block context
         */
        grid_block(Bio::GridblockContext* context);
    };

    /**
     * The route a droplet takes.
     *
     * Basically a vector of positions, with the index being the timestep.
     */
    struct route : public std::vector<position>
    {
        /**
         * Create an empty route.
         */
        route() : std::vector<position>() {}
        
        /**
         * Create a route from a /RouteContext/.
         *
         * @param[in] context the route context
         */
        route(Bio::RouteContext* context);
    };

    /**
     * Explicit actuations of a cell.
     */
    struct cell_actuations
    {
        position cell; //!< the cell whose actuations are described
        std::vector<actuation> actuations; //!< the vector of actuations

        /**
         * Create an empty cell actuation scheme.
         */
        cell_actuations() : cell(), actuations() {}

        /**
         * Construct the explicit cell actuations from a /CellActuationContext/.
         *
         * @param[in] context the context
         */
        cell_actuations(Bio::CellActuationContext* context);
    };

    /**
     * A assignment of a pin-ID to cells.
     */
    struct pin_assignments
    {
        uint32_t id; //!< ID of the pin
        std::vector<position> positions; //!< the assigned cells

        /**
         * Create an empty pin assignment.
         */
        pin_assignments() : id(0), positions() {}

        /**
         * Construct a pin assignment from a /PinToPositionsAssignmentContext/.
         *
         * @param[in] context the context
         */
        pin_assignments(Bio::PinToPositionsAssignmentContext* context);

        /**
         * Construct a pin assignment from a /PositionToPinAssignmentContext/.
         *
         * @param[in] context the context
         */
        pin_assignments(Bio::PositionToPinAssignmentContext* context);
    };

    /**
     * Explicit actuations of a pin, which cascades to multiple cells.
     */
    struct pin_actuations
    {
        uint32_t id; //!< ID of the pin whose actuations are described
        std::vector<actuation> actuations; //!< the actuation vector.

        /**
         * Create an empty pin actuation
         */
        pin_actuations() : id(0), actuations() {}

        /**
         * Construct a pin actuation from a /PinActuationContext/.
         *
         * @param[in] context the context
         */
        pin_actuations(Bio::PinActuationContext* context);
    };

    /** 
     * A mixer which mixes droplets and gets actuated for certain timesteps.
     */
    struct mixer
    {
        time_range time; //!< the range of timesteps where the mixer is active
        grid_block block; //!< block of cells to actuate

        /**
         * Create an empty mixer (with no time nor block).
         */
        mixer() : time(), block() {}

        /**
         * Construct a mixer from a /MixerContext/.
         *
         * @param[in] context the context
         */
        mixer(Bio::MixerContext* context);
    };
}
