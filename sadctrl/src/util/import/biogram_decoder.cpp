// Copyright (c) 2018 Philipp Kiener

#include "biogram_decoder.h"

#include "biogram/bio_types.h"

#include <antlr4-runtime.h>
#include "biogram/Bio.h"
#include "biogram/BioLexerGrammar.h"

#include <algorithm>
#include <array>
#include <cstdint>
#include <fstream>
#include <sstream>
#include <vector>

namespace
{
    biogram::grid_block determine_grid(Bio::BioContext* bio)
    {
        biogram::grid_block result;

        for (auto grid : bio->grid())
        {
            for (auto block : grid->gridblock())
            {
                biogram::grid_block current(block);

                std::array<uint8_t, 2> xs = {current.one.x, current.two.x};
                std::array<uint8_t, 2> ys = {current.one.y, current.two.y};

                if (xs[0] > xs[1]) std::swap(xs[0], xs[1]);
                if (ys[0] > ys[1]) std::swap(ys[0], ys[1]);

                detail::copy_if_less(result.one.x, xs[0]);
                detail::copy_if_less(result.one.y, ys[0]);

                detail::copy_if_greater(result.two.x, xs[1]);
                detail::copy_if_greater(result.two.y, ys[1]);
            }
        }

        return result;
    }

    std::vector<biogram::route> parse_routes(Bio::BioContext* bio)
    {
        std::vector<biogram::route> routes;

        for (auto context : bio->routes())
        {
            for (auto route : context->route())
            {
                routes.emplace_back(route);
            }
        }

        return routes;
    }

    std::vector<biogram::cell_actuations> parse_cell_actuations(Bio::BioContext* bio)
    {
        std::vector<biogram::cell_actuations> cell_actuations;

        for (auto context : bio->cellActuations())
        {
            for (auto cell : context->cellActuation())
            {
                cell_actuations.emplace_back(cell);
            };
        };

        return cell_actuations;
    }

    std::vector<biogram::pin_assignments> parse_pin_assignments(Bio::BioContext* bio)
    {
        std::vector<biogram::pin_assignments> pin_assignments;

        for (auto context : bio->pinAssignments())
        {
            for (auto assignment : context->assignment())
            {
                auto pin_to_pos = assignment->pinToPositionsAssignment();
                auto pos_to_pin = assignment->positionToPinAssignment();

                if (pin_to_pos)
                {
                    pin_assignments.emplace_back(pin_to_pos);
                }
                else if (pos_to_pin)
                {
                    pin_assignments.emplace_back(pos_to_pin);
                }
            }
        }

        return pin_assignments;
    }

    std::vector<biogram::pin_actuations> parse_pin_actuations(Bio::BioContext* bio)
    {
        std::vector<biogram::pin_actuations> pin_actuations;

        for (auto context : bio->pinActuations())
        {
            for (auto actuation : context->pinActuation())
            {
                pin_actuations.emplace_back(actuation);
            }
        }

        return pin_actuations;
    }

    std::vector<biogram::mixer> parse_mixers(Bio::BioContext* bio)
    {
        std::vector<biogram::mixer> mixers;

        for (auto context : bio->mixers())
        {
            for (auto mixer : context->mixer())
            {
                mixers.emplace_back(mixer);
            }
        }

        return mixers;
    }

    /**
    * A decoder to decipher a parsed /BioContext/ into a list of frames.
    */
    class biogram_decoder
    {
    public:
        /**
        * Construct the decoder from the given context.
        *
        * The context is parsed, so this is quite complex.
        *
        * @param[in] context the context to parse
        */
        biogram_decoder(Bio::BioContext* context);

        /**
        * Decode the parsed information into a list of frames.
        *
        * @return the list of frames
        */
        std::vector<sadmip::bit_matrix> frames() const;

    private:
        biogram::position to_sadmip(const biogram::position& pos) const;

        uint16_t determine_timestep() const;
        void check_routes(uint16_t timestep, sadmip::bit_matrix& frame) const;
        void check_mixers(uint16_t timestep, sadmip::bit_matrix& frame) const;
        void check_cell_actuations(uint16_t timestep, sadmip::bit_matrix& frame) const;
        void check_pin_actuations(uint16_t timestep, sadmip::bit_matrix& frame) const;

        uint8_t _rows; //!< the amount of rows needed for the experiment
        uint8_t _cols; //!< the amount of columns needed for the experiment

        /** a vector of all found routes */
        std::vector<biogram::route> _routes;

        /** a vector of all found mixers */
        std::vector<biogram::mixer> _mixers;

        /** a vector of all found cell actuations */
        std::vector<biogram::cell_actuations> _cell_actuations;

        /** a vector of all found pin-to-cell assignments */
        std::vector<biogram::pin_assignments> _pin_assignments;

        /** a vector of all pin actuations */
        std::vector<biogram::pin_actuations> _pin_actuations;
    };

    biogram_decoder::biogram_decoder(Bio::BioContext* context)
    {
        auto grid = determine_grid(context);
        _rows = grid.two.y - grid.one.y;
        _cols = grid.two.x - grid.one.x;

        _routes = parse_routes(context);
        _mixers = parse_mixers(context);
        _cell_actuations = parse_cell_actuations(context);
        _pin_assignments = parse_pin_assignments(context);
        _pin_actuations = parse_pin_actuations(context);
    }

    std::vector<sadmip::bit_matrix> biogram_decoder::frames() const
    {
        std::vector<sadmip::bit_matrix> frames;
        uint16_t timesteps = determine_timestep();

        for (uint16_t i = 0; i < timesteps; ++i)
        {
            sadmip::bit_matrix frame(_rows, _cols);

            check_routes(i, frame);
            check_mixers(i, frame);
            check_cell_actuations(i, frame);
            check_pin_actuations(i, frame);

            frames.push_back(frame);
        }

        return frames;
    }

    biogram::position biogram_decoder::to_sadmip(const biogram::position& pos) const
    {
        biogram::position result;

        result.x = _rows - pos.y;
        result.y = pos.x - 1;

        return result;
    }
    uint16_t biogram_decoder::determine_timestep() const
    {
        uint16_t result = 0;

        for (auto r : _routes)
        {
            detail::copy_if_greater(result, static_cast<uint16_t>(r.size()));
        }

        for (auto c : _cell_actuations)
        {
            detail::copy_if_greater(result, static_cast<uint16_t>(c.actuations.size()));
        }

        for (auto p : _pin_actuations)
        {
            detail::copy_if_greater(result, static_cast<uint16_t>(p.actuations.size()));
        }

        return result;
    }

    void biogram_decoder::check_routes(uint16_t timestep, sadmip::bit_matrix& frame) const
    {
        for (auto r : _routes)
        {
            if (r.size() <= timestep) continue;

            auto sadmip = to_sadmip(r[timestep]);
            frame.set(sadmip.x, sadmip.y, true);
        }
    }

    void biogram_decoder::check_mixers(uint16_t timestep, sadmip::bit_matrix& frame) const
    {
        for (auto m : _mixers)
        {
            if (m.time.begin > timestep || m.time.end < timestep) continue;

            auto sadmip_one = to_sadmip(m.block.one);
            auto sadmip_two = to_sadmip(m.block.two);

            uint8_t row_start = sadmip_one.x;
            uint8_t row_end = sadmip_two.x;

            uint8_t col_start = sadmip_one.y;
            uint8_t col_end = sadmip_two.y;

            if (row_end < row_start) std::swap(row_start, row_end);
            if (col_end < col_start) std::swap(col_start, col_end);

            for (uint8_t row = row_start; row <= row_end; ++row)
            {
                for (uint8_t col = col_start; col <= col_end; ++col)
                {
                    frame.set(row, col, true);
                }
            }
        }
    }

    void biogram_decoder::check_cell_actuations(uint16_t timestep, sadmip::bit_matrix& frame) const
    {
        for (auto c : _cell_actuations)
        {
            if (c.actuations.size() <= timestep) continue;
            auto sadmip = to_sadmip(c.cell);

            switch (c.actuations[timestep])
            {
                case biogram::actuation::on:
                    frame.set(sadmip.x, sadmip.y, true);
                    break;

                case biogram::actuation::off:
                    frame.set(sadmip.x, sadmip.y, false);
                    break;
            }
        }
    }

    void biogram_decoder::check_pin_actuations(uint16_t timestep, sadmip::bit_matrix& frame) const
    {
        for (auto p : _pin_actuations)
        {
            if (p.actuations.size() <= timestep) continue;
            auto id = p.id;
            auto act = p.actuations[timestep];

            for (auto asgn : _pin_assignments)
            {
                if (asgn.id != id) continue;

                for (auto cell : asgn.positions)
                {
                    auto sadmip = to_sadmip(cell);

                    switch (act)
                    {
                        case biogram::actuation::on:
                            frame.set(sadmip.x, sadmip.y, true);
                            break;

                        case biogram::actuation::off:
                            frame.set(sadmip.x, sadmip.y, false);
                            break;
                    }
                }
            }
        }
    }
}


bool decode_biogram(const std::string& filename, std::vector<sadmip::bit_matrix>& frames)
{
    using namespace antlr4;

    std::ifstream fstream;
    fstream.open(filename, std::ios_base::in);
    if (!fstream.is_open()) return false;

    class throw_on_error : public BaseErrorListener
    {
    public:
        virtual void syntaxError(
            Recognizer* recognizer,
            Token* offendingSymbol,
            size_t line,
            size_t charPositionInLine,
            const std::string& msg,
            std::exception_ptr e
        ) override
        {
            std::stringstream msg_writer;
            msg_writer << "line " << line << ":" << charPositionInLine << " "<< msg << "\n";

            throw std::runtime_error(msg_writer.str());
        }
    };

    try
    {
        throw_on_error listener;
        
        ANTLRInputStream input(fstream);
        BioLexerGrammar lexer(&input);
        CommonTokenStream tokens(&lexer);
        Bio parser(&tokens);

        lexer.addErrorListener(&listener);
        parser.addErrorListener(&listener);
        
        biogram_decoder decoder(parser.bio());
        frames = decoder.frames();
    }
    catch (const std::exception& e)
    {
        // Outputting directly to stdout is not a good idea, I know.
        // I'll not change it right now, however, as there will be no other
        // output at this moment.
        // I'll come back to this when I'm adding a third decoder.

        std::cout << e.what();
        fstream.close();
        return false;
    }

    fstream.close();
    return true;
}
