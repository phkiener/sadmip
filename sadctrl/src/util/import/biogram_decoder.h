// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <datatypes/bit_matrix.h>

#include <functional>
#include <string>
#include <vector>

/**
 * Decode a BioGram file into a list of frames.
 *
 * The files is attempted to be opened and parsed. Then, a collection of frames
 * is created as is described in the file.
 *
 * If an error occurs during opening, parsing or construction of the frames,
 * false is returned.
 *
 * @param[in]  filename name of the file to open
 * @param[out] frames   the decoded frames
 * @return              true on success, false on failure
 */
bool decode_biogram(
    const std::string& filename, 
    std::vector<sadmip::bit_matrix>& frames
);

namespace detail
{
    /**
     * Copy /compare/ to /value/ if /compare/ is less than /value/.
     *
     * @tparam T      the type to handle
     * @tparam comp   the comparator, defaults to std::less<T>
     * @param[in,out] value   the value to take initially and overwrite if needed
     * @param[in]     compare the value to compare with and store if needed
     * @return        true if /value/ was overwritten, false otherwise
     */
    template<typename T, typename comp = std::less<T>>
    bool copy_if_less(T& value, const T& compare)
    {
        comp less_than;
        if (less_than(compare, value))
        {
            value = compare;
            return true;
        }

        return false;
    }

    /**
     * Copy /compare/ to /value/ if /compare/ is greater than /value/.
     *
     * @tparam T      the type to handle
     * @tparam comp   the comparator, defaults to std::greater<T>
     * @param[in,out] value   the value to take initially and overwrite if needed
     * @param[in]     compare the value to compare with and store if needed
     * @return        true if /value/ was overwritten, false otherwise
     */
    template<typename T, typename comp = std::greater<T>>
    bool copy_if_greater(T& value, const T& compare)
    {
        comp greater_than;
        if (greater_than(compare, value))
        {
            value = compare;
            return true;
        }

        return false;
    }
}