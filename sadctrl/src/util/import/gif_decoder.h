// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <datatypes/bit_matrix.h>

#include <string>
#include <vector>

/**
 * Decode a GIF file into a list of frames.
 *
 * The file is attempted to be opened and decoded. Then, each frame is decoded
 * seperately, being first greyscaled and then binarized. Every white pixel
 * will be seen as an active cell and every black pixel will be seen as an
 * inactive cell. The thusly created /bit_matrix/ is appended to the given list
 * of matrices.
 *
 * If an error occurs during decoding, false is returned. This can be due to
 * the given file not existing, not being readable or not being able to be
 * decoded. Note that an error might occur while some frames have already
 * been processed; if false is returned, the state of /frames/ is undefined.
 *
 * @param[in]  filename               name of the file to open
 * @param[out] frames                 the decoded frames
 * @param[in]  binarization_threshold highest greyscale value for black pixels
 * @param[in]  inverse                instead of black pixels being off, black
 *                                    pixels shall denote on
 * @return                            true if decoding was successful, false
 *                                    if an error happened
 */
bool decode_gif(
    const std::string& filename, 
    std::vector<sadmip::bit_matrix>& frames, 
    uint8_t binarization_threshold = 127,
    bool inverse = false
);
