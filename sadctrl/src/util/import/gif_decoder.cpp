// Copyright (c) 2018 Philipp Kiener

#include "gif_decoder.h"

extern "C" 
{ 
#include <libnsgif.h> 
}

#include <cstdlib>
#include <fstream>

bool decode_gif(
    const std::string& filename, 
    std::vector<sadmip::bit_matrix>& frames, 
    uint8_t binarization_threshold,
    bool inverse
)
{
    std::fstream file(filename, std::ios_base::in | std::ios_base::binary);

    if (!file.is_open())
    {
        file.close();
        return false;
    }

    std::streamsize length = 0;
    file.seekg(0, file.end);
    length = file.tellg();
    file.seekg(0, file.beg);

    char* buffer = new char[length];
    file.read(buffer, length);

    if (file.gcount() != length || file.fail())
    {
        file.close();
        delete[] buffer;

        return false;
    }

    file.close();

    gif_animation anim;
    gif_bitmap_callback_vt callbacks;
    callbacks.bitmap_create = [](int width, int height)
    {
        return malloc(width * height * 4);
    };
    
    callbacks.bitmap_destroy = [](void* bitmap)
    {
        free(bitmap);
    };

    callbacks.bitmap_get_buffer = [](void* bitmap)
    {
        return reinterpret_cast<unsigned char*>(bitmap);
    };

    callbacks.bitmap_modified = nullptr;
    callbacks.bitmap_set_opaque = nullptr;
    callbacks.bitmap_test_opaque = nullptr;

    gif_create(&anim, &callbacks);

    gif_result result;

    do
    {
        result = gif_initialise(&anim, length, reinterpret_cast<unsigned char*>(buffer));

        if (result != GIF_OK && result != GIF_WORKING)
        {
            gif_finalise(&anim);
            delete[] buffer;

            return false;
        }
    } 
    while (result != GIF_OK);

    for (unsigned int i = 0; i < anim.frame_count; ++i)
    {
        result = gif_decode_frame(&anim, i);

        if (result != GIF_OK)
        {
            gif_finalise(&anim);
            delete[] buffer;

            return false;
        }

        uint8_t rows = static_cast<uint8_t>(anim.height);
        uint8_t cols = static_cast<uint8_t>(anim.width);

        unsigned char* bitmap = reinterpret_cast<unsigned char*>(anim.frame_image);
        sadmip::bit_matrix matrix(rows, cols);

        for (uint8_t row = 0; row < rows; ++row)
        {
            for (uint8_t col = 0; col < cols; ++col)
            {
                size_t idx = (row * anim.width + col) * 4;

                unsigned char red = bitmap[idx];
                unsigned char green = bitmap[idx + 1];
                unsigned char blue = bitmap[idx + 2];

                unsigned char greyscale = 0;
                greyscale += static_cast<unsigned char>(0.299 * red);
                greyscale += static_cast<unsigned char>(0.587 * green);
                greyscale += static_cast<unsigned char>(0.114 * blue);

                bool active = inverse ? greyscale < binarization_threshold
                    : greyscale > binarization_threshold;

                matrix.set(row, col, active);
            }
        }

        frames.push_back(matrix);
    }

    gif_finalise(&anim);
    delete[] buffer;

    return true;
}
