// Copyright (c) 2018 Philipp Kiener

#pragma once

#include <executor.h>

#include <message/message.h>
#include <message/command.h>

#include <communication/data_buffer.h>
#include <communication/transporter.h>

/**
 * A remote executor to execute commands on a remote device.
 *
 * This executor executes a command by sending it via transporter and
 * awaiting the answer from it. This answer is then returned.
 */
class remote_executor : public sadmip::executor
{
public:
    /**
     * Create a remote executor with the given transporter to use.
     *
     * The executor will use the given transporter to send commands
     * and receive answers.
     *
     * @param[in] transporter the transporter to use
     * @param[in] timeout     the timeout for the underlying buffer
     */
    remote_executor(sadmip::transporter& transporter, int timeout);

    std::unique_ptr<sadmip::message> execute(const sadmip::command& cmd) override;

    /**
     * Returns the last error that has happened, if any.
     *
     * @return the last error that has happened.
     */
    sadmip::deserialization_error error() const;

private:
    sadmip::data_buffer _buffer; //!< the data buffer
    sadmip::deserialization_error _last_error; //!< the error that happened
};
