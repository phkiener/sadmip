// Copyright (c) 2018 Philipp Kiener

#include "remote_executor.h"

#include <message/ack.h>
#include <message/command.h>
#include <message/error.h>

remote_executor::remote_executor(sadmip::transporter& transporter, int timeout)
    : _buffer(transporter, timeout), _last_error(sadmip::deserialization_error::NO_ERROR)
{

}

std::unique_ptr<sadmip::message> remote_executor::execute(const sadmip::command& cmd)
{
    cmd.serialize(_buffer);
    
    if (!_buffer.flush())
    {
        // command not (fully) sent.
        _last_error = sadmip::deserialization_error::NO_ERROR;
        return nullptr;
    }

    std::unique_ptr<sadmip::message> received;

    while (true)
    {
        sadmip::deserialization_error error;
        received = sadmip::message::deserialize(_buffer, &error);

        if (!received)
        {
            _last_error = error;
            return nullptr;
        }

        if (   received->type() == sadmip::message_type::ack
            || received->type() == sadmip::message_type::error)
        {
            _last_error = sadmip::deserialization_error::NO_ERROR;
            return received;
        }
    }
}

sadmip::deserialization_error remote_executor::error() const
{
    return _last_error;
}
