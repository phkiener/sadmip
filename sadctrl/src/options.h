// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <string>

/**
 * A struct containing all commandline-specifiable options.
 *
 * Each of these can be filled by the CLI-parser.
 */
struct commandline_options
{
    int timeout_ms = 5000; //!< the timeout for reading operations
    std::string device = ""; //!< the serial device to access
    char** oneshot_argv = NULL; //!< pointer to the first of extra args
    int oneshot_argc = 0; //!< number of extra arguments
};
