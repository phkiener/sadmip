// Copyright (c) 2018 Philipp Kiener

#include <normal_mode.h>

#include "format_answer.h"
#include <command_parser/command_parser.h>
#include <util/string_utils.h>
#include <util/remote_executor.h>
#include <serial.h>

#include <exception>
#include <iostream>
#include <memory>
#include <message/command.h>
#include <regex>

#ifdef __GNUC__
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#else
#include <filesystem>
namespace fs = std::experimental::filesystem;
#endif

#include <linenoise.h>

namespace
{
    /**
     * Runs the given command through the given executor and outputs the result to the given stream.
     *
     * The executor /exec/ is used to execute the command /cmd/, and the resulting
     * answer - be it ACK or ERR - is printed to /output/.
     * If an ACK was received, 0 is returned, denoting that the command has
     * been executed successfully. If an ERR was received, the error code is returned,
     * signalling an error. If no answer was received, -1 is returned.
     *
     * @param[in] exec   the executor to use
     * @param[in] cmd    the command to execute
     * @param[in] output the stream to write to
     * @return           0 on success, a positive integer if an ERR was received,
     *                   -1 on a timeout
     */
    int run(remote_executor& exec, sadmip::command& cmd, std::ostream& output)
    {
        using namespace normal_mode;

        auto answer = exec.execute(cmd);

        if (answer == nullptr)
        {
            switch (exec.error())
            {
                case sadmip::deserialization_error::HEADER_TIMEOUT:
                    output << "Timeout while receiving the header.\n";
                    break;

                case sadmip::deserialization_error::INVALID_HEADER:
                    output << "Cannot parse the received header.\n";
                    break;

                case sadmip::deserialization_error::PAYLOAD_TIMEOUT:
                    output << "Timeout while receiving the payload.\n";
                    break;

                case sadmip::deserialization_error::INVALID_PAYLOAD:
                    output << "Error while parsing the payload.\n";
                    break;

                default:
                    output << "Error while sending the command.\n";
                    break;
            }

            return -1;
        }

        output << *answer << "\n";

        if (answer->type() == sadmip::message_type::ack)
        {
            return 0;
        }
        else if (answer->type() == sadmip::message_type::error)
        {
            return (dynamic_cast<const sadmip::error&>(*answer)).error_code();
        }
        else
        {
            return -3;
        }
    }    
    
    /**
     * Fetch a line of user input.
     *
     * This includes using the arrow keys to navigate the line and a history.
     * The optionally specified prompt is printed before reading the line.
     *
     * @param[in] prompt a prompt to print
     * @return           the read line
     */
    std::string get_line(const std::string& prompt = "")
    {
        std::string line;
        std::unique_ptr<char[]> buf(::linenoise(prompt.c_str()));

        line.assign(buf.get());
        linenoiseHistoryAdd(buf.get());

        return line;
    }

    std::vector<std::string> command_names;
    void completionHook(char const* prefix, linenoiseCompletions* lc)
    {
        for (auto name : command_names)
        {
            if (prefix_of_icase(prefix, name))
            {
                linenoiseAddCompletion(lc, name.c_str());
            }
        }

        std::string incomplete_path(prefix);
        std::size_t last_dirsep = incomplete_path.find_last_of('/');

        std::string path = incomplete_path.substr(0, last_dirsep);
        std::string file_prefix = incomplete_path.substr(last_dirsep + 1);

        if (fs::exists(fs::path(path)))
        {
            for (const auto& sub : fs::directory_iterator(path))
            {
                std::string completion = sub.path().generic_string();
                if (completion.substr(0, strlen(prefix)).compare(prefix) != 0)
                {
                    continue;
                }

                if (sub.status().type() == fs::file_type::directory)
                {
                    completion += '/';
                }

                linenoiseAddCompletion(lc, completion.c_str());
            }
        }
    }
}

int normal_mode::main(commandline_options opts)
{
    serial connection(opts.device, 100);

    remote_executor exec(connection, opts.timeout_ms);
    command_parser parser;

    if (opts.oneshot_argv != NULL)
    {
        auto command = parser.parse(
            opts.oneshot_argc,
            opts.oneshot_argv
        );

        if (command)
        {
            return run(exec, *command, std::cout);
        }
        else
        {
            std::cout << "Cannot parse command.\n"
                << parser.error().what() << "\n";
            return -2;
        }

    }
    else
    {
        linenoiseHistorySetMaxLen(32);
        linenoiseSetCompletionCallback(completionHook);
        command_names = parser.subcommands();
        
        std::cout
            << "Starting interactive mode.\n"
            << "Type 'exit' or 'quit' to leave.\n"
            << "Type 'help' to get an overview of all possible commands.\n";

        while (true)
        {
            const auto exit_regex = std::regex("^(quit|exit)$", std::regex_constants::icase);
            const auto help_regex = std::regex("^help( (\\S+))?$", std::regex_constants::icase);
            std::smatch regex_match;

            std::string input = get_line("> ");

            if (std::regex_match(input, regex_match, exit_regex))
            {
                linenoiseHistoryFree();
                return 0;
            }

            if (std::regex_match(input, regex_match, help_regex))
            {
                std::string topic = regex_match.size() == 3 ? regex_match[2].str() : "";
                parser.help(std::cout, topic);
                continue;
            }
            
            try 
            {
                auto command = parser.parse(input);

                if (command)
                {
                    run(exec, *command, std::cout);
                }
                else
                {
                    std::cout << "Cannot parse command: "
                        << parser.error().what() << "\n";
                }
            }
            catch (const std::exception& e)
            {
                linenoiseHistoryFree();
                throw e;
            }
        }
    }
}
