// Copyright (c) 2018 Philipp Kiener

#pragma once

#include <message/ack.h>
#include <message/error.h>
#include <message/heartbeat.h>

#include <iostream>

namespace normal_mode
{
    /**
     * Prints a message to the given stream.
     *
     * @param[in] whence the stream to write to
     * @param[in] msg    the message to print
     * @return           the modified stream
     */
    std::ostream& operator<<(std::ostream& whence, const sadmip::message& msg);

    /**
     * Prints a generic ACK to the given stream.
     *
     * @param[in] whence the stream to write to
     * @param[in] ack    the ACK to print
     * @return           the modified stream
     */
    std::ostream& operator<<(std::ostream& whence, const sadmip::ack& ack);

    /**
     * Prints an ERR to the given stream.
     *
     * @param[in] whence the stream to write to
     * @param[in] err    the ERR to print
     * @return           the modified stream
     */
    std::ostream& operator<<(std::ostream& whence, const sadmip::error& err);

    /**
     * Prints a heartbeat to the given stream.
     *
     * @param[in] whence the stream to write to
     * @param[in] hbt    the heartbeat to print
     * @return           the modified stream
     */
    std::ostream& operator<<(std::ostream& whence, const sadmip::heartbeat& hbt);

}