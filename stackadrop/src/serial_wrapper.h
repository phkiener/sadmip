// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <communication/transporter.h>
#include <communication/data_buffer.h>

#include <cstdint>
#include <USBSerial.h>

/**
 * A wrapper around the USBDevice's USBSerial.
 *
 * This wrapper provides the interface to let the virtual serial port over USB
 * act as a /transporter/.
 */
class serial_wrapper : public sadmip::transporter
{
public:
    /**
     * Construct a wrapper around the virtual USB serial. 
     */
    serial_wrapper();

    size_t available() override;
    size_t write(const uint8_t* buf, size_t num) override;
    size_t read(uint8_t* buf, size_t num) override;

    void attach_buffer(sadmip::data_buffer* buf);

private:
    USBSerial _usb{0x1f00, 0x2012, 0x0001, false}; //!< the usb-serial handle
    sadmip::data_buffer* _buffer{nullptr}; //!< the buffer to store data in

    /**
     * Sends all data to the configured buffer.
     * 
     * If no data is configured, the data is kept.
     */
    void send_data_to_buffer();
};
