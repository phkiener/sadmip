// Copyright (c) 2018 Philipp Kiener

#include "peripherals/status_led.h"

extern status_led red;
extern status_led yellow;
extern status_led green;

extern "C" void HardFault_Handler()
{
    red.blink(0.1);
    yellow.on();
    green.off();
}

extern "C" void BusFault_Handler()
{
    red.blink(0.1);
    yellow.blink(0.1);
    green.off();
}

extern "C" void MemManage_Handler()
{
    red.blink(0.1);
    yellow.blink(0.8);
    green.on();
}

extern "C" void UsageFault_Handler()
{
    red.blink(0.1);
    yellow.off();
    green.off();
}
