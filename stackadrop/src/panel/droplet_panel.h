// Copyright (c) 2018 Philipp Kiener

#pragma once

#include <grid.h>

#include "mbed.h"

#include <cstdint>

/**
 * Implementation of the droplet panel.
 *
 * The panel will not activate the correct cells if the mounted and configured
 * panel do not match.
 */
class droplet_panel : public sadmip::grid
{
public:
    /**
     * Creates a panel accessing the given pins and ports.
     *
     * @param spi      the interface to transmit the row data over
     * @param latch    the latch pin to signal when the data shall be applied
     * @param blanking the blanking pin to signal when data shall be received
     */
    droplet_panel(SPI* spi, PinName latch, PinName blanking);

    bool display(const sadmip::bit_matrix& frame) override;

protected:
    /**
     * Retrieve the row and column of the cell to fetch for output on the pin.
     *
     * As the panels use different indexing for the cells due to the layout of
     * the circuit board, the /n/th bit is not neccessarily the /n/th cell in
     * the matrix. Derived classes implement this function to run the coordinate
     * converison.
     *
     * @param[in]  pin the number of the pin to check
     * @param[out] row the calculated row to get the state of
     * @param[out] col the calculated column to get the state of
     */
    virtual void retrieve_coordinates(uint8_t pin, uint8_t& row, uint8_t& col) = 0;

private:
    sadmip::bit_matrix _frame; //!< the local copy of the frame

    SPI* _spi; //!< the connection which is used to send data

    /**
     * The latch-pin to enable data applying.
     *
     * The latch shall be set to /1/ and immediately after to /0/ after all
     * needed data has been transmitted. This signals the panel to move the
     * data from the incoming register to the outgoing registers - which means
     * that it will be displayed on the panel.
     */
    DigitalOut _latch;

    /**
     * The blanking-pin to enable data transmission.
     *
     * The blanking shall be set to /1/ before sending data and shall be set to
     * /0/ again if all data has been sent.
     */
    DigitalOut _blanking;
};