// Copyright (c) 2018 Philipp Kiener

#include "led_panel_old.h"

namespace
{
const uint8_t MCP_OPCODEW = 0b01000000; //!< Opcode for MCP23S17 with LSB (bit0) set to write (0), address OR'd in later, bits 1-3
const uint8_t MCP_OPCODER = 0b01000001; //!< Opcode for MCP23S17 with LSB (bit0) set to read (1), address OR'd in later, bits 1-3
const uint8_t MCP_IOCON = 0x0A; //!< MCP23x17 Configuration Register
const uint8_t MCP_IODIRA = 0x00; //!<Config register for i/o
const uint8_t MCP_IODIRB = 0x01; //!< DDD
const uint8_t MCP_GPIOA = 0x12; //!< Opcode GPIOA
const uint8_t MCP_GPIOB = 0x13; //!< Opcode GPIOB
    
    uint8_t lo(uint16_t val)
    {
        return val & 0x00FF;
    }

    uint8_t hi(uint16_t val)
    {
        return val >> 8;
    }

    void spi_write(SPI* spi, uint8_t reg, uint16_t word)
    {
        spi->write(MCP_OPCODEW);
        spi->write(reg);
        spi->write(lo(word));
        spi->write(hi(word));
    }

    void spi_read_dummy(SPI* spi)
    {
        spi->write(MCP_OPCODER);
        spi->write(MCP_GPIOA);
        spi->write(0x00);
        spi->write(MCP_GPIOB);
        spi->write(0x00);
    }
}

led_panel_old::led_panel_old(SPI* spi, PinName chipsel, PinName latch, PinName blanking)
    : _spi(spi), _chipsel(chipsel), _latch(latch), _blanking(blanking), _row(0)
{
    memset(_rows, 0, 32);

    _chipsel = 1;
    _latch = 0;
    _blanking = 1;

    _chipsel = 0;
    spi_write(_spi, MCP_IODIRA, 0x0000);
    _chipsel = 1;

    _updater.attach(callback(this, &led_panel_old::update), 0.001);
}

bool led_panel_old::display(const sadmip::bit_matrix& frame)
{
    if (frame.rows() != 16 || frame.cols() != 16)
    {
        return false;
    }

    for (uint8_t i = 0; i < 16; ++i)
    {
        _rows[i] = 0;

        for (uint8_t j = 0; j < 16; ++j)
        {
            if (frame.at(i, j))
            {
                _rows[i] |= 1 << j;
            }
        }
    }

    return true;
}

void led_panel_old::update()
{
    uint16_t row_select = (_row < 8 ? 0x0001 << _row : 0x0100 << (15 - _row));
    uint16_t row_value = _rows[_row];
    
    // poor man's modulo
    _row += 1;
    _row &= 15;

    _blanking = 1;

    _chipsel = 0;
    spi_write(_spi, MCP_GPIOA, row_value);
    _chipsel = 1;

    _chipsel = 0;
    spi_read_dummy(_spi);
    _chipsel = 1;

    _latch = 1;
    _latch = 0;
    
    _chipsel = 0;
    spi_write(_spi, MCP_GPIOA, row_select);
    _chipsel = 1;

    _blanking = 0;
}
