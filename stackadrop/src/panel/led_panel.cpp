// Copyright (c) 2018 Philipp Kiener

#include "led_panel.h"

namespace
{
    void spi_write(SPI* spi, uint16_t word)
    {
        spi->write(word >> 8);
        spi->write(word & 0x00FF);
    }
}

led_panel::led_panel(SPI* spi, PinName latch, PinName blanking)
    : _spi(spi), _latch(latch), _blanking(blanking), _row(0)
{
    memset(_rows, 0, 32);

    _latch = 0;
    _blanking = 1;

    _updater.attach(callback(this, &led_panel::update), 0.001);
}

bool led_panel::display(const sadmip::bit_matrix& frame)
{
    if (frame.rows() != 16 || frame.cols() != 16)
    {
        return false;
    }

    memset(_rows, 0, 32);
    
    for (uint8_t i = 0; i < 16; ++i)
    {
        for (uint8_t j = 0; j < 16; ++j)
        {
            if (frame(i, j))
            {
                _rows[i] |= (uint16_t)(1 << j);
            }
        }
    }

    return true;
}

void led_panel::update()
{
    uint16_t row_select = 1 << _row;
    uint16_t row_value = _rows[_row];
    _row = _row == 15 ? 0 : _row + 1;
    
    _blanking = 1;

    spi_write(_spi, row_select);
    spi_write(_spi, row_value);

    _latch = 1;
    _latch = 0;

    _blanking = 0;
}
