// Copyright (c) 2018 Philipp Kiener

#pragma once

#include <grid.h>
#include "mbed.h"

#include <cstdint>

/**
 * Implementation of the old LED panel that can be mounted.
 *
 * Note that there exists another version of the /led_panel_old/, fittingly called
 * /led_panel/, which uses a slightly different code (and doesn't need an extra cable).
 *
 * The panel will not show the frames correctly if the mounted and configured panel
 * do not match.
 */
class led_panel_old : public sadmip::grid
{
public:
    /**
     * Creates an led panel accessing the given pins and ports.
     *
     * @param spi      the interface to transmit the row data over
     * @param chipsel  the pin to use to select the receiving chip
     * @param latch    the latch pin to signal when the data shall be applied
     * @param blanking the blanking pin to signal when data shall be received
     */
    led_panel_old(SPI* spi, PinName chipsel, PinName latch, PinName blanking);

    bool display(const sadmip::bit_matrix& frame) override;

protected:
    /**
     * Updates the display, displaying the next row.
     *
     * If the last row is reached, it'll be set to 0 for the next update.
     */
    void update();

private:
    uint16_t _rows[16]; //!< the local buffer
    uint8_t _row; //!< the current row

    Ticker _updater; //!< the row changing ticker

    SPI* _spi; //!< the connection which is used to send data

    /**
     * The chipsel-pin to switch the receiving chip.
     *
     * Because the old LED panel has two chips installed, this pin is needed
     * to switch between the pins when sending data.
     */
    DigitalOut _chipsel;

    /**
     * The latch-pin to enable data applying.
     *
     * The latch shall be set to /1/ and immediately after to /0/ after all
     * needed data has been transmitted. This signals the panel to move the
     * data from the incoming register to the outgoing registers - which means
     * that it will be displayed on the panel.
     */
    DigitalOut _latch;
    
    /**
     * The blanking-pin to enable data transmission.
     *
     * The blanking shall be set to /1/ before sending data and shall be set to
     * /0/ again if all data has been sent.
     */
    DigitalOut _blanking;
};
