// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "droplet_panel.h"
#include "mbed.h"

#include <cstdint>

/**
* Implementation of the droplet panel with triangular cells.
*
* The panel will not activate the correct cells if the mounted and configured
* panel do not match.
*/
class triangle_panel : public droplet_panel
{
public:    
    /**
    * Creates a panel with triangular cells accessing the given pins and ports.
    *
    * @param spi      the interface to transmit the row data over
    * @param latch    the latch pin to signal when the data shall be applied
    * @param blanking the blanking pin to signal when data shall be received
    */
    triangle_panel(SPI* spi, PinName latch, PinName blanking);

protected:
    void retrieve_coordinates(uint8_t pin, uint8_t& row, uint8_t& col) override;
};
