// Copyright (c) 2018 Philipp Kiener

#include "droplet_panel.h"

droplet_panel::droplet_panel(SPI* spi, PinName latch, PinName blanking)
    : _spi(spi), _latch(latch), _blanking(blanking), _frame(16, 16)
{
    // start with a clear frame
    for (uint8_t row = 0; row < 16; ++row)
    {
        _spi->write(0);
        _spi->write(0);
    }

    _latch = 0;
    _blanking = 1;
}

bool droplet_panel::display(const sadmip::bit_matrix& frame)
{
    // mirror the frame along X
    for (uint8_t row = 0; row < 16; ++row)
    {
        for (uint8_t col = 0; col < 16; ++col)
        {
            _frame.set(row, col, frame.at(row, 15 - col));
        }
    }

    for (uint8_t idx = 0; idx < 16 * 16; idx += 8)
    {
        uint8_t transmit = 0;

        for (uint8_t bit = 0; bit < 7; ++bit)
        {
            uint8_t row = 0;
            uint8_t col = 0;

            if (_frame(row, col))
            {
                transmit |= 1 << (7 - bit);
            }
        }

        _spi->write(transmit);
    }

    _latch = 1;
    _latch = 0;

    return true;
}
