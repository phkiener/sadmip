// Copyright (c) 2018 Philipp Kiener

#include "experiment.h"

#include "pins.h"

#include "panel/hexagon_panel.h"
#include "panel/square_panel.h"
#include "panel/triangle_panel.h"
#include "panel/led_panel.h"
#include "panel/led_panel_old.h"

#ifdef SAD_WITH_MEMORY_INFO
#include "mbed_stats.h"
#endif

namespace
{
    static PinName latch(LATCH);
    static PinName blanking(BLANKING);
    static PinName chipsel(CHIPSEL);
    static SPI* spi = new SPI(SPI_MOSI, SPI_MISO, SPI_CLCK);
}

experiment::experiment(status_led& led) : _led(led), context()
{
    register_gridtype<hexagon_panel>("hexagon", spi, latch, blanking);
    register_gridtype<square_panel>("square", spi, latch, blanking);
    register_gridtype<triangle_panel>("triangle", spi, latch, blanking);
    register_gridtype<led_panel>("led", spi, latch, blanking);
    register_gridtype<led_panel_old>("led_old", spi, chipsel, latch, blanking);

    set_frame(0, sadmip::bit_matrix(16, 16));
}

uint8_t experiment::rows() const
{
    return 16;
}

uint8_t experiment::cols() const
{
    return 16;
}

bool experiment::memory_info(
    uint32_t& stack_reserve,
    uint32_t& stack_max,
    uint32_t& heap_reserve,
    uint32_t& heap_current,
    uint32_t& heap_fails) const
{
#ifdef SAD_WITH_MEMORY_INFO
    mbed_stats_heap_t heap_stats;
    mbed_stats_stack_t stack_stats;

    mbed_stats_heap_get(&heap_stats);
    mbed_stats_stack_get(&stack_stats);

    stack_reserve = stack_stats.reserved_size;
    stack_max = stack_stats.max_size;

    heap_reserve = heap_stats.reserved_size;
    heap_current = heap_stats.total_size;
    heap_fails = heap_stats.alloc_fail_cnt;

    return true;
#else
    return false;
#endif
}

void experiment::on_playback_start()
{
    _ticker.attach(this, &experiment::next_frame, timestep() / 1000.f);
    _led.on();
}

void experiment::on_playback_stop()
{
    _ticker.detach();
}
