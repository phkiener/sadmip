// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "mbed.h"

/**
 * A class to control a status LED.
 *
 * The pin given in the constructor will be seen as LED and can be controlled
 * in various ways; it can be toggled on or off or set to blink continuously
 * or light up for a set interval of time.
 */
class status_led
{
public:
    /**
     * Create a wrapper for the given pin to use as LED.
     *
     * @param[in] pin the pin to send data to
     */
    status_led(PinName pin);

    /**
     * Turns the LED on.
     *
     * If /secs/ is non-zero, the LED will turn off itself after /secs/ seconds.
     * If /secs/ is negative, nothing will happen.
     * If /secs/ is zero, the LED will be turned on until manually turned off.
     *
     * @param[in] secs duration in seconds for the LED to shine
     */
    void on(float secs = 0.0);

    /**
     * Makes the LED blink with a given interval.
     *
     * /interval/ is the duration in seconds between each toggling of the state
     * (on/off). Zero or negative values are not permitted and result in nothing
     * being done. /secs/, if given, denotes the duration for which the LED
     * shall blink. If /zero/, it will blink until manually turned off. If negative,
     * nothing will happen. If positive and non-zero, it will blink for the
     * given number of seconds, then turn itself off.
     *
     * @param[in] interval seconds between each state-toggling of the LED
     * @param[in] secs     number of seconds before the LED turns itself off
     */
    void blink(float interval = 0.5, float secs = 0.0);

    /**
     * Turns the LED off.
     *
     * Either continuous or blinking light will be turned off.
     */
    void off();

private:
    DigitalOut _led; //!< the pin to send data to
    Ticker _blink; //!< the timer used for blinking
    Ticker _timer; //!< the timer used for duration
};
