// Copyright (c) 2018 Philipp Kiener

#include "status_led.h"

status_led::status_led(PinName pin)
    : _led(pin)
{

}

void status_led::on(float secs)
{
    if (secs < 0.0)
    {
        return;
    }

    if (secs > 0.0)
    {
        _timer.attach(callback(this, &status_led::off), secs);
    }

    _led = 1;
}

void status_led::blink(float interval, float secs)
{
    if (secs < 0.0 || interval <= 0.0)
    {
        return;
    }

    if (secs > 0.0)
    {
        _timer.attach(callback(this, &status_led::off), secs);
    }

    _blink.attach([this] {this->_led = !this->_led; }, interval);
    _led = 1;
}

void status_led::off()
{
    _led = 0;
    _blink.detach();
    _timer.detach();
}
