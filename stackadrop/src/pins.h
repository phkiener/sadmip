// Copyright (c) 2018 Philipp Kiener
#pragma once

#include "mbed.h"

#define RLED PC_4
#define YLED PC_5
#define GLED PB_0
#define BTN0 PB_10
#define BTN1 PB_1

#define LATCH PA_4
#define BLANKING PA_3
#define CHIPSEL PB_9
#define SPI_MOSI PA_7
#define SPI_MISO NC
#define SPI_CLCK PA_5
