// Copyright (c) 2018 Philipp Kiener

#include "serial_wrapper.h"

serial_wrapper::serial_wrapper()
{
    _usb.attach(this, &serial_wrapper::send_data_to_buffer);
}

size_t serial_wrapper::available()
{
    return static_cast<size_t>(_usb.available());
}

size_t serial_wrapper::write(const uint8_t* buf, size_t num)
{
    const size_t BLOCK_SIZE = 32;
    size_t i = 0;

    while (i < num)
    {
        uint16_t to_write = static_cast<uint16_t>(num - i > BLOCK_SIZE ? BLOCK_SIZE : num - i);
        if (_usb.writeBlock((uint8_t*)buf + i, to_write))
        {
            i += to_write;
        }
    }

    return i;
}

size_t serial_wrapper::read(uint8_t* buf, size_t num)
{
    size_t i = 0;

    while (i < num)
    {
        buf[i++] = _usb._getc();
    }

    return i;
}

void serial_wrapper::attach_buffer(sadmip::data_buffer* buf)
{
    _buffer = buf;
}

void serial_wrapper::send_data_to_buffer()
{
    while(_usb.readable() && _buffer != nullptr)
    {
        _buffer->append_to_readbuffer(_usb._getc());
    }
}