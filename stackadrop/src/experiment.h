// Copyright (c) 2018 Philipp Kiener

#pragma once

#include "peripherals/status_led.h"

#include <context.h>

#include "mbed.h"

#include <cstdint>

/**
 * An experiment captures the "state" of the StackADrop.
 *
 * The experiment serves as the context to execute commands on; it contains all
 * information about frames, configuration and timesteps as well as the means to
 * modify them.
 */
class experiment : public sadmip::context
{
public:
    /**
     * Initialize the experiment with the given pins to use as LEDs.
     *
     * The LEDs are used to display the status - running, stopped, etc.
     *
     * @param led the led to use when displaying the playing state
     */
    experiment(status_led& led);

    uint8_t rows() const override;
    uint8_t cols() const override;
    
    bool memory_info(
        uint32_t& stack_reserve,
        uint32_t& stack_max,
        uint32_t& heap_reserve,
        uint32_t& heap_current,
        uint32_t& heap_fails) const override;

protected:
    void on_playback_start() override;
    void on_playback_stop() override;

private:
    status_led& _led; //!< the green LED
    Ticker _ticker; //!< ticker to update the frame
};
