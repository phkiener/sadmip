// Copyright (c) 2018 Philipp Kiener
#include "mbed.h"

#ifdef SAD_SEND_HEARTBEAT
#include "rtc_api.h"
#endif

#include <sadmip.h>

#include "pins.h"
#include "experiment.h"
#include "serial_wrapper.h"
#include "peripherals/status_led.h"

#include <PinDetect.h>

// Outputs

status_led red(RLED);
status_led yellow(YLED);
status_led green(GLED);

// Inputs

PinDetect btn1(BTN1);
PinDetect btn0(BTN0);

// State
serial_wrapper wrapper;
sadmip::data_buffer data_stream(wrapper, -1000, false);
experiment context(green);
enum device_state {idle, receiving, working, sending} state;

int main()
{
    btn1.attach_asserted([] {context.seek_frame(1, sadmip::seek_origin::current);});
    btn0.attach_asserted([] {context.seek_frame(-1, sadmip::seek_origin::current);});

    btn1.attach_asserted_held([] {if (context.is_playing()) context.stop(); else context.play(false);});
    btn0.attach_asserted_held([] {context.stop(); context.seek_frame(0, sadmip::seek_origin::start);});

    btn1.setSampleFrequency();
    btn0.setSampleFrequency();

    wrapper.attach_buffer(&data_stream);
    context.set_grid("led_old");

    std::unique_ptr<sadmip::message> answer = nullptr;
    std::unique_ptr<sadmip::message> query = nullptr;
    sadmip::deserialization_error error;

#ifdef SAD_SEND_HEARTBEAT
    const sadmip::heartbeat hbt(sadmip::heartbeat::device_status::idle);
#endif

    yellow.blink(0.5);

    while (true)
    {
        answer = nullptr;
        query = nullptr;
        state = idle;

#ifdef SAD_SEND_HEARTBEAT
        size_t loopcounter = 0;
        const size_t threshold = 2'000'000;

        while (!data_stream.available())
        {
            loopcounter += 1;

            if (loopcounter >= threshold)
            {
                loopcounter = 0;
                data_stream << hbt;
                data_stream << sadmip::data_buffer_signal::flush;
            }
        }
#else
        while (!data_stream.available()) { /* wait for data */ };
#endif

        state = receiving;

        red.blink(0.5);
        query = sadmip::message::deserialize(data_stream, &error);
        red.off();

        state = working;

        if (query == nullptr || query->type() != sadmip::message_type::command)
        {
            answer = std::make_unique<sadmip::error>(sadmip::parse_error(0xFF));
        }
        else /* (query != nullptr && query->type() == sadmip::message_type::command) */
        {
            sadmip::command* command = (sadmip::command*)(query.get());
            answer = command->execute(context);

            if (answer == nullptr)
            {
                answer = std::make_unique<sadmip::error>(sadmip::not_applicable(query->opcode()));
            }
        }

        state = sending;
        red.blink(1);
        answer->serialize(data_stream);
        data_stream.flush();
        red.off();
    }

    while (1) {};
}
