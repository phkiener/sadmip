// Copyright (c) 2018 Philipp Kiener

#include <io/memory_writer.h>

sadmip::memory_writer::memory_writer(std::vector<uint8_t>& destination)
    : _resource(destination)
{

}

size_t sadmip::memory_writer::size() const
{
    return _resource.size();
}

void sadmip::memory_writer::put(uint8_t byte)
{
    _resource.push_back(byte);
}

void sadmip::memory_writer::write(const uint8_t* bytes, size_t size)
{
    _resource.reserve(_resource.capacity() + size);
    _resource.insert(_resource.cend(), bytes, bytes + size);
}
