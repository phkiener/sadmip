// Copyright (c) 2018 Philipp Kiener

#include <io/read_stream.h>
#include <utils/endian_helpers.h>

uint8_t sadmip::read_stream::get(bool* okay)
{
    uint8_t byte = 0;

    if (read(&byte, 1) != 1)
    {
        if (okay) *okay = false;
        return 0;
    }

    if (okay) *okay = true;
    return byte;
}

size_t sadmip::read_stream::read(std::vector<uint8_t>& dest, size_t num)
{
    if (num == 0) return 0;

    dest.clear();
    dest.resize(num);

    size_t num_read = read(dest.data(), num);
    
    // remove any non-read bytes
    if (num_read < num) dest.resize(num_read);

    return num_read;
}

std::vector<uint8_t> sadmip::read_stream::read(size_t num)
{
    if (num == 0) return std::vector<uint8_t>();
    
    std::vector<uint8_t> result;
    read(result, num);

    return result;
}

sadmip::read_stream::operator bool() const
{
    return good();
}

bool sadmip::read_stream::good() const
{
    return !_error;
}

bool sadmip::read_stream::bad() const
{
    return _error;
}

sadmip::read_stream& sadmip::read_stream::fail()
{
    _error = true;
    return *this;
}

sadmip::read_stream& sadmip::read_stream::success()
{
    _error = false;
    return *this;
}

sadmip::read_stream::read_stream()
    : _error(false)
{

}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, bool& value)
{
    bool okay = true;
    uint8_t read = stream.get(&okay);

    if (!okay)
    {
        return stream.fail();
    }

    value = read != 0;
    return stream.success();
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, int8_t& value)
{
    bool okay = true;
    uint8_t read = stream.get(&okay);

    if (!okay)
    {
        return stream.fail();
    }

    value = *reinterpret_cast<int8_t*>(&read);
    
    // Why not static_cast?
    // 
    // Because the range of a int8_t is [-127;127] and the range of a uint8_t
    // is [0;255] - I want the binary re-interpretation to make sure the
    // value is correctly transmitted.

    return stream.success();
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, uint8_t& value)
{
    bool okay = true;
    uint8_t read = stream.get(&okay);

    if (!okay)
    {
        return stream.fail();
    }

    value = read;
    return stream.success();
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, int16_t& value)
{
    const size_t size = sizeof(int16_t);

    uint8_t buf[size];
    if (stream.read(buf, size) != size)
    {
        return stream.fail();
    }

    int16_t raw = *(reinterpret_cast<int16_t*>(buf));
    value = from_big_endian(raw);

    return stream.success();
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, uint16_t& value)
{
    const size_t size = sizeof(uint16_t);

    uint8_t buf[size];
    if (stream.read(buf, size) != size)
    {
        return stream.fail();
    }

    uint16_t raw = *(reinterpret_cast<uint16_t*>(buf));
    value = from_big_endian(raw);

    return stream.success();
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, int32_t& value)
{
    const size_t size = sizeof(int32_t);

    uint8_t buf[size];
    if (stream.read(buf, size) != size)
    {
        return stream.fail();
    }

    int32_t raw = *(reinterpret_cast<int32_t*>(buf));
    value = from_big_endian(raw);

    return stream.success();
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, uint32_t& value)
{
    const size_t size = sizeof(uint32_t);

    uint8_t buf[size];
    if (stream.read(buf, size) != size)
    {
        return stream.fail();
    }

    uint32_t raw = *(reinterpret_cast<uint32_t*>(buf));
    value = from_big_endian(raw);

    return stream.success();
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, int64_t& value)
{
    const size_t size = sizeof(int64_t);

    uint8_t buf[size];
    if (stream.read(buf, size) != size)
    {
        return stream.fail();
    }

    int64_t raw = *(reinterpret_cast<int64_t*>(buf));
    value = from_big_endian(raw);

    return stream.success();
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, uint64_t& value)
{
    const size_t size = sizeof(uint64_t);

    uint8_t buf[size];
    if (stream.read(buf, size) != size)
    {
        return stream.fail();
    }

    uint64_t raw = *(reinterpret_cast<uint64_t*>(buf));
    value = from_big_endian(raw);

    return stream.success();
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, float& value)
{
    const size_t size = sizeof(float);

    uint8_t buf[size];
    if (stream.read(buf, size) != size)
    {
        return stream.fail();
    }

    float raw = *(reinterpret_cast<float*>(buf));
    value = from_big_endian(raw);

    return stream.success();
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, double& value)
{
    const size_t size = sizeof(double);

    uint8_t buf[size];
    if (stream.read(buf, size) != size)
    {
        return stream.fail();
    }

    double raw = *(reinterpret_cast<double*>(buf));
    value = from_big_endian(raw);

    return stream.success();

}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, std::string& str)
{
    std::vector<uint8_t> as_list;
    stream >> as_list;

    if (stream)
    {
        str = std::string(as_list.begin(), as_list.end());
    }
    
    return stream;
}
