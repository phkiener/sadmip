// Copyright (c) 2018 Philipp Kiener

#include <io/write_stream.h>
#include <utils/endian_helpers.h>

bool sadmip::write_stream::flush()
{
    return true;
}

void sadmip::write_stream::put(uint8_t byte)
{
    write(&byte, 1);
}

void sadmip::write_stream::write(const std::vector<uint8_t>& bytes)
{
    write(bytes.data(), bytes.size());
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const bool& value)
{
    stream.put(value ? 0xFF : 0);
    return stream;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const int8_t& value)
{
    stream.put(static_cast<int8_t>(value));
    return stream;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const uint8_t& value)
{
    stream.put(value);
    return stream;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const int16_t& value)
{
    const size_t size = sizeof value;

    int16_t converted = to_big_endian(value);
    stream.write(reinterpret_cast<uint8_t*>(&converted), size);
    return stream;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const uint16_t& value)
{
    const size_t size = sizeof value;

    uint16_t converted = to_big_endian(value);
    stream.write(reinterpret_cast<uint8_t*>(&converted), size);
    return stream;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const int32_t& value)
{
    const size_t size = sizeof value;

    int32_t converted = to_big_endian(value);
    stream.write(reinterpret_cast<uint8_t*>(&converted), size);
    return stream;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const uint32_t& value)
{
    const size_t size = sizeof value;

    uint32_t converted = to_big_endian(value);
    stream.write(reinterpret_cast<uint8_t*>(&converted), size);
    return stream;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const int64_t& value)
{
    const size_t size = sizeof value;

    int64_t converted = to_big_endian(value);
    stream.write(reinterpret_cast<uint8_t*>(&converted), size);
    return stream;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const uint64_t& value)
{
    const size_t size = sizeof value;

    uint64_t converted = to_big_endian(value);
    stream.write(reinterpret_cast<uint8_t*>(&converted), size);
    return stream;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const float& value)
{
    const size_t size = sizeof value;

    float converted = to_big_endian(value);
    stream.write(reinterpret_cast<uint8_t*>(&converted), size);
    return stream;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const double& value)
{
    const size_t size = sizeof value;

    double converted = to_big_endian(value);
    stream.write(reinterpret_cast<uint8_t*>(&converted), size);
    return stream;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const std::string& str)
{
    std::vector<uint8_t> as_list(str.begin(), str.end());
    return stream << as_list;
}
