// Copyright (c) 2018 Philipp Kiener

#include <io/memory_reader.h>

sadmip::memory_reader::memory_reader(const std::vector<uint8_t>& resource)
    : _resource(resource), _idx(0), read_stream()
{

}

size_t sadmip::memory_reader::read(uint8_t* dest, size_t num)
{
    size_t i = 0;
    while (i < num)
    {
        if (_idx >= _resource.size())
        {
            break;
        }

        dest[i++] = _resource[_idx++];
    }

    return i;
}
