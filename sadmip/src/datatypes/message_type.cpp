// Copyright (c) 2018 Philipp Kiener

#include <datatypes/message_type.h>

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const message_type& type)
{
    switch (type)
    {
        case message_type::command:   stream.put(1); break;
        case message_type::ack:       stream.put(2); break;
        case message_type::error:     stream.put(3); break;
        case message_type::heartbeat: stream.put(4); break;
    }

    return stream;
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, message_type& type)
{
    bool okay = true;
    uint8_t read = stream.get(&okay);

    if (!okay)
    {
        return stream.fail();
    }

    switch (read)
    {
        case 1:  type = message_type::command; break;
        case 2:  type = message_type::ack; break;
        case 3:  type = message_type::error; break;
        case 4:  type = message_type::heartbeat; break;
        default: return stream.fail();
    }

    return stream.success();
}
