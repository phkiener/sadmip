// Copyright (c) 2018 Philipp Kiener

#include <datatypes/bit_matrix.h>

#include <algorithm>
#include <cstring>
#include <cmath>

namespace
{
    uint8_t bytes_per_row(uint8_t cols)
    {
        // <https://stackoverflow.com/a/9194117>
        // round up to multiple of 8, divide by 8
        return ((cols + 7) & ~7) >> 3;
    }

    uint8_t bitmask(uint8_t col)
    {
        switch (col & 7)
        {
            case 0:  return 0b10000000;
            case 1:  return 0b01000000;
            case 2:  return 0b00100000;
            case 3:  return 0b00010000;
            case 4:  return 0b00001000;
            case 5:  return 0b00000100;
            case 6:  return 0b00000010;
            case 7:  return 0b00000001;
            default: return 0;
        }
    }
}

sadmip::bit_matrix::bit_matrix()
    : _rows(0), _cols(0), _data(nullptr), _datalen(0)
{

}

sadmip::bit_matrix::bit_matrix(uint8_t rows, uint8_t cols)
    : _rows(rows), _cols(cols), _data(nullptr)
{
    _datalen = _rows * bytes_per_row(_cols);

    if (_datalen > 0)
    {
        _data = new uint8_t[_datalen];
        ::memset(_data, 0, _datalen);
    }
}

sadmip::bit_matrix::bit_matrix(const bit_matrix& rhs)
    : _rows(rhs._rows), _cols(rhs._cols), _data(nullptr), _datalen(rhs._datalen)
{
    if (_datalen > 0)
    {
        _data = new uint8_t[_datalen];
        ::memcpy(_data, rhs._data, _datalen);
    }
}

sadmip::bit_matrix::bit_matrix(bit_matrix&& rhs)
    : _rows(rhs._rows), _cols(rhs._cols), _data(nullptr), _datalen(0)
{
    std::swap(_data, rhs._data);
    std::swap(_datalen, rhs._datalen);
}

sadmip::bit_matrix::~bit_matrix()
{
    if (_data != nullptr)
    {
        delete[] _data;
        _datalen = 0;
    }
}

sadmip::bit_matrix& sadmip::bit_matrix::operator=(const bit_matrix& rhs)
{
    _rows = rhs._rows;
    _cols = rhs._cols;

    if (_datalen == rhs._datalen)
    {
        ::memcpy(_data, rhs._data, _datalen);
    }
    else
    {
        if (_data != nullptr)
        {
            delete[] _data;
        }

        _datalen = rhs._datalen;

        if (_datalen > 0)
        {
            _data = new uint8_t[_datalen];
            ::memcpy(_data, rhs._data, _datalen);
        }
        else
        {
            _data = nullptr;
        }
    }

    return *this;
}

sadmip::bit_matrix& sadmip::bit_matrix::operator=(bit_matrix&& rhs)
{
    _rows = rhs._rows;
    _cols = rhs._cols;

    std::swap(_data, rhs._data);
    std::swap(_datalen, rhs._datalen);

    return *this;
}

bool sadmip::bit_matrix::at(uint8_t row, uint8_t col) const
{
    auto byte = byte_for(row, col);
    if (!byte) return false;

    return *byte & bitmask(col);
}

bool sadmip::bit_matrix::operator()(uint8_t row, uint8_t col) const
{
    return at(row, col);
}

void sadmip::bit_matrix::set(uint8_t row, uint8_t col, bool on)
{
    auto byte = byte_for(row, col);
    if (!byte) return;

    if (on)
    {
        *byte |= bitmask(col);
    }
    else
    {
        *byte &= ~bitmask(col);
    }
}

uint8_t sadmip::bit_matrix::rows() const
{
    return _rows;
}

uint8_t sadmip::bit_matrix::cols() const
{
    return _cols;
}

bool sadmip::bit_matrix::valid() const
{
    return _data != nullptr;
}

sadmip::bit_matrix& sadmip::bit_matrix::expand(uint8_t rows, uint8_t cols)
{
    if (rows <= _rows && cols <= _cols)
    {
        return *this;
    }

    uint16_t datalen = rows * bytes_per_row(cols);
    uint8_t* data = new uint8_t[datalen];
    ::memset(data, 0, datalen);

    for (uint8_t row = 0; row < _rows; ++row)
    {
        for (uint8_t byte = 0; byte < bytes_per_row(_cols); ++byte)
        {
            uint8_t* in_new = data + (row * bytes_per_row(cols)) + byte;
            uint8_t* in_old = byte_for(row, 0) + byte;
            
            *in_new = *in_old;
        }
    }

    delete[] _data;

    _rows = rows;
    _cols = cols;
    _data = data;
    _datalen = datalen;

    return *this;
}

uint8_t* sadmip::bit_matrix::byte_for(uint8_t row, uint8_t col) const
{
    if (row >= _rows || col >= _cols) return nullptr;
    
    return 
        _data                          // first byte of matrix
        + (row * bytes_per_row(_cols)) // first byte of /row/
        + (col >> 3);                  // byte containing /col/
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const bit_matrix& mat)
{
    if (mat.valid() && mat._datalen > 0)
    {
        stream << mat.rows() << mat.cols();
        stream.write(mat._data, mat._datalen);
    }
    else
    {
        stream << static_cast<uint8_t>(0) << static_cast<uint8_t>(0);
    }

    return stream;
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, bit_matrix& mat)
{
    if (stream >> mat._rows >> mat._cols)
    {
        if (mat._data != nullptr)
        {
            delete[] mat._data;
            mat._datalen = 0;
        }

        mat._datalen = mat._rows * bytes_per_row(mat._cols);

        if (mat._datalen > 0)
        {
            mat._data = new uint8_t[mat._datalen];
            auto read = stream.read(mat._data, mat._datalen);

            if (read != mat._datalen)
            {
                return stream.fail();
            }
        }

        return stream.success();
        
    }

    return stream.fail();
}
