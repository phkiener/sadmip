// Copyright (c) 2018 Philipp Kiener

#include <datatypes/panel_type.h>

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const panel_type& type)
{
    switch (type)
    {
        case panel_type::leds_old:       stream.put(0x11); break;
        case panel_type::leds:           stream.put(0x12); break;
        case panel_type::square_cells:   stream.put(0x21); break;
        case panel_type::triangle_cells: stream.put(0x22); break;
        case panel_type::hexagon_cells:  stream.put(0x23); break;
    }

    return stream;
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, panel_type& type)
{
    bool okay = true;
    uint8_t read = stream.get(&okay);

    if (!okay)
    {
        return stream.fail();
    }

    switch (read)
    {
        case 0x11: type = panel_type::leds_old; break;
        case 0x12: type = panel_type::leds; break;
        case 0x21: type = panel_type::square_cells; break;
        case 0x22: type = panel_type::triangle_cells; break;
        case 0x23: type = panel_type::hexagon_cells; break;
        default:   return stream.fail();
    }

    return stream.success();
}
