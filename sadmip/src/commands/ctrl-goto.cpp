// Copyright (c) 2018 Philipp Kiener

#include <commands/ctrl-goto.h>

#include <message/ack.h>
#include <message/error.h>

sadmip::commands::ctrl_goto::ctrl_goto(uint16_t index) 
    : _index(index), command(0x45)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::ctrl_goto::execute(context& ctxt)
{
    if (_index >= ctxt.frame_count())
    {
        return std::make_unique<error>(index_out_of_bounds(opcode()));
    }

    ctxt.seek_frame(_index, sadmip::seek_origin::start);
    return std::make_unique<ack>(opcode());
}

void sadmip::commands::ctrl_goto::write_payload(write_stream& stream) const
{
    stream << _index;
}

bool sadmip::commands::ctrl_goto::parse_payload(read_stream& stream)
{
    return stream >> _index;
}
