// Copyright (c) 2018 Philipp Kiener

#include <commands/set-path.h>

#include <message/ack.h>
#include <message/error.h>

namespace
{
    int8_t delta_row(sadmip::commands::set_path::direction d)
    {
        using namespace sadmip::commands;

        switch (d)
        {
            case set_path::direction::up:   return -1;
            case set_path::direction::down: return  1;
            default:                        return  0;
        }
    }
    
    int8_t delta_col(sadmip::commands::set_path::direction d)
    {
        using namespace sadmip::commands;

        switch (d)
        {
            case set_path::direction::left:  return -1;
            case set_path::direction::right: return  1;
            default:                         return  0;
        }
    }

    struct pos
    {
        uint8_t row;
        uint8_t col;
    };

    pos operator+(pos p, sadmip::commands::set_path::direction d)
    {
        uint8_t row = p.row + delta_row(d);
        uint8_t col = p.col + delta_col(d);

        return pos{row, col};
    }
}

sadmip::commands::set_path::set_path(uint8_t start_row, uint8_t start_col, bool append)
    : _startrow(start_row), _startcol(start_col), _append(append), command(0x22)
{

}

void sadmip::commands::set_path::add_move(direction dir)
{
    _moves.push_back(dir);
}

void sadmip::commands::set_path::set_moves(const std::vector<direction> moves)
{
    _moves = moves;
}

std::unique_ptr<sadmip::message> sadmip::commands::set_path::execute(context& ctxt)
{
    std::vector<pos> positions;
    positions.reserve(_moves.size() + 1);
    positions.push_back(std::move(pos { _startrow, _startcol }));

    for (direction dir : _moves)
    {
        pos p = positions.back() + dir;

        if (p.row >= ctxt.rows() || p.col >= ctxt.cols())
        {
            return std::make_unique<error>(index_out_of_bounds(opcode()));
        }

        positions.push_back(std::move(p));
    }

    uint16_t startframe = ctxt.frame_index();
    ctxt.set_cell(positions[0].row, positions[0].col, true);

    for (uint16_t i = 1; i < positions.size(); ++i)
    {
        uint16_t frame = startframe + i;
        if (frame >= ctxt.frame_count())
        {
            if (_append)
            {
                ctxt.append_frame(std::move(bit_matrix(ctxt.rows(), ctxt.cols())));
            }
            else
            {
                break;
            }
        }

        ctxt.set_cell(frame, positions[i].row, positions[i].col, true);
    }

    return std::make_unique<ack>(opcode());
}

void sadmip::commands::set_path::write_payload(write_stream& stream) const
{
    stream << _startrow << _startcol << _append << _moves;
}

bool sadmip::commands::set_path::parse_payload(read_stream& stream)
{
    return stream >> _startrow >> _startcol >> _append >> _moves;
}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, commands::set_path::direction direction)
{
    
    return stream << static_cast<uint8_t>(direction);
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, commands::set_path::direction& direction)
{
    using dir = commands::set_path::direction;

    bool okay = true;
    uint8_t byte = stream.get(&okay);

    if (!okay) return stream.fail();

    switch (byte)
    {
        case 0x00: direction = dir::still; break;
        case 0xF0: direction = dir::up;    break;
        case 0x10: direction = dir::down;  break;
        case 0x0F: direction = dir::left;  break;
        case 0x01: direction = dir::right; break;
        default:   direction = dir::still; break; 
    }

    return stream.success();
}
