// Copyright (c) 2018 Philipp Kiener

#include <commands/meta-framecount.h>

sadmip::commands::meta_framecount::meta_framecount()
    : command(0x83)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::meta_framecount::execute(context& context)
{
    return std::make_unique<result>(context.frame_count());
}

sadmip::commands::meta_framecount::result::result(uint16_t count)
    : _frames(count), ack(0x83)
{

}

uint16_t sadmip::commands::meta_framecount::result::count() const
{
    return _frames;
}

void sadmip::commands::meta_framecount::result::write_payload(write_stream& stream) const
{
    stream << _frames;
}

bool sadmip::commands::meta_framecount::result::parse_payload(read_stream& stream)
{
    return stream >> _frames;
}
