// Copyright (c) 2018 Philipp Kiener

#include <commands/set-matrix.h>

#include <message/ack.h>
#include <message/error.h>

sadmip::commands::set_matrix::set_matrix(const bit_matrix& mat, bool append)
    : _matrix(mat), _append(append), command(0x23)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::set_matrix::execute(context& ctxt)
{
    if (_append 
        ? ctxt.append_frame(std::move(_matrix)) 
        : ctxt.set_frame(std::move(_matrix)))
    {
        return std::make_unique<ack>(opcode());
    }
    else
    {
        return std::make_unique<error>(dimension_mismatch(opcode()));
    }
}

void sadmip::commands::set_matrix::write_payload(write_stream& stream) const
{
    stream << _append << _matrix;
}

bool sadmip::commands::set_matrix::parse_payload(read_stream& stream)
{
    return stream >> _append >> _matrix;
}
