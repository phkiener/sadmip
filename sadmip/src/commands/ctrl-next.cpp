// Copyright (c) 2018 Philipp Kiener

#include <commands/ctrl-next.h>

#include <message/ack.h>
#include <message/error.h>

sadmip::commands::ctrl_next::ctrl_next(uint8_t skip) 
    : _skip(skip), command(0x42)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::ctrl_next::execute(context& ctxt)
{
    if (ctxt.seek_frame(1 + _skip, seek_origin::current))
    {
        return std::make_unique<ack>(opcode());
    }
    else
    {
        return std::make_unique<error>(index_out_of_bounds(opcode()));
    }
}

void sadmip::commands::ctrl_next::write_payload(write_stream& stream) const
{
    stream << _skip;
}

bool sadmip::commands::ctrl_next::parse_payload(read_stream& stream)
{
    return stream >> _skip;
}
