// Copyright (c) 2018 Philipp Kiener

#include <commands/set-timestep.h>

#include <message/ack.h>
#include <message/error.h>

sadmip::commands::set_timestep::set_timestep(uint16_t timestep)
    : _timestep(timestep), command(0x29)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::set_timestep::execute(context& ctxt)
{
    if (_timestep != 0)
    {
        ctxt.set_timestep(_timestep);
        return std::make_unique<ack>(opcode());
    }
    else
    {
        return std::make_unique<error>(invalid_argument(opcode()));
    }
}

void sadmip::commands::set_timestep::write_payload(write_stream& stream) const
{
    stream << _timestep;
}

bool sadmip::commands::set_timestep::parse_payload(read_stream& stream)
{
    return stream >> _timestep;
}
