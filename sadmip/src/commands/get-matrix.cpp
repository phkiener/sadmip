// Copyright (c) 2018 Philipp Kiener

#include <commands/get-matrix.h>

#include <message/error.h>

sadmip::commands::get_matrix::get_matrix()
    : _current(true), _index(0), command(0x13)
{

}

sadmip::commands::get_matrix::get_matrix(uint16_t index)
    : _current(false), _index(index), command(0x13)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::get_matrix::execute(context& ctxt)
{
    auto frame = _current ? ctxt.frame() : ctxt.frame(_index);
    
    if (frame.valid())
    {
        return std::make_unique<result>(std::move(frame));
    }
    else
    {
        return std::make_unique<error>(index_out_of_bounds(opcode()));
    }
}

void sadmip::commands::get_matrix::write_payload(write_stream& stream) const
{
    stream << _current << _index;
}

bool sadmip::commands::get_matrix::parse_payload(read_stream& stream)
{
    return stream >> _current >> _index;
}

sadmip::commands::get_matrix::result::result(const bit_matrix& mat)
    : _matrix(mat), ack(0x13)
{

}

sadmip::commands::get_matrix::result::result(bit_matrix&& mat)
    : _matrix(std::move(mat)), ack(0x13)
{

}

const sadmip::bit_matrix& sadmip::commands::get_matrix::result::matrix() const
{
    return _matrix;
}

void sadmip::commands::get_matrix::result::write_payload(write_stream& stream) const
{
    stream << _matrix;
}

bool sadmip::commands::get_matrix::result::parse_payload(read_stream& stream)
{
    return stream >> _matrix;
}
