// Copyright (c) 2018 Philipp Kiener

#include <commands/set-cell.h>

#include <message/ack.h>
#include <message/error.h>

sadmip::commands::set_cell::set_cell(uint8_t row, uint8_t col, bool active)
    : _row(row), _col(col), _active(active), command(0x21)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::set_cell::execute(context& ctxt)
{
    if (ctxt.set_cell(_row, _col, _active))
    {
        return std::make_unique<ack>(opcode());
    }
    else
    {
        return std::make_unique<error>(index_out_of_bounds(opcode()));
    }
}

void sadmip::commands::set_cell::write_payload(write_stream& stream) const
{
    stream << _row << _col << _active;
}

bool sadmip::commands::set_cell::parse_payload(read_stream& stream)
{
    return stream >> _row >> _col >> _active;
}
