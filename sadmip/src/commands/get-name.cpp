// Copyright (c) 2018 Philipp Kiener

#include <commands/get-name.h>

sadmip::commands::get_name::get_name()
    : command(0x18)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::get_name::execute(context& ctxt)
{
    return std::make_unique<result>(ctxt.name());
}

sadmip::commands::get_name::result::result(const std::string& name)
    : _name(name), ack(0x18)
{

}

const std::string& sadmip::commands::get_name::result::name() const
{
    return _name;
}

void sadmip::commands::get_name::result::write_payload(write_stream& stream) const
{
    stream << _name;
}

bool sadmip::commands::get_name::result::parse_payload(read_stream& stream)
{
    return stream >> _name;
}
