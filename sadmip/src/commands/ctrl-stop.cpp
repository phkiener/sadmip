// Copyright (c) 2018 Philipp Kiener

#include <commands/ctrl-stop.h>

#include <message/ack.h>

sadmip::commands::ctrl_stop::ctrl_stop()
    : command(0x44)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::ctrl_stop::execute(context& ctxt)
{
    ctxt.stop();
    return std::make_unique<ack>(opcode());
}
