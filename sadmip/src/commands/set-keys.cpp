// Copyright (c) 2018 Philipp Kiener

#include <commands/set-keys.h>

#include <message/ack.h>
#include <message/error.h>

sadmip::commands::set_keys::set_keys(mode mode, const std::vector<std::string>& keys)
    : _mode(mode), _keys(keys), command(0x2B)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::set_keys::execute(context& context)
{
    std::unique_ptr<message> result;
    
    switch (_mode) 
    {
        case mode::add:
            for (auto key : _keys)
            {
                if (!context.add_meta(key))
                {
                    result = std::make_unique<error>(invalid_argument(opcode()));
                    break;
                }
            }

            result = std::make_unique<ack>(opcode());
            break;

        case mode::erase:
            for (auto key : _keys)
            {
                if (!context.remove_meta(key))
                {
                    result = std::make_unique<error>(unknown_key(opcode()));
                    break;
                }

            }

            result = std::make_unique<ack>(opcode());
            break;

        case mode::replace:
            context.clear_meta();
            for (auto key : _keys)
            {
                context.add_meta(key);
            }
            result = std::make_unique<ack>(opcode());

            break;
        
        default: result = std::make_unique<error>(not_applicable(opcode()));
    }

    return result;
}

void sadmip::commands::set_keys::write_payload(write_stream& stream) const
{
    stream << static_cast<uint8_t>(_mode) << _keys;
}

bool sadmip::commands::set_keys::parse_payload(read_stream& stream)
{
    uint8_t mode;

    if (stream >> mode >> _keys)
    {
        switch (mode)
        {
            case 1:
                _mode = mode::add;
                return stream.success();

            case 2:
                _mode = mode::erase;
                return stream.success();

            case 3:
                _mode = mode::replace;
                return stream.success();

        }
    }

    return stream.fail();
}
