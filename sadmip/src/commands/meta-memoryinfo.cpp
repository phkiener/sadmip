// Copyright (c) 2018 Philipp Kiener

#include <commands/meta-memoryinfo.h>
#include <message/error.h>

sadmip::commands::meta_memoryinfo::meta_memoryinfo()
    : command(0x84)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::meta_memoryinfo::execute(context& context)
{
    auto ack = std::make_unique<result>();

    if (context.memory_info(
        ack->_diagnostics[0],
        ack->_diagnostics[1],
        ack->_diagnostics[2],
        ack->_diagnostics[3],
        ack->_diagnostics[4]))
    {
        return ack;
    }
    else
    {
        return std::make_unique<error>(not_applicable(opcode()));
    }
}

sadmip::commands::meta_memoryinfo::result::result()
    : ack(0x84)
{

}


void sadmip::commands::meta_memoryinfo::result::write_payload(write_stream& stream) const
{
    stream << _diagnostics[0] << _diagnostics[1] << _diagnostics[2] << _diagnostics[3] << _diagnostics[4];
}

bool sadmip::commands::meta_memoryinfo::result::parse_payload(read_stream& stream)
{
    return stream >> _diagnostics[0] >> _diagnostics[1] >> _diagnostics[2] >> _diagnostics[3] >> _diagnostics[4];
}
