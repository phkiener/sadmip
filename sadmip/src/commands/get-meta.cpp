// Copyright (c) 2018 Philipp Kiener

#include <commands/get-meta.h>

sadmip::commands::get_meta::result::result()
    : ack(0x1A)
{

}

void sadmip::commands::get_meta::result::add_meta(const std::string& key, const std::string& value)
{
    _meta[key] = value;
}

const std::map<std::string, std::string>& sadmip::commands::get_meta::result::meta() const
{
    return _meta;
}

void sadmip::commands::get_meta::result::write_payload(write_stream& stream) const
{
    std::vector<std::string> keys;
    std::vector<std::string> values;

    for (auto pair : _meta)
    {
        keys.push_back(pair.first);
        values.push_back(pair.second);
    }

    stream << keys << values;
}

bool sadmip::commands::get_meta::result::parse_payload(read_stream& stream)
{
    std::vector<std::string> keys;
    std::vector<std::string> values;

    if (stream >> keys >> values)
    {
        if (keys.size() != values.size())
        {
            return stream.fail();
        }

        for (size_t i = 0; i < keys.size(); ++i)
        {
            _meta[keys.at(i)] = values.at(i);
        }

        return stream.success();
    }

    return stream.fail();
}

sadmip::commands::get_meta::get_meta()
    : command(0x1A)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::get_meta::execute(context& context)
{
    auto res = std::make_unique<result>();

    for (const auto& key : context.meta_keys())
    {
        res->add_meta(key, context.get_meta(key));
    }

    return res;
}
