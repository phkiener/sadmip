// Copyright (c) 2018 Philipp Kiener

#include <commands/get-keys.h>

sadmip::commands::get_keys::result::result(const std::vector<std::string>& keys)
    : _keys(keys), ack(0x1B)
{

}

const std::vector<std::string>& sadmip::commands::get_keys::result::keys() const
{
    return _keys;
}

void sadmip::commands::get_keys::result::write_payload(write_stream& stream) const
{
    stream << _keys;
}

bool sadmip::commands::get_keys::result::parse_payload(read_stream& stream)
{
    return stream >> _keys;
}

sadmip::commands::get_keys::get_keys()
    : command(0x1B)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::get_keys::execute(context& context)
{
    return std::make_unique<result>(std::move(context.meta_keys()));
}
