// Copyright (c) 2018 Philipp Kiener

#include <commands/meta-configuregrid.h>

#include <message/ack.h>
#include <message/error.h>

sadmip::commands::meta_configuregrid::meta_configuregrid()
    : _report(true), _gridtype(""), command(0x81)
{
}

sadmip::commands::meta_configuregrid::meta_configuregrid(const std::string& gridtype)
    : _report(false), _gridtype(gridtype), command(0x81)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::meta_configuregrid::execute(context& ctxt)
{
    if (_report)
    {
        return std::make_unique<result>(ctxt.grid_types());
    }
    else
    {
        if (ctxt.set_grid(_gridtype))
        {
            return std::make_unique<result>();
        }
        else
        {
            return std::make_unique<error>(not_applicable(opcode()));
        }
    }
}

void sadmip::commands::meta_configuregrid::write_payload(write_stream& stream) const
{
    if (_report) return;
    
    stream << _gridtype;
}

bool sadmip::commands::meta_configuregrid::parse_payload(read_stream& stream)
{
    if (stream >> _gridtype)
    {
        _report = false;
    }
    else
    {
        _report = true;
        _gridtype = "";
    }

    return stream.success();
}

sadmip::commands::meta_configuregrid::result::result()
    : _report(false), _types(), ack(0x81)
{

}

sadmip::commands::meta_configuregrid::result::result(const std::vector<std::string>& types)
    : _report(true), _types(types), ack(0x81)
{

}

const std::vector<std::string>& sadmip::commands::meta_configuregrid::result::types() const
{
    return _types;
}

void sadmip::commands::meta_configuregrid::result::write_payload(write_stream& stream) const
{
    if (_report) stream << _types;
}

bool sadmip::commands::meta_configuregrid::result::parse_payload(read_stream& stream)
{
    if (stream >> _types)
    {
        _report = true;
    }
    else
    {
        _report = false;
        _types.clear();
    }

    return stream.success();
}
