// Copyright (c) 2018 Philipp Kiener

#include <commands/set-meta.h>

#include <message/ack.h>
#include <message/error.h>

sadmip::commands::set_meta::set_meta()
    : command(0x2A)
{

}

void sadmip::commands::set_meta::add_pair(const std::string& key, const std::string& value)
{
    _keys.push_back(key);
    _values.push_back(value);
}

std::unique_ptr<sadmip::message> sadmip::commands::set_meta::execute(context& context)
{
    if (_keys.size() != _values.size())
    {
        return std::make_unique<error>(dimension_mismatch(opcode()));
    }

    context.clear_meta();

    for (size_t i = 0; i < _keys.size() && i < _values.size(); ++i)
    {
        context.add_meta(_keys.at(i), _values.at(i));
    }

    return std::make_unique<ack>(opcode());
}

void sadmip::commands::set_meta::write_payload(write_stream& stream) const
{
    stream << _keys << _values;
}

bool sadmip::commands::set_meta::parse_payload(read_stream& stream)
{
    std::vector<std::string> keys;
    std::vector<std::string> values;

    if (stream >> keys >> values)
    {
        if (keys.size() != values.size())
        {
            return stream.fail();
        }

        _keys = keys;
        _values = values;

        return stream.success();
    }

    return stream.fail();
}
