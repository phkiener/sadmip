// Copyright (c) 2018 Philipp Kiener

#include <commands/ctrl-start.h>
#include <message/ack.h>

sadmip::commands::ctrl_start::ctrl_start(uint16_t timestep, bool loop) 
    : _loop(loop), _timestep(timestep), command(0x41)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::ctrl_start::execute(context& ctxt)
{
    if (_timestep != 0)
    {
        ctxt.set_timestep(_timestep);
    }

    ctxt.stop();
    ctxt.play(_loop);

    return std::make_unique<ack>(opcode());
}

void sadmip::commands::ctrl_start::write_payload(write_stream& stream) const
{
    stream << _loop << _timestep;
}

bool sadmip::commands::ctrl_start::parse_payload(read_stream& stream)
{
    return stream >> _loop >> _timestep;
}
