// Copyright (c) 2018 Philipp Kiener

#include <commands/get-timestep.h>

sadmip::commands::get_timestep::get_timestep()
    : command(0x19)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::get_timestep::execute(context& ctxt)
{
    return std::make_unique<result>(ctxt.timestep());
}

sadmip::commands::get_timestep::result::result(uint16_t timestep)
    : _timestep(timestep), ack(0x19)
{

}

uint16_t sadmip::commands::get_timestep::result::timestep() const
{
    return _timestep;
}

void sadmip::commands::get_timestep::result::write_payload(write_stream& stream) const
{
    stream << _timestep;
}

bool sadmip::commands::get_timestep::result::parse_payload(read_stream& stream)
{
    return stream >> _timestep;
}
