// Copyright (c) 2018 Philipp Kiener

#include <commands/ctrl-reset.h>
#include <message/ack.h>

sadmip::commands::ctrl_reset::ctrl_reset(bool continue_playing) 
    : _continue_playing(continue_playing), command(0x46)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::ctrl_reset::execute(context& ctxt)
{
    if (!_continue_playing)
    {
        ctxt.stop();
    }

    ctxt.seek_frame(0, seek_origin::start);
    return std::make_unique<ack>(opcode());
}

void sadmip::commands::ctrl_reset::write_payload(write_stream& stream) const
{
    stream << _continue_playing;
}

bool sadmip::commands::ctrl_reset::parse_payload(read_stream& stream)
{
    return stream >> _continue_playing;
}
