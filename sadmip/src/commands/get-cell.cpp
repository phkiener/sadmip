// Copyright (c) 2018 Philipp Kiener

#include <commands/get-cell.h>

#include <message/error.h>

sadmip::commands::get_cell::get_cell(uint8_t row, uint8_t col)
    : _row(row), _col(col), command(0x11)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::get_cell::execute(context& ctxt)
{
    if (_row >= ctxt.rows() || _col >= ctxt.cols())
    {
        return std::make_unique<error>(index_out_of_bounds(opcode()));
    }
    else
    {
        bool active = ctxt.cell(_row, _col);
        return std::make_unique<result>(active);
    }
}

void sadmip::commands::get_cell::write_payload(write_stream& stream) const
{
    stream << _row << _col;
}

bool sadmip::commands::get_cell::parse_payload(read_stream& stream)
{
    return stream >> _row >> _col;
}

sadmip::commands::get_cell::result::result(bool active)
    : _active(active), ack(0x11)
{

}

bool sadmip::commands::get_cell::result::active() const
{
    return _active;
}

void sadmip::commands::get_cell::result::write_payload(write_stream& stream) const
{
    stream << _active;
}

bool sadmip::commands::get_cell::result::parse_payload(read_stream& stream)
{
    return stream >> _active;
}
