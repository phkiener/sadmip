// Copyright (c) 2018 Philipp Kiener

#include <commands/set-name.h>

#include <message/ack.h>

sadmip::commands::set_name::set_name(const std::string& name)
    : _name(name), command(0x28)
{

}
std::unique_ptr<sadmip::message> sadmip::commands::set_name::execute(context& ctxt)
{
    ctxt.set_name(_name);
    return std::make_unique<ack>(opcode());
}

void sadmip::commands::set_name::write_payload(write_stream& stream) const
{
    stream << _name;
}

bool sadmip::commands::set_name::parse_payload(read_stream& stream)
{
    return stream >> _name;
}
