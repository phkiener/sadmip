// Copyright (c) 2018 Philipp Kiener

#include <commands/get-experiment.h>

sadmip::commands::get_experiment::get_experiment()
    : command(0x14)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::get_experiment::execute(context& ctxt)
{
    auto ret = std::make_unique<result>(ctxt.name(), ctxt.timestep());

    for (uint16_t i = 0; i < ctxt.frame_count(); ++i)
    {
        ret->_frames.emplace_back(std::move(ctxt.frame(i)));
    }

    return ret;
}

sadmip::commands::get_experiment::result::result(const std::string& name, uint16_t timestep)
    : _name(name), _timestep(timestep), ack(0x14)
{

}

const std::string& sadmip::commands::get_experiment::result::name() const
{
    return _name;
}

uint16_t sadmip::commands::get_experiment::result::timestep() const
{
    return _timestep;
}

const std::vector<sadmip::bit_matrix>& sadmip::commands::get_experiment::result::frames() const
{
    return _frames;
}

void sadmip::commands::get_experiment::result::write_payload(write_stream& stream) const
{
    stream << _name << _timestep << _frames;
}

bool sadmip::commands::get_experiment::result::parse_payload(read_stream& stream)
{
    return stream >> _name >> _timestep >> _frames;
}
