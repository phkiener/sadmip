// Copyright (c) 2018 Philipp Kiener

#include <commands/meta-dimensions.h>

sadmip::commands::meta_dimensions::meta_dimensions()
    : command(0x85)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::meta_dimensions::execute(context& context)
{
    return std::make_unique<result>(context.rows(), context.cols());
}

sadmip::commands::meta_dimensions::result::result(uint8_t rows, uint8_t cols)
    : _rows(rows), _cols(cols), ack(0x85)
{

}

uint8_t sadmip::commands::meta_dimensions::result::rows() const
{
    return _rows;
}

uint8_t sadmip::commands::meta_dimensions::result::cols() const
{
    return _cols;
}

void sadmip::commands::meta_dimensions::result::write_payload(write_stream& stream) const
{
    stream << _rows << _cols;
}

bool sadmip::commands::meta_dimensions::result::parse_payload(read_stream& stream)
{
    return stream >> _rows >> _cols;
}
