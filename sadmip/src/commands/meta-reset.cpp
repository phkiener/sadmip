// Copyright (c) 2018 Philipp Kiener

#include <commands/meta-reset.h>

#include <message/ack.h>

sadmip::commands::meta_reset::meta_reset()
    : command(0x82)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::meta_reset::execute(context& context)
{
    context.reset();
    return std::make_unique<ack>(opcode());
}
