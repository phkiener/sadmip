// Copyright (c) 2018 Philipp Kiener

#include <commands/get-value.h>

#include <message/error.h>

sadmip::commands::get_value::get_value(const std::string& key)
    : _key(key), command(0x1C)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::get_value::execute(context& context)
{
    std::string value = context.get_meta(_key);

    if (value.empty())
    {
        return std::make_unique<error>(unknown_key(opcode()));
    }
    else
    {
        return std::make_unique<result>(value);
    }
}

void sadmip::commands::get_value::write_payload(write_stream& stream) const
{
    stream << _key;
}

bool sadmip::commands::get_value::parse_payload(read_stream& stream)
{
    return stream >> _key;
}

sadmip::commands::get_value::result::result(const std::string& value)
    : _value(value), ack(0x1C)
{

}

const std::string& sadmip::commands::get_value::result::value() const
{
    return _value;
}

void sadmip::commands::get_value::result::write_payload(write_stream& stream) const
{
    stream << _value;
}

bool sadmip::commands::get_value::result::parse_payload(read_stream& stream)
{
    return stream >> _value;
}