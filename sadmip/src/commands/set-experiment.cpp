// Copyright (c) 2018 Philipp Kiener

#include <commands/set-experiment.h>

#include <message/ack.h>
#include <message/error.h>

sadmip::commands::set_experiment::set_experiment(const std::string& name, uint16_t timestep)
    : _name(name), _timestep(timestep), command(0x24)
{
    _autostart = false;
    _loop = false;
}

void sadmip::commands::set_experiment::set_frames(const std::vector<sadmip::bit_matrix>& frames)
{
    _frames.assign(frames.cbegin(), frames.cend());
}

void sadmip::commands::set_experiment::set_autostart(bool active, bool loop)
{
    _autostart = active;
    _loop = loop;
}

std::unique_ptr<sadmip::message> sadmip::commands::set_experiment::execute(context& ctxt)
{
    ctxt.stop();
    ctxt.set_name(_name);

    if (_timestep != 0)
    {
        ctxt.set_timestep(_timestep);
    }

    ctxt.clear_frames();
    if (_frames.size() >= 1)
    {
        if (!ctxt.set_frame(std::move(_frames[0])))
        {
            return std::make_unique<error>(dimension_mismatch(opcode()));
        }

        for (uint16_t i = 1; i < _frames.size(); ++i)
        {
            if (!ctxt.append_frame(_frames[i]))
            {
                return std::make_unique<error>(dimension_mismatch(opcode()));
            }
        }
    }

    if (_autostart)
    {
        ctxt.play(_loop);
    }

    return std::make_unique<ack>(opcode());
}

void sadmip::commands::set_experiment::write_payload(write_stream& stream) const
{
    stream << _autostart << _loop << _name << _timestep << _frames;
}

bool sadmip::commands::set_experiment::parse_payload(read_stream& stream)
{
    return stream >> _autostart >> _loop >> _name >> _timestep >> _frames;
}
