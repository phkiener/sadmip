// Copyright (c) 2018 Philipp Kiener

#include <commands/set-value.h>

#include <message/ack.h>
#include <message/error.h>

sadmip::commands::set_value::set_value(const std::string& key, const std::string& value)
    : _key(key), _value(value), command(0x2C)
{

}

std::unique_ptr<sadmip::message> sadmip::commands::set_value::execute(context& context)
{
    if (context.set_meta(_key, _value))
    {
        return std::make_unique<ack>(opcode());
    }
    else
    {
        return std::make_unique<error>(unknown_key(opcode()));
    }
}

void sadmip::commands::set_value::write_payload(write_stream& stream) const
{
    stream << _key << _value;
}

bool sadmip::commands::set_value::parse_payload(read_stream& stream)
{
    return stream >> _key >> _value;
}
