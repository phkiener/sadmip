// Copyright (c) 2018 Philipp Kiener

#include <communication/transporter.h>

std::vector<uint8_t> sadmip::transporter::read_all()
{
    const size_t ready = available();
    if (ready == 0) { return std::vector<uint8_t>(); }
    
    uint8_t* buf = new uint8_t[ready];
    size_t num_read = read(buf, ready);

    std::vector<uint8_t> result;
    result.assign(buf, buf + num_read);
    delete[] buf;

    return result;
}
