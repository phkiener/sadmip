// Copyright (c) 2018 Philipp Kiener

#include <communication/data_buffer.h>

#include <algorithm>

#ifdef SADMIP_USE_TIMEOUTS
#include <chrono>
namespace
{
    inline size_t current_time_in_ms()
    {
        using namespace std::chrono;

        return static_cast<size_t>(
            duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch()).count()
            );
    }
}
#endif

sadmip::data_buffer::data_buffer(transporter& transporter, int timeout, bool can_fetch)
    : _fetch(can_fetch), _timeout(timeout), _threshold(64), _transporter(transporter), read_stream()
{

}

void sadmip::data_buffer::set_writing_threshold(size_t bytes)
{
    _threshold = bytes;
}

bool sadmip::data_buffer::flush()
{
    if (_write.size() == 0) return true;

    size_t written = _transporter.write(_write.data(), _write.size());
    _write.erase(_write.begin(), _write.begin() + written);

    return _write.empty();
}

bool sadmip::data_buffer::fetch(size_t amount)
{
    if (!_fetch) return false;
    
#ifdef SADMIP_USE_TIMEOUTS
    auto start = current_time_in_ms();
#endif

    size_t recv = 0;

    do
    {
        while (_transporter.available() == 0)
        {
#ifdef SADMIP_USE_TIMEOUTS
            if (_timeout >= 0)
            {
                auto now = current_time_in_ms();
                size_t timeout = static_cast<size_t>(_timeout);

                if (now - start >= timeout)
                {
                    return false;
                }
            }
#endif
        }

        std::vector<uint8_t> new_data;

        if (amount == 0)
        {
            new_data = _transporter.read_all();
        }
        else
        {
            size_t to_read = amount - recv;
            uint8_t* buf = new uint8_t[to_read];

            size_t read = _transporter.read(buf, to_read);
            recv += read;

            new_data.assign(buf, buf + read);
            delete[] buf;
        }

        append_to_readbuffer(new_data);

    } while (recv < amount);
    

    return true;
}

bool sadmip::data_buffer::fetch_immediate()
{
    if (!_fetch) return false;

    auto new_data = _transporter.read_all();
    append_to_readbuffer(new_data);

    return !new_data.empty();
}

bool sadmip::data_buffer::wait_for_bytes(size_t bytes)
{
    while (_read.size() < bytes)
    {
        if (!_fetch) continue;
        if (!fetch(bytes)) return false;
    }

    return true;
}

size_t sadmip::data_buffer::available() const
{
    return _read.size();
}

void sadmip::data_buffer::write(const uint8_t* bytes, size_t size)
{
    _write.insert(_write.end(), bytes, bytes + size);

    if (_write.size() > _threshold)
    {
        flush();
    }
}

size_t sadmip::data_buffer::read(uint8_t* dest, size_t num)
{
    size_t i = 0;
    
#ifdef SADMIP_USE_TIMEOUTS
    auto start = current_time_in_ms();
    auto now = start;
#endif

    while (i < num)
    {
        // read from buffer first
        if (!_read.empty())
        {
            const size_t left = num - i;
            size_t amount = left > _read.size() ? _read.size() : left;

            std::copy(_read.cbegin(), _read.cbegin() + amount, dest + i);
            _read.erase(_read.cbegin(), _read.cbegin() + amount);

            i += amount;

#ifdef SADMIP_USE_TIMEOUTS
            start = current_time_in_ms();
#endif
        }
        // if buffer is empty, fetch new data
        else if (_fetch)
        {
            if (!fetch())
            {
                break;
            }
        }
        // otherwise, just try again or timeout
#ifdef SADMIP_USE_TIMEOUTS
        else
        {
            if (_timeout >= 0)
            {
                now = current_time_in_ms();
                size_t timeout = static_cast<size_t>(_timeout);

                if (now - start > timeout)
                {
                    break;
                }
            }
        }
#endif
    }

    return i;
}

void sadmip::data_buffer::append_to_readbuffer(const uint8_t byte)
{
    _read.push_back(byte);
}

void sadmip::data_buffer::append_to_readbuffer(const uint8_t* bytes, size_t size)
{
    _read.insert(_read.end(), bytes, bytes + size);
}

void sadmip::data_buffer::append_to_readbuffer(const std::vector<uint8_t>& bytes)
{
    _read.insert(_read.end(), bytes.cbegin(), bytes.cend());
}

sadmip::data_buffer& sadmip::operator<<(data_buffer& buffer, data_buffer_signal signal)
{
    switch (signal)
    {
        case data_buffer_signal::fetch: buffer.fetch(); break;
        case data_buffer_signal::flush: buffer.flush(); break;
        case data_buffer_signal::sync:  buffer.fetch(); buffer.flush(); break;
    }
    
    return buffer;
}
