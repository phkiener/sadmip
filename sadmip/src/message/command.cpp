// Copyright (c) 2018 Philipp Kiener

#include <message/command.h>

#include <commands/all.h>
#include <io/memory_reader.h>
#include <memory>

sadmip::command::command(uint8_t opcode)
    : message(message_type::command, opcode)
{

}

void sadmip::command::write_payload(write_stream& stream) const
{
    return;
}

bool sadmip::command::parse_payload(read_stream& stream)
{
    return true;
}
