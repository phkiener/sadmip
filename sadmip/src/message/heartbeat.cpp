// Copyright (c) 2018 Philipp Kiener

#include <message/heartbeat.h>

sadmip::heartbeat::heartbeat(device_status status)
    : _status(status), message(message_type::heartbeat, static_cast<uint8_t>(status))
{

}

sadmip::heartbeat::device_status sadmip::heartbeat::status() const
{
    return _status;
}

void sadmip::heartbeat::write_payload(write_stream& stream) const
{
    // nothing
}

bool sadmip::heartbeat::parse_payload(read_stream& stream)
{
    return true;
}
