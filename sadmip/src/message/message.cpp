// Copyright (c) 2018 Philipp Kiener

#include <message/message.h>

#include <message/ack.h>
#include <message/command.h>
#include <message/heartbeat.h>
#include <message/error.h>
#include <commands/all.h>

#include <io/memory_reader.h>
#include <io/memory_writer.h>

void sadmip::message::serialize(write_stream& stream) const
{
    std::vector<uint8_t> payload;

    memory_writer payload_stream(payload);
    write_payload(payload_stream);

    if (payload.size() > UINT32_MAX)
    {
        payload.resize(UINT32_MAX);
    }

    uint32_t size = static_cast<uint32_t>(payload.size());

    stream << type() << opcode() << size;
    stream.flush();

    if (payload.size() != 0)
    {
        stream.write(payload);
        stream.flush();
    }
}

std::unique_ptr<sadmip::message> sadmip::message::deserialize(read_stream& stream, deserialization_error* errcode)
{
    using namespace commands;
    
    std::vector<uint8_t> header;
    if (stream.read(header, 6) != 6)
    {
        if (errcode) { *errcode = deserialization_error::HEADER_TIMEOUT; }
        return nullptr;
    }

    memory_reader header_reader(header);
    
    uint8_t type = 0;
    uint8_t opcode = 0;
    uint32_t payload_size = 0;    

    if (!(header_reader >> type >> opcode >> payload_size))
    {
        if (errcode) { *errcode = deserialization_error::INVALID_HEADER; }
        return nullptr;
    }

    std::unique_ptr<message> message = nullptr;

    switch (type)
    {
        case 1: /* command */
            switch (opcode)
            {
                case 0x11:
                    message = std::make_unique<get_cell>(0, 0);
                    break;

                case 0x13:
                    message = std::make_unique<get_matrix>();
                    break;

                case 0x14:
                    message = std::make_unique<get_experiment>();
                    break;

                case 0x18:
                    message = std::make_unique<get_name>();
                    break;

                case 0x19:
                    message = std::make_unique<get_timestep>();
                    break;

                case 0x1A:
                    message = std::make_unique<get_meta>();
                    break;

                case 0x1B:
                    message = std::make_unique<get_keys>();
                    break;

                case 0x1C:
                    message = std::make_unique<get_value>("");
                    break;

                case 0x21:
                    message = std::make_unique<set_cell>(0, 0, false);
                    break;

                case 0x22:
                    message = std::make_unique<set_path>(0, 0);
                    break;

                case 0x23:
                    message = std::make_unique<set_matrix>(bit_matrix());
                    break;

                case 0x24:
                    message = std::make_unique<set_experiment>("", 0);
                    break;

                case 0x28:
                    message = std::make_unique<set_name>("");
                    break;

                case 0x29:
                    message = std::make_unique<set_timestep>(0);
                    break;

                case 0x2A:
                    message = std::make_unique<set_meta>();
                    break;

                case 0x2B:
                    message = std::make_unique<set_keys>(set_keys::mode::replace);
                    break;

                case 0x2C:
                    message = std::make_unique<set_value>("", "");
                    break;

                case 0x41:
                    message = std::make_unique<ctrl_start>();
                    break;

                case 0x42:
                    message = std::make_unique<ctrl_next>();
                    break;

                case 0x43:
                    message = std::make_unique<ctrl_prev>();
                    break;

                case 0x44:
                    message = std::make_unique<ctrl_stop>();
                    break;

                case 0x45:
                    message = std::make_unique<ctrl_goto>(0);
                    break;

                case 0x46:
                    message = std::make_unique<ctrl_reset>();
                    break;

                case 0x81:
                    message = std::make_unique<meta_configuregrid>();
                    break;

                case 0x82:
                    message = std::make_unique<meta_reset>();
                    break;

                case 0x83:
                    message = std::make_unique<meta_framecount>();
                    break;

                case 0x84:
                    message = std::make_unique<meta_memoryinfo>();
                    break;
                    
                case 0x85:
                    message = std::make_unique<meta_dimensions>();
                    break;
            }
            break;

        case 2: /* ack */
            switch (opcode)
            {
                case 0x11: 
                    message = std::make_unique<get_cell::result>(false); 
                    break;

                case 0x13: 
                    message = std::make_unique<get_matrix::result>(bit_matrix()); 
                    break;

                case 0x14:
                    message = std::make_unique<get_experiment::result>("", 0);
                    break;

                case 0x18: 
                    message = std::make_unique<get_name::result>(""); 
                    break;

                case 0x19: 
                    message = std::make_unique<get_timestep::result>(0); 
                    break;

                case 0x1A:
                    message = std::make_unique<get_meta::result>();
                    break;

                case 0x1B:
                    message = std::make_unique<get_keys::result>(std::vector<std::string>());
                    break;

                case 0x1C:
                    message = std::make_unique<get_value::result>("");
                    break;

                case 0x81:
                    message = std::make_unique<meta_configuregrid::result>(std::vector<std::string>());
                    break;

                case 0x83:
                    message = std::make_unique<meta_framecount::result>(0);
                    break;

                case 0x84:
                    message = std::make_unique<meta_memoryinfo::result>();
                    break;

                case 0x85:
                    message = std::make_unique<meta_dimensions::result>(0, 0);
                    break;

                default:
                    message = std::make_unique<ack>(opcode); 
                    break;
            }
            break;

        case 3: /* error */
            message = std::make_unique<error>(opcode);
            break;

        case 4: /* heartbeat */
            switch (opcode)
            {
                case 1: 
                    message = std::make_unique<heartbeat>(heartbeat::device_status::idle); 
                    break;

                case 2: 
                    message = std::make_unique<heartbeat>(heartbeat::device_status::receiving_data); 
                    break;

                case 3: 
                    message = std::make_unique<heartbeat>(heartbeat::device_status::working_command); 
                    break;
            }
            break;
    }

    if (!message)
    {
        if (errcode) { *errcode = deserialization_error::INVALID_HEADER; }
        return nullptr;
    }

    std::vector<uint8_t> payload;
    if (stream.read(payload, payload_size) != payload_size)
    {
        if (errcode) { *errcode = deserialization_error::PAYLOAD_TIMEOUT; }
        return nullptr;
    }

    memory_reader reader(payload);
    if (!message->parse_payload(reader))
    {
        if (errcode) { *errcode = deserialization_error::INVALID_PAYLOAD; }
        return nullptr;
    }

    if (errcode) { *errcode = deserialization_error::NO_ERROR; }
    return message;
}

sadmip::message_type sadmip::message::type() const
{
    return _type;
}

uint8_t sadmip::message::opcode() const
{
    return _opcode;
}

sadmip::message::message(message_type type, uint8_t opcode)
    : _type(type), _opcode(opcode)
{

}

sadmip::write_stream& sadmip::operator<<(write_stream& stream, const message& msg)
{
    msg.serialize(stream);
    return stream;
}

sadmip::read_stream& sadmip::operator>>(read_stream& stream, std::unique_ptr<message>& msg_ptr)
{
    msg_ptr = sadmip::message::deserialize(stream);
    return stream;
}
