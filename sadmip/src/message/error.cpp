// Copyright (c) 2018 Philipp Kiener

#include <message/error.h>

sadmip::error::error(uint8_t opcode, uint8_t error_code, const std::string& what)
    : message(message_type::error, opcode), _error(error_code), _message(what)
{

}

uint8_t sadmip::error::error_code() const
{
    return _error;
}

const std::string& sadmip::error::what() const
{
    return _message;
}

void sadmip::error::write_payload(write_stream& stream) const
{
    stream << _error << _message;
}

bool sadmip::error::parse_payload(read_stream& stream)
{
    if (stream >> _error >> _message)
    {
        return stream.success();
    }
    else
    {
        return stream.fail();
    }
}
