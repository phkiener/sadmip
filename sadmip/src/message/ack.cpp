// Copyright (c) 2018 Philipp Kiener

#include <message/ack.h>

sadmip::ack::ack(uint8_t opcode)
    : message(message_type::ack, opcode)
{

}

void sadmip::ack::write_payload(write_stream& stream) const
{
    return;
}

bool sadmip::ack::parse_payload(read_stream& stream)
{
    return true;
}
