// Copyright (c) 2018 Philipp Kiener

#include <context.h>

const std::string& sadmip::context::name() const
{
    return _name;
}

void sadmip::context::set_name(const std::string& name)
{
    _name = name;
}

uint16_t sadmip::context::timestep() const
{
    return _timestep;
}

void sadmip::context::set_timestep(uint16_t timestep)
{
    if (_timestep == 0) return;
    
    _timestep = timestep;

    if (is_playing())
    {
        stop();
        play(is_looping());
    }
}

bool sadmip::context::cell(uint8_t row, uint8_t col) const
{
    return _frames[_idx].at(row, col);
}

sadmip::bit_matrix sadmip::context::frame() const
{
    return _frames[_idx];
}

sadmip::bit_matrix sadmip::context::frame(uint16_t idx) const
{
    if (idx >= _frames.size()) return sadmip::bit_matrix();

    return _frames[idx];
}

bool sadmip::context::set_cell(uint8_t row, uint8_t col, bool active)
{
    return set_cell(_idx, row, col, active);
}

bool sadmip::context::set_cell(uint16_t idx, uint8_t row, uint8_t col, bool active)
{
    if (idx >= _frames.size()) return false;

    _frames[idx].set(row, col, active);
    
    if (idx == _idx) 
    {
        refresh_grid();
    }

    return true;
}

void sadmip::context::clear_frames()
{
    _frames.clear();
    _frames.push_back(bit_matrix(rows(), cols()));
    _idx = 0;

    refresh_grid();
}

bool sadmip::context::append_frame(const bit_matrix& frame)
{
    if (frame.rows() > rows() || frame.cols() > cols())
    {
        return false;
    }

    _frames.push_back(frame);
    _frames.back().expand(rows(), cols());

    return true;
}

bool sadmip::context::append_frame(bit_matrix&& frame)
{
    if (frame.rows() > rows() || frame.cols() > cols())
    {
        return false;
    }
    frame.expand(rows(), cols());
    _frames.emplace_back(std::move(frame));
    return true;
}

bool sadmip::context::set_frames(const std::vector<bit_matrix>& frames)
{
    for (const auto& frame : frames)
    {
        if (frame.cols() > cols() || frame.rows() > rows())
        {
            return false;
        }
    }

    _idx = 0;
    _frames = frames;

    for (auto& frame : _frames) 
    {
        frame.expand(rows(), cols());
    }

    refresh_grid();

    return true;
}

bool sadmip::context::set_frames(std::vector<bit_matrix>&& frames)
{
    for (const auto& frame : frames)
    {
        if (frame.cols() > cols() || frame.rows() > rows())
        {
            return false;
        }
    }

    _idx = 0;
    _frames = std::move(frames);

    for (auto& frame : _frames) 
    {
        frame.expand(rows(), cols());
    }
    
    refresh_grid();

    return true;
}

bool sadmip::context::set_frame(const bit_matrix& frame)
{
    return set_frame(_idx, frame);
}

bool sadmip::context::set_frame(bit_matrix&& frame)
{
    return set_frame(_idx, std::move(frame));
}

bool sadmip::context::set_frame(uint16_t idx, const bit_matrix& frame)
{
    if (idx >= _frames.size() 
        || frame.rows() > rows()
        || frame.cols() > cols())
    {
        return false;
    }

    _frames[idx] = frame;
    _frames[idx].expand(rows(), cols());

    if (idx == _idx)
    {
        refresh_grid();
    }

    return true;
}

bool sadmip::context::set_frame(uint16_t idx, bit_matrix&& frame)
{
    if (idx >= _frames.size() 
        || frame.rows() > rows()
        || frame.cols() > cols())
    {
        return false;
    }

    _frames[idx] = std::move(frame.expand(rows(), cols()));

    if (idx == _idx)
    {
        refresh_grid();
    }

    return true;
}

uint16_t sadmip::context::frame_count() const
{
    return static_cast<uint16_t>(_frames.size());
}

uint16_t sadmip::context::frame_index() const
{
    return _idx;
}

bool sadmip::context::seek_frame(int16_t offset, seek_origin origin)
{
    uint16_t new_idx = 0;

    switch (origin)
    {
        case sadmip::seek_origin::current:
            new_idx = _idx + offset;
            break;

        case sadmip::seek_origin::start:
            new_idx = abs(offset);
            break;

        case sadmip::seek_origin::end:
            new_idx = frame_count() - abs(offset);
            break;
    }

    if (new_idx < _frames.size())
    {
        _idx = new_idx;
        refresh_grid();
        return true;
    }
    else
    {
        return false;
    }
}

void sadmip::context::play(bool loop)
{
    _state_flags[0] = true;
    _state_flags[1] = loop;

    on_playback_start();
}

void sadmip::context::stop()
{
    _state_flags[0] = false;

    on_playback_stop();
}

bool sadmip::context::is_playing()
{
    return _state_flags[0];
}

bool sadmip::context::is_looping()
{
    return _state_flags[1];
}

bool sadmip::context::set_grid(const std::string& type)
{
    if (type.compare("none") == 0)
    {
        _grid = nullptr;
        return true;
    }

    auto pair = _gridtypes.find(type);

    if (pair == _gridtypes.cend())
    {
        return false;
    }
    else
    {
        _grid = pair->second();
        refresh_grid();
        return true;
    }
}

std::vector<std::string> sadmip::context::grid_types() const
{
    std::vector<std::string> names;

    for (const auto pair : _gridtypes)
    {
        names.push_back(pair.first);
    }

    names.push_back("none");

    return names;
}

std::vector<std::string> sadmip::context::meta_keys() const
{
    std::vector<std::string> keys;

    for (const auto& pair : _meta)
    {
        keys.push_back(pair.first);
    }

    return keys;
}

std::string sadmip::context::get_meta(const std::string& key) const
{
    return has_meta_key(key) ? _meta.at(key) : "";
}

bool sadmip::context::set_meta(const std::string& key, const std::string& value)
{
    if (has_meta_key(key))
    {
        _meta[key] = value;
        return true;
    }

    return false;
}

bool sadmip::context::add_meta(const std::string& key, const std::string& value)
{
    if (has_meta_key(key))
    {
        return false;
    }

    _meta[key] = value;
    return true;
}

bool sadmip::context::remove_meta(const std::string& key)
{
    if (has_meta_key(key))
    {
        _meta.erase(key);
        return true;
    }

    return false;
}

void sadmip::context::clear_meta()
{
    _meta.clear();
}

void sadmip::context::reset()
{
    clear_frames();
    clear_meta();

    _name = "Experiment";
    _timestep = 500;

    _state_flags[0] = false;
    _state_flags[1] = false;
}

bool sadmip::context::memory_info(uint32_t& stack_reserve, uint32_t& stack_max, uint32_t& heap_reserve, uint32_t& heap_current, uint32_t& heap_fails) const
{
    return false;
}

sadmip::context::context()
{
    _name = "Experiment";
    _timestep = 500;

    _state_flags[0] = false;
    _state_flags[1] = false;

    _idx = 0;
    _frames.emplace_back(rows(), cols());
}

void sadmip::context::next_frame()
{
    if (!seek_frame(1, sadmip::seek_origin::current))
    {
        if (is_looping())
        {
            seek_frame(0, sadmip::seek_origin::start);
        }
        else
        {
            stop();
        }
    }

    refresh_grid();
}

void sadmip::context::on_playback_start()
{
    // Per default, nothing is done here.
}

void sadmip::context::on_playback_stop()
{
    // Per default, nothing is done here.
}

void sadmip::context::refresh_grid()
{
    if (_grid)
    {
        _grid->display(_frames[_idx]);
    }
}

bool sadmip::context::has_meta_key(const std::string& key) const
{
    return _meta.find(key) != _meta.end();
}
