// Copyright (c) 2018 Philipp Kiener

#include <utils/endian_helpers.h>

#include <algorithm>
#include <string.h>

bool sadmip::host_is_big_endian()
{
    uint16_t check = 0xFF00;
    return 0xFF == *(uint8_t*)&check;
}

bool sadmip::host_is_little_endian()
{
    uint16_t check = 0xFF00;
    return 0x00 == *(uint8_t*)&check;
}

void sadmip::swap_bytes(uint8_t* dst, const uint8_t* src, size_t num)
{
    memcpy(dst, src, num);
    swap_bytes(dst, num);
}

void sadmip::swap_bytes(uint8_t* buf, size_t num)
{
    std::reverse(buf, buf + num);
}
