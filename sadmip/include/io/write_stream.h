// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cstdint>
#include <cstddef>
#include <string>
#include <vector>

namespace sadmip
{
    class write_stream
    {
    public:

        /**
        * Flushes the stream, forcing a write on all buffered data.
        *
        * @return true if data was flushed, false if not.
        */
        virtual bool flush();

        /**
         * Puts a raw byte into the stream.
         *
         * The byte is put into the stream. This may cause the stream
         * to be flushed, i.e. sent over a network.
         *
         * @param[in] byte the byte to write
         */
        virtual void put(uint8_t byte);

        /**
         * Writes a number of raw bytes into the stream.
         *
         * /size/ bytes starting from /bytes/ are copied into the stream.
         * This may cause the stream to be flushed, i.e. sent over a
         * network.
         *
         * @param[in] bytes pointer to the bytes to copied
         * @param[in] size  number of bytes to copy
         */
        virtual void write(const uint8_t* bytes, size_t size) = 0;

        /**
         * Writes a number of raw bytes into the stream.
         *
         * The given bytes are copied into the stream. This may cause the
         * stream to be flushed, i.e. sent over a network.
         *
         * @param[in] bytes the bytes to copy
         */
        virtual void write(const std::vector<uint8_t>& bytes);
    };

    /**
     * Writes a /bool/ to the stream.
     *
     * @param[in] stream the stream to write to
     * @param[in] value  the value to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const bool& value);

    /**
     * Writes an /int8_t/ to the stream.
     *
     * @param[in] stream the stream to write to
     * @param[in] value  the value to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const int8_t& value);

    /**
     * Writes a /uint8_t/ to the stream.
     *
     * @param[in] stream the stream to write to
     * @param[in] value  the value to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const uint8_t& value);

    /**
     * Writes a formatted /int16_t/ to the stream.
     *
     * The value is written as big endian.
     *
     * @param[in] stream the stream to write to
     * @param[in] value  the value to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const int16_t& value);

    /**
     * Writes a formatted /uint16_t/ to the stream.
     *
     * The value is written as big endian.
     *
     * @param[in] stream the stream to write to
     * @param[in] value  the value to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const uint16_t& value);

    /**
     * Writes a formatted /int32_t/ to the stream.
     *
     * The value is written as big endian.
     *
     * @param[in] stream the stream to write to
     * @param[in] value  the value to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const int32_t& value);

    /**
     * Writes a formatted /uint32_t/ to the stream.
     *
     * The value is written as big endian.
     *
     * @param[in] stream the stream to write to
     * @param[in] value  the value to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const uint32_t& value);

    /**
     * Writes a formatted /int64_t/ to the stream.
     *
     * The value is written as big endian.
     *
     * @param[in] stream the stream to write to
     * @param[in] value  the value to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const int64_t& value);

    /**
     * Writes a formatted /uint64_t/ to the stream.
     *
     * The value is written as big endian.
     *
     * @param[in] stream the stream to write to
     * @param[in] value  the value to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const uint64_t& value);

    /**
     * Writes a formatted /float/ to the stream.
     *
     * The value is written as big endian.
     *
     * @param[in] stream the stream to write to
     * @param[in] value  the value to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const float& value);

    /**
     * Writes a formatted /double/ to the stream.
     *
     * The value is written as big endian.
     *
     * @param[in] stream the stream to write to
     * @param[in] value  the value to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const double& value);

    /**
     * Writes a formatted vector of values to the stream.
     *
     * This requires the template type to have an overload of the
     * operator<<.
     *
     * The vector is stored as a list of values, where each element is
     * stored on its own and without a delimiter. The values are prepended with
     * the length of the list as /uint16_t/.

     * @param[in] stream the stream to write to
     * @param[in] values the values to write
     * @return           the modified stream
     */
    template <typename T>
    write_stream& operator<<(write_stream& stream, const std::vector<T>& values)
    {
        uint16_t size = static_cast<uint16_t>(values.size());
        stream << size;

        for (uint16_t i = 0; i < size; ++i)
        {
            stream << values.at(i);
        }

        return stream;
    }

    /**
     * Writes a formatted string to the stream.
     *
     * The string is seen as a /std::vector<char>/.
     *
     * @param[in] stream the stream to write to
     * @param[in] str    the string to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const std::string& str);
}
