// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cstdint>
#include <cstddef>
#include <vector>

#include "write_stream.h"

namespace sadmip
{
    /**
     * A memory writer to write formatted and raw data to a vector of bytes.
     *
     * The memory writer works on top of a /std::vector<uint8_t>/ and does not
     * buffer its output. Every call to /put(uint8_t)/ directly writes to the
     * given vector.
     */
    class memory_writer : public write_stream
    {
    public:
        /**
         * Create a memory writer which writes to the given vector.
         *
         * @param[in] destination the buffer which to write to
         */
        memory_writer(std::vector<uint8_t>& destination);

        /**
         * Returns the size of the memory region that was written to.
         *
         * @return the total number of bytes that have been written to this stream
         */
        size_t size() const;

        void put(uint8_t byte) override;
        void write(const uint8_t* bytes, size_t size) override;

    private:
        std::vector<uint8_t>& _resource; //!< where to write the bytes
    };
}
