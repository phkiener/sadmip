// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cstdint>
#include <cstddef>
#include <vector>

#include "read_stream.h"

namespace sadmip
{
    /**
     * A memory reader to read formatted and raw data from a vector of bytes.
     *
     * The reader will never wait and thus always fail if not enough data is
     * available. The given vector is not modified, but only stepped through.
     */
    class memory_reader : public read_stream
    {
    public:
        /**
         * Creates a memory reader to read from the given vector of bytes.
         *
         * @param[in] resource the vector to read from
         */
        memory_reader(const std::vector<uint8_t>& resource);

        size_t read(uint8_t* dest, size_t num) override;

    private:
        const std::vector<uint8_t>& _resource;
        size_t _idx;
    };
}