// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cstdint>
#include <cstddef>
#include <string>
#include <vector>

namespace sadmip
{
    /**
     * Interface for a stream on a readable resource.
     *
     * The resource can be read either raw using the class methods or using
     * formatting (or rather, parsing) using the overloads of the /opeartor >>/.
     */
    class read_stream
    {
    public:        
        /**
         * Reads the first character in the stream.
         *
         * The first character is read and consumed. It is removed from the
         * steam. If no character can be read, a NUL character is returned and
         * the pointed-at bool /okay/ is set to false. If a character
         * could be read, it is set to /true/.
         * If /okay/ is a nullptr, there is no way of discerning whether a
         * returned NUL-byte is due to no byte being available or the acutal
         * NUL-byte being read.
         *
         * @return the topmost character
         */
        virtual uint8_t get(bool* okay = nullptr);
        
        /**
         * Reads a raw chunk of data from the stream.
         *
         * Up to /num/ bytes are copied to /dest/. The number of bytes actually
         * read will be returned - it is implementation defined what shall
         * happen if not enough bytes are able to be read.
         *
         * @param[in,out] dest where to put the data
         * @param[in]     num  how many bytes to read at most
         * @return             number of bytes actually read and copied
         */
        virtual size_t read(uint8_t* dest, size_t num) = 0;
        
        /**
         * Reads a raw chunk of data from the stream.
         *
         * Up to /num/ bytes are copied to /dest/. The number of bytes actually
         * read will be returned - it is implementation defined what shall
         * happen if not enough bytes are able to be read.
         *
         * @param[in,out] dest where to put the data
         * @param[in]     num  how many bytes to read at most
         * @return             number of bytes actually read and copied
         */
        virtual size_t read(std::vector<uint8_t>& dest, size_t num);
        
        /**
         * Reads a raw chunk of data from the stream.
         *
         * Up to /num/ bytes are returned in a /std::vector/. It is 
         * implementation defined what shall happen if not enough bytes are 
         * able to be read.
         *
         * @param[in] size how many bytes to read at most
         * @return         the read bytes
         */
        virtual std::vector<uint8_t> read(size_t num);

        /**
         * Checks the stream for errors.
         *
         * @return true if there are no errors, false if an error has happened
         */
        operator bool() const;
        
        /**
         * Checks the stream for errors.
         *
         * @return true if there are no errors, false if an error has happened
         */
        bool good() const;

        /**
         * Checks the stream for errors.
         *
         * @return true if an error has occurred, false if no error happened
         */
        bool bad() const;

        /**
         * Sets an error.
         *
         * If - due to any reason - an error happens during reading, use this
         * method to set an error so that the stream knows when something went
         * wrong.
         *
         * @return the stream
         */
        read_stream& fail();

        /**
         * Clears the error.
         *
         * This clears the error state, forcing the stream to be "good" again.
         *
         * @return the stream
         */
        read_stream& success();

    protected:
        /**
         * Initialize the read_stream.
         */
        read_stream();

        bool _error; //!< whether an error happened
    };

    /**
     * Reads a /bool/ from the stream.
     *
     * A zero-byte encountered sets the value to /false/, a non-zero value
     * sets the value to /true/.
     *
     * @param[in]  stream the stream to read from
     * @param[out] value  the read value
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, bool& value);

    /**
     * Reads an /int8_t/ from the stream.
     *
     * @param[in]  stream the stream to read from
     * @param[out] value  the read value
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, int8_t& value);

    /**
     * Reads a /uint8_t/ from the stream.
     *
     * @param[in]  stream the stream to read from
     * @param[out] value  the read value
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, uint8_t& value);

    /**
     * Reads a formatted /int16_t/ from the stream.
     *
     * The value is converted from big endian to host endianness.
     *
     * @param[in]  stream the stream to read from
     * @param[out] value  the read value
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, int16_t& value);

    /**
     * Reads a formatted /uint16_t/ from the stream.
     *
     * The value is converted from big endian to host endianness.
     *
     * @param[in]  stream the stream to read from
     * @param[out] value  the read value
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, uint16_t& value);

    /**
     * Reads a formatted /int32_t/ from the stream.
     *
     * The value is converted from big endian to host endianness.
     *
     * @param[in]  stream the stream to read from
     * @param[out] value  the read value
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, int32_t& value);

    /**
     * Reads a formatted /uint32_t/ from the stream.
     *
     * The value is converted from big endian to host endianness.
     *
     * @param[in]  stream the stream to read from
     * @param[out] value  the read value
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, uint32_t& value);

    /**
     * Reads a formatted /int64_t/ from the stream.
     *
     * The value is converted from big endian to host endianness.
     *
     * @param[in]  stream the stream to read from
     * @param[out] value  the read value
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, int64_t& value);

    /**
     * Reads a formatted /uint64_t/ from the stream.
     *
     * The value is converted from big endian to host endianness.
     *
     * @param[in]  stream the stream to read from
     * @param[out] value  the read value
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, uint64_t& value);

    /**
     * Reads a formatted /float/ from the stream.
     *
     * The value is converted from big endian to host endianness.
     *
     * @param[in]  stream the stream to read from
     * @param[out] value  the read value
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, float& value);

    /**
     * Reads a formatted /double/ from the stream.
     *
     * The value is converted from big endian to host endianness.
     *
     * @param[in]  stream the stream to read from
     * @param[out] value  the read value
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, double& value);

    /**
     * Reads a formatted string from the stream.
     *
     * The string is seen as a /std::vector<char>/.
     *
     * @param[in]  stream the stream to read from
     * @param[out] str    the read string
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, std::string& str);

    /**
     * Reads a formatted vector of values from the stream.
     *
     * The vector is read in a format as described in the sadmip specification for
     * reading lists.
     * The template type MUST be default-constructible and copy-assignable.
     *
     * @param[in]  stream the stream to read from
     * @param[out] values the read list
     * @return            the modified stream
     */
    template <typename T>
    read_stream& operator>>(read_stream& stream, std::vector<T>& values)
    {
        return read_list_of_values(stream, values, T());
    }

    /**
    * Reads a formatted vector of values from the stream.
    *
    * The vector is read in a format as described in the writing of vectors.
    * The template type MUST be copy-assignable. Use this method if the type
    * is not default constructible.
    *
    * @param[in]  stream the stream to read from
    * @param[out] values the read list
    * @param[in]  base   the default value for non-default constructible types
    * @return            the modified stream
    */
    template <typename T>
    read_stream& read_list_of_values(read_stream& stream, std::vector<T>& values, const T base)
    {
        uint16_t size = 0;
        if (!(stream >> size))
        {
            return stream.fail();
        }

        values.clear();
        values.reserve(size);

        for (uint16_t i = 0; i < size; ++i)
        {
            T read = base;

            if (stream >> read)
            {
                values.push_back(read);
            }
            else
            {
                return stream.fail();
            }
            
        }

        return stream.success();
    }
}
