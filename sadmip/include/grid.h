// Copyright (c) 2018 Philipp Kiener

#pragma once

#include <datatypes/bit_matrix.h>

namespace sadmip
{
    /**
     * A grid to display the frames of an experiment.
     *
     * Any initialization required should be done within the constructor, so that
     * using the panel boils down to repeatedly calling /display/.
     */
    class grid
    {
    public:
        /**
         * Destructs the panel.
         *
         * Any resources (such as a local buffer) are freed.
         */
        virtual ~grid() {};

        /**
         * Display a frame on the grid.
         *
         * This method issues the panel to display the frame as described by the
         * given /bit_matrix/. The frame may (should) be stored in a temporary buffer
         * local to the panel itself.
         *
         * @param frame the frame to display
         */
        virtual bool display(const sadmip::bit_matrix& frame) = 0;
    };
}
