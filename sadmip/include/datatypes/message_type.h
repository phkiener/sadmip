// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cstdint>

#include <io/read_stream.h>
#include <io/write_stream.h>

namespace sadmip
{
    /**
     * All possible types of a message.
     *
     * The type of a message is encoded in this enum.
     */
    enum class message_type : uint8_t
    {
        command = 1, //!< the message is a command to execute
        ack = 2, //!< the message is an ack for a command
        error = 3, //!< the message is an error for a command
        heartbeat = 4 //!< the message is a heartbeat
    };

    /**
     * Serializes the message type to the given stream.
     *
     * The message type is written as a single byte.
     *
     * @param[in] stream the stream to write to
     * @param[in] type   the type to serialize
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const message_type& type);

    /**
     * Parses a message type from the given stream.
     *
     * If either no byte can be read or the byte is invalid, the stream gets
     * marked with an error. In that case, no byte will be consumed.
     *
     * @param[in]  stream the stream to read from
     * @param[out] type   the read type
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, message_type& type);
}
