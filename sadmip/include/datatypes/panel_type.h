// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <io/read_stream.h>
#include <io/write_stream.h>

#include <cstdint>

namespace sadmip
{
    /**
     * All existing panel types.
     *
     * This types encodes the type of panel that is to be configured. For
     * each type of panel there exists one entry in this enum.
     */
    enum class panel_type : uint8_t
    {
        leds_old = 0x11, //!< an old led-matrix with the expander cable
        leds = 0x12, //!< a new led-matrix without the extra cable
        square_cells = 0x21, //!< a panel of square cells
        triangle_cells = 0x22, //!< a panel of triangular cells
        hexagon_cells = 0x23, //!< a panel of hexagonal cells
    };

    /**
     * Writes a /panel_type/ to the stream.
     *
     * The panel type is written as a single byte.
     *
     * @param[in] stream the stream to write to
     * @param[in] type   the type to write
     * @return           the modified buffer
     */
    write_stream& operator<<(write_stream& stream, const panel_type& type);
    
    /**
     * Reads a /panel_type/ from the stream.
     *
     * A single byte is always consumed.
     *
     * @param[in]  stream the stream to read from
     * @param[out] type   the read type
     * @return            the modified buffer
     */
    read_stream& operator>>(read_stream& stream, panel_type& type);
}
