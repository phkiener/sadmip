// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cstdint>

#include <io/read_stream.h>
#include <io/write_stream.h>

namespace sadmip
{
    /**
     * A matrix of bits in the given dimensions.
     *
     * Each cell is a single bit. A row always consists of whole bytes; if the
     * number of columns is not a multiple of 8, a padding is added to each row
     * to make it so.
     */
    class bit_matrix
    {
    public:
        friend write_stream& operator<<(write_stream& stream, const bit_matrix& mat);
        friend read_stream& operator>>(read_stream& stream, bit_matrix& mat);

        /**
         * Constructs an invalid matrix.
         */
        bit_matrix();

        /**
         * Constructs a bitmatrix with given dimensions.
         *
         * @param[in] rows the number of rows in the matrix
         * @param[in] cols the number of columns in the matrix
         */
        bit_matrix(uint8_t rows, uint8_t cols);

        /**
         * Construct a copy of the given bitmatrix.
         *
         * @param[in] rhs the matrix to copy
         */
        bit_matrix(const bit_matrix& rhs);

        /**
         * Move the given matrix into this matrix.
         *
         * /rhs/ will be invalidated.
         *
         * @param[in] rhs the matrix to move to this
         */
        bit_matrix(bit_matrix&& rhs);

        /**
         * Destruct the matrix.
         */
        ~bit_matrix();

        /**
         * Copy the given matrix to this matrix.
         *
         * @param[in] rhs the matrix to copy
         */
        bit_matrix& operator= (const bit_matrix& rhs);

        /**
         * Move the given matrix to this matrix.
         *
         * /rhs/ will be invalidated.
         *
         * @param[in] rhs the matrix to move
         */
        bit_matrix& operator= (bit_matrix&& rhs);

        /**
         * Returns the state of the referenced cell.
         *
         * @param[in] row index of the row of the cell to reference
         * @param[in] col index of the column of the cell to reference
         * @return        true if the cell is active, false otherwise
         */
        bool at(uint8_t row, uint8_t col) const;
        
        /**
         * Returns the state of the referenced cell.
         *
         * @param[in] row index of the row of the cell to reference
         * @param[in] col index of the column of the cell to reference
         * @return        true if the cell is active, false otherwise
         */
        bool operator()(uint8_t row, uint8_t col) const;

        /**
         * Sets the state of the referenced cell.
         *
         * @param[in] row index of the row of the cell to reference
         * @param[in] col index of the column of the cell to reference
         * @param[in] on  new state of the cell
         */
        void set(uint8_t row, uint8_t col, bool on);

        /**
         * Returns the number of rows this matrix has.
         *
         * @return number of rows of this matrix
         */
        uint8_t rows() const;

        /**
         * Returns the number of columns this matrix has.
         *
         * @return number of columns of this matrix
         */
        uint8_t cols() const;

        /**
         * Checks if the matrix is valid.
         *
         * @return true if the matrix is valid, false otherwise
         */
        bool valid() const;

        /**
         * Expand the current matrix to higher dimensions.
         *
         * Either /rows/ or /cols/ must be higher than the value of /rows()/
         * or /cols()/ respectively. 
         * 
         * @param[in] rows the new amount of rows to expand to
         * @param[in] cols the new amount of columns to expand to
         * @return         this object
         */
        bit_matrix& expand(uint8_t rows, uint8_t cols);

    private:
        uint8_t _rows; //!< number of rows in the matrix
        uint8_t _cols; //!< number of colums in the matrix
        uint8_t* _data; //!< the matrix data
        uint16_t _datalen; //!< size of the allocated buffer

        /**
         * Returns a pointer to the byte that holds the given row and column.
         *
         * @param[in] row row to look for
         * @param[in] col column to look for
         * @return        pointer to the byte for that cell
         */
        uint8_t* byte_for(uint8_t row, uint8_t col) const;
    };

    /**
     * Serializes the bitmarix to the given stream.
     *
     * The bitmatrix is serialized headless. If you want a headed matrix, you
     * must serialize the dimensions yourself.
     *
     * @param[in] stream the stream to append to
     * @param[in] mat    the matrix to serialize
     * @return           the modified buffer
     */
    write_stream& operator<<(write_stream& stream, const bit_matrix& mat);

    /**
     * Reads a matrix from the given stream.
     *
     * Note that the size of the matrix must be known beforehand.
     *
     * @param[in]     stream the stream to read from
     * @param[in,out] mat    the read matrix
     * @return               the modified buffer
     */
    read_stream& operator>>(read_stream& stream, bit_matrix& mat);
}
