// Copyright (c) 2018 Philipp Kiener
#pragma once

#include "message.h"

#include <string>

namespace sadmip
{
    /**
     * An error message.
     *
     * This message is an ERR for any command. It always contains an error code,
     * which specified what kind of error happened.
     */
    class error : public message
    {
    public:
        /**
         * Constructs an ERR with the given opcode and error code.
         *
         * If not given, the error code must be parsed out of the payload.
         *
         * @param opcode     the opcode for this ERR
         * @param error_code the error code for this ERR
         * @param what       the error message for this ERR
         */
        error(uint8_t opcode, uint8_t error_code = 0, const std::string& what = "");

        /**
         * Returns this error's error code.
         *
         * @return the error code of this ERR
         */
        uint8_t error_code() const;

        /**
         * Returns this error's message.
         *
         * @return the message of this ERR
         */
        const std::string& what() const;

    protected:
        virtual void write_payload(write_stream& stream) const override;
        virtual bool parse_payload(read_stream& stream) override;

    private:
        uint8_t _error; //!< the error code
        std::string _message; //!< the (optional) error message
    };

    // some nice helpers to make life easier

    /**
     * Returns an index-out-of-bounds error.
     *
     * The error will have it's error code filled according to the type
     * of error.
     *
     * @param[in] opcode the opcode for the error
     * @param[in] what   the error message
     * @return           the error structure
     */
    inline error index_out_of_bounds(uint8_t opcode, const std::string& what = "")
    {
        return error(opcode, 0x01, what);
    }

    /**
     * Returns an unknown-key error.
     *
     * The error will have it's error code filled according to the type
     * of error.
     *
     * @param[in] opcode the opcode for the error
     * @param[in] what   the error message
     * @return           the error structure
     */
    inline error unknown_key(uint8_t opcode, const std::string& what = "")
    {
        return error(opcode, 0x02, what);
    }

    /**
     * Returns an unknown-opcode error.
     *
     * The error will have it's error code filled according to the type
     * of error.
     *
     * @param[in] opcode the opcode for the error
     * @param[in] what   the error message
     * @return           the error structure
     */
    inline error unknown_opcode(uint8_t opcode, const std::string& what = "")
    {
        return error(opcode, 0x03, what);
    }

    /**
     * Returns a not-applicable error.
     *
     * The error will have it's error code filled according to the type
     * of error.
     *
     * @param[in] opcode the opcode for the error
     * @param[in] what   the error message
     * @return           the error structure
     */
    inline error not_applicable(uint8_t opcode, const std::string& what = "")
    {
        return error(opcode, 0x04, what);
    }

    /**
     * Returns a dimension-mismatch error.
     *
     * The error will have it's error code filled according to the type
     * of error.
     *
     * @param[in] opcode the opcode for the error
     * @param[in] what   the error message
     * @return           the error structure
     */
    inline error dimension_mismatch(uint8_t opcode, const std::string& what = "")
    {
        return error(opcode, 0x05, what);
    }

    /**
     * Returns an invalid-argument error.
     *
     * The error will have it's error code filled according to the type
     * of error.
     *
     * @param[in] opcode the opcode for the error
     * @param[in] what   the error message
     * @return           the error structure
     */
    inline error invalid_argument(uint8_t opcode, const std::string& what = "")
    {
        return error(opcode, 0x06, what);
    }

    /**
     * Returns a parse-error error.
     *
     * The error will have it's error code filled according to the type
     * of error.
     *
     * @param[in] opcode the opcode for the error
     * @param[in] what   the error message
     * @return           the error structure
     */
    inline error parse_error(uint8_t opcode, const std::string& what = "")
    {
        return error(opcode, 0x07, what);
    }
}
