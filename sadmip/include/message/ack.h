// Copyright (c) 2018 Philipp Kiener
#pragma once

#include "message.h"

namespace sadmip
{
    /**
     * A generic ACK without a payload.
     *
     * This message is a generic ACK for any command whose ACK does not require
     * a payload.
     * When constructing a payload, this message builds an empty payload.
     * When parsing a payload, any data within the payload is ignored - parsing
     * will always succeed.
     */
    class ack : public message
    {
    public:
        /**
         * Constructs a generic ACK with the given opcode.
         *
         * @param opcode the opcode for this ACK
         */
        ack(uint8_t opcode);

    protected:
        virtual void write_payload(write_stream& stream) const override;
        virtual bool parse_payload(read_stream& stream) override;
    };
}
