// Copyright (c) 2018 Philipp Kiener
#pragma once

#include "message.h"

#include <context.h>
#include <memory>

namespace sadmip
{
    /**
     * A generic command without a payload.
     *
     * This message is a genercic command that does not contain a payload.
     * When parsing a payload, any data within the payload is ignored - parsing
     * will always succeed.
     */
    class command : public message
    {
    public:
        /**
         * Constructs a generic command with the given opcode.
         *
         * @param opcode the opcode for this command
         */
        command(uint8_t opcode);
        
        /**
         * Execute this command in a given context.
         *
         * The context can be modified, an probably will be based on the command
         * that is executed.
         *
         * @param[in,out] context   the execution context
         * @return                  the answer containing information or an error
         */
        virtual std::unique_ptr<message> execute(context& context) = 0;

    protected:
        virtual void write_payload(write_stream& stream) const override;
        virtual bool parse_payload(read_stream& stream) override;
    };
}
