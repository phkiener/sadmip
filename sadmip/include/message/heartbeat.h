// Copyright (c) 2018 Philipp Kiener
#pragma once

#include "message.h"

namespace sadmip
{
    /**
     * A heartbeat to signal the device is still running.
     *
     * This should only be sent by the device to signal the connected client
     * that the device has not gone rogue yet. It differs from other types
     * of messages since its opcode is not a true opcode - it much rather is
     * the "payload", as the heartbeat itself has no payload.
     */
    class heartbeat : public message
    {
    public:

        /**
         * The possible states of the device.
         *
         * The device can always be in one of the states listed here.
         * Note that there is technically another state, "sending data". I leave
         * it to the reader to figure out why we don't need a heartbeat for that.
         */
        enum class device_status : uint8_t
        {
            idle = 1, //!< the device is idle; nothing is happening
            receiving_data = 2, //!< the device has received data and waits for more
            working_command = 3, //!< the device is currently working on a command
        };

        /**
         * Create a heartbeat with the given status.
         *
         * @param[in] status the status to note down
         */
        heartbeat(device_status status);

        /**
         * Return the status sent with this message.
         *
         * @return the status of the device transmitted
         */
        device_status status() const;

    protected:
        virtual void write_payload(write_stream& stream) const override;
        virtual bool parse_payload(read_stream& stream) override;

    private:
        device_status _status; //!< the device status
    };
}
