// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cstdint>
#include <memory>
#include <vector>

#include <datatypes/message_type.h>

#include <io/read_stream.h>
#include <io/write_stream.h>

namespace sadmip
{
    /**
     * The error code for deserialization operations.
     *
     * When passed as an optional parameter into /message::deserialize/, the
     * value will reflect the error that has happened while deserializing.
     */
    enum class deserialization_error : uint8_t
    {
        NO_ERROR = 0, //!< No error happened
        HEADER_TIMEOUT, //!< Timeout while receiving the header
        INVALID_HEADER, //!< Header could not be parsed (e.g. invalid opcode)
        PAYLOAD_TIMEOUT, //!< Timeout while receiving the payload
        INVALID_PAYLOAD, //!< Payload could not be parsed
    };

    /**
     * A message is the base communication entity.
     *
     * Messages will be sent back-and-forth between a client and a device
     * continuously. Generally, the client will send a command, which the
     * device receives, parses, executes and answers with an answer, which
     * is then sent to the client, which receives and parses the answer.
     */
    class message
    {
    public:
        /**
         * Serializes the message to the given stream.
         *
         * The message is converted to a binary blob according to the sadmip
         * protocol and written to the stream.
         *
         * @param[in] stream the stream to write to
         */
        void serialize(write_stream& stream) const;

        /**
         * Reads a message from the given stream.
         *
         * The message is read with the payload. Depending on the found type
         * and opcode, a certain object is created. That object then parses
         * the payload on its own, reporting the result. If the payload was
         * successfully parsed, the created object is returned. If an error
         * occurs, a null pointer is returned.
         *
         * @param[in]  stream  the stream to read from
         * @param[out] errcode pointer to a value that will hold the deserialization
         *                     error if one happened
         * @return             pointer to the
         */
        static std::unique_ptr<message> deserialize(read_stream& stream, deserialization_error* errcode = nullptr);

        /**
         * Returns the type of this message.
         *
         * The type is always one of the four values command, ack, error or
         * heartbeat.
         *
         * @return the type of this message
         */
        message_type type() const;

        /**
         * Returns the opcode of this message.
         *
         * The opcode, together with the type, identify how to parse the payload.
         *
         * @return the opcode of this message
         */
        uint8_t opcode() const;

    protected:

        /**
         * Constructs a message with the given parameters.
         *
         * The message is basically just a header. This is intended to be
         * called from derived classes to fill in the gaps.
         *
         * @param type   the type of message
         * @param opcode the opcode of the message
         */
        message(message_type type, uint8_t opcode);

        /**
         * Builds a payload for this message.
         *
         * The payload is built according to the structure of the concrete message.
         *
         * @param[in] stream the stream to write to
         */
        virtual void write_payload(write_stream& stream) const = 0;

        /**
         * Parses a payload into this message.
         *
         * The given payload is parsed and the internal state configured according
         * to the concrete message. If a parse error occurs, false is returned
         * and the internal stay may be in an undefined state.
         *
         * @param[in] stream the stream to read from
         * @return true if parsing was successful, false if an error occurred
         */
        virtual bool parse_payload(read_stream& stream) = 0;

    private:
        message_type _type; //!< the type of message
        uint8_t _opcode; //!< the opcode of this message
    };

    /**
     * Writes a message to a stream.
     *
     * @param[in] stream the stream to write to
     * @param[in] msg    the message to write
     * @return           the modified stream
     */
    write_stream& operator<<(write_stream& stream, const message& msg);

    /**
     * Reads a message from a stream.
     *
     * The message is put into the given smart pointer.
     *
     * @param[in] stream  the stream to read from
     * @param[in] msg_ptr smart pointer which to use when storing the message
     * @return            the modified stream
     */
    read_stream& operator>>(read_stream& stream, std::unique_ptr<message>& msg_ptr);
}
