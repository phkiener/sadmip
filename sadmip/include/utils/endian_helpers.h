// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cstddef>
#include <cstdint>

namespace sadmip
{
    /**
     * Detects if the host is a big endian machine.
     *
     * Checks the endianness of the host system.
     *
     * @return true if the host is big endian, false if it is not
     */
    bool host_is_big_endian();

    /**
     * Detects if the host is a little endian machine.
     *
     * Checks the endianness of the host system.
     *
     * @return true if the host is little endian, false if it is not
     */
    bool host_is_little_endian();

    /**
     * Swaps the byte order of a given byte array.
     *
     * The /num/ bytes starting from /src/ are copied to /dst/ in reversed
     * order. Both /dst/ and /src/ must contain at least /num/ elements.
     * If either /dst/ or /src/ are NULL, nothing will be done.
     *
     * @param[in,out] dst target of copying
     * @param[in]     src where to copy from
     * @param[in]     num number of bytes to copy from src
     */
    void swap_bytes(uint8_t* dst, const uint8_t* src, size_t num);

    /**
     * Swaps the byte order of the given byte array.
     *
     * The /num/ bytes starting from /buf/ are swapped in place.
     * @see swap_bytes(uint8_t*, const uint8_t*, size_t)
     *
     * @param[in] buf the buffer to swap
     * @param[in] num number of bytes to swap
     */
    void swap_bytes(uint8_t* buf, size_t num);

    /**
     * Convert the given value to big endian.
     *
     * The given value is seen as a chunk of binary data; if neccessary, the
     * /sizeof(type)/ bytes starting from /&in/ are swapped.
     *
     * @tparam    type the type of data to convert
     * @param[in] in   the value to convert
     * @return         the given value stored as big endian
     */
    template <typename type>
    type to_big_endian(const type& in);

    /**
     * Convert the given value to host endian from big endian.
     *
     * The given value is seen as a chunk of binary data; if neccessary, the
     * /sizeof(type)/ bytes starting from /&in/ are swapped.
     *
     * @tparam    type the type of data to convert
     * @param[in] in   the value to convert
     * @return         the given value stored as big endian
     */
    template <typename type>
    type from_big_endian(const type& in);

    /**
     * Convert the given value to little endian.
     *
     * The given value is seen as a chunk of binary data; if neccessary, the
     * /sizeof(type)/ bytes starting from /&in/ are swapped.
     *
     * @tparam    type the type of data to convert
     * @param[in] in   the value to convert
     * @return         the given value stored as big endian
     */
    template <typename type>
    type to_little_endian(const type& in);

    /**
     * Convert the given value to host endian from little endian.
     *
     * The given value is seen as a chunk of binary data; if neccessary, the
     * /sizeof(type)/ bytes starting from /&in/ are swapped.
     *
     * @tparam    type the type of data to convert
     * @param[in] in   the value to convert
     * @return         the given value stored as big endian
     */
    template <typename type>
    type from_little_endian(const type& in);
}

template<typename type>
type sadmip::to_big_endian(const type& in)
{
    const size_t size = sizeof in;

    if (size == 1) return in;
    if (host_is_big_endian()) return in;

    type result = in;
    swap_bytes(reinterpret_cast<uint8_t*>(&result), size);
    return result;
}

template<typename type>
type sadmip::from_big_endian(const type& in)
{
    const size_t size = sizeof in;

    if (size == 1) return in;
    if (host_is_big_endian()) return in;

    type result = in;
    swap_bytes(reinterpret_cast<uint8_t*>(&result), size);
    return result;
}

template<typename type>
type sadmip::to_little_endian(const type& in)
{
    const size_t size = sizeof in;

    if (size == 1) return in;
    if (host_is_little_endian()) return in;

    type result = in;
    swap_bytes(reinterpret_cast<uint8_t*>(&result), size);
    return result;
}

template<typename type>
type sadmip::from_little_endian(const type& in)
{
    const size_t size = sizeof in;

    if (size == 1) return in;
    if (host_is_little_endian()) return in;

    type result = in;
    swap_bytes(reinterpret_cast<uint8_t*>(&result), size);
    return result;
}
