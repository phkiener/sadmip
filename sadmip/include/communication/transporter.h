// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cstdint>
#include <vector>

namespace sadmip
{
    /**
     * A transporter is the low-level bidirectional data exchange layer.
     *
     * The transporter is used to transmit the buffered data towards another
     * device while simultaneously receiving data from the same device. The
     * actual hardware transportation can be anything, from RS-232 serial to
     * unladen swallows.
     */
    class transporter
    {
    public:

        /**
         * Returns the number of bytes that can be read from the device.
         *
         * This function may update internal state. It should not wait
         * longer than needed, though, as it is generally expected to return
         * very quickly.
         *
         * "The number of bytes that can be read" refer to the minimum amount
         * of bytes that can be read from the device without causing it to wait
         * or fall into an error state.
         *
         * @return number of bytes available to be read
         */
        virtual size_t available() = 0;
        
        /**
         * Transmits /num/ bytes of /buf/ using the device.
         *
         * The data is written as-is and sent directly to the device. Internal
         * buffering may occur, although it is encouraged that this method
         * does not buffer and write exactly to the device.
         * If /buf/ is /NULL/ or /num/ is 0, nothing is done. Note that the
         * element /buf[num - 1]/ is accessed, so make sure /buf/ is large enough
         * to not cause a segfault.
         *
         * @param[in] buf pointer to the array of bytes to write
         * @param[in] num number of bytes (in buf) to write
         * @return        number of bytes actually written
         */
        virtual size_t write(const uint8_t* buf, size_t num) = 0;

        /**
         * Reads /num/ bytes from the device.
         *
         * The bytes are stored in /buf/; make sure it is large enough to contain
         * that amount of bytes, upto /buf[num - 1]/. The data is read as-is.
         * This method may block execution depending on the concrete implementation.
         * If /buf/ is /NULL/ or /num/ is 0, nothing is done.
         *
         * @param[in,out] buf buffer to store the read bytes
         * @param[in]     num number of bytes to read at most
         * @return            number of bytes that have actually been read
         */
        virtual size_t read(uint8_t* buf, size_t num) = 0;

        /**
         * Reads all available data.
         *
         * All bytes available on the resource are received and returned.
         * In the default implementation, the number of available bytes is queried
         * and a read-call for that many bytes is issued. These bytes are then
         * returned.
         * If your resource offers a more optimized way of doing this, you
         * may override this method.
         *
         * @return the read data
         */
        virtual std::vector<uint8_t> read_all();
    };
}