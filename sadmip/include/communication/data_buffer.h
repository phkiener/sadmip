// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cstdint>
#include <string>
#include <deque>
#include <vector>

#include <communication/transporter.h>
#include <io/read_stream.h>
#include <io/write_stream.h>

namespace sadmip
{
    /**
     * Special signals that can only be "written" to a data_buffer.
     *
     * Once the signal is written, a specific action will be taken.
     */
    enum class data_buffer_signal : uint8_t
    {
        flush = 1, //!< flushes the data buffer
        fetch = 2, //!< fetches new data for the data buffer
        sync = 3, //!< syncs the data buffer
    };

    /**
     * A data buffer to write and read sadmip-formatted data with ease.
     *
     * The data buffer will be configured to use a /transporter/. It will then
     * autmagically use the transporter to send and receive data on demand.
     * The buffer consists of two sub-buffers, a write-buffer and a read-buffer.
     * The write-buffer will be cleared as soon as data has been sent - all
     * data that is written can be seen as committed and can not be taken back.
     * The read buffer will be filled by the transporter and will be cleared
     * on use - as soon as data is read, it is removed from the read buffer.
     * A single bxte can be peeked, though.
     *
     * The data buffer supports two modes of operation; raw and formatted. Raw
     * data is written as-is, without any conversion whatsoever. This is used
     * for either single bytes or raw chunks of binary data that must be transmitted
     * as-is. This should only be used if you know what you're doing.
     * Formatted mode takes in not every data type there is - but for those it
     * does take in, the data will be formatted according to the sadmip spec.
     *
     * /get()/ and overloads, as well as /put()/ and overloads will all work
     * with raw data, whereas /operator<</ and /operator>>/ will work with
     * formatted data. Custom types can define an overload for
     * /data_buffer& operator<<(data_buffer&, const your_type&)/ to write and
     * /data_buffer& operator>>(data_buffer&, your_type&/ to read formatted
     * data.
     */
    class data_buffer : public read_stream, public write_stream
    {
    public:

        /**
         * Initializes the data buffer with the given transporter.
         *
         * The transporter needs to live for the whole lifetime of this data
         * buffer. If it is destructed earlier, it may burn your house down.
         * Or it may segfault. Same thing, really.
         *
         * Note that the supplied timeout is used only for reading operations.
         * If the time between receiving two bytes exceeds this duration,
         * the reading will fail. A negative timeout means that the buffer
         * will wait forever, if needed. Use that with care.
         *
         * @param[in] the       transporter to use
         * @param[in] timeout   the timeout for reading in milliseconds
         */
        explicit data_buffer(transporter& transporter, int timeout = 5000, bool can_fetch = true);

        /**
         * Sets the threshold for flushing the write buffer.
         *
         * After every read-operation, the amount of data within the buffer
         * is compared with the set threshold. If it is larger, it is flushed
         * and the buffer is emptied. Note that the flushing can happen manually
         * using /flush()/. No /flush()/ing will happen automatically, it will
         * consider the new threshold only with the next call to /write/.
         *
         * @param[in] bytes size-threshold for the write buffer
         */
        void set_writing_threshold(size_t bytes);

        /**
         * Fetches new data from the transporter.
         *
         * This method respects the configured timeout; if the timeout is set
         * to 0; this method will fail if no data is available when calling.
         * If it is set to a number larger than 0, the call will wait at most
         * that many milliseconds before either returning false if no data was
         * able to be fetched or true if data was able to be fetched.
         * If the timeout is negative, this method will wait forever until
         * data is available.
         *
         * @param[in] amount number of bytes to fetch, all available if 0
         * @return           true if data was fetched, false otherwise
         */
        bool fetch(size_t amount = 0);

        /**
         * Fetches new data from the transporter.
         * 
         * This method ignores the configured timeout; it simply fetches
         * all data that is available.
         * 
         * @return true if data was fetched, false otherwise 
         */
        bool fetch_immediate();

        /**
         * Waits for a certain number of bytes to arrive.
         *
         * This method is subject to the timeout set on construction.
         * /fetch()/ is called as often as needed - as long as it succeeds - 
         * until the given number of bytes is available for reading.
         *
         * @param[in] bytes the number of bytes to wait for
         * @return          true if the bytes are available now, false on a timeout
         */
        bool wait_for_bytes(size_t bytes);

        /**
         * Returns the number of bytes in the read buffer.
         * 
         * @return number of bytes present in the read buffer
         */
        size_t available() const;

        bool flush() override;
        void write(const uint8_t* bytes, size_t size) override;

        virtual size_t read(uint8_t* dest, size_t num) override;

        /**
         * Append a single byte to the readbuffer.
         * 
         * If - for some reason - fetching the data from the resource
         * does not work, the resource can supply the data on its own with
         * the /append_to_readbuffer/-methods.
         * 
         * @param[in] byte to be appended
         */
        void append_to_readbuffer(const uint8_t byte);

        /**
         * Append an array of bytes to the readbuffer.
         * 
         * If - for some reason - fetching the data from the resource
         * does not work, the resource can supply the data on its own with
         * the /append_to_readbuffer/-methods.
         * 
         * @param[in] byte to be appended
         */
        void append_to_readbuffer(const uint8_t* bytes, size_t size);

        /**
         * Append a vector of bytes to the readbuffer.
         * 
         * If - for some reason - fetching the data from the resource
         * does not work, the resource can supply the data on its own with
         * the /append_to_readbuffer/-methods.
         * 
         * @param[in] byte to be appended
         */
        void append_to_readbuffer(const std::vector<uint8_t>& bytes);

    private:
        std::vector<uint8_t> _write; //!< the write-buffer
        std::deque<uint8_t> _read; //!< the read-buffer
        bool _fetch; //!< whether the data buffer may fetch the resource

        int _timeout; // the timeout for reading in milliseconds
        size_t _threshold; //!< the size-threshold for the write buffer

        transporter& _transporter; //!< the transporter to use
    };

    /**
     * "Writes" a signal to the data buffer.
     *
     * Depending on the signal, a specific action is taken.
     * /data_buffer_signal::flush/ flushes the buffer,
     * /data_buffer_signal::fetch/ fetches new data,
     * /data_buffer_signal::sync/ does a fetch first, then a flush.
     *
     * @param[in] buffer the buffer to work on
     * @param[in] signal the signal to send
     * @return           the modified buffer
     */
    data_buffer& operator<<(data_buffer& buffer, data_buffer_signal signal);
}
