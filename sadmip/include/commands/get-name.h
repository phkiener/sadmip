// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <message/ack.h>

#include <string>

namespace sadmip
{
    namespace commands
    {
        /**
        * A command to query the name of the experiment.
        *
        * The name is returned as a special /ack/.
        */
        class get_name : public command
        {
        public:

            /**
            * A specialized ack for the get-name command.
            *
            * This ack contains the name.
            */
            class result : public ack
            {
            public:
                /**
                * Create an ACK for the command with the given name.
                *
                * @param[in] name the name to return
                */
                result(const std::string& name);

                /**
                 * Returns a reference to the stored name.
                 *
                 * @return the stored name
                 */
                const std::string& name() const;

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                std::string _name; //!< the name to return
            };

            /**
            * Create a command to get the name of the experiment.
            */
            get_name();

            std::unique_ptr<message> execute(context& context) override;
        };
    }
}
