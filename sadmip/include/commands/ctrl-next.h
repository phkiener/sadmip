// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to skip the current frame.
         *
         * The command skips the current frame, regardless of playing state
         * or timestep, and goes to the next frame - or another frame,
         * defined by a number of frames to skip.
         */
        class ctrl_next : public command
        {
        public:
            /**
             * Create a command to go to the next frame, skipping a certain 
             * amount of frames.
             * 
             * The command will skip the given number of frames. If there are
             * not enough frames left, executing this command will yield an error.
             *
             * @param[in] skip the amount of frames to skip
             */
            ctrl_next(uint8_t skip = 0);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            uint8_t _skip; //!< amount of frames to skip
        };
    }
}
