// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/ack.h>
#include <message/command.h>

#include <string>
#include <map>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to retrieve the whole state of meta-information.
         *
         * All keys and their respective values are returned.
         */
        class get_meta : public command
        {
        public:

            /**
             * The result of the get-meta command.
             *
             * It contains the set of all keys and their respective values.
             */
            class result : public ack
            {
            public:
                /**
                 * Constructs a result for the get-meta command.
                 *
                 * Initially, it contains no information whatsoever. Fill this
                 * ACK using /add_meta/.
                 */
                result();

                /**
                 * Add a key-value-pair to the result.
                 *
                 * @param[in] key   the key to add
                 * @param[in] value the keys value
                 */
                void add_meta(const std::string& key, const std::string& value);

                /**
                 * Return a map containing all meta-information.
                 *
                 * @return the map of all meta-information
                 */
                const std::map<std::string, std::string>& meta() const;

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                std::map<std::string, std::string> _meta; //!< the meta information
            };

            /**
             * Create a command to retrieve the whole state of meta-information.
             */
            get_meta();

            std::unique_ptr<message> execute(context& context) override;
        };
    }
}
