// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to go to a previous frame.
         *
         * Regardless of playing state or timestep, the command will set
         * the active frame to the frame before the current frame - or another
         * frame, as configured by the given amount of frames to skip.
         */
        class ctrl_prev : public command
        {
        public:
            /**
             * Creates a command to go to a previous frame.
             *
             * The command will skip a given number of frames. If there are not
             * enough frames to skip, executing this command will yield an
             * error.
             *
             * @param[in] skip the amount of frames to skip
             */
            ctrl_prev(uint8_t skip = 0);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            uint8_t _skip; //!< amount of frames to skip
        };
    }
}
