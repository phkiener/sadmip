// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * Command to start the playback of the active experiment.
         *
         * The playback can configured with a timestep and can be set to loop
         * or stop after arriving at the last frame.
         */
        class ctrl_start : public command
        {
        public:
            /**
             * Creates a command to start the playback of the active experiment.
             *
             * The command is configured to set the given timestep (a value of 0
             * means "keep the current setting") and whether to loop or not.
             *
             * @param[in] timestep the timestep of playback
             * @param[in] loop     whether to loop the playback
             */
            ctrl_start(uint16_t timestep = 0, bool loop = false);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            bool _loop; //!< whether to loop the playback
            uint16_t _timestep; //!< timestep between frames in milliseconds
        };
    }
}
