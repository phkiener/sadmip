// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * Resets the device to the first frame.
         *
         * The command will reset the device to the first frame in the loaded
         * experiment, continuing from then on or stopping the playback.
         */
        class ctrl_reset : public command
        {
        public:
            /**
             * Create a command to reset the playback.
             *
             * The experiments skips to the first frame and either continues
             * playback or stops it, depending on the given parameter.
             *
             * @param[in] continue_playing whether to start the playback again
             */
            ctrl_reset(bool continue_playing = false);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            bool _continue_playing; //!< whether to start the playback again
        };
    }
}
