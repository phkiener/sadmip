// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <message/ack.h>

#include <string>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to fetch a list of all meta-keys.
         *
         * This command returns a list of keys, for which each key has a value
         * of meta-information stored. It is safe to call a get-value with
         * every of the keys returned here.
         */
        class get_keys : public command
        {
        public:
            /** 
             * A specialized ack for the get-keys command.
             *
             * This ack contains a payload; the list of meta-keys.
             */
            class result : public ack
            {
            public:
                /**
                 * Create an ACK for the command with the given keys.
                 *
                 * @param[in] keys the keys of meta-information
                 */
                result(const std::vector<std::string>& keys);

                /**
                 * Returns the list of keys.
                 *
                 * @return the list of keys
                 */
                const std::vector<std::string>& keys() const;

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                std::vector<std::string> _keys; //!< the found keys
            };

            /**
             * Create a command to return a list of meta-keys.
             */
            get_keys();

            std::unique_ptr<message> execute(context& context) override;
        };
    }
}
