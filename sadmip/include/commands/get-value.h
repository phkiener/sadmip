// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <message/ack.h>

#include <string>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to fetch a certain piece of meta-information.
         *
         * This command returns a certain piece of meta-information or an
         * error of the given key is unknown.
         */
        class get_value : public command
        {
        public:
            /** 
             * A specialized ack for the get-value command.
             *
             * This ack contains a payload; the requested meta-value.
             */
            class result : public ack
            {
            public:
                /**
                * Create an ACK for the command with the given status.
                *
                * @param[in] value the requested meta value
                */
                result(const std::string& value);

                /**
                 * Returns the stored value.
                 *
                 * @return the stored value
                 */
                const std::string& value() const;

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                std::string _value; //!< the requested value
            };

            /**
             * Create a command to return a value of meta-information.
             *
             * @param[in] key the key whose value to return
             */
            get_value(const std::string& key);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            std::string _key; //!< the key to look up
        };
    }
}
