// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

#include <string>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to set a specific value of meta information.
         *
         * The command discards the previously stored value and replaces it
         * with the given value - iff the key is known.
         */
        class set_value : public command
        {
        public:
            /**
             * Creates a command to set a specific meta information value.
             *
             * @param[in] key   the key whose value to set
             * @param[in] value the value to set for the key
             */
            set_value(const std::string& key, const std::string& value);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            std::string _key; //!< the key to access
            std::string _value; //!< the value to store
        };
    }
}
