// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to stop the playback of the experiment.
         *
         * Playback is halted where it is, it will not be set to the first
         * frame again.
         */
        class ctrl_stop : public command
        {
        public:

            /**
             * Creates a command to stop the playback.
             */
            explicit ctrl_stop();

            std::unique_ptr<message> execute(context& context) override;
        };
    }
}
