// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <message/ack.h>
#include <datatypes/bit_matrix.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to query the currently displayed frame.
         *
         * The frame is returned as a special /ack/.
         */
        class get_matrix : public command
        {
        public:

            /**
             * A specialized ack for the get-matrix command.
             *
             * This ack contains the current frame.
             */
            class result : public ack
            {
            public:
                /**
                 * Create an ACK for the command with the given frame.
                 *
                 * @param[in] mat the current frame as bitmatrix
                 */
                result(const bit_matrix& mat);

                /**
                 * Create an ACK for the command with the given frame.
                 *
                 * @param[in] mat the frame to move into the result
                 */
                result(bit_matrix&& mat);

                /**
                 * Return a reference to the stored matrix.
                 *
                 * @return the stored matrix
                 */
                const bit_matrix& matrix() const;

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                bit_matrix _matrix; //!< the matrix to return
            };

            /**
             * Create a command to get the currently displayed frame.
             */
            get_matrix();

            /**
             * Create a command to fetch a specific frame.
             *
             * @param[in] index index of the frame to fetch
             */
            get_matrix(uint16_t index);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            bool _current; //!< take the current index
            uint16_t _index; //!< if _current is false, the index to fetch
        };
    }
}
