// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <datatypes/bit_matrix.h>

#include <cstdint>
#include <string>
#include <vector>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to replace the currently active experiment with a new one.
         *
         * The command contains information about the name and timestep, as well
         * as all frames in that experiment. The command can also specify autostart
         * and looping, which cause the device to start playing immediately
         * once the command is received.
         */
        class set_experiment : public command
        {
        public:
            /**
             * Create a command to replace the current experiment.
             *
             * The command is initialized with the required values
             * for name and timestep. It will start out with autostart
             * and looping set to false and no frames in the experiment.
             * Set the frames and autostart using /set_frames()/ and
             * /set_autostart()/.
             *
             * @param[in] name     the name of the experiment
             * @param[in] timestep the timestep of the experiment
             */
            set_experiment(const std::string& name, uint16_t timestep);

            /**
             * Set the frames for the experiment.
             *
             * All frames are copied, any frames set earlier are discarded.
             * If any of the set frames differ in dimensions, nothing is done.
             *
             * @param[in] frames the frames to set
             */
            void set_frames(const std::vector<sadmip::bit_matrix>& frames);

            /**
             * Sets the autostart-settings for the experiment.
             *
             * These settings do not persist on the device; they are one-shot
             * settings that are only applied once the command is executed on the
             * device.
             *
             * @param[in] autostart whether to start playback after receiving all frames
             * @param[in] loop      whether to loop the autostarted playback
             */
            void set_autostart(bool active, bool loop = false);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            std::string _name; //!< the name to set
            uint16_t _timestep; //!< the timestep to set
            std::vector<sadmip::bit_matrix> _frames; //!< the frames

            bool _autostart; //!< whether to start playback immediatly
            bool _loop; //!< if _autostart, whether to loop that playback
        };
    }
}
