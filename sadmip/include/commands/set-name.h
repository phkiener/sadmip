// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

#include <string>

namespace sadmip
{

    namespace commands
    {
        /**
        * Command to modify the name of the experiment.
        *
        * An empty name is legal. Due to practical reasons, the length must not
        * exceed 16'777'214 characters.
        * Yes, really.
        * If you think of a name that long, you got some real problems.
        * Unless you're an organic chemist. Then, I can understand.
        */
        class set_name : public command
        {
        public:
            /**
             * Create a command to set the name of an experiment.
             *
             * @param[in] name the name to set
             */
            set_name(const std::string& name);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            std::string _name; //!< the name to set
        };
    }
}
