// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <message/ack.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to retrieve the amount of frames in the active experiment.
         */
        class meta_framecount : public command
        {
        public:
            /** 
             * A specialized ack for the meta-framecount command.
             *
             * This ack contains a payload; the number of frames.
             */
            class result : public ack
            {
            public:
                /**
                 * Create an ACK for the command with the given count.
                 *
                 * @param[in] count number of frames
                 */
                result(uint16_t count);

                /**
                * Returns the number of frames cell.
                *
                * @return the number of frames cell
                */
                uint16_t count() const;

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                uint16_t _frames; //!< the amount of frames
            };
            /**
             * Create a command to reset the device's experiment.
             */
            meta_framecount();

            std::unique_ptr<message> execute(context& context) override;
        };
    }
}
