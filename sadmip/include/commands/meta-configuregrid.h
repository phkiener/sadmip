// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/ack.h>
#include <message/command.h>

#include <datatypes/panel_type.h>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to configure the type of grid to use.
         *
         * Using this command changes the way the device transmits data to display
         * the frames within an experiment. Make sure to not configure it in a
         * wrong way, or chaos will ensure. At the very least, the display will
         * show gargabe. Or nothing at all.
         */
        class meta_configuregrid : public command
        {
        public:
            /** 
             * A specialized ack for the Meta-ConfigureGrid command.
             *
             * This ack contains an optional payload; the list of supported grid types.
             */
            class result : public ack
            {
            public:

                /**
                 * Create an ACK for the command with the given status.
                 *
                 * This empties the payload to only send the confirmation of
                 * configuration.
                 */
                result();

                /**
                 * Create an ACK for the command with the given status.
                 *
                 * @param[in] types the supported grid types to report
                 */
                result(const std::vector<std::string>& types);

                /**
                 * Whether this ACK reports something (i.e. list of gridtypes).
                 *
                 * @return true if the ACK contains a list of grid types, false
                 *         otherwise
                 */
                inline bool report() const { return _report; }

                /**
                 * Returns the supported grid types.
                 *
                 * @return list of strings denoting supported grid types
                 */
                const std::vector<std::string>& types() const;

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                bool _report; //!< whether this result is for a reporting operation
                std::vector<std::string> _types; //!< the reported list of types
            };

            /**
             * Create a command to query all available grid types.
             *
             * Instead of configuring the device to set a certain grid type,
             * the device is queried to report a list of all available grid types.
             */
            meta_configuregrid();

            /**
             * Create a command with the given grid type to configure.
             *
             * The command will instruct the device to configure itself so all
             * data transmitted will be displayed according to the grid type.
             *
             * @param[in] gridtype the panel type to configure the device for
             */
            meta_configuregrid(const std::string& gridtype);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            bool _report; //!< whether to report the available types or not
            std::string _gridtype; //!< the panel type to configure
        };
    }
}
