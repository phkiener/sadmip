// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

#include <string>
#include <vector>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to change the set of keys of meta-information.
         *
         * Keys can either be added or removed. Adding a key that is already
         * present, as well as removing a key that is not present, results
         * in an error.
         */
        class set_keys : public command
        {
        public:
            /**
             * The key-setting mode.
             */
            enum class mode : uint8_t
            {
                add = 1, 
                erase = 2,
                replace = 3
            };

            /**
             * Create a command to modify the set of keys.
             *
             * Dependent on the mode given, a different action is taken with the
             * given keys.
             *
             * @param[in] mode the action to take
             * @param[in] keys the keys to act on
             */
            set_keys(mode mode, const std::vector<std::string>& keys = std::vector<std::string>());

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            mode _mode; //!< the mode of modification
            std::vector<std::string> _keys; //!< the keys to add/replace/erase
        };
    }
}