// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <message/ack.h>
#include <datatypes/bit_matrix.h>

#include <cstdint>
#include <string>
#include <vector>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to query the whole state of the experiment.
         *
         * The command returns the whole state of the experiment in
         * a specialized /ack/.
         */
        class get_experiment : public command
        {
        public:

            /**
             * A specialized ack for the get-experiment command.
             *
             * This ack contains the payload; the dimensions of the grid,
             * the name and timestep as well as all stored frames.
             */
            class result : public ack
            {
            public:
                friend class get_experiment;

                /**
                 * Create an ACK for the command with the given data.
                 *
                 * Use /set_frames()/ to store the experiment's frames.
                 *
                 * @param[in] name     the name of the experiment
                 * @param[in] timestep the timestep between frames
                 */
                result(const std::string& name, uint16_t timestep);

                /**
                 * Returns a reference to the stored name.
                 *
                 * @return the stored name
                 */
                const std::string& name() const;

                /**
                 * Returns the stored timestep.
                 *
                 * @return the stored timestep
                 */
                uint16_t timestep() const;

                /**
                 * Returns a reference to the stored frames.
                 *
                 * @return reference to the stored frames
                 */
                const std::vector<bit_matrix>& frames() const;

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                std::string _name; //!< the name to return
                uint16_t _timestep; //!< the timestep to return

                std::vector<bit_matrix> _frames; //!< the frames to return
            };

            /**
             * Create a command to get the whole state of the device.
             *
             * The state is returned via the special /ack/.
             */
            get_experiment();

            std::unique_ptr<message> execute(context& context) override;
        };
    }
}
