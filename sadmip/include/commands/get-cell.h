// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <message/ack.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to query the status of a single cell.
         *
         * The command returns either a generic error or a special /ack/.
         */
        class get_cell : public command
        {
        public:
            /** 
             * A specialized ack for the get-cell command.
             *
             * This ack contains a payload; the status of the cell.
             */
            class result : public ack
            {
            public:
                /**
                * Create an ACK for the command with the given status.
                *
                * @param[in] active whether the referenced cell is active
                */
                result(bool active);

                /**
                 * Returns whether the requested cell is active.
                 *
                 * @return true if the cell is active, false otherwise
                 */
                bool active() const;

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                bool _active; //!< whether the cell is active
            };

            /**
             * Create a command to get the state of a given cell.
             *
             * The cell's state is returned via the special /ack/.
             *
             * @param[in] row the row of the cell
             * @param[in] col the column of the cell
             */
            get_cell(uint8_t row, uint8_t col);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            uint8_t _row; //!< the row to reference
            uint8_t _col; //!< the cell to reference
        };
    }
}
