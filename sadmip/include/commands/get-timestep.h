// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <message/ack.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to query the currently configured timestep between frames.
         *
         * The value is returned as a special /ack/.
         */
        class get_timestep : public command
        {
        public:
            /**
             * A specialized ack for the get-timestep command.
             *
             * This ack contains the timestep.
             */
            class result : public ack
            {
            public:
                /**
                 * Create an ACK for the command with the given timestep.
                 *
                 * @param[in] timestep the timestep to return
                 */
                result(uint16_t timestep);

                /**
                 * Returns the stored timestep.
                 *
                 * @return the stored timestep
                 */
                uint16_t timestep() const;

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                uint16_t _timestep; //!< the timestep to return
            };

            /**
             * Create a command to get the currently configured timestep.
             */
            get_timestep();

            std::unique_ptr<message> execute(context& context) override;
        };
    }
}
