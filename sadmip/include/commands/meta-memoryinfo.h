// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <message/ack.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to retrieve the amount of frames in the active experiment.
         */
        class meta_memoryinfo : public command
        {
        public:
            /** 
             * A specialized ack for the meta-memoryinfo command.
             *
             * This ack contains a payload; the many different diagnostics.
             */
            class result : public ack
            {
            public:
                friend class meta_memoryinfo;

                /**
                 * Create an ACK for the command with the given count.
                 */
                result();

                /**
                 * Returns the number of bytes reserved for the stack.
                 *
                 * @return number of bytes reserved for the stack
                 */
                inline uint32_t stack_reserved() const { return _diagnostics[0]; }

                /**
                 * Returns the number of bytes the stack has used at most.
                 *
                 * @return max number of bytes the stack has used
                 */
                inline uint32_t stack_max() const { return _diagnostics[1]; }

                /**
                 * Returns the number of bytes that are reserved for the heap.
                 *
                 * @return number of bytes reserved for the heap
                 */
                inline uint32_t heap_reserved() const { return _diagnostics[2]; }

                /**
                 * Returns the currently allocated number of bytes on the heap.
                 *
                 * @return number of bytes used on the heap
                 */
                inline uint32_t heap_current() const { return _diagnostics[3]; }

                /**
                 * Returns how many allocations on the heap have failed.
                 *
                 * @return number of failed allocations
                 */
                inline uint32_t heap_fails() const { return _diagnostics[4]; }

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                uint32_t _diagnostics[5]; //!< the diagnostics
            };

            /**
             * Create a command to reset the device's experiment.
             */
            meta_memoryinfo();

            std::unique_ptr<message> execute(context& context) override;
        };
    }
}
