// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <io/read_stream.h>
#include <io/write_stream.h>

#include <cstdint>
#include <vector>

namespace sadmip
{
    namespace commands
    {
        /**
        * A command to set a path for a droplet.
        *
        * The referenced cell is activated on the current frame.
        * For each direction given, the next frame will have the previous cell
        * deactivated and the cell above/below/left to/right to it activated.
        *
        * If there are not enough frames or the path moves out of the
        * grid, an index-out-of-bounds error is emitted and no frames are modified.
        */
        class set_path : public command
        {
        public:

            /**
            * All legal droplet movement directions.
            */
            enum class direction : uint8_t
            {
                still = 0, //!< stay put
                up = 0xF0, //!< row - 1
                down = 0x10, //!< row + 1
                left = 0x0F, //!< col - 1
                right = 0x01, //!< col + 1
            };

            /**
            * Create a command for droplet movement starting with the given position.
            *
            * @param[in] start_row the row of the cell to start with
            * @param[in] start_col the column of the cell to start with
            * @param[in] append    whether to add frames as needed to not
            */
            set_path(uint8_t start_row, uint8_t start_col, bool append = false);

            /**
            * Adds a single move to the list of directions.
            *
            * @param[in] dir the move to add
            */
            void add_move(direction dir);

            /**
            * Replaces the current list of moves with the given vector.
            *
            * All prior moves are discarded.
            *
            * @param[in] moves the moves to set
            */
            void set_moves(const std::vector<direction> moves);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            uint8_t _startrow; //!< index of the first cell's row 
            uint8_t _startcol; //!< index of the first cell's column
            bool _append; //!< if true, add frames to the experiment as needed

            std::vector<direction> _moves; //!< the moves
        };
    }

    /**
     * Serializes a direction to the stream.
     *
     * The direction is written as a single byte, depending on the move.
     *   up:    0xF0
     *   down:  0x10
     *   left:  0x0F
     *   right: 0x01
     *   still: 0x00
     *
     * @param[in] stream    the stream to write to
     * @param[in] direction the direction to serialize
     * @return              the modified stream
     */
    write_stream& operator<<(write_stream& stream, commands::set_path::direction direction);

    /**
     * Deserializes a direction from the stream.
     *
     * Deserializing happens inverse to serializing; see the respective operator.
     *
     * @param[in]  stream    the stream to read from
     * @param[out] direction the read direction
     * @return               the modified stream
     */
    read_stream& operator>>(read_stream& stream, commands::set_path::direction& direction);
}
