// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

#include <string>
#include <vector>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to replace the set of meta information.
         *
         * The command discards all previously stored keys and their values
         * and replaces them with the keys and values contained within this
         * command.
         */
        class set_meta : public command
        {
        public:
            /**
             * Create a command to replace the meta information on the device.
             */
            set_meta();
            
            /**
             * Add a key-value pair to the command.
             *
             * @param[in] key   the key to store
             * @param[in] value the value for that key
             */
            void add_pair(const std::string& key, const std::string& value);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            std::vector<std::string> _keys; //!< the stored keys
            std::vector<std::string> _values; //!< the stored values
        };
    }
}