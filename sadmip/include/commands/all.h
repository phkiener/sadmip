// Copyright (c) 2018 Philipp Kiener
#pragma once

// compound include for all commands

#include <commands/ctrl-goto.h>
#include <commands/ctrl-next.h>
#include <commands/ctrl-prev.h>
#include <commands/ctrl-reset.h>
#include <commands/ctrl-start.h>
#include <commands/ctrl-stop.h>
#include <commands/get-cell.h>
#include <commands/get-experiment.h>
#include <commands/get-keys.h>
#include <commands/get-matrix.h>
#include <commands/get-meta.h>
#include <commands/get-name.h>
#include <commands/get-timestep.h>
#include <commands/get-value.h>
#include <commands/set-cell.h>
#include <commands/set-experiment.h>
#include <commands/set-keys.h>
#include <commands/set-matrix.h>
#include <commands/set-meta.h>
#include <commands/set-name.h>
#include <commands/set-path.h>
#include <commands/set-timestep.h>
#include <commands/set-value.h>
#include <commands/meta-configuregrid.h>
#include <commands/meta-reset.h>
#include <commands/meta-framecount.h>
#include <commands/meta-memoryinfo.h>
#include <commands/meta-dimensions.h>