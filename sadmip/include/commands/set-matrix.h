// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <datatypes/bit_matrix.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to replace the current grid with a new grid.
         *
         * If the grid's dimensions do not match, this returns an error.
         * Even if the set grid is smaller! One could think of... five ways
         * to put a smaller matrix "into" a bigger matrix. I chose none.
         */
        class set_matrix : public command
        {
        public:
            /**
             * Create a command to set the grid to the given matrix.
             *
             * @param[in] mat    the matrix to apply
             * @param[in] append whether to append the matrix instead of
             *                   replacing it
             */
            set_matrix(const bit_matrix& mat, bool append = false);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            bit_matrix _matrix; //!< the matrix to set
            bool _append; //!< whether to append the matrix
        };
    }
}
