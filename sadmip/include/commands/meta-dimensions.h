// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>
#include <message/ack.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to retrieve the dimensions of the device's grid.
         */
        class meta_dimensions : public command
        {
        public:
            /** 
             * A specialized ack for the meta-dimensions command.
             *
             * This ack contains a payload; the number of rows and columns 
             * the device has.
             */
            class result : public ack
            {
            public:
                /**
                 * Create an ACK for the command with the given dimensions.
                 *
                 * @param[in] rows number of rows
                 * @param[in] cols number of columns
                 */
                result(uint8_t rows, uint8_t cols);

                uint8_t rows() const;
                uint8_t cols() const;

            protected:
                virtual void write_payload(write_stream& stream) const override;
                virtual bool parse_payload(read_stream& stream) override;

            private:
                uint8_t _rows; //!< number of rows the device has
                uint8_t _cols; //!< number of columns the device has
            };
            /**
             * Create a command to query the device's dimensions.
             */
            meta_dimensions();

            std::unique_ptr<message> execute(context& context) override;
        };
    }
}
