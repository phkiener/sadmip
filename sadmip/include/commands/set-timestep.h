// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

#include <cstdint>

namespace sadmip
{
    
    namespace commands
    {
        /**
         * Command to modify the timestep of the experiment.
         *
         * A timestep of 0 is forbidden. Timesteps are given in milliseconds.
         */
        class set_timestep : public command
        {
        public:
            /**
             * Create a command to set the timestep of an experiment.
             *
             * @param[in] timestep the timestep in milliseconds
             */
            set_timestep(uint16_t timestep);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            uint16_t _timestep; //!< the timestep to set
        };
    }
}
