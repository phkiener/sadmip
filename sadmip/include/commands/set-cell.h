// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

#include <cstdint>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to set the state of a specific cell in the current frame.
         */
        class set_cell : public command
        {
        public:
            /**
             * Create a command to set the given cell on or off.
             *
             * The cell given by indices /row/ and /col/ will be set on
             * if /active/ is true and off if it is false.
             *
             * @param[in] row    the row of the cell
             * @param[in] col    the column of the cell
             * @param[in] active whether the cell should be turned on or off
             */
            set_cell(uint8_t row, uint8_t col, bool active);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            uint8_t _row; //!< row of the cell to set
            uint8_t _col; //!< colum of the cell to set
            bool _active; //!< whether to set the cell on or off
        };
    }
}
