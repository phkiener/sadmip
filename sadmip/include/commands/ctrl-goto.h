// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to jump to a certain frame in the active experiment.
         *
         * Using this command, the device can skip to any frame within the
         * loaded experiment. This is the absolute movement-alternative to the
         * relative /ctrl_next/ and /ctrl_prev/.
         */
        class ctrl_goto : public command
        {
        public:
            /**
             * Create a command with the given looping-setting.
             *
             * The command will be configured to either enable or disable looping,
             * based on the boolean passed in.
             *
             * @param[in] do_loop whether to enable looping or not
             */
            ctrl_goto(uint16_t index);

            std::unique_ptr<message> execute(context& context) override;

        protected:
            virtual void write_payload(write_stream& stream) const override;
            virtual bool parse_payload(read_stream& stream) override;

        private:
            uint16_t _index; //!< the index to jump to
        };
    }
}
