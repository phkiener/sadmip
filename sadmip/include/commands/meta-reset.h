// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/command.h>

namespace sadmip
{
    namespace commands
    {
        /**
         * A command to reset the device's experiment to standard configuration.
         *
         * All Meta-settings shall be kept, but the experiment shall be returned
         * into a default state - all frames shall be discarded, the name and
         * timestep be set to sensible defaults and the meta information
         * cleared.
         */
        class meta_reset : public command
        {
        public:
            /**
             * Create a command to reset the device's experiment.
             */
            meta_reset();

            std::unique_ptr<message> execute(context& context) override;
        };
    }
}
