// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <bitset>
#include <cstdint>
#include <cstddef>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <datatypes/bit_matrix.h>
#include <datatypes/panel_type.h>
#include <grid.h>

namespace sadmip
{
    /**
    * The origin to use when seeking a frame.
    *
    * The origin determines how to use the offset.
    */
    enum class seek_origin : int8_t
    {
        current = 0, //!< seek from current position
        start = 1, //!< seek from the start of the file forwards
        end = -1, //!< seek from the end of the file backwards
    };

    /**
     * An execution context for commands.
     *
     * When handed to a command, it will use this object to get and set data
     * and resources. This context is the layer of abstraction between the device
     * and the "runtime" layer, the neccessary glue to connect commands to the
     * side-effects they shall produce.
     */
    class context
    {
    public:

        /**
         * Returns the name of the experiment.
         *
         * Since the name of an experiment must always be set, this shall
         * always return a value.
         *
         * @return the name of the experiment
         */
        const std::string& name() const;

        /**
         * Sets the name of the experiment.
         *
         * The current name is discarded.
         *
         * @param[in] name the new name of the experiment
         */
        void set_name(const std::string& name);

        /**
         * Returns the delay between frames in milliseconds.
         *
         * Since the timestep of an experiment must always be set, this shall
         * always return a value.
         *
         * @return the timestep in milliseconds
         */
        uint16_t timestep() const;

        /**
         * Sets the timestep of the experiment.
         *
         * Whether to update the timestep while the experiment is playing or
         * just update it on the next playback is implementation dependent.
         *
         * @param[in] timestep the new timestep to set
         */
        void set_timestep(uint16_t timestep);

        /**
         * Returns the number of rows this experiment has.
         *
         * The valid row-indices are [0;/rows()/).
         *
         * @return number of rows of this experiment
         */
        inline virtual uint8_t rows() const { return 0; }

        /**
         * Returns the number of columns this experiment has.
         *
         * The valid column-indices are [0;/cols()/).
         *
         * @return number of columns of this experiment
         */
        inline virtual uint8_t cols() const { return 0; }

        /**
         * Returns the current state of a cell.
         *
         * If either /row/ or /col/ are out of bounds, false is returned. 
         *
         * @param[in] row index of the row the cell is in
         * @param[in] col index of the column the cell is in
         * @return        state of the cell; true if it is active (on), false if
         *                it is not (off)
         */
        bool cell(uint8_t row, uint8_t col) const;

        /**
         * Sets a cell of the current frame either active or inactive.
         *
         * The cell is set on if active is true and off if active is false.
         *
         * @param[in] row    index of the row of the cell
         * @param[in] col    index of the column of the cell
         * @param[in] active whether to set the cell active or inactive 
         * @return           true if setting was successfull, false if the
         *                   row or column are out of bounds
         */
        bool set_cell(uint8_t row, uint8_t col, bool active);

        /**
         * Sets a cell in a specific frame either active or inactive.
         *
         * The cell is set on if active is true and off if active is false.
         *
         * @param[in] idx    index of the frame to modify
         * @param[in] row    index of the row of the cell
         * @param[in] col    index of the column of the cell
         * @param[in] active whether to set the cell active or inactive 
         * @return           true if setting was successfull, false if the
         *                   row or column are out of bounds
         */
        bool set_cell(uint16_t idx, uint8_t row, uint8_t col, bool active);

        /**
         * Returns the currently active frame.
         *
         * If the experiment contains no frames, an empty frame (that is, one with
         * all cells being set inactive) shall be returned.
         *
         * @return the currently active frame
         */
        bit_matrix frame() const;

        /**
         * Returns the frame at a certain index.
         *
         * If /idx/ is out of bounds, an invalid frame shall be returned.
         *
         * @param[in] idx index of the frame to be returned
         * @return        the frame at the given index
         */
        bit_matrix frame(uint16_t idx) const;

        /**
         * Clears all set frames. 
         *
         * This method removes all frames from the experiment.
         * After this operation, only a single empty frame remains.
         */
        void clear_frames();

        /**
         * Appends a single frame to the experiment.
         *
         * If the frame would exceed the maximum size of the experiment,
         * nothing is done.
         *
         * @param[in] frame the frame to append
         * @return          true if the frame was appended, false if the
         *                  dimensions of the frame and the device do not match
         *                  or no more frames can be appended
         */
        bool append_frame(const bit_matrix& frame);
        
        /**
         * Appends a single frame to the experiment.
         *
         * If the frame would exceed the maximum size of the experiment,
         * nothing is done.
         *
         * @param[in] frame the frame to append
         * @return          true if the frame was appended, false if the
         *                  dimensions of the frame and the device do not match
         *                  or no more frames can be appended
         */
        bool append_frame(bit_matrix&& frame);

        /**
         * Replaces the current set of frames with the given set of frames.
         *
         * The old set is discarded and the index is set to 0.
         * If the dimensions do not match, nothing is done and false returned.
         * All frames are copied into the experiment.
         *
         * @param[in] frames the set of frames to replace the current set with
         * @return           true if the set was replaced, false if an error happened
         */
        bool set_frames(const std::vector<bit_matrix>& frames);

        /**
         * Replaces the current set of frames with the given set of frames.
         *
         * The old set is discarded and the index is set to 0.
         * If the dimensions do not match, nothing is done and false returned.
         * All frames are moved into the experiment and thus invalidated.
         *
         * @param[in] frames the set of frames to replace the current set with
         * @return           true if the set was replaced, false if an error happened
         */
        bool set_frames(std::vector<bit_matrix>&& frames);

        /**
         * Sets the current frame to the given frame.
         *
         * Whether to refresh the display is implementation dependent.
         *
         * @param[in] frame the frame to set as the current frame
         * @return          true if the frame was appended, false if the
         *                  dimensions of the frame and the device do not match
         */
        bool set_frame(const bit_matrix& frame);

        /**
         * Moves the given frame into the current frame.
         *
         * The current frame is discarded and overwritten with the given frame,
         * which in turn is invalidated.
         *
         * @param[in] frame the frame to move into the current frame
         * @return          true if the frame was appended, false if the
         *                  dimensions of the frame and the device do not match
         */
        bool set_frame(bit_matrix&& frame);

        /**
         * Sets the specified frame to the given frame.
         *
         * If /idx/ is out of bounds, nothing is done.
         *
         * @param[in] idx   index of the frame to set
         * @param[in] frame the frame to set
         * @return          true if the frame was appended, false if the
         *                  dimensions of the frame and the device do not match
         *                  or the index is out of bounds
         */
        bool set_frame(uint16_t idx, const bit_matrix& frame);

        /**
         * Moves the given frame to the specified frame.
         *
         * If /idx/ is out of bounds, nothing is done.
         * The specified frame is discarded and overwritten with the given frame,
         * which in turn is invalidated.
         *
         * @param[in] idx   index of the frame to set
         * @param[in] frame the frame to move
         * @return          true if the frame was set, false if the
         *                  dimensions of the frame and the device do not match
         *                  or the index is out of bounds
        */
        bool set_frame(uint16_t idx, bit_matrix&& frame);

        /**
         * Returns the number of frames the experiment contains.
         *
         * @return number of frames in the experiment
         */
        uint16_t frame_count() const;

        /**
         * Returns the index of the currently displayed frame.
         *
         * @return index of the currently displayed frame
         */
        uint16_t frame_index() const;

        /**
         * Sets the current frame to a specific frame in the experiment.
         *
         * Seeking is done in one of three ways:
         *  - relative to the current position (seek_origin::current)
         *  - absolute from the first frame forwards (seek_origin::start)
         *  - absolute from the last frame backwards (seek_origin::end)
         *
         * If /origin/ is /current/, a positive offset goes to a later frame,
         * whereas a negative offset goes to a previous frame. If /origin/
         * is /start/ or /end/ and /offset/ is negative, /offset/ has its
         * sign removed. That means /seek_frame(-1, seek_origin::start)/ is
         * the same as /seek_frame(1, seek_origin::start)/.
         *
         * If the target index is out of bounds, nothing is done.
         * Whether to refresh the display is implementation dependent.
         *
         * @param[in] offset the offset to seek
         * @param[in] origin the origin for seeking
         * @return           true if seeking was successfull, false if the
         *                   frame to seek to is out of bounds
         */
        bool seek_frame(int16_t offset, seek_origin origin);

        /**
         * Starts playback of the current experiment.
         *
         * Playback works as follows:
         *  (1) display the frame at the current index
         *  (2) wait /timestep/ milliseconds
         *  (3) increment the current index to one
         *    (3.1) if /loop/ is false and the current index is the last frame, stop
         *    (3.2) if /loop/ is true and the current index is the last frame,
         *          set the current index to the first frame
         *  (4) goto 1.
         *
         * If the playback is already running, only the looping status is updated.
         *
         * @param[in] loop whether to loop after reaching the last frame
         */
        void play(bool loop = false);

        /**
         * Stops the playback of the current experiment.
         *
         * The playback is halted and may resume exactly where it is now.
         * To start again from the first frame, use /seek_frame(int32_t, seek_origin).
         */
        void stop();

        /**
         * Checks whether playback of this experiment is enabled.
         *
         * @return true if the experiment is currently played through, false
         *         otherwise
         */
        bool is_playing();

        /**
         * Check whether the experiment is set to loop.
         *
         * @return true if the experiment is set to loop instead of end, false
         *         otherwise
         */
        bool is_looping();

        /**
         * Changes the grid that is used.
         *
         * The current grid is discarded (not literally, the hardware does not
         * spontaneously combust) and a new grid with the given type is
         * created (not literally, the hardware does not spontaneously emerge).
         *
         * @param[in] type name of the gridtype to activate
         * @return         true if the grid was created, false if the type is
         *                 unknown
         */
        bool set_grid(const std::string& type);

        /**
         * Returns a list of all registered grid types.
         *
         * Each of these strings can be passed to /set_grid/ to activate a grid.
         *
         * @return list of registered grid types
         */
        std::vector<std::string> grid_types() const;

        /**
         * Returns a list of all stored meta-keys.
         *
         * Every key that has an associated value will be returned.
         * For each of these, a call to /get_meta/ shall succeed.
         *
         * @return list of all meta keys
         */
        std::vector<std::string> meta_keys() const;

        /**
         * Returns a specific value of meta information.
         *
         * If the key is not known, an empty string shall be returned.
         *
         * @param[in] key the key to look up
         * @return        the found value associated with the key, or an empty string
         */
        std::string get_meta(const std::string& key) const;

        /**
         * Sets a specific value of meta information.
         *
         * If the key is not found, false is returned and nothing is done.
         * Otherwise, true is returned and the old meta-value is discarded and
         * replaced with the new value.
         *
         * @param[in] key   the key whose value to replace
         * @param[in] value the value to set
         * @return          true if the value was set, false if the key was not found
         */
        bool set_meta(const std::string& key, const std::string& value);

        /**
         * Adds a key-value pair to the meta information.
         *
         * If the key is already present, false is returned. Otherwise, the
         * given key-value pair is added to the meta-information.
         *
         * @param[in] key   the key to add
         * @param[in] value the initial value for that key
         * @return          true if the pair was added, false if the key is already
         *                  present
         */
        bool add_meta(const std::string& key, const std::string& value = "empty");

        /**
         * Removes a key-value pair from the meta-information.
         *
         * If the key is not present, false is returned. Otherwise, the key
         * and its associaded value are discarded and true is returned.
         *
         * @param[in] key the key to discard
         * @return        true if the key was discarded, false otherwise
         */
        bool remove_meta(const std::string& key);
        
        /**
         * Removes all stored meta-information.
         */
        void clear_meta();

        /**
         * Resets the context back to its default state.
         */
        void reset();

        /**
         * Queries memory diagnostics about the device.
         *
         * @param[out] stack_reserve the number of bytes currently reserved for the stack
         * @param[out] stack_max     the number of bytes the stack has needed at most
         * @param[out] heap_reserve  the number of bytes currently reserved for the heap
         * @param[out] heap_current  the number of bytes the heap uses currently
         * @param[out] heap_fails    how many allocations on the heap have failed
         * @return                   true if diagnostics are available, false if not 
         */
        virtual bool memory_info(
            uint32_t& stack_reserve,
            uint32_t& stack_max,
            uint32_t& heap_reserve,
            uint32_t& heap_current,
            uint32_t& heap_fails) const;

    protected:
        /**
         * Initialize the context with its default state.
         */
        context();

        /**
         * Scrolls to the next frame within the experiment and displays it.
         *
         * If looping is enabled, this will wrap around to the first frame if
         * the last frame is reached. Otherwise, it will stop the playback.
         * The grid will be refreshed, if any is active.
         */
        void next_frame();

        /**
         * Called after play() has finished.
         *
         * Overwrite this method to supply your own housekeeping, e.g. a
         * ticker to update the panel. Nothing like this is done off the shelf.
         */
        virtual void on_playback_start();
        /**
         * Called after stop() has finished.
         *
         * Overwrite this method to supply your own housekeeping, e.g. stopping a
         * ticker to update the panel. Nothing like this is done off the shelf.
         */
        virtual void on_playback_stop();

        /**
         * Register a panel type.
         *
         * The template-type will be constructed; the constructor is called
         * with the given arguments. If there already is a type registered with
         * the given name, the previous type will be removed.
         *
         * @tparam    gridtype  the type to construct
         * @tparam    ctor_args arguments for the constructor to call
         * @param[in] name      the name to register the type as
         */
        template<typename gridtype, typename... ctor_args>
        void register_gridtype(const std::string& name, ctor_args&&... args)
        {
            _gridtypes[name] = [&]()
            {
                return std::make_unique<gridtype>(std::forward<ctor_args>(args)...);
            };
        }

        /**
         * Refreshes the configured grid, if any.
         */
        void refresh_grid();

        /**
         * Checks whether the map of meta-information contains the given key.
         *
         * @param[in] key the key to look for
         * @return    true if the key is present, false otherwise
         */
        bool has_meta_key(const std::string& key) const;

    private:
        std::string _name; //!< the name of the experiment
        uint16_t _timestep; //!< the configured timestep

        std::vector<sadmip::bit_matrix> _frames; //!< the collection of frames
        uint16_t _idx; //!< index of the current frame in the collection

        std::map<std::string, std::string> _meta; //!< map of meta-information

        std::unique_ptr<sadmip::grid> _grid; //!< the configured grid

        using gridctor = std::function<std::unique_ptr<sadmip::grid>()>;
        std::map<std::string, gridctor> _gridtypes; //!< mapping of names to the grid classes
        
        /**
         * Flags describing the state of the context.
         *
         * The bit indices and their meaning are as follows:
         *   0: The context is in playback
         *   1: Playback is set to loop
         */
        std::bitset<2> _state_flags;
    };
}
