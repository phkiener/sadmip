// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <message/message.h>
#include <message/command.h>

#include <memory>

namespace sadmip
{
    /**
     * An executor to execute commands and return answers.
     *
     * In some way or another, an executor is invoked with a command and returns
     * an answer - either by executing that command with a stored context, or by
     * transmitting that command to another executor.
     */
    class executor
    {
    public:
        /**
         * Execute the given command and return its answer.
         *
         * The command is executed in one way or another and will always
         * return an answer - be it ACK or ERR. 
         *
         * @param[in]  command the command to execute
         * @return             smart pointer to the generated answer
         */
        virtual std::unique_ptr<message> execute(const command& command) = 0;
    };
}