// Copyright (c) 2018 Philipp Kiener
#pragma once

// compound header for EVERYTHING!
#include "context.h"
#include "executor.h"
#include "grid.h"
#include "commands/all.h"
#include "communication/data_buffer.h"
#include "communication/transporter.h"
#include "datatypes/bit_matrix.h"
#include "datatypes/message_type.h"
#include "datatypes/panel_type.h"
#include "io/memory_reader.h"
#include "io/memory_writer.h"
#include "io/read_stream.h"
#include "io/write_stream.h"
#include "message/ack.h"
#include "message/command.h"
#include "message/error.h"
#include "message/heartbeat.h"
#include "message/message.h"
