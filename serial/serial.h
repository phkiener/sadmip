// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <cstddef>
#include <cstdint>
#include <deque>
#include <string>
#include <thread>
#include <mutex>

#include <communication/transporter.h>

/**
 * A class to handle writing and reading from/to a serial port.
 */
class serial : public sadmip::transporter
{
public:
    /**
     * Open a serial connection on the given port.
     *
     * If the port cannot be openend, an exception will be thrown.
     * The /timeout/ is used to determine when a call to /read/ may timeout
     * without all data having been read. If it is 0, the call will never wait
     * for data to arrive. If it is negative, it will wait indefinitely. For
     * all other vailes, it will return the read data as soon as either all
     * requested bytes have been read or no bytes have been received for
     * /timeout/ milliseconds.
     *
     * @param[in] port    name of the port to open
     * @param[in] timeout the timeout for reading in milliseconds
     * @throws    std::runtime_exception if the port cannot be openend
     */
    explicit serial(const std::string& port, int timeout = 5000);

    /**
     * Closes and deletes the serial connection.
     *
     * Any pending data is discarded.
     */
    ~serial();

    serial(const serial&) = delete;
    serial& operator=(const serial&) = delete;

    size_t available() override;
    size_t write(const uint8_t* buf, size_t num) override;
    size_t read(uint8_t* buf, size_t num) override;

private:
    /**
     * Fetch all data in the receive-buffer.
     */
    void fetch();

    class serial_impl* _pImpl; //!< implementation class

    std::thread _fetcher; //!< the fetcher-thread
    bool _dofetch; //!< whether to keep fetching
    std::mutex _buffermutex;
    std::deque<uint8_t> _buffer; //! the buffer to store received data
};
