// Copyright (c) 2018 Philipp Kiener

#include "serial.h"

#include <deque>
#include <exception>
#include <mutex>
#include <thread>

#ifdef WIN32
#include <Windows.h>

namespace
{
    std::string format_error(DWORD error)
    {
        if (error == 0) { return "No error."; }

        const DWORD lang = MAKELANGID(LANG_SYSTEM_DEFAULT, SUBLANG_SYS_DEFAULT);
        LPVOID lpMsgBuf;

        DWORD size = FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER |
            FORMAT_MESSAGE_FROM_SYSTEM |
            FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL,
            error,
            lang,
            (LPTSTR) &lpMsgBuf,
            0,
            NULL
        );

        std::string message((LPCTSTR)lpMsgBuf, size);
        LocalFree(lpMsgBuf);

        if (message.at(message.size() - 1) == '\n')
        {
            message.pop_back();
        }

        return message;
    }
}
#else
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>

namespace
{
    std::string format_error(int error)
    {
        std::string msg(strerror(error));
        return msg;
    }
}
#endif

class serial_impl
{
public:
    serial_impl(const std::string& port, int timeout);
    ~serial_impl();

    size_t read(uint8_t* dest, size_t num);
    size_t write(const uint8_t* src, size_t num);

    size_t available();

private:
#ifdef WIN32
    HANDLE _handle;
    COMSTAT _comstat;
    DWORD _error;

    void fetch_cominfo();
#else
    int _handle;
#endif
};

#ifdef WIN32
serial_impl::serial_impl(const std::string& port, int timeout)
{
    _handle = CreateFile(
        port.c_str(),
        GENERIC_READ | GENERIC_WRITE,
        0,
        0,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        0
    );

    if (_handle == INVALID_HANDLE_VALUE)
    {
        throw std::runtime_error(format_error(GetLastError()));
    }

    if (!SetupComm(_handle, 16384, 16384))
    {
        throw std::runtime_error(format_error(GetLastError()));
    }

    DCB serial_params = { 0 };
    serial_params.DCBlength = sizeof(serial_params);

    if (!GetCommState(_handle, &serial_params))
    {
        throw std::runtime_error(format_error(GetLastError()));
    }

    serial_params.BaudRate = CBR_9600;
    serial_params.ByteSize = 8;
    serial_params.StopBits = ONESTOPBIT;
    serial_params.Parity = NOPARITY;

    COMMTIMEOUTS timeouts = { 0 };
    timeouts.ReadIntervalTimeout = timeout;
    timeouts.ReadTotalTimeoutConstant = timeout;
    timeouts.ReadTotalTimeoutMultiplier = 1;
    timeouts.WriteTotalTimeoutConstant = timeout;
    timeouts.WriteTotalTimeoutMultiplier = 1;

    if (!SetCommState(_handle, &serial_params)
        || !SetCommTimeouts(_handle, &timeouts))
    {
        throw std::runtime_error(format_error(GetLastError()));
    }

    fetch_cominfo();
}

serial_impl::~serial_impl()
{
    CloseHandle(_handle);
}

size_t serial_impl::read(uint8_t* dest, size_t num)
{
    DWORD read = 0;
    const size_t block_size = 32;

    while (read < num)
    {
        DWORD current_batch = (num - read) > block_size
            ? block_size
            : static_cast<DWORD>(num - read);
        DWORD current_read = 0;

        if (!ReadFile(_handle, dest + read, current_batch, &current_read, NULL))
        {
            throw std::runtime_error(format_error(GetLastError()));
        }

        read += current_read;
    }

    return static_cast<size_t>(read);
}

size_t serial_impl::write(const uint8_t* src, size_t num)
{
    size_t written = 0;
    const size_t block_size = 32;

    while (written < num)
    {
        DWORD current_batch = (num - written) > block_size
            ? block_size
            : static_cast<DWORD>(num - written);
        DWORD current_write = 0;

        if (!WriteFile(_handle, src + written, current_batch, &current_write, NULL))
        {
            throw std::runtime_error(format_error(GetLastError()));
        }

        written += current_write;
        FlushFileBuffers(_handle);

        // give the mbed time to fetch
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    return static_cast<size_t>(written);
}

size_t serial_impl::available()
{
    fetch_cominfo();
    return _comstat.cbInQue;
}

void serial_impl::fetch_cominfo()
{
    ClearCommError(_handle, &_error, &_comstat);
}
#else

serial_impl::serial_impl(const std::string& port, int timeout)
{
    _handle = open(port.c_str(), O_RDWR | O_NOCTTY);

    if (_handle < 0)
    {
        throw std::runtime_error(format_error(errno));
    }

    struct termios tty;
    memset(&tty, 0, sizeof tty);

    if (tcgetattr(_handle, &tty) != 0)
    {
        throw std::runtime_error(format_error(errno));
    }

    /* disable any echoing or processing, we want that port raw */

    tty.c_cflag |= CLOCAL | CREAD;
    tty.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ISIG | IEXTEN);
    tty.c_oflag &= ~(OPOST | ONLCR | OCRNL);
    tty.c_iflag &= ~(INLCR | IGNCR | ICRNL | IGNBRK);

    /* setup serial mode (8N1 at 9600 Baud) */

    cfsetospeed(&tty, 9600);
    cfsetispeed(&tty, 9600);

    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;

    tty.c_iflag &= ~(INPCK | ISTRIP);
    tty.c_cflag &= ~(PARENB | PARODD);

    tty.c_cflag &= ~CSTOPB;

    /* disable flow control */

    tty.c_iflag &= ~(IXON | IXOFF | IXANY);
    tty.c_cflag &= ~CRTSCTS;

    /* set timeout and minimum read */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(_handle, TCSANOW, &tty) != 0)
    {
        throw std::runtime_error(format_error(errno));
    }

    tcflush(_handle, TCIFLUSH);
}

serial_impl::~serial_impl()
{
    close(_handle);
}

size_t serial_impl::read(uint8_t* dest, size_t num)
{
    size_t read = 0;
    const size_t block_size = 32;

    while (read < num)
    {
        size_t current_batch = (num - read) > block_size
            ? block_size
            : num - read;

        ssize_t actual = ::read(_handle, dest + read, current_batch);

        if (actual <= 0)
        {
            throw std::runtime_error(format_error(errno));
        }

        read += actual;
    }

    return read;
}

size_t serial_impl::write(const uint8_t* src, size_t num)
{
    size_t written = 0;
    const size_t block_size = 32;

    while (written < num)
    {
        size_t current_batch = (num - written) > block_size
            ? block_size
            : num - written;

        ssize_t actual = ::write(_handle, src + written, current_batch);

        if (actual <= 0)
        {
            throw std::runtime_error(format_error(errno));
        }

        written += actual;
        if (tcdrain(_handle) != 0)
        {
            throw std::runtime_error(format_error(errno));
        }

        // give the mbed time to fetch
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    return written;
}

size_t serial_impl::available()
{
    size_t available = 0;
    ioctl(_handle, FIONREAD, &available);
    return available;
}
#endif

serial::serial(const std::string& port, int timeout)
    : _pImpl(nullptr), _dofetch(true)
{
    _pImpl = new serial_impl(port, timeout);
    _fetcher = std::thread([this]
    {
        while (this->_dofetch)
        {
            fetch();
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    });
}

serial::~serial()
{
    _dofetch = false;
    _fetcher.join();

    delete _pImpl;
}

size_t serial::available()
{
    return _buffer.size();
}

size_t serial::write(const uint8_t* buf, size_t num)
{
    return _pImpl->write(buf, num);
}

size_t serial::read(uint8_t* buf, size_t num)
{
    size_t read = 0;

    while (read < num)
    {
        size_t ready = available();

        if (ready > 0)
        {
            size_t current = ready > (num - read) ? num - read : ready;

            _buffermutex.lock();
            std::copy(_buffer.cbegin(), _buffer.cbegin() + current, buf + read);
            _buffer.erase(_buffer.cbegin(), _buffer.cbegin() + current);
            _buffermutex.unlock();

            read += current;
        }
    }

    return read;
}

void serial::fetch()
{
    size_t ready = _pImpl->available();
    if (ready == 0) return;

    uint8_t* buf = new uint8_t[ready];
    size_t recv = _pImpl->read(buf, ready);

    _buffermutex.lock();
    _buffer.insert(_buffer.cend(), buf, buf + recv);
    _buffermutex.unlock();

    delete[] buf;
}
