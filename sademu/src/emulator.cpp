// Copyright (c) 2018 Philipp Kiener

#include "emulator.h"
#include "display/text.h"

#include <chrono>

emulator::emulator(uint8_t rows, uint8_t cols)
    : _rows(rows), _columns(cols), context()
{
    register_gridtype<cli_interface>("text");
    set_frame(0, sadmip::bit_matrix(rows, cols));
}

emulator::~emulator()
{
    stop();
}

uint8_t emulator::rows() const
{
    return _rows;
}

uint8_t emulator::cols() const
{
    return _columns;
}

void emulator::on_playback_start()
{
    _playback.attach_ms([this]() { next_frame(); }, timestep());
}

void emulator::on_playback_stop()
{
    _playback.deattach();
}
