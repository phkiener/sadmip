// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <datatypes/bit_matrix.h>
#include <grid.h>

/**
 * A grid to print frames to stdout.
 *
 * Using this requires a terminal that supports ANSI control sequences.
 */
class cli_interface : public sadmip::grid
{
public:
    bool display(const sadmip::bit_matrix& frame) override;

    /**
     * Destructs the grid.
     *
     * By doing this, all lingering output is cleared.
     * The cursor will be left at the start.
     */
    ~cli_interface();

private:
    unsigned lines_to_clear; //!< how many lines to clear (from the previous call)
};
