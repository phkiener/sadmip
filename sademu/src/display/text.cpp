// Copyright (c) 2018 Philipp Kiener

#include "text.h"

#include <iostream>
#include <sstream>
#include <mutex>

std::mutex print_mutex;

#define SAVE_CURSOR "\x1B[s"
#define RESTORE_CURSOR "\x1B[u"
#define CLEAR_LINE "\x1B[2K"
#define LINE_UP "\x1B[1A"

bool cli_interface::display(const sadmip::bit_matrix& frame)
{
    static bool once = false;

    if (print_mutex.try_lock())
    {
        if (!once)
        {
            std::cout << SAVE_CURSOR;
            once = true;
        }

        std::stringstream output_buffer;

        output_buffer << '+' << std::string(frame.cols(), '-') << "+\n";

        for (auto r = 0; r < frame.rows(); ++r)
        {
            output_buffer << '|';

            for (auto c = 0; c < frame.cols(); ++c)
            {
                output_buffer << (frame.at(r, c) ? '#' : ' ');
            }

            output_buffer << "|\n";
        }

        output_buffer << '+' << std::string(frame.cols(), '-') << "+\n";

        lines_to_clear = 2 + frame.rows();

        std::cout << RESTORE_CURSOR << output_buffer.str();
        print_mutex.unlock();
    }

    return true;
}

cli_interface::~cli_interface()
{
    for (unsigned i = 0; i <= lines_to_clear; ++i)
    {
        std::cout << CLEAR_LINE << LINE_UP;
    }
}
