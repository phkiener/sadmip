// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <sadmip.h>
#include <datatypes/bit_matrix.h>

#include <threadutils/ticker.h>

#include <cstdint>
#include <map>
#include <memory>
#include <string>
#include <thread>
#include <vector>

/**
* An emulator to emulate the state of the StackADrop.
*
* Contains everything the SAD contains, except for the peripherals.
*/
class emulator : public sadmip::context
{
public:
    /**
    * Initialize the emulator.
    *
    * @param[in] rows the number of rows frames need to have
    * @param[in] cols the number of columns frames need to have
    */
    emulator(uint8_t rows, uint8_t cols);

    /**
     * Destroy the emulator
     */
    ~emulator();

    uint8_t rows() const override;
    uint8_t cols() const override;

protected:
    void on_playback_start() override;
    void on_playback_stop() override;

private:
    ticker _playback; //!< ticker for playback frame advancement

    uint8_t _rows; //!< row count for frames
    uint8_t _columns; //!< column count for frames
};
