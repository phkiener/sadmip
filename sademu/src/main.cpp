// Copyright (c) 2018 Philipp Kiener

#include <display/text.h>
#include <serial.h>
#include <emulator.h>

#include <CLI11.hpp>

#include <sadmip.h>

#include <iostream>
#include <memory>
#include <string>

int main(int argc, char** argv)
{
    std::string device;
    unsigned rows = 16, cols = 16;
    
    CLI::App app("A StackADrop emulator", "sademu");
    app.add_option("device", device, "The device to use as serial port")->required();
    app.add_option("rows", rows, "Row count of the grid");
    app.add_option("cols", cols, "Column count of the grid");
    app.ignore_case();
    
    try
    {
        app.parse(argc, argv);

        serial port(device);
        sadmip::data_buffer data_stream(port);
        emulator emu(rows, cols);

        std::cout << "Started emulator on " << device << "\n";
        std::cout << "Press Ctrl-C to quit.\n\n";

        while (true)
        {
            std::unique_ptr<sadmip::message> query = nullptr;
            std::unique_ptr<sadmip::message> answer = nullptr;
            sadmip::deserialization_error error;

            while (!data_stream.available()) { data_stream.fetch(); };

            query = sadmip::message::deserialize(data_stream, &error);

            if (query == nullptr || query->type() != sadmip::message_type::command)
            {
                answer = std::make_unique<sadmip::error>(sadmip::parse_error(0xFF));
            }
            else
            {
                sadmip::command* command = (sadmip::command*)(query.get());
                answer = command->execute(emu);

                if (answer == nullptr)
                {
                    answer = std::make_unique<sadmip::error>(
                        sadmip::not_applicable(query->opcode())
                    );
                }
            }

            answer->serialize(data_stream);
            data_stream.flush();
        }

        return 0;
    }
    catch (const CLI::Error& e)
    {
        return app.exit(e);
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what();
        return -1;
    }
}