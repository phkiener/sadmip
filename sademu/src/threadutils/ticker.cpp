// Copyright (c) 2018 Philipp Kiener

#include "ticker.h"

ticker::~ticker()
{
    deattach();
}

void ticker::attach_s(std::function<void()> function, double seconds)
{
    attach(function, std::chrono::duration<double>(seconds));
}

void ticker::attach_ms(std::function<void()> function, double milliseconds)
{
    attach(function, std::chrono::duration<double, std::milli>(milliseconds));
}

void ticker::attach_us(std::function<void()> function, double microseconds)
{
    attach(function, std::chrono::duration<double, std::micro>(microseconds));
}

void ticker::deattach()
{
    if (_stop)
    {
        return;
    }
    
    _stop = true;

    if (_worker.joinable())
    {
        _worker.join();
    }
}
