// Copyright (c) 2018 Philipp Kiener
#pragma once

#include <chrono>
#include <functional>
#include <thread>

/**
 * A ticker which repeatedly calls a function.
 *
 * As a shorthand for a threaded approach, the ticker starts a new thread
 * which repeatedly calls a given function, until told to stop.
 */
class ticker
{
public:

    /**
     * Destroy the ticker.
     *
     * This deattaches any function that may be attached.
     */
    ~ticker();

    /**
     * Attach a function to the ticker.
     *
     * The given function is called after an interval specified by duration repeatedly
     * until the object is destroyed or /deattach()/ is called.
     *
     * @param[in] function the function to call
     * @param[in] duration the interval between calls
     */
    template <typename _Rep, typename _Period = std::ratio<1, 1>>
    void attach(std::function<void()> function, std::chrono::duration<_Rep, _Period> duration);

    /**
     * Attach a function to the ticker.
     *
     * The given function is called after an interval specified by duration repeatedly
     * until the object is destroyed or /deattach()/ is called.
     *
     * @param[in] function the function to call
     * @param[in] seconds  number of seconds between calls
     */
    void attach_s(std::function<void()> function, double seconds);

    /**
     * Attach a function to the ticker.
     *
     * The given function is called after an interval specified by duration repeatedly
     * until the object is destroyed or /deattach()/ is called.
     *
     * @param[in] function     the function to call
     * @param[in] milliseconds number of milliseconds between calls
     */
    void attach_ms(std::function<void()> function, double milliseconds);

    /**
     * Attach a function to the ticker.
     *
     * The given function is called after an interval specified by duration repeatedly
     * until the object is destroyed or /deattach()/ is called.
     *
     * @param[in] function     the function to call
     * @param[in] microseconds number of microseconds between calls
     */
    void attach_us(std::function<void()> function, double microseconds);

    /**
     * Deattaches the current function.
     *
     * If no function was attached, nothing happens.
     * Note that this call may wait for the started thread to finish; this can
     * take quite some time if the given function takes a long time.
     */
    void deattach();

private:
    std::thread _worker; //!< the thread doing the work
    bool _stop = true; //!< signalling bool to stop the worker
};

template<typename _Rep, typename _Period>
inline void ticker::attach(std::function<void()> function, std::chrono::duration<_Rep, _Period> duration)
{
    if (_stop == false)
    {
        deattach();
    }

    _stop = false;
    _worker = std::thread([duration, function] (bool& stop)
    {
        while (!stop)
        {
            std::this_thread::sleep_for(duration);
            function();
        }
    }, std::ref(_stop));
}
