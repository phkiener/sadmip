# The StackADrop Manipulation Interface Protocol

## About

This is the reference implementation of the SADMIP protocol. The SADMIP is used
to manipulate and query the status of a StackADrop DMFB using a set of commands.
Although it is tailored to fit the StackADrop, it is technically not limited to
the StackADrop itself. The protocol can handle quite some variety on the device's
end, which means it can be implemented on any device that provides a *Context*.

## Requirements

### For *sadctrl*

- [CMake](https://cmake.org/) >= 3.9
- A C++ compiler for your native platform
- A terminal that supports ANSI escape sequences

### For *sademu*

- [CMake](https://cmake.org/) >= 3.9
- A C++ compiler for your native platform
- A terminal that supports ANSI escape sequences

### For *stackadrop*

- [CMake](https://cmake.org/) >= 3.9
- [arm-none-eabi-gcc](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm)
- `make`
- A tool to flash the device like [dfu-util](http://dfu-util.sourceforge.net/)
- (Windows only) The DFU driver from [zadig](http://zadig.akeo.ie/)

## Installation

*sadmip* is provided as multiple projects:

- `sadctrl`, a commandline-based client to interact with the device from a desktop
- `sademu`, an emulator for the StackADrop (it does not emulate the StackADrop itself
  (as in: the hardware), but instead emulates the firmware that powers the StackADrop;
  accepting and executing commands, etc.)
- `stackadrop`, the code which is being run on the device itself

All projects are based on a shared codebase, `sadmip/`.

### Building *sadctrl*

The easy part is building the command line client to control the device.
Open a terminal emulator in this directory and do the following:

    cd sadctrl
    mkdir build
    cd build
    cmake ..
    make

Alternatively, use the `CMake-GUI`.

### Building *sademu*

This is identical to building `sadctrl`.
Open a terminal emulator in this directory and do the following:

    cd sademu
    mkdir build
    cd build
    cmake ..
    make

Alternatively, use the `CMake-GUI`.

### Building *stackadrop*

The code in `stackadrop/` is meant to be the reference implementation of a device,
built specifically for the *StackADrop* built by Maximilian Lünert. It depends
on the `mbed os` which is provided for the STM32F11RE MCU used by the
*StackADrop*. Same as for `sadctrl`, open a terminal emulator in this directory:

    cd stackadrop
    mkdir build
    cd build
    cmake -G "Unix Makefiles" ..
    make

Or use the `CMake-GUI` again. Note: The generator specified **must** be
Unix Makefiles, as Visual Studio for example does not like cross-compiling.

Running `make` will produce `stackadrop` and `stackadrop.bin` - feed the former
to your debugger and the latter to the device.

### Flashing the *stackadrop*

Refer to your chosen tool's documentation for info on how to flash the created
binary to the device. The flash starts at `0x08000000`. The StackADrop must be
in the bootloader-mode; hold down "BOOT" and press "RESET", then release "BOOT".
If the yellow LED blinks (the one closer to the buttons), try again. If it does
not, you're in the bootloader.

**Disclaimer**: I no longer have (physical) access to a StackADrop. I can probably
construct my very own one to continue development, but I'll not do this before 2019.
As a result, the implementation for the StackADrop is in a vague state between
"supported" and "experimental". Basically, stuff may go wrong and I don't have the
means to fix it right now.

### What's with `sadmip/`?

`sadmip/` contains the shared code for both the commandline interface as well as
the device itself. Using two different compilers is the reason for the
not-especially-common structure, but even that is no reason to just duplicate code.
You need not build this module, even though it contains a CMakeLists.txt-file.
Pinky-promise.

## Usage

### sadctrl

`sadctrl` is equipped with extensive help texts. The general usage is:

`sadctrl [-t|--timeout TIMEOUT] [--machine] [-h|--help] DEVICE [-- COMMAND]`

- *timeout* specifies the reading timeout in milliseconds. A negative amount means
  "never timeout", 0 means "timeout instantly"
- Providing *machine* enters machine-mode, which is not suitable for human interaction
  but prints all heartbeats and works on a line-basis; use this when calling
  `sadctrl` from another program
- *help* should be self-explanatory
- *device* is the serial port to use; `dev/ttyACM0` or `COM4` or something alike
- *command* is "one-shot mode" and not allowed in machine-mode; the command
  is directly parsed and transmitted, no further commands can be sent from
  that call

When `sadctrl` is started without a given command, the interactive mode is entered.
This is signaled by the following prompt:

    Starting interactive mode
    Type 'exit' or 'quit' to leave.
    Type 'help' to get an overview of all possible commands.
    >

Commands can then be sent by simply typing them. Use the in-program help to view
all commands or use `help COMMAND` to view more info about a specific command.

### sademu

`sademu` is very bare-bones at the moment. The usage is:

`sademu [-h|--help] DEVICE [ROWS] [COLS]`

- *device* is a serial port that will be used
- *rows* denotes how many rows the grid shall have, defaults to 16
- *cols* denotes how many columns the grid shall have, defaults to 16

A serial port-loopback is intended to be used. Refer to the tools of your platform
for how to do this; examples include [com0com](http://com0com.sourceforge.net/)
for Windows or [socat](https://linux.die.net/man/1/socat) for Unix.
When a loopback from A to B is created, start `sademu` with A and `sadctrl` with
B as devices to allow communication between them.

### The StackADrop

The StackADrop is equipped with three controllable LEDs and two buttons:
BTN0 (right) and BTN1 (left). There are two more LEDs (3.3V and 5V) and two more
buttons (BOOT and RESET) which are not relevant to the usage of the StackADrop
in general.

- If the *red* LED is blinking rapidly (every half second), the StackADrop is
  currently receiving data
- If the *red* LED is blinking slowly (ever second), the StackADrop is currently
  sending data
- If the *yellow* LED is blinking rapidly (every half second), the StackADrop
  is running
- If the *green* LED is active, the StackADrop is in playback

- Pressing BTN1 shortly goes to the previous frame, holding it starts the playback
- Pressing BTN0 shortly goes to the next frame, holding it stops the playback and
  jumps to the first frame

*Note for Unix*: Sometimes, gargabe will be sent to the StackADrop upon accessing
the (virtual) serial port for the first time, no matter which program is used to
do so. This causes the StackADrop to wait for more data (rapidly blinking red LED)
and renders it unusable while it does so - resetting does not help. To fix this,
start `sadctrl --machine DEVICE` and, in another terminal, just type
`echo "1" >> DEVICE` where `DEVICE` is the serial port. Repeat
that line as often as neccessary until an answer will pop up in the `sadctrl` window
saying a parse error has occurred. This is to be expected (since garbage was sent)
and no reason to worry. If heartbeats appear, exit the `sadctrl` process and the
device is ready to be used for real.

### That's a nice protocol you got there... got a specification for it?

I do have a specification as part of my thesis; it is outdated as some things have
changed, though. I'm still not completely satisfied with its format as well.

Writing it down in a really neat format is on my mental list, though.
So... soon(TM)?

## Roadmap - The things to come

There's some stuff that I have planned to implement before moving to a
2.0-release.

- Better errors for commands
- Support for sparse matrices
- Neater UI for sadctrl

### Planned rebranding

To move away from being seemingly focused solely on the StackADrop, I'm planning
to rebrand (rename) the protocol and programs using it. The code in 'stackadrop/'
will then serve merely as an example of using the protocol on an embedded device.
